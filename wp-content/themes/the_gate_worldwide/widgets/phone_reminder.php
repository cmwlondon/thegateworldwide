<?php
/**
 * Widget for Display Phone number
 * 
 * @author Plamen Nikolov
 * 
 * @package WordPress
 * @subpackage Bike_Cation
 * @since Bike-Cation 1.0
 * 
 */
class PhoneReminder extends WP_Widget {
    
      function PhoneReminder() {
             /* Widget settings. */
            $widget_ops = array(
              'classname' => 'phone-reminder',
              'description' => 'Allows you to display a custom phone number');
            
            $control_ops = array(
               'id_base' => 'phone-remainder-widget'
            );
            
            /* Create the widget. */
            $this->WP_Widget('phone-remainder-widget', 'Phone Reminder', $widget_ops, $control_ops );
      }

     /**
     * Back-end of the widget
     */
      function form ($instance) {
        /* Set up some default widget settings. */
           ?>
          <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title:</label><br />
            <input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?>" value="<?php echo $instance['title'] ?>" size="25" />
          </p>
          <p>
            <label for="">Phone Number:</label><br />
            <span><strong><?php global $wptuts_option; echo $wptuts_option['bikecation_callus_phone_number_input']; ?></strong></span>
            <small style="color:#999;">(see "Phone Settings")</small>
          </p>
      
      <?php
    }


    /**
     * Back-end datamanagement of the widget
     */
    function update ($new_instance, $old_instance) {
          $instance = $old_instance;
        
          $instance['title'] = $new_instance['title'];
          $instance['phone_number'] = $new_instance['phone_number'];
        
          return $instance;
    }


    /**
     * Front-end of the widget
     */
    function widget ($args, $instance) {
          ?>
          <div class="phone_reminder">
              <div><?php echo $instance['title']; ?></div>
              <span><?php global $wptuts_option; echo $wptuts_option['bikecation_callus_phone_number_input']; ?></span>
          </div>
          <?php
     }
}

add_action('widgets_init', create_function('', 'return register_widget("PhoneReminder");'));
?>