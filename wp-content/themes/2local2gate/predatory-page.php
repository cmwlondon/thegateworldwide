<?php

/**
 * Template name: predatory
 *
 * @package WordPress
 * @subpackage 2local2gate
 */
get_header();
?>

<section>
	<div class="container pd-btm"> 
		<div class="col-md-6">
			<div class="shadow">
                            <img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_03.jpg') ?>" width="100%" />
				</div>
                    <div class="cup-img"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_14.png') ?>" />
			</div>
			<div class="cup-text" ><p class="cup-p">Competition is in our blood here too, both personally and professionally. We know the levels of commitment it takes to succeed at national and international level.</p>

				<p class="cup-p">With client-side marketeers in our management team, you’ll also find us ruthlessly commercial.
					We’re not selling Art here.</p>

			</div>
			<div class="shadow"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_09.jpg') ?>" width="100%" /></div>
			<div class="cup-img"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_27.png') ?>" />
			</div>
                        <div class="cup-text"><p class="cup-p">As an agency we are properly joined-up; Strategy, Creative,  <a class="number-link" href="<?php echo site_url() ?>/fullservice"><strong>Media Planning &amp; Buying</strong></a> all in-house, all  working together to find an angle and create a competitive edge.
				</p>
			</div>
			<div class="shadow"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_15.jpg') ?>" width="100%" /></div>
		</div>

		<div class="col-md-6">
			<div class="cup-img"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_023.png') ?>" />
			</div>
                    <div class="cup-text" ><p class="cup-p">We use our five principles of <strong>Predatory Thinking</strong>
					to help frame business problems and inspire strategic and creative solutions.</p>

                            <p class="cup-p">It’s not a formula, more a series of points for collective discussion that help us co-create the <a class="number-link" href="<?php echo site_url() ?>/stories"><strong>most effective and competitive approaches.</strong></a></p>
			</div>
			<div class="shadow"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_06.jpg') ?>" width="100%" /></div>
			<div class="cup-img"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_20.png') ?>" />
			</div>
			<div class="cup-text" ><p class="cup-p">But if you think this all sounds rather aggressive,
                                take a look at our <a class="number-link" href="<?php echo site_url() ?>/stories"><strong>case study stories.</strong></a></p>

				<p class="cup-p">Because all this fighting talk seems to produce
					a surprisingly human and compassionate
					view of the world.
				</p>
			</div>

			<div class="shadow"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_12.jpg') ?>" width="100%" /></div>
			<div class="cup-img"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Predatory-thinking_35.png') ?>" />
			</div>
			<div class="cup-text"><p class="cup-p">If you are intrigued by Predatory Thinking and
                                curious to see what commercial success it could deliver for your business, <a class="number-link" href="<?php echo site_url() ?>/contact-us"><strong>get in touch</strong></a>.
				</p>
				<p class="cup-p">
					We love being on the same side as people
					who share our values, fighting their corner,
					and helping them level the playing field.
				</p>

				</p>
			</div>
		</div>
	</div>
</section>


<?php

get_footer();
