<?php
// main page banner image
function culture_banner_meta_box_callback( $post ) {
    wp_nonce_field( 'culture_banner_save_meta_box_data', 'culture_banner_meta_box_nonce' );

    $values = array(
	    "culture_title" => get_post_meta( $post->ID, 'culture_title', true ),
	    "culture_banner" => get_post_meta( $post->ID, 'culture_banner', true ),
	    "culture_banner_id" => get_post_meta( $post->ID, 'culture_banner_id', true ),
	    "culture_banner_w" => get_post_meta( $post->ID, 'culture_banner_w', true ),
	    "culture_banner_h" => get_post_meta( $post->ID, 'culture_banner_h', true )
    );

	$fieldSettings = array(
		"titleField" => "culture_title",
		"imagePrefix" => "culture_banner"
	);

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_banner.php';
}

function culture_banner_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['culture_banner_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['culture_banner_meta_box_nonce'], 'culture_banner_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('culture_title', 'culture_title'),
        array('culture_banner', 'culture_banner'),
        array('culture_banner_id', 'culture_banner_id'),
        array('culture_banner_w', 'culture_banner_w'),
        array('culture_banner_h', 'culture_banner_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'culture_banner_save_meta_box_data' );

// first copy block
function culture_copy1_meta_box_callback( $post ) {
    wp_nonce_field( 'culture_copy1_save_meta_box_data', 'culture_copy1_meta_box_nonce' );

    $values = array(
    	"cc1_header" => get_post_meta( $post->ID, 'cc1_header', true ),
    	"cc1_text" => get_post_meta( $post->ID, 'cc1_text', true )
    );
    
    $fieldSettings = array(
    	"headerfield" => "cc1_header",
    	"headerlabel" => "header",
    	"textfield" => "cc1_text",
    	"textwpeditclass" => "ccopy1_wpedit",
    	"textwpeditid" => "ccopy1"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_copy.php';
}

function culture_copy1_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['culture_copy1_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['culture_copy1_meta_box_nonce'], 'culture_copy1_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('cc1_header', 'cc1_header'), // form element name/id, metakey name
        array('cc1_text', 'cc1_text')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'culture_copy1_save_meta_box_data' );

// second pic/slideshow
function culture_slideshow1_meta_box_callback( $post ) {
    wp_nonce_field( 'culture_slideshow1_save_meta_box_data', 'culture_slideshow1_meta_box_nonce' );

    $values = array(
	    "cs1_order" => get_post_meta( $post->ID, 'cs1_order', true ),
	    "cs1_items" => get_post_meta( $post->ID, 'cs1_items', true ),
	    "cs1_nextid" => get_post_meta( $post->ID, 'cs1_nextid', true )
    );

	$fieldSettings = array(
        "prefix" => "cs1"
	);

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_slideshow.php';
}

function culture_slideshow1_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['culture_slideshow1_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['culture_slideshow1_meta_box_nonce'], 'culture_slideshow1_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    // handle client logos
    $csOrder = $_POST['cs1_order'];
    $slideItems = array();

    if ($csOrder != '') {
        $slides = explode(',', $csOrder);

/*
useless buggy php a single *ESCAPED* quote in a serialized string breaks unserialize()
https://davidwalsh.name/php-serialize-unserialize-issues
//to safely serialize
$safe_string_to_store = base64_encode(serialize($multidimensional_array));

//to unserialize...
$array_restored_from_db = unserialize(base64_decode($encoded_serialized_string));
*/

        foreach ( $slides AS $key => $slideID ) {
            $slideItems[] = array(
                "id" => $slideID,
                "picid" => $_POST[$slideID . '_image_id'],
                // "image" => $_POST[$clientID . '_image'],
                "width" => $_POST[$slideID . '_image_w'],
                "height" => $_POST[$slideID . '_image_h'],
                "alt" => $_POST[$slideID . '_alt'],
                "delay" => $_POST[$slideID . '_delay']
            );        
        }
    }

    update_post_meta( $post_id, 'cs1_order', $csOrder );
    update_post_meta( $post_id, 'cs1_nextid', $_POST['cs1_nextid'] );
    update_post_meta( $post_id, 'cs1_items', base64_encode(serialize($slideItems)) );

}
add_action( 'save_post', 'culture_slideshow1_save_meta_box_data' );

// second copy block
function culture_copy2_meta_box_callback( $post ) {
    wp_nonce_field( 'culture_copy2_save_meta_box_data', 'culture_copy2_meta_box_nonce' );

    $values = array(
    	"cc2_header" => get_post_meta( $post->ID, 'cc2_header', true ),
    	"cc2_text" => get_post_meta( $post->ID, 'cc2_text', true )
    );
    
    $fieldSettings = array(
    	"headerfield" => "cc2_header",
    	"headerlabel" => "header",
    	"textfield" => "cc2_text",
    	"textwpeditclass" => "ccopy2_wpedit",
    	"textwpeditid" => "ccopy2"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_copy.php';
}

function culture_copy2_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['culture_copy2_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['culture_copy2_meta_box_nonce'], 'culture_copy2_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('cc2_header', 'cc2_header'), // form element name/id, metakey name
        array('cc2_text', 'cc2_text')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'culture_copy2_save_meta_box_data' );

// third image / slideshow
function culture_slideshow2_meta_box_callback( $post ) {
    wp_nonce_field( 'culture_slideshow2_save_meta_box_data', 'culture_slideshow2_meta_box_nonce' );

    $values = array(
        "cs2_order" => get_post_meta( $post->ID, 'cs2_order', true ),
        "cs2_items" => get_post_meta( $post->ID, 'cs2_items', true ),
        "cs2_nextid" => get_post_meta( $post->ID, 'cs2_nextid', true )
    );

    $fieldSettings = array(
        "prefix" => "cs2"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_slideshow.php';
}

function culture_slideshow2_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['culture_slideshow2_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['culture_slideshow2_meta_box_nonce'], 'culture_slideshow2_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    // handle client logos
    $csOrder = $_POST['cs2_order'];
    $slideItems = array();

    if ($csOrder != '') {
        $slides = explode(',', $csOrder);

/*
useless buggy php a single *ESCAPED* quote in a serialized string breaks unserialize()
https://davidwalsh.name/php-serialize-unserialize-issues
//to safely serialize
$safe_string_to_store = base64_encode(serialize($multidimensional_array));

//to unserialize...
$array_restored_from_db = unserialize(base64_decode($encoded_serialized_string));
*/

        foreach ( $slides AS $key => $slideID ) {
            $slideItems[] = array(
                "id" => $slideID,
                "picid" => $_POST[$slideID . '_image_id'],
                // "image" => $_POST[$clientID . '_image'],
                "width" => $_POST[$slideID . '_image_w'],
                "height" => $_POST[$slideID . '_image_h'],
                "alt" => $_POST[$slideID . '_alt'],
                "delay" => $_POST[$slideID . '_delay']
            );        
        }
    }

    update_post_meta( $post_id, 'cs2_order', $csOrder );
    update_post_meta( $post_id, 'cs2_nextid', $_POST['cs2_nextid'] );
    update_post_meta( $post_id, 'cs2_items', base64_encode(serialize($slideItems)) );

}
add_action( 'save_post', 'culture_slideshow2_save_meta_box_data' );

// third copy block
function culture_copy3_meta_box_callback( $post ) {
    wp_nonce_field( 'culture_copy3_save_meta_box_data', 'culture_copy3_meta_box_nonce' );

    $values = array(
    	"cc3_header" => get_post_meta( $post->ID, 'cc3_header', true ),
    	"cc3_text" => get_post_meta( $post->ID, 'cc3_text', true )
    );
    
    $fieldSettings = array(
    	"headerfield" => "cc3_header",
    	"headerlabel" => "header",
    	"textfield" => "cc3_text",
    	"textwpeditclass" => "ccopy3_wpedit",
    	"textwpeditid" => "ccopy3"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_copy.php';
}

function culture_copy3_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['culture_copy3_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['culture_copy3_meta_box_nonce'], 'culture_copy3_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('cc3_header', 'cc3_header'), // form element name/id, metakey name
        array('cc3_text', 'cc3_text')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'culture_copy3_save_meta_box_data' );

// 'next page' link
function culture_nextpage_meta_box_callback( $post ) {
    wp_nonce_field( 'culture_nextpage_save_meta_box_data', 'culture_nextpage_meta_box_nonce' );

    $values = array(
    	"culture_nextpage" => get_post_meta( $post->ID, 'culture_nextpage', true )
    );

    $fieldSettings = array(
    	"nextpagefield" => "culture_nextpage"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_nextpage.php';
}

function culture_nextpage_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['culture_nextpage_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['culture_nextpage_meta_box_nonce'], 'culture_nextpage_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('culture_nextpage', 'culture_nextpage')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'culture_nextpage_save_meta_box_data' );

?>