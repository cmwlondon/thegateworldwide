/*
 * Bike-cation 1.0
 * Text Editor init js
 */

jQuery(document).ready(function() {
    
    function register_standalone_text_editor(id) {
        
        jQuery(id).addClass("mceEditor");
        
        if ( typeof( tinyMCE ) == 'object' && typeof( tinyMCE.execCommand ) == 'function' ) {
            tinyMCE.settings = {
                theme : 'advanced',
                skin : 'wp_theme',
                language : "en",
                height:"200",
                width:"100%",
               theme_advanced_buttons1:"bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,wp_more,|,code", 
               theme_advanced_buttons2:"formatselect,underline,justifyfull,forecolor,|,pastetext,pasteword,removeformat,|,charmap,|,outdent,indent,|,undo,redo,wp_help", 
               theme_advanced_buttons3:"", 
               theme_advanced_buttons4:"", 
               language:"en", 
               theme_advanced_toolbar_location:"top", 
               theme_advanced_toolbar_align:"left", theme_advanced_statusbar_location:"bottom", theme_advanced_resizing:true, theme_advanced_resize_horizontal:false, dialog_type:"modal", 
               formats:{
                    alignleft : [
                        {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles : {textAlign : 'left'}},
                        {selector : 'img,table', classes : 'alignleft'}
                    ],
                    aligncenter : [
                        {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles : {textAlign : 'center'}},
                        {selector : 'img,table', classes : 'aligncenter'}
                    ],
                    alignright : [
                        {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles : {textAlign : 'right'}},
                        {selector : 'img,table', classes : 'alignright'}
                    ],
                    strikethrough : {inline : 'del'}
              }
            }
            tinyMCE.execCommand("mceAddControl", false, "tinymce");
         }
     }
});
