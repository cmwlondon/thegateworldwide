<?php
/*
Template Name: ourwork.php
work landing page, links to seven work items
@package WordPress
@subpackage stackbartlett
*/

$metadata = get_post_meta($post->ID);
$syncedItems = synchronise_work(explode(',', $metadata['wi_order'][0]), $post->ID, 'wi_order');
$assigned = (count($syncedItems['assigned']) > 0 ) ? get_work_by_order($syncedItems['assigned']) : [];

$assignedCount = count($assigned);
$itemIndex = 0;

// get properties for full width banner (first work item)
$mainItem = $assigned[$itemIndex];
$mainItemMetaData = get_post_meta($mainItem->ID);
$firstItem = array(
	"alt" => strtoupper($mainItemMetaData['work_title'][0]),
	"link" => get_permalink($mainItem->ID),
	"src" => $mainItemMetaData['work_thumbnail'][0],
	"w" => $mainItemMetaData['work_thumbnail_w'][0],
	"h" => $mainItemMetaData['work_thumbnail_h'][0]
);

// build rest of work items
$output = "";
$itemIndex = 1;
do {
	$item = $assigned[$itemIndex];
	$itemMetaData = get_post_meta($item->ID);
	$title = strtoupper($itemMetaData['work_title'][0]);
	$link = get_permalink($item->ID);
	$image = $itemMetaData['work_thumbnail'][0];
	$image_w = $itemMetaData['work_thumbnail_w'][0];
	$image_h = $itemMetaData['work_thumbnail_h'][0];

	// start/end rows
	if ( $itemIndex % 2 !== 0 ) {
		$output = $output . "</div></div><div class=\"double\"><div class=\"inner clearfix\">";
	}
	// set position of item in row
	$side = ($itemIndex % 2 !== 0) ? 'left' : 'right';

	$output = $output . "<article class=\"column $side\"><a href=\"{$link}\" class=\"car\" title=\"{$title}\"><img alt=\"{$title}\" data-width=\"{$image_w}\" data-height=\"{$image_h}\" src=\"{$image}\"></a></article>";

	$itemIndex++;
} while ($itemIndex < $assignedCount);

$nextPageID = $metadata['w_nextpage'][0];
$nextPageURL = get_permalink($nextPageID);
$nextPage = get_page_by_postid($nextPageID)[0];

get_header();
?>
	</div>

	<div id="content" class="site-content ourwork">
		<div class="single carimage">
			<article class="column"><a href="<?php echo $firstItem['link']; ?>" class="car" title="<?php echo $firstItem['alt']; ?>"><img alt="<?php echo $firstItem['alt']; ?>" data-width="<?php echo $firstItem['w']; ?>" data-height="<?php echo $firstItem['h']; ?>" src="<?php echo $firstItem['src']; ?>"></a></article>
		</div>

		<div class="double">
			<div class="inner clearfix">
<?php echo $output;?>
			</div>
		</div>

		<div class="text nextlink">
			<p>Next: <a href="<?php echo $nextPageURL; ?>"><?php echo strtoupper($nextPage->post_title); ?></a></p>
		</div>


<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
