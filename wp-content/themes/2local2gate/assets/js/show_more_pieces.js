$(function(){
    $('#show-more').on('click', function(e) {
        e.preventDefault();

        if(!$(this).hasClass('hidden')) {
            var listState = clientContainer.attr('data-state'),
                excludeList = clientContainer.attr('data-ids');

            showMorePieces({
                "itemsperpage": 6,
                "exclude" : excludeList,
                "state": listState
            });
        }
    })
});

function showMorePieces(data) {

    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'show_more_work_by_state'}),
        beforeSend  : function() {
            /* Disable button */
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {

                clientContainer.attr({'data-ids' : response.campaign.ids});

                var itemTitle = '';

                /* Append new posts */
                $.each(response.campaign.posts, function(key, post) {
                    var listElement = clientContainer.find('.itemTemplate').clone();
                    listElement
                    .removeClass('itemTemplate')
                    .addClass('appended')
                    .addClass('dynamic')
                    .attr({"id" : "work" + post.id});

                    listElement.find ('a.overlayLink').attr({
                        "href" : post.url
                    });
                    listElement.find ('figure').append(post.thumbnail);
                    listElement.find ('header h3').html(post.title);
                    listElement.find ('header p').html(post.excerpt);

                    clientContainer.append(listElement);
                });

                if (response.campaign.hide_show_more == 1) {
                    $('#show-more').removeClass('hidden');
                }
                clientContainer.masonry( 'reloadItems' );
                clientContainer.masonry( 'layout' );
            }
            else {
                /* Enable button */
                $('#show-more').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            /* Enable button */
            $('#show-more').removeClass('hidden');
        }
    });

}
