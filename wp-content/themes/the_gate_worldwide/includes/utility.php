<?php
/**
 * Utility functions
 *
 * @package WordPress
 * @subpackage thegate_local
 */

/**
 * Truncates the given string to the given length
 * @param string $string  sdsfsdf
 * @param int $length
 * @param string $suffix
 *
 * @return string
 */
function util_truncate_string($string, $max = 20, $suffix = '')
{
    if(strlen($string) <= $max)
        return $string;

    return substr( $string, 0, strrpos( substr( $string, 0, $max), ' ' ) ) . ' ' . $suffix;
}

/**
 * Gets the terms associated to this post
 *
 * @param integer $_post_id
 *
 * @return array Array('my_taxonomy' => array([term Object1], [term Object2])); [term Object] holds the info for the term
 */
function get_post_taxonomy_terms($_post_id = false)
{
    if($_post_id != false)
    {
        $_post_taxonomy_terms = array();

        /* Determine the post type */
        $_post_type =  get_post_type( $_post_id ) ;

        /* Get the taxonomies registered for this post type */
        $_post_taxonomies = get_object_taxonomies( $_post_type );

        /* For each taxonomy collect the terms for this post */
        foreach($_post_taxonomies as $taxonomy)
        {
            $_temp_terms = array();
            $_temp_terms = get_the_terms( get_the_ID(), $taxonomy );

            if($_temp_terms && !is_wp_error($_temp_terms))
                $_post_taxonomy_terms[$taxonomy] = $_temp_terms;
        }

        return $_post_taxonomy_terms;
    }
    else return false;
}


function the_gate_format_date($date)
{
    return date(get_option('date_format'), strtotime($date));
}


?>