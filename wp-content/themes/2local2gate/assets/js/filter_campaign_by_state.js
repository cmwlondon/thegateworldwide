$(function(){
    
    $('.states li').on('click', function(e) {
        e.preventDefault();

        var state = $(this).data('state');

        showFilteredCampaignsByClient({
            "numberposts": -1,
            "state": state
        });
    })
});

function showFilteredCampaignsByClient(data) {
    console.log("data.state: [%s]", data.state);
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'filter_work_by_state'}),
        beforeSend  : function() {},
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
            }
        },
        error       : function(xhrObj, status, error) {
        }
    });
};
