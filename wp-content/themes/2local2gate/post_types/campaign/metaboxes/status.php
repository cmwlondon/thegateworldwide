<?php
$states = array(
    array(
        'value' => 'evo',
        'caption' => 'Evolutionary'
    ),
    array(
        'value' => 'revo',
        'caption' => 'Revolutionary'
    )
);
$mb->the_field('state');
// $mb->the_value() 
?>
<div class="office box">
    <label for="<?php $mb->the_name(); ?>"><?php _e("Select something: "); ?></label><br/>
    <select name="<?php $mb->the_name(); ?>" id="<?php $mb->the_name(); ?>" style="width: 100%">
        <?php foreach ($states as $state) : ?>
            <option value="<?php echo $state['value']; ?>" <?php $mb->the_select_state($state['value']); ?>>
                <?php echo $state['caption'];?>
            </option>
        <?php endforeach; ?>
    </select>
</div>
