    <?php
/**
 * Template Name: Work
 *
 * @package WordPress
 * @subpackage TheGate
 */

$featured_campaigns = get_latest_featured_posts('campaign', 6);

/* Get latest campaign for each client. Clients with no capmaign related will be skipped */
$clients_latest_campaigns = get_clients_latest_campaigns();
$clients_with_campaigns = array_keys($clients_latest_campaigns);

/* Get latest campaign or campaign element for each media type. Media types with no capmaign or campaign element related will be skipped */
$media_with_work = get_media_types_containing_campaign_elements();

$media_latest_work = get_recent_campaign_elements(7, 0);
$hide_show_more = count($media_latest_work) < 7;
if(count($media_latest_work) == 7) array_pop($media_latest_work);

/* Load script for slider only if there are featured campaigns */
if(!empty($featured_campaigns)) wp_enqueue_script('anything_slider');

/* Add ajax handler url in js variable to make ajax path dynamical in js */
wp_localize_script( 'filter_campaigns_by_client', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
wp_enqueue_script('filter_campaigns_by_client');
wp_localize_script( 'filter_work_by_media', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
wp_enqueue_script('filter_work_by_media');
wp_localize_script( 'show_more_pieces', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
wp_enqueue_script('show_more_pieces');

the_post();
get_header();
?>

</header>
<!-- main -->
<div id="main">
    <?php if(!empty($featured_campaigns)) : ?>
        <?php if(count($featured_campaigns) > 1) : ?>
        <ul class="featured-prev-next">
            <li style="display: none" class="prev"><a href="#"></a></li>
            <li style="display: none" class="next"><a href="#"></a></li>
        </ul>
        <?php endif; ?>
        <div class="work-slider-wrapper">
        	<style type="text/css" scoped>
				.work-category-slider{
					list-style-type: none;
				}
			</style>
            <ul class="work-category-slider" style="padding-left: 0; margin-top: 0">
                <?php foreach ($featured_campaigns as $featured_campaign) : ?>
                    <li>
                        <div class="work-desc-wrap" style="display: none">
                            <div class="work-desc">
                                <h3 class="work-title"><span><?php echo apply_filters('the_title', $featured_campaign->post_title); ?></span> <a href="<?php echo get_permalink($featured_campaign->ID); ?>">&nbsp;</a></h3>
                                <?php echo apply_filters('the_excerpt', $featured_campaign->post_excerpt); ?>
                            </div>
                        </div>
                        <a href="<?php echo get_permalink($featured_campaign->ID); ?>"><?php echo (has_post_thumbnail($featured_campaign->ID)) ? get_the_post_thumbnail($featured_campaign->ID, 'homepage-thumbnail', array('alt' => '')) : get_placeholder_image('homepage-thumbnail'); ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="main-holder controls-holder" style="display: none">
                <div class="slidecontrols">
                </div>
            </div>
            <div class="visual">
            </div>
        </div>
    <?php endif; ?>
    <div class="main-holder">
        <!-- content -->
        <div id="content">
            <!-- simple-list -->
            <a href="#" class="title"><?php _e('Work', 'Work Listing Page'); ?></a>
            <ul class="tab-titles">
                <li class="current"><a href="#" class="by-client"><?php _e('By client', 'Work Listing Page'); ?></a></li>
                <li><a href="#" class="by-media"><?php _e('By media'); ?></a></li>
            </ul>
            <ul class="simple-list">
                <li id="by-client" class="by-client">
                    <div class="connected-items">
                        <div class="connected-items-holder">
                            <h2><?php _e('By client', 'Work Listing Page'); ?></h2>
                            <ul class="dropdown-menu">
                                <li id="sort-client" class="headlink">
                                    <div class="dropdown-title-wrap">
                                        <span class="dropdown-label"><span><?php _e('View by', 'Work Listing Page'); ?></span><?php _e(' Client', 'Work Listing Page'); ?>:</span>
                                        <div href="#" class="dropdown-title"><a class="filtered" data-category="" href="#"><?php _e('All', 'Work Listing Page'); ?></a></div>
                                    </div>
                                    <ul class="dropdown-submenu">
                                        <li><a href="#" data-client="" class="first"><?php _e('All', 'Work Listing Page'); ?></a></li>
                                        <?php foreach ($clients_with_campaigns as $key => $client_id) : ?>
                                            <li><a href="#" data-client="<?php echo $client_id; ?>"><?php echo apply_filters('the_title', get_the_title($client_id)); ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            </ul>

                            <ul class="list">
                                <?php $count = 1; ?>
                                <?php foreach ($clients_latest_campaigns as $client => $campaign) : ?>
                                    <li class="by-client-row-<?php echo ceil($count / 3); ?> m-by-client-row-<?php echo ceil($count / 2); ?>">
                                        <div class="holder">
                                            <figure>
                                                <a href="<?php echo get_permalink($campaign->ID); ?>"><?php echo (has_post_thumbnail($campaign->ID)) ? get_the_post_thumbnail($campaign->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?></a>
                                            </figure>
                                            <header>
                                                <h3><?php echo apply_filters('the_title', $campaign->post_title); ?></h3>
                                            </header>
                                            <?php echo apply_filters('the_excerpt', $campaign->post_excerpt); ?>
                                            <a class="more" href="#"><?php _e('READ MORE', 'Work Listing Page'); ?></a>
                                        </div>
                                    </li>
                                    <?php $count++; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </li>
                <li id="by-media" class="by-media">
                    <!-- connected-items -->
                    <div class="connected-items">
                        <div class="connected-items-holder">
                    <h2><?php _e('By media', 'Work Listing Page'); ?></h2>
                        <ul class="dropdown-menu">
                            <li id="sort-media" class="headlink">
                                <div class="dropdown-title-wrap">
                                    <span class="dropdown-label"><span><?php _e('View by', 'Work Listing Page'); ?></span><?php _e(' Type', 'Work Listing Page'); ?>:</span>
                                    <div href="#" class="dropdown-title"><a class="filtered" data-category="" href="#"><?php _e('All', 'Work Listing Page'); ?></a></div>
                                </div>
                                <ul class="dropdown-submenu">
                                    <li><a href="#" data-media="" class="first"><?php _e('All', 'Work Listing Page'); ?></a></li>
                                    <?php foreach ($media_with_work as $media) : ?>
                                        <li><a href="#" data-media="<?php echo $media->term_id; ?>"><?php echo apply_filters('the_category', $media->name); ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>

                        <ul class="list">
                                <?php $count = 1; ?>
                                <?php foreach ($media_latest_work as $campaign_element) : ?>
                                    <?php $client_name = get_post_meta($campaign_element->ID, 'campaign_client', true); ?>
                                    <?php $check_client_name = !empty($client_name) ? get_the_title($client_name) : ''; ?>
                                    <li class="by-media-row-<?php echo ceil($count / 3); ?> m-by-media-row-<?php echo ceil($count / 2); ?>">
                                        <div class="holder">
                                            <figure>
                                                <a href="<?php echo get_permalink($campaign_element->ID); ?>"><?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?></a>
                                                <strong class="title"><?php echo get_post_category($campaign_element->ID); ?></strong>
                                            </figure>
                                            <header>
                                                <h3><?php echo apply_filters('the_title', $campaign_element->post_title); ?></h3>
                                                <span class="client"><?php echo $check_client_name ?></span>
                                            </header>
                                            <?php echo apply_filters('the_excerpt', $campaign_element->post_excerpt); ?>
                                            <a class="more" href="#"><?php _e('READ MORE', 'Work Listing Page'); ?></a>
                                        </div>
                                    </li>
                                    <?php $count++; ?>
                                <?php endforeach; ?>
                            </ul>
                            <div class="show-more-work">
                                <a id="show-more" <?php echo ($hide_show_more) ? 'class="hidden"' : ''; ?> href="#"><p><?php _e('SHOW MORE', 'Work Listing Page'); ?></p><span></span></a>
                            </div>
                        </div>
                    </div>
                    <!-- .connected-items end -->
                </li>
            </ul>
        </div>
    </div>
</div>

<?php get_footer();
