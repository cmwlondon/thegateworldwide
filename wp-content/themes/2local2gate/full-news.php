<?php
/**
 * Template Name: News
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $featured_news_page,
       $all_news_page,
       $featured_news,
       $category;

/* Get first children that has template "News Featured" */
$featured_news_page = get_first_children_page_by_template('part-news_featured.php');
/* Get first children that has template "News All" */
$all_news_page = get_first_children_page_by_template('part-news_all.php');


$featured_news = get_latest_featured_posts('post', 6);

/* Check if category id  is given as get parameter in order to filter news */
$category = (isset($_GET['category']) && (int)$_GET['category']) ? get_category($_GET['category']) : false;

wp_enqueue_script('show_more_news');
wp_localize_script( 'show_more_news', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
wp_enqueue_script('filter_news_by_category');
wp_localize_script( 'filter_news_by_category', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
wp_enqueue_script('anything_slider');

wp_enqueue_script('isotope_grid');
wp_enqueue_script('masonry_grid');
wp_enqueue_script('froogaloop_min');

?>

<?php get_header();?>

    <!-- visual -->
</header>
<!-- main -->
<div id="main">
    <div class="outerBox">
        <div class="innerPadding">
            <span class="title"><?php the_title(); ?></span>
            <!-- simple-list -->
            <?php get_template_part('part', 'news_all'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>