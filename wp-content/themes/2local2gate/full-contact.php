<?php
/**
 * Template Name: Contact
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $local_page,
       $worldwide_page,
       $validation_mapping;

the_post();

/* Get first children that has template "Local" */
$local_page = get_first_children_page_by_template('part-local.php');
/* Get first children that has template "Worldwide" */
$worldwide_page = get_first_children_page_by_template('part-worldwide.php');

wp_enqueue_script('contact');
wp_enqueue_script('validation_helper');
wp_enqueue_script('google_maps_infobox');
wp_enqueue_script('scroll_to');

/* Create a var in js that will contain url to ajax handler and not to be hardoded */
wp_localize_script( 'contact', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );

get_header();
?>
    <!-- visual -->
</header>
<div id="main">
    <div class="main-holder">
        <!-- content -->
        <div id="content">
            <a class="title" href="#"><?php the_title(); ?></a>
            <!-- simple-list -->
            <ul class="fake-tab-titles">
                <li class="current"><a href="#" class="local"><?php echo get_post_meta($local_page->ID, 'menu_label', true); ?></a></li>
                <li><a href="#" class="worldwide"><?php echo get_post_meta($worldwide_page->ID, 'menu_label', true); ?></a></li>
            </ul>
            <ul class="simple-list">
                <?php get_template_part('part', 'local'); ?>
                <?php get_template_part('part', 'worldwide'); ?>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIcwBgTynNogi5lGkPoLxDzzref7hVSCM&amp;sensor=false"></script>
<script type="text/javascript">
    var pageID = <?php echo (int)$local_page->ID; ?>;
    var validationMapping = new Array();

    <?php foreach ($validation_mapping as $id => $rules) : ?>
        var properties = new Array();

        <?php if(is_array($rules)) :
            foreach ($rules as $rule) : ?>
                properties.push('<?php echo lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $rule)))); ?>');
            <?php endforeach;
        endif; ?>
        validationMapping.push({name: '<?php _e($id); ?>', properties: properties});
    <?php endforeach; ?>
</script>
<?php get_footer();