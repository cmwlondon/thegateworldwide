<?php
/*
custom post type: 'work'
single item template
single-work.php

@package WordPress
@subpackage stackbartlett
*/

// get page header image
/*
$thumbnailID = get_post_thumbnail_id($post->ID);
$imageData = wp_get_attachment_image_src( $thumbnailID, 'full');
$headerImage = $imageData[0];
*/

$metadata = get_post_meta($post->ID);
// $metadata[meta key][0];

$posttags = get_the_tags();
$taglist = [];
if ($posttags) {
    foreach($posttags AS $tag) {
        $taglist[] = $tag->name;
    }
}

$metadata = get_post_meta($post->ID);

$work_title = $metadata['work_title'][0];

$banner = array(
	"type" => $metadata['banner_type'][0],
	"title" => $metadata['wb_option_alt'][0],
	"vimeo" => $metadata['wb_option_vimeo'][0],
	"src" => $metadata['wb_option_image'][0],
	"id" => $metadata['wb_option_image_id'][0],
	"w" => $metadata['wb_option_image_w'][0],
	"h" => $metadata['wb_option_image_h'][0]
);
$bannerpath = wp_get_attachment_image_src($banner['id'],'full')[0];

$pic1 = array(
	"title" => $metadata['wp1_alt'][0],
	"src" => $metadata['wp1_image'][0],
	"id" => $metadata['wp1_image_id'][0],
	"w" => $metadata['wp1_image_w'][0],
	"h" => $metadata['wp1_image_h'][0]
);
$pic1path = wp_get_attachment_image_src($pic1['id'],'full')[0];

$pic2 = array(
	"title" => $metadata['wp2_alt'][0],
	"src" => $metadata['wp2_image'][0],
	"id" => $metadata['wp2_image_id'][0],
	"w" => $metadata['wp2_image_w'][0],
	"h" => $metadata['wp2_image_h'][0]
);
$pic2path = wp_get_attachment_image_src($pic2['id'],'full')[0];

$copy1 = array(
	"header" => $metadata['wc1_header'][0],
	"text" => html_entity_decode($metadata['wc1_text'][0])
);

$copy2 = array(
	"header" => $metadata['wc2_header'][0],
	"text" => html_entity_decode($metadata['wc2_text'][0])
);

$copy3 = array(
	"header" => $metadata['wc3_header'][0],
	"text" => html_entity_decode($metadata['wc3_text'][0])
);

$copy4 = array(
	"header" => $metadata['wc4_header'][0],
	"text" => html_entity_decode($metadata['wc4_text'][0])
);

$nextPageID = $metadata['work_nextwork'][0];
$nextPageURL = get_permalink($nextPageID);
$nextPage = get_work_by_id($nextPageID)[0];


get_header();?>
	</div>

<div id="content" class="site-content work">
		<div class="text intro">
			<h1><?php echo strtoupper($work_title); ?></h1>
			<div class="hr"><hr></div>
			<h2><?php echo $copy1['header']; ?></h2>
			<p><?php echo $copy1['text']; ?></p>
		</div>

<?php switch ( $banner['type'] ) : ?>
<?php case "video" : ?>
		<div class="single carimage videobutton">
			<article class="column">
				<div class="layer trigger">
					<a href="" class="play button">
						<div class="playicon"><img alt="play" src="<?php echo $theme_root; ?>/img/play-button.png"></div>
					</a>
				</div>
				<div class="layer image">
					<div href="" class="carl">
						<img alt="" data-width="<?php echo $banner["w"]; ?>" data-height="<?php echo $banner["h"]; ?>" src="<?php echo $bannerpath; ?>">
					</div>
				</div>
				<div class="layer video" data-video="<?php echo $banner["vimeo"]; ?>"></div>
			</article>
		</div>
<?php break; ?>
<?php case "image" : ?>
		<div class="single carimage">
			<article class="column"><div class="car"><img alt="<?php echo $banner["title"]; ?>" data-width="<?php echo $banner["w"]; ?>" data-height="<?php echo $banner["h"]; ?>" src="<?php echo $bannerpath; ?>"></div></article>
		</div>
<?php break; ?>
<?php endswitch; ?>

		<div class="text">
			<?php if ( $copy2['header'] != '' ) : ?><h2><?php echo $copy2['header']; ?></h2><?php endif; ?>
			<p><?php echo $copy2['text']; ?></p>
		</div>

		<?php if ( $pic1path != '' ) : ?>
		<div class="single carimage">
			<article class="column"><div class="car"><img alt="<?php echo $pic1['title']; ?>" data-width="<?php echo $pic1['w']; ?>" data-height="<?php echo $pic1['h']; ?>" src="<?php echo $pic1path; ?>"></div></article>
		</div>
		<?php endif; ?>

		<div class="text">
			<?php if ( $copy3['header'] != '' ) : ?><h2><?php echo $copy3['header']; ?></h2><?php endif; ?>
			<p><?php echo $copy3['text']; ?></p>
		</div>

		<?php if ( $pic2path != '' ) : ?>
		<div class="single carimage">
			<article class="column"><div class="car"><img alt="<?php echo $pic2['title']; ?>" data-width="<?php echo $pic2['w']; ?>" data-height="<?php echo $pic2['h']; ?>" src="<?php echo $pic2path; ?>"></div></article>
		</div>
		<?php endif; ?>

		<?php if ($copy4['text'] != '' || $copy4['header'] != '' ) : ?>
		<div class="text">
			<?php if ( $copy4['header'] != '' ) : ?><h2><?php echo $copy4['header']; ?></h2><?php endif; ?>
			<p><?php echo $copy4['text']; ?></p>
		</div>
		<?php endif; ?>

		<div class="text nextlink">
			<p>Next: <a href="<?php echo $nextPageURL; ?>"><?php echo strtoupper($nextPage->post_title); ?></a></p>
		</div>

<?php get_footer(); ?>