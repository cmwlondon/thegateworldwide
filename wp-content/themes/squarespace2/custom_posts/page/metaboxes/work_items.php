<?php
$theme_root = get_template_directory_uri();

$syncedItems = synchronise_work(explode(',', $values['wi_order']), $post->ID, 'wi_order');
$assigned = (count($syncedItems['assigned']) > 0 ) ? get_work_by_order($syncedItems['assigned']) : [];
$unassigned = (count($syncedItems['unassigned']) > 0 ) ? get_work_by_order($syncedItems['unassigned']) : [];

$dummy1State = (count($assigned) > 0) ? ' hidden' : '';
$dummy2State = (count($unassigned) > 0) ? ' hidden' : '';

?>

<div class="customField itemSorter workItemSorter">
	<p>(First Item displayed as full width banner.)</p>
    <input type="hidden" id="wi_order" name="wi_order" value="<?php echo implode(',', $syncedItems['assigned']); ?>">

	<p>Assigned</p>
    <ul id="workItemAssign"  class="slideConnected">
        <li class="clearfix dummy <?php echo $dummy1State; ?>" id="itemDummy1">
            <div class="thumbnail"><img alt="-" src="<?php echo $theme_root; ?>/img/work_dummy.png"></div>
            <h3>-</h3>
        </li>
	<?php
	foreach ( $assigned AS $workItem) :
		$itemMetadata = get_post_meta($workItem->ID);
	?>
	<li class="clearfix" id="<?php echo "workItem_".$workItem->ID; ?>"><div class="thumbnail"><img alt="<?php echo $itemMetadata['work_title'][0]; ?>" src="<?php echo $itemMetadata['work_thumbnail'][0]; ?>" data-width="<?php echo $itemMetadata['work_thumbnail_w'][0]; ?>" data-height="<?php echo $itemMetadata['work_thumbnail_h'][0]; ?>"></div><h3><?php echo $itemMetadata['work_title'][0]; ?></h3></li>
	<?php endforeach; ?>
	</ul>

	<p>Unassigned</p>
    <ul id="workItemUnassign" class="slideConnected">
        <li class="clearfix dummy <?php echo $dummy2State; ?>" id="itemDummy2">
            <div class="thumbnail"><img alt="-" src="<?php echo $theme_root; ?>/img/work_dummy.png"></div>
            <h3>-</h3>
        </li>
	<?php
	foreach ( $unassigned AS $workItem) :
		$itemMetadata = get_post_meta($workItem->ID);
	?>
	<li class="clearfix" id="<?php echo "workItem_".$workItem->ID; ?>"><div class="thumbnail"><img alt="<?php echo $itemMetadata['work_title'][0]; ?>" src="<?php echo $itemMetadata['work_thumbnail'][0]; ?>" data-width="<?php echo $itemMetadata['work_thumbnail_w'][0]; ?>" data-height="<?php echo $itemMetadata['work_thumbnail_h'][0]; ?>"></div><h3><?php echo $itemMetadata['work_title'][0]; ?></h3></li>
	<?php endforeach; ?>
	</ul>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // make people order items sortable
        if (jQuery('#workItemAssign').length != 0) {

            jQuery('#workItemAssign, #workItemUnassign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    var newOrderList = [];
                    jQuery('#workItemAssign li').not(".dummy").each(function(i){
                        newOrderList.push( jQuery(this).attr("id").substr(9) );
                    });
                    console.log("stop1 %s", newOrderList.join());
                    jQuery('#wi_order').val(newOrderList.join());
                }
            }).disableSelection();

            jQuery('#workItemAssign').on('sortstop',function(event, ui) {
            	var realItems = jQuery(this).find('li').not(".dummy"),
            		realItemCount = realItems.length,
            		dummyItem = jQuery(this).find('li.dummy');
                
                console.log('assign: ' + realItemCount);

                if ( realItemCount == 0 ) {
					dummyItem.removeClass('hidden');
                } else {
					dummyItem.addClass('hidden');
                }
            });
            jQuery('#workItemUnassign').on('sortstop',function(event, ui) {
            	var realItems = jQuery(this).find('li').not(".dummy"),
            		realItemCount = realItems.length,
            		dummyItem = jQuery(this).find('li.dummy');
                
                console.log('unassign: ' + realItemCount);

                if ( realItemCount == 0 ) {
					dummyItem.removeClass('hidden');
                } else {
					dummyItem.addClass('hidden');
                }
            });
        }
    });
</script>
