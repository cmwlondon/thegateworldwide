<?php

$front_page_id = get_option('page_on_front');
$front_page_id = ($front_page_id) ? $front_page_id : -1;

/* Register Page Metabox 'Sites Details' */
$sites_details = new WPAlchemy_MetaBox(array
(
    'id'        => 'sites_details',
    'title'     => __('Sites Details'),
    'types'     => array('page'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'high',
    'template'  => get_template_directory() . '/post_types/page/metaboxes/sites_details.php',
    'autosave'  => true,
    'prefix'    => 'sites_details_',
    'include_post_id' => $front_page_id,
    'save_filter' => 'gate_sites_details_save_filter',
));

/* Register Page Metabox 'Worldwide Posts' */
$worldwide_posts = new WPAlchemy_MetaBox(array
(
    'id'        => 'worldwide_posts',
    'title'     => __('Sites Feed'),
    'types'     => array('page'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'low',
    'template'  => get_template_directory() . '/post_types/page/metaboxes/worldwide_posts.php',
    'autosave'  => true,
    'include_post_id' => $front_page_id,
));


function gate_sites_details_save_filter($meta, $post_id)
{
    $sites = array('asia', 'new_york', 'london', 'edinburgh');
    $url_keys = array('follow_url', 'contact_url');

    foreach ($sites as $site)
    {
        foreach ($url_keys as $url_key)
        {
            $meta_key = $site . '_' . $url_key;
            if(isset($meta[$meta_key]))
            {
                $value = $meta[$meta_key];
                $validation = preg_match( '/^(http|https):\/\/([a-z0-9-]\.)*/i', $value );

                if( !$validation && trim($value) !== '' )
                {
                    $meta[$meta_key] = 'http://' . $value;
                }
            }
        }
    }

    return $meta;
}