$(function(){
    $('form.contact').on('submit', function(e) {
        e.preventDefault();
        $('#fail').addClass('hidden');
        $('#success').addClass('hidden');

        var validation = new Validation(validationMapping);

        if(validation.validate()) {
            var formSerializedObject = {}
            $($(this).serializeArray()).each(function(key, object) {
                formSerializedObject[object.name] = object.value;
            });

            data = {post_id: pageID, params: formSerializedObject};

            $.each(validation.fields, function(index, field){
                data.params[field.name] = field.value;
            });

            $.each(validation.fields, function(key, field) {
                $('[name="' + field.name + '"]').parent('div').removeClass('error');
            });

            sendMail(data);
        }
        else {
            $.each(validation.fields, function(key, field) {

                if(field.errors.length > 0) {
                    $('[name="' + field.name + '"]').parent('div').addClass('error');
                }
                else {
                    $('[name="' + field.name + '"]').parent('div').removeClass('error');
                }
            });
        }
    })
});

function sendMail(data) {

    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'send_email'}),
        beforeSend  : function() {
            /* Clear errors */

            /* Show loader */
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
                /* Show success message */
                //show_success_message();
                //$('#success').removeClass('hidden');
				$('.contact-field').css('display','none');
				$('.contact [type=submit]').css('visibility','hidden');
				$('.contact-success').css('display','block');
            }
            else {
                /* Show fail message */
                $('#fail').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            /* Log the error description */
            console.log(status, error);
        }
    });

}
