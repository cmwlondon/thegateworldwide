<?php
/**
 * Twitter Options in the Settings
 */

add_action( 'admin_init', 'twitter_options_init' );
/* Register our settings. Add the settings section, and settings fields */
function twitter_options_init()
{
    add_settings_section(
            'twitter_settings_section',
            '',
            'twitter_section_text',
            'twitter-info-settings' );

    add_settings_field(
            'twitter_switch_checkbox',
            'Switch On/Off Twitter feed',
            'twitter_switch_checkbox',
            'twitter-info-settings',
            'twitter_settings_section' );

    add_settings_field(
            'twitter_screen_name',
            'Twitter Screen name',
            'twitter_screen_name_input',
            'twitter-info-settings',
            'twitter_settings_section' );

    add_settings_field(
            'twitter_app_consumer_key',
            'Twitter Application Consumer Key',
            'twitter_app_consumer_key_input',
            'twitter-info-settings',
            'twitter_settings_section' );

    add_settings_field(
            'twitter_app_consumer_secret',
            'Twitter Application Consumer Secret',
            'twitter_app_consumer_secret_input',
            'twitter-info-settings',
            'twitter_settings_section' );

    add_settings_field(
            'access_token',
            'Account Access Token',
            'access_token_input',
            'twitter-info-settings',
            'twitter_settings_section' );

    add_settings_field(
            'access_token_secret',
            'Account Access Token Secret',
            'access_token_secret_input',
            'twitter-info-settings',
            'twitter_settings_section' );

    add_settings_field(
            'tweet_count',
            'Tweets to show',
            'tweet_count_input',
            'twitter-info-settings',
            'twitter_settings_section' );

    register_setting(
            'twitter-info-settings',
            'twitter-info-settings',
            '' );
}

add_action( 'admin_menu', 'twitter_options_add_page' );
/* Add sub page to the Settings Menu */
function twitter_options_add_page()
{
    add_options_page(
            'Twitter',
            'Twitter Feed',
            'manage_options',
            'twitter-info-settings',
            'twitter_options_page' );
}

/* Callbacks function */
global $value;
$value = get_option( 'twitter-info-settings' );

function twitter_switch_checkbox()
{
    global $value;
?>
    <input type="checkbox" name="twitter-info-settings[twitter_switch_checkbox]"  value="1" <?php checked( 1, $value['twitter_switch_checkbox'] ); ?> size="50" />

<?php
}

function twitter_screen_name_input()
{
    global $value;
?>
    <input type="text" name="twitter-info-settings[twitter_screen_name]" value="<?php esc_attr_e( $value['twitter_screen_name'] ); ?>" size="50" />

<?php
}

function twitter_app_consumer_key_input()
{
    global $value;
?>
    <input type="text" name="twitter-info-settings[twitter_app_consumer_key]" value="<?php esc_attr_e( $value['twitter_app_consumer_key'] ); ?>" size="50" />

<?php
}

function twitter_app_consumer_secret_input()
{
    global $value;
?>
    <input type="text" name="twitter-info-settings[twitter_app_consumer_secret]" value="<?php esc_attr_e( $value['twitter_app_consumer_secret'] ); ?>" size="50" />

<?php
}

function access_token_input()
{
    global $value;
?>
    <input type="text" name="twitter-info-settings[access_token]" value="<?php esc_attr_e( $value['access_token'] ); ?>" size="50" />

<?php
}

function access_token_secret_input()
{
    global $value;
?>
    <input type="text" name="twitter-info-settings[access_token_secret]" value="<?php esc_attr_e( $value['access_token_secret'] ); ?>" size="50" />

<?php
}

function tweet_count_input()
{
    global $value;
?>
    <input type="text" name="twitter-info-settings[tweet_count]" value="<?php esc_attr_e( $value['tweet_count'] ); ?>" size="50" />

<?php
}

function twitter_section_text()
{
?>
    <p><?php _e( 'Enter your twitter account information.' ); ?></p>
<?
}

function twitter_options_page()
{
?>
    <div class="wrap">
        <div class="icon32" id="icon-options-general"><br /></div>
        <h2><?php _e( 'Twitter Profile Information' ); ?></h2>
        <form action="options.php" method="post">

            <?php settings_fields( 'twitter-info-settings' ); ?>

            <?php do_settings_sections( 'twitter-info-settings' ); ?>

            <?php submit_button(); ?>

        </form>
    </div>
<?php
}
