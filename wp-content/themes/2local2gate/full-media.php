<?php
/**
 * Template Name: Media
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $media_capabilities_page,
       $media_experience_page,
       $media_philosophy_page,
       $media_values_page;

the_post();

/* Get first children that has template "Capabilities" */
$media_capabilities_page = get_first_children_page_by_template('part-media_capabilities.php');
/* Get first children that has template "Experience" */
$media_experience_page = get_first_children_page_by_template('part-media_experience.php');
/* Get first children that has template "Philosophy" */
$media_philosophy_page = get_first_children_page_by_template('part-media_philosophy.php');
/* Get first child that has template "Values" */
$media_values_page = get_first_children_page_by_template('part-media_values.php');

get_header();

?>
    <!-- visual -->
</header>
<!-- main -->
<div id="main" class="mediaPage">
    <div class="main-holder">
        <!-- content -->
        <div id="content">
            <?php get_template_part('part', 'media_philosophy'); ?>
            <?php 
            /*
            get_template_part('part', 'media_values');
            get_template_part('part', 'media_capabilities');
            */
            ?>
        </div>
    </div>
</div>
<?php

get_footer();