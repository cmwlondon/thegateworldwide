<?php
/**
 * @package Wordpress Multisite
 * @subpackage The Gate Local
 */

/**
 * Register post type Campaign element
 *
 */

function thegate_local_home_box_init()
{
    $labels = array(
        'name'                => _x( 'Stories Box', 'post type general name' ),
        'singular_name'       => _x( 'StoriesBox', 'post type singular name' ),
        'add_new'             => _x( 'Add New Stories Box', 'campaign_element' ),
        'add_new_item'        => __( 'Add New Stories Box'),
        'edit_item'           => __( 'Edit Stories Box' ),
        'new_item'            => __( 'New Stories Box' ),
        'all_items'           => __( 'All Stories Boxes' ),
        'view_item'           => __( 'View Stories Box' ),
        'search_items'        => __( 'Search Stories Box' ),
        'not_found'           => __( 'No Stories Box found' ),
        'not_found_in_trash'  => __( 'No Stories Box found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Stories Boxes'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => 'Stories-boxes',
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt'),
	   'taxonomies'	=>	array('post_tag')
    );

    register_post_type( 'home_box', $args );
}
add_action( 'init', 'thegate_local_home_box_init' );

/* Styling for the custom post type icon */
add_action( 'admin_head', 'add_home_box_large_icon' );

function add_home_box_large_icon()
{
    ?>
    <style type="text/css" media="screen">
        #icon-edit.icon32-posts-campaign_element {background: url(<?php bloginfo('template_url') ?>/post_types/homebox/img/campaign_element32x32.png) no-repeat;}
    </style>
<?php
}

/* Add filter to ensure the text office, or office, is displayed when user updates a office */
add_filter( 'post_updated_messages', 'codex_home_box_updated_messages' );
function codex_home_box_updated_messages( $messages ) {
    global $post, $post_ID;

    $messages['home_box'] = array(
        0 => '', /* Unused. Messages start at index 1.*/
        1 => sprintf( __( 'home box updated. <a href="%s">View Story Box</a>' ), esc_url( get_permalink( $post_ID ) ) ),
        2 => __( 'Custom field updated.' ),
        3 => __( 'Custom field deleted.' ),
    );
    return $messages;
}

/* Custom Metaboxes */
/*
$campaign_element_video = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_video',
    'title'     => 'Video details',
    'types'     => array( 'campaign_element' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/video_details.php',
    'autosave'  => true,
));
*/
/*
$campaign_element_swf = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_swf',
    'title'     => 'SWF',
    'types'     => array( 'campaign_element' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/swf.php',
    'autosave'  => true,
));
*/

$home_box_mp4 = new WPAlchemy_MetaBox( array(
    'id'        => 'home_box_mp4',
    'title'     => 'MP4 Video',
    'types'     => array( 'home_box' ),
    'context'   => 'normal',
    'priority'  => 'high',
    'template'  => get_template_directory() . '/post_types/homebox/metaboxes/mp4.php',
    'autosave'  => true,
));

/*
$home_box_description = new WPAlchemy_MetaBox( array(
    'id'        => 'home_box_description',
    'title'     => 'Description',
    'types'     => array( 'home_box' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/homebox/metaboxes/description.php',
    'autosave'  => true,
));
*/

$home_box_linkto_campaign = new WPAlchemy_MetaBox( array(
    'id'        => 'home_box_linkto_campaign',
    'title'     => 'Campaigns',
    'types'     => array( 'home_box' ),
    'context'   => 'side',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/homebox/metaboxes/campaign.php',
    'autosave'  => true,
));

add_filter( 'manage_edit-home_box_columns', 'add_new_home_box_columns' );
function add_new_home_box_columns( $home_box_columns )
{
    $new_columns['cb']              = '<input type="checkbox" />';
    $new_columns['id']              = __( 'ID' );
    $new_columns['title']           = _x( 'Title', 'column name' );
    $new_columns['tile']       = _x( 'Tile', 'column name' );
    $new_columns['tile_background']       = _x( 'Tile background', 'column name' );
    // $new_columns['video']           = _x( 'Video', 'column name' );
    $new_columns['video']           = _x( 'Video', 'column name' );
    $new_columns['filter']           = _x( 'Category', 'column name' );
    // $new_columns['client']          = _x('Client');
    $new_columns['date']            = __( 'Date' );

    return $new_columns;
}

/* Add sortable columns */
add_filter('manage_edit-home_box_sortable_columns', 'add_sortable_columns_to_home_box');
function add_sortable_columns_to_home_box($columns)
{
    $columns['id'] = 'id';

    return $columns;
}

add_action( 'manage_home_box_posts_custom_column', 'manage_home_box_columns', 10, 2 );

function manage_home_box_columns( $column_name, $id )
{
    global $post, $home_box_video, $home_box_mp4, $campaigns_purchaser;
    $campaigns_purchaser->the_meta();
    // $campaign_element_video->the_meta();
    $home_box_mp4->the_meta();

    $hbtags = wp_get_post_tags($post->ID);
    $hbtags = $hbtags[0]->name;
    switch( $column_name )
    {
        case 'id':
            echo $post->ID;
            break;

        case 'tile':
            echo get_the_post_thumbnail( $post->ID, array( 80, 60 ) );
            break;

        case 'tile_background':
            echo MultiPostThumbnails::the_post_thumbnail('home_box', 'homeboxbackground', $box->ID, array( 80, 60 ));
            // echo get_the_post_thumbnail( $post->ID, array( 80, 60 ) );
            break;

        case 'video':
            echo $home_box_mp4->get_the_value( 'document' ) ? __( 'Yes' ) : __( 'No');
            break;

        case 'filter':
            echo $hbtags;
            break;
        /*
        case 'client':
                $client_id = $campaigns_purchaser->get_the_value('client');
                echo get_the_title($client_id);
            break;
        case 'video':
            // echo $campaign_element_video->get_the_value( 'video_embedcode' ) ? __( 'Yes' ) : __( 'No');
            break;
        */
    }
}


/* Add metabox for secondary images */
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Tile background',
            'id' => 'homeboxbackground',
            'post_type' => 'home_box'
        )
    );
}