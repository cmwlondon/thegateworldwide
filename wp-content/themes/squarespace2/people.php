<?php
/*
Template Name: people

@package WordPress
@subpackage stackbartlett
*/

$metadata = get_post_meta($post->ID);
$syncedItems = synchronise_people(explode(',', $metadata['ppl_order'][0]), $post->ID, 'ppl_order');
$people = (count($syncedItems['assigned']) > 0 ) ? get_people_in_order($syncedItems['assigned']) : [];

$nextPageID = $metadata['ppl_nextpage'][0];
$nextPageURL = get_permalink($nextPageID);
$nextPage = get_page_by_postid($nextPageID)[0];

$output = '';
foreach ( $people AS $person ) {
	$metadata = get_post_meta($person->ID);

	$name = $metadata['p_name'][0];
	$role = $metadata['p_role'][0];
	$bio = $metadata['p_bio'][0];
	$link = $metadata['p_link'][0];

	$portrait = [
		"id" => $metadata['person_normal_id'][0],
		"image" => $metadata['person_normal'][0],
		"width" => $metadata['person_normal_w'][0],
		"height" => $metadata['person_normal_h'][0]
	];

	$output = $output . "<div class=\"person flexboxItem\">\n<div class=\"portrait\"><img title=\"$name - $role\" alt=\"$name - $role\" data-width=\"".$portrait['width']."\" data-height=\"".$portrait['height']."\" src=\"".$portrait['image']."\"></div>\n<div class=\"tb\">\n<h2>$name</h2>\n<h3>$role</h3>\n<p>$bio</p>\n";

	// is there a link associated with this person?
	if ($link != '') {
		// yes, what type of link
		preg_match('/(instagram|linkedin|youtube|twitter|facebook)/', $link, $linktype);
		if ( $linktype ) {
			$linkstring =  strtoupper( $linktype[0] );
		}
		$output = $output . "<a href=\"$link\" class=\"social\">$linkstring</a>\n";
	}
	$output = $output . "</div>\n</div>\n";
}

get_header();
?>
	</div>

	<div id="content" class="site-content ourpeople">

		<div class="people clearfix">
			<div class="inner flexboxContainer">
			<?php echo $output; ?>
			</div>
		</div>

		<div class="text nextlink">
			<p>Next: <a href="<?php echo $nextPageURL; ?>"><?php echo strtoupper($nextPage->post_title); ?></a></p>
		</div>
<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
