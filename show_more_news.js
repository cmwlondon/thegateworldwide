$(function(){
    $('#show-more').on('click', function(e) {
        e.preventDefault();

        var offset = $('.newsList li').length - 1;
        // var category = $('#all-news .filtered').first().data('category') || '0';
        var category = newsDropDown.current;

        if(!$(this).hasClass('hidden')) {
            showMoreNews({offset: offset, numberposts: 6, category: category, post_type: ['post']});
        }
    })
});

function showMoreNews(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'show_more'}),
        beforeSend  : function() {
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {

                /* Append new posts */
                $.each(response.posts, function(key, post) {
                    var listElement = $('.itemTemplate').clone();
                    listElement
                    .removeClass('itemTemplate')
                    .addClass('appended')
                    .addClass('dynamic');

                    // post.date
                    // post.category
                    listElement.find ('a.overlayLink').attr({
                        "href" : post.url
                    });
                    listElement.find ('figure').append(post.thumbnail);
                    listElement.find ('header h3').html(post.title);
                    listElement.find ('header p').html(post.excerpt);

                    newsContainer.append(listElement);
                });

                if(!response.hide_show_more)
                $('#show-more').removeClass('hidden');
                newsContainer.masonry( 'reloadItems' );
                newsContainer.masonry( 'layout' );

            }
            else {
                $('#show-more').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            $('#show-more').removeClass('hidden');
        }
    });

}
