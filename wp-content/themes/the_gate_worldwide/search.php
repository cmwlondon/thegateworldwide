<?php
/**
 * Search page
 * 
 * @package WordPress
 * @subpackage thegate_worldwide
 */
?>
<?php get_header();?>
<div id="main" >
	<div id="content">
      	<div class="dark_sep" style="margin-top: 0;"></div>
		<h2 class="title">Search results for: <?php the_search_query(); ?></h2>
		<form id="search_again" class="base" method="get" action="">
			<fieldset class="search_form">
				<input type="text" name="s" />
				<input class="submit" type="submit" value="" />
			</fieldset>
		</form>
		
		<div class="light_sep"></div>
		<div id="search_results" class="left">
			<div class="head">1-6 of 132 Results</div>
			<ul>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				    <li>
						<a href="<?php the_permalink() ?>" title="" class="title" rel="bookmark"><?php the_title(); ?></a>
						<div class="rich_text">
							<?php 
								the_excerpt();
							 ?>
				      		<?php wp_link_pages(); ?>
						</div>
						<div class="tags">
							<?php 
							$links = prepare_post_terms_links();
							$count = count($links);
							if($count > 0){
								foreach($links as $key => $link) : ?>
									<a href="<?php echo $link['href'] ?>" title="<?php echo $link['name']; ?>"><?php echo $link['name']; ?></a><?php if($key != $count-1) echo ',';?>
							<?php 
								endforeach; 
								}
							?>
						</div>
					</li>
				 <?php endwhile; else: ?>
				
				 <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				 <?php endif; ?>
			</ul>
		</div>
	</div>
	<div id="column" class="right">
        <?php if ( is_active_sidebar('pages-widget-area') ) : ?>
            <?php dynamic_sidebar( 'pages-widget-area' ); ?>
            <script type="text/javascript"> $('#column div:first').css({"margin-top" : "0"}); </script>
        <?php endif; ?>
    </div>
	<div class="clear"></div>
	<div class="dark_sep" style="margin-top: 12px;margin-bottom:10px"></div>
  <?php //get_sidebar();?>
  <?php get_footer();?>