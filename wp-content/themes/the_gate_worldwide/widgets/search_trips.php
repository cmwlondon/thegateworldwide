<?php
/**
 * Widget for Search in Trips
 * 
 * @author Plamen Nikolov
 * 
 * @package WordPress
 * @subpackage Bike_Cation
 * @since Bike-Cation 1.0
 * 
 */
class SearchTrips extends WP_Widget {
    
      function SearchTrips() {
             /* Widget settings. */
            $widget_ops = array(
              'classname' => 'search-trips',
              'description' => 'Allows you to search through trips');
            
            $control_ops = array(
               'id_base' => 'search-trips-widget'
            );
            
            /* Create the widget. */
            $this->WP_Widget('search-trips-widget', 'Search Trips', $widget_ops, $control_ops );
      }

      /**
       * Backend of the Widget
       */
      function form ($instance) { ?>
          <p>
            <label for="<?php echo $this->get_field_id('title'); ?>" class="description">Title:</label><br />
            <input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo $instance['title'] ?>" size="25" />
          </p>
          <p>
            <label for="<?php echo $this->get_field_id('search_criterias'); ?>" class="description">Search Criterias:</label><br />
          <?php
           $search_criterias = array(
                'traveller_types'       => 'Traveller types',
                'trip_categories'       => 'Trip Categories', 
                'trip_destinations'     => 'Destinations', 
                'preferred_dates'       => 'Preferred dates', 
                'trip_difficulty'       => 'Trip Difficulty', 
                'accommodation_types'   => 'Accomodation type', 
                'budget'                => 'Budget',
          ); ?>
          
          <?php foreach($search_criterias as $key => $value) : ?>
            <label class="checkbox_label">
                <input type="checkbox" class="check-box" name="<?php echo $this->get_field_name('search_criterias') ?>[]" value="<?php echo $key; ?>"<?php if( in_array($key, (array)$instance['search_criterias']) ) : ?> checked <?php endif; ?>/> <?php echo $value; ?>
            </label>
          <?php endforeach; ?>
      </p>
   <?php }
    
    /**
     * Backend data management of the widget
     */
    function update ($new_instance, $old_instance) {
          $instance = $old_instance;
        
          $instance['title']                = $new_instance['title'];
          $instance['search_criterias']     = $new_instance['search_criterias'];
        
          return $instance;
    }


    /**
     * Front end of the Widget
     */
    function widget($args, $instance) { global $wp_query;  ?>
     <div class="box">
         <div class="box_header">
            <h2><?php echo $instance['title']; ?></h2>
        </div>
        <div class="box_content">
            <form id="find_holiday" method="get" action="<?php bloginfo('url') ?>/trips">
                <fieldset>
                    
                    <?php if ( in_array('traveller_types', $instance['search_criterias']) ) : ?>
                    <label class="select">   
                       <span class="label"><?php _e('What type of traveller are you?'); ?></span> 
                       <?php $wp_query->query['traveller_types'] ?>
                       <select name="traveller_types" class="custom_select">
                            <option value=""><?php _e("Don't mind"); ?></option>
                           <?php
                                $tax_terms = get_terms( 'traveller_types' );
                                foreach($tax_terms as $tax_term ) : ?>
                           <option <?php if($wp_query->query['traveller_types'] == $tax_term->slug) echo 'selected="selected"'; ?> value="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></option>
                           <?php endforeach; ?>
                       </select>
                    </label>
                    <?php endif; ?>
                    
                    <?php if ( in_array('trip_categories', $instance['search_criterias']) ) : ?>
                    <label class="select">   
                       <span class="label"><?php _e('What kind of trip do you prefer?'); ?></span> 
                       <select name="trip_categories" class="custom_select">
                           <option value=""><?php _e("Don't mind"); ?></option>
                           <?php
                                $tax_terms = get_terms( 'trip_categories' );
                                foreach($tax_terms as $tax_term ) : ?>
                           <option <?php if($wp_query->query['trip_categories'] == $tax_term->slug) echo 'selected="selected"'; ?> value="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></option>
                           <?php endforeach; ?>
                       </select>
                    </label>
                    <?php endif; ?>
                    
                    <?php if ( in_array('trip_destinations', $instance['search_criterias']) ) : ?>
                    <label class="select">   
                       <span class="label"><?php _e('Where would you like to go?'); ?></span> 
                       <select name="trip_destinations" class="custom_select">
                           <option value=""><?php _e("Don't mind"); ?></option>
                           <?php
                                $tax_terms = get_terms( 'trip_destinations' );
                                foreach($tax_terms as $tax_term ) : ?>
                           <option <?php if($wp_query->query['trip_destinations'] == $tax_term->slug) echo 'selected="selected"'; ?> value="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></option>
                           <?php endforeach; ?>
                       </select>
                    </label>
                    <?php endif; ?>
                    
                    <?php if ( in_array('preferred_dates', $instance['search_criterias']) ) : ?>
                    <label class="select">   
                       <span class="label"><?php _e('Preferred dates'); ?></span> 
                       <select name="preferred_dates" class="custom_select">
                           <option value=""><?php _e("Don't mind"); ?></option>
                           <?php 
                            $months = array();
                            $currentMonth = (int)date('m'); 
                           ?>
                           <?php for($x = $currentMonth; $x < $currentMonth+12; $x++) : $time =  mktime(0, 0, 0, $x, 1); ?>  
                                <option <?php if($_GET['preferred_dates'] == date('Y-m', $time)) echo 'selected="selected"'; ?> value="<?php echo date('Y-m', $time) ?>"><?php echo date('F Y', $time) ?></option>
                           <?php endfor; ?>
                       </select>
                    </label>
                    <?php endif; ?>
                    
                    <?php if ( in_array('trip_difficulty', $instance['search_criterias']) ) : ?>
                    <span class="label"><?php _e('How much of a challenge?'); ?></span>
                    <div id="checkboxes">  
                        <div class="left">
                        <span class="gear"></span><br />
                        <label>
                            <input type="checkbox" class="custom_checkbox"
                            <?php  if( isset( $_GET['difficulty'] ) && in_array( 'Easy', $_GET['difficulty'] ) ) echo ' checked="checked" '; ?>
                             name="difficulty[]" value="Easy" />
                            <span><?php _e('Easy'); ?></span>
                        </label>
                    </div>
                    <div class="left">
                        <span class="gear"></span><span class="gear"></span><br />
                        <label>
                            <input type="checkbox" class="custom_checkbox"
                            <?php  if( isset( $_GET['difficulty'] ) && in_array( 'Moderate', $_GET['difficulty'] ) ) echo ' checked="checked" '; ?>
                             name="difficulty[]" value="Moderate" />
                            <span><?php _e('Moderate'); ?></span>
                        </label>
                    </div>
                    <div class="left">
                        <span class="gear"></span><span class="gear"></span><span class="gear"></span><br />
                        <label class="no_margin">
                            <input type="checkbox" class="custom_checkbox"
                            <?php  if( isset( $_GET['difficulty'] ) && in_array( 'Tough', $_GET['difficulty'] ) ) echo ' checked="checked" '; ?>
                             name="difficulty[]" value="Tough" />
                            <span><?php _e('Tough'); ?></span>
                        </label>
                    </div>
                    <div class="clear"></div>
                    </div>
                    <?php endif; ?>
                    
                    <?php if ( in_array('accommodation_types', $instance['search_criterias']) ) : ?>
                    <label class="select">   
                       <span class="label"><?php _e('Accomodation type'); ?></span> 
                       <select name="accommodation_types" class="custom_select">
                           <option value=""><?php _e("Don't mind"); ?></option>
                           <?php
                                $tax_terms = get_terms( 'accommodation_types' );
                                foreach($tax_terms as $tax_term ) : ?>
                           <option <?php if($wp_query->query['accommodation_types'] == $tax_term->slug) echo 'selected="selected"'; ?> value="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></option>
                           <?php endforeach; ?>
                       </select>
                    </label>
                    <?php endif; ?>
                    
                    <?php if ( in_array('budget', $instance['search_criterias']) ) : ?>
                    <label class="select">   
                       <span class="label"><?php _e('What is your budget?'); ?></span> 
                       <select name="budgets" class="custom_select">
                           <option value=""><?php _e("Don't mind"); ?></option>
                           <?php
                                $tax_terms = get_terms( 'budgets' );
                                foreach($tax_terms as $tax_term ) : ?>
                           <option <?php if($wp_query->query['budgets'] == $tax_term->slug) echo 'selected="selected"'; ?> value="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></option>
                           <?php endforeach; ?>
                       </select>
                    </label>
                    <?php endif; ?>
                    
                    <input type="submit" class="submit" value="" /> 
                    <div class="clear"></div>
                </fieldset>
            </form>
        </div>
     </div>
        
     <?php
     }
}

add_action('widgets_init', create_function('', 'return register_widget("SearchTrips");'));
?>