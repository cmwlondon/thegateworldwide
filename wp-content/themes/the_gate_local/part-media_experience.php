<?php
/**
 * Template Name: Media Experience
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $media_experience_page;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

if($media_experience_page) : ?>
<li id="experience">
    <h2><?php echo apply_filters('the_title', get_the_title($media_experience_page->ID)); ?></h2>
    <a href="#" class="opener"><?php echo apply_filters('the_title', get_post_meta($media_experience_page->ID, 'menu_label', true)); ?></a>
    <div class="slide">
        <figure class="alignright border"><?php echo (has_post_thumbnail($media_experience_page->ID)) ? get_the_post_thumbnail($media_experience_page->ID, 'about-experience') : get_placeholder_image('about-experience'); ?></figure>
        <div class="text indent">
            <h3><?php echo get_post_meta($media_experience_page->ID, 'about_subtitle_subtitle', true); ?></h3>
            <?php echo apply_filters('the_content', $media_experience_page->post_content); ?>
            <div class="roll-wrap">
                <ul class="roll">
                    <?php $first_category_references = get_post_meta($media_experience_page->ID, 'experience_references_first_category_references', true); ?>
                    <?php if(is_array($first_category_references)) : ?>
                        <?php foreach ($first_category_references as $reference) : ?>
                            <?php if(isset($reference['text']) && trim($reference['text']) !== '') : ?>
                                <li><?php echo $reference['text']; ?></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                <ul class="roll">
                    <?php $second_category_references = get_post_meta($media_experience_page->ID, 'experience_references_second_category_references', true); ?>
                    <?php if(is_array($second_category_references)) : ?>
                        <?php foreach ($second_category_references as $reference) : ?>
                            <?php if(isset($reference['text']) && trim($reference['text']) !== '') : ?>
                                <li><?php echo $reference['text']; ?></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</li>
<?php endif;