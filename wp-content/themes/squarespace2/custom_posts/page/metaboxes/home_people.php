<?php
?>
<div class="customField mediumtext">
	<label for="hp_title">Title</label>
	<input type="text" id="hp_title" name="hp_title" value="<?php echo $values['hp_title']; ?>">
</div>
 
<div class="customField mediumtext">
	<label for="hp_link">Link</label>
	<input type="text" id="hp_link" name="hp_link" value="<?php echo $values['hp_link']; ?>">
</div>
 
 <div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Image</label>
		<input type="hidden" id="hp_image" name="hp_image" value="<?php echo $values['hp_image']; ?>">
		<input type="hidden" id="hp_image_id" name="hp_image_id" value="<?php echo $values['hp_image_id']; ?>" class="ident">
		<input type="hidden" id="hp_image_w" name="hp_image_w" value="<?php echo $values['hp_image_w']; ?>" class="width">
		<input type="hidden" id="hp_image_h" name="hp_image_h" value="<?php echo $values['hp_image_h']; ?>" class="height">
		<a href="" class="hp_image_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['hp_image']; ?>" <?php if ($values['hp_image'] == '' || $values['hp_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['hp_image'] == '' || $values['hp_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['hp_image']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.hp_image_Picker')
    }));
});
</script>
