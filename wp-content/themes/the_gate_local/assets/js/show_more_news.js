$(function(){
    $('#show-more').on('click', function(e) {
        e.preventDefault();

        var offset = $('#all-news .list li').length;
        var category = $('#all-news .filtered').first().data('category') || '0';

        if(!$(this).hasClass('hidden')) {
            showMoreNews({offset: offset, numberposts: 6, category: category, post_type: ['post']});
        }
    })
});

function showMoreNews(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'show_more'}),
        beforeSend  : function() {
            /* Disable button */
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
                var listClasses = [];

                /* Append new posts */
                $.each(response.posts, function(key, post) {
                    var listElement = $('#all-news .list li').first().clone();
                    var rowClass = 'row-' + post.row;

                    listClasses.push(rowClass);
                    if(post.category && !Number(data.category)) {
                        if($('figure .title', listElement).length > 0) {
                            $('figure .title', listElement).text(post.category);
                        }
                        else {
                            $('figure', listElement).append($('<strong/>').addClass('title').text(post.category));
                        }
                    }
                    else {
                        $('figure .title', listElement).remove();
                    }

                    $(listElement).attr('class', rowClass);
                    
                    post.thumbnail = post.thumbnail.replace('<img','<img onload="this.setAttribute(\'data-loaded\',\'yep\')" data-loaded="nope"');
                    $('figure a', listElement).attr('href', post.url).html(post.thumbnail);
                    
                    $('header h3 a', listElement).html(post.title).attr('href', post.url);
                    $('header h4', listElement).html(post.date);
                    $('.holder', listElement).height('auto');
					 if(post.postCount != -1){
					 	$('.views-counter', listElement).css('display','');
					 	$('.views-counter span', listElement).html(post.postCount);
					 }else
						$('.views-counter', listElement).css('display', 'none');
					 
                    if($('p', listElement).length > 0) {
                        var excerpts = $('p', listElement);
                        excerpts.remove();
                    }
                    $('header', listElement).after($(post.excerpt));

                    $('a.more', listElement).attr('href', post.url);

                    listElement.appendTo($('#all-news .list'));
                });

                /* Set equal hight for list items in same row */
                setTimeout(function(){
                    if($('[data-loaded="nope"]').length != 0){
                        setTimeout(arguments.callee, 250);
                        return;
                    }
                    $.setEqualHeightByClassWithImages($.unique(listClasses));
                }, 500)

                /* Enable button if posts are as much as expected */
                if(!response.hide_show_more)
                $('#show-more').removeClass('hidden');
            }
            else {
                /* Enable button */
                $('#show-more').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            /* Enable button */
            $('#show-more').removeClass('hidden');
        }
    });

}
