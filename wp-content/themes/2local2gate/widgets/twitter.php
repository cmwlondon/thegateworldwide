<?php
/**
 * Retrieve latest tweets
 */
 if( !function_exists( 'format_date_like_twitter' ) ) {
        function format_date_like_twitter($date) {
            $cur_time = time();

            if(date("d m Y", strtotime($date)) == date("d m Y", $cur_time))
            {
                $formatted_date = date('h:i', strtotime($date));
            }
            elseif(date("Y", strtotime($date)) == date("Y", $cur_time))
            {
                $formatted_date = date('j F', strtotime($date));
            }
            else
            {
                $formatted_date = date('j F, Y', strtotime($date));
            }

            return $formatted_date;
        }
 }
 if( !function_exists( 'get_connection_with_access_token' ) ) {
        function get_connection_with_access_token($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
            $connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);

            return $connection;
        }
 }
 if( !function_exists( 'get_tweets' ) ) {
        function get_tweets( $screen_name = '', $consumerkey = '', $consumersecret = '', $accesstoken = '', $accesstokensecret = '', $count = 5 )
        {
            $response = '';
            $connection = get_connection_with_access_token( $consumerkey, $consumersecret, $accesstoken, $accesstokensecret );
            $tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$screen_name."&count=".$count);

            $tweet_format =
                    '<li>
                        <article class="column">
                            <header>
                                <em class="time">%1$s</em>
                                <strong class="title"><a href="http://twitter.com/%2$s">@%2$s</a></strong>
                            </header>
                            <p>%3$s</p>
                        </article>
                    </li>';

            if( is_array($tweets) && count($tweets) > 0 )
            {
                foreach( $tweets as $item )
                {
                    $tweet = $item->text;
                    $tweet = str_replace("{$screen_name}: ", '', $tweet );
                    $formatted_date = format_date_like_twitter($item->created_at);

                    /* Make all URLs clickable */
                    $tweet = preg_replace('/(http[^\s]+)/', '<a href="$1" target="_blank">$1</a>', $tweet);
                    /* Replace hashtags */
                    $tweet = preg_replace('#@([\\d\\w]+)#', '<a href="http://twitter.com/$1" target="_blank">$0</a>', $tweet);
                    $tweet = preg_replace('#\#([\\d\\w]+)#', '<a href="http://twitter.com/search?q=%23$1&src=hash" target="_blank">$0</a>', $tweet);

                    /* Append current tweet to the response */
                    $response .= vsprintf( $tweet_format , array($formatted_date, $screen_name, $tweet) );
                }
            }
            return $response;
        }
}

/**
 * Widget for Display latest tweets
 *
 * @author Kristian Arsov
 * @author Rozalia Stoilova
 *
 * @package WordPress Multisite
 * @subpackage The gate
 * @since The Gate
 *
 */
class AthLatestTweets extends WP_Widget {

      function AthLatestTweets()
      {
             /* Widget settings. */
            $widget_ops = array(
              'classname'   => 'latest-tweets',
              'description' => __( 'Allows you to display a latest tweets.') );

            $control_ops = array(
               'id_base' => 'latest-tweets-widget'
            );

            /* Create the widget. */
            $this->WP_Widget( 'latest-tweets-widget', 'Latest tweets', $widget_ops, $control_ops );
      }

     /**
     * Back-end of the widget
     */
      function form( $instance )
      {
        /* Set up some default widget settings. */
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('screen_name') ?>"><?php _e( 'Twitter screen name:' ); ?></label><br />
            <input type="text" name="<?php echo $this->get_field_name('screen_name') ?>" id="<?php echo $this->get_field_id('screen_name') ?>" value="<?php echo isset($instance['screen_name']) ? $instance['screen_name'] : '';  ?>" size="25" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('app_consumer_key') ?>"><?php _e( 'Twitter Application Consumer Key:' ); ?></label><br />
            <input type="text" name="<?php echo $this->get_field_name('app_consumer_key') ?>" id="<?php echo $this->get_field_id('app_consumer_key') ?>" value="<?php echo isset($instance['app_consumer_key']) ? $instance['app_consumer_key'] : ''; ?>" size="25" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('app_consumer_secret') ?>"><?php _e( 'Twitter Application Consumer Secret:' ); ?></label><br />
            <input type="text" name="<?php echo $this->get_field_name('app_consumer_secret') ?>" id="<?php echo $this->get_field_id('app_consumer_secret') ?>" value="<?php echo isset($instance['app_consumer_secret']) ?$instance['app_consumer_secret'] : ''; ?>" size="25" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('access_token') ?>"><?php _e( 'Account Access Token:' ); ?></label><br />
            <input type="text" name="<?php echo $this->get_field_name('access_token') ?>" id="<?php echo $this->get_field_id('access_token') ?>" value="<?php echo isset($instance['access_token']) ? $instance['access_token'] : ''; ?>" size="25" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('access_token_secret') ?>"><?php _e( 'Account Access Token Secret:' ); ?></label><br />
            <input type="text" name="<?php echo $this->get_field_name('access_token_secret') ?>" id="<?php echo $this->get_field_id('access_token_secret') ?>" value="<?php echo isset($instance['access_token_secret']) ? $instance['access_token_secret'] : ''?>" size="25" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('tweets_count') ?>"><?php _e( 'Tweets to show:' ); ?></label><br />
            <input type="text" name="<?php echo $this->get_field_name('tweets_count') ?>" id="<?php echo $this->get_field_id('tweets_count') ?>" value="<?php echo isset($instance['tweets_count']) ? $instance['tweets_count'] : ''; ?>" size="10" />
        </p>

      <?php
    }

    /**
     * Back-end datamanagement of the widget
     */
    function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;

        $instance['screen_name']            = $new_instance['screen_name'];
        $instance['app_consumer_key']       = $new_instance['app_consumer_key'];
        $instance['app_consumer_secret']    = $new_instance['app_consumer_secret'];
        $instance['access_token']           = $new_instance['access_token'];
        $instance['access_token_secret']    = $new_instance['access_token_secret'];
        $instance['tweets_count']           = $new_instance['tweets_count'];

        return $instance;
    }

    /**
     * Front-end of the widget
     */
    function widget( $args, $instance )
    {
        $tweets_count = (int)$instance['tweets_count'] > 0 ? (int)$instance['tweets_count'] : null;
        $latest_tweets = get_tweets(
                            $instance['screen_name'],
                            $instance['app_consumer_key'],
                            $instance['app_consumer_secret'],
                            $instance['access_token'],
                            $instance['access_token_secret'],
                            $tweets_count );
    ?>
        <div class="holder">
            <div class="frame twitter">
                <?php if( strlen( $latest_tweets ) > 0 ) : ?>
                <ul id="scroller">
                    <?php echo $latest_tweets; ?>
                </ul>
                <?php else : ?>
                    <h2><?php _e( 'Sorry! No tweets found :' ); ?></h2>
                <?php endif; ?>
            </div>
        </div>
    <?php
    }
}

add_action( 'widgets_init', create_function( '', 'return register_widget("AthLatestTweets");' ) );
?>