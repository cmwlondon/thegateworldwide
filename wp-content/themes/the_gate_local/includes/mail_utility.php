<?php
/**
 * Mail Utility class
 */

class MailHelper
{
    public $emails;
    public $fields;
    public $email_message;

    public function __construct($fields, $emails, $subject = null)
    {
        $this->fields = $fields;
        $this->emails = is_array($emails) ? $emails : array($emails);

        $this->subject = ($subject) ? $subject : (__('Generated Email from ') . get_option('blogname') . __(' Contact Form'));
    }

    public function send()
    {
        $to = implode(', ', $this->emails);
        $this->prepare();

        $result = wp_mail($to, $this->subject, $this->email_message);

        return $result;
    }
    private function prepare()
    {
        $this->email_message = "\n";

        foreach ($this->fields as $field => $value) {
            $this->email_message .= $field . ': ' . $value . "\n";
        }

        $this->email_message .= "\n\n";
        $this->email_message .= __('This email is sent from ') . get_the_title() . __(' Page on ') . get_option('blogname') . __(' Site. ') . get_permalink();
    }
}

?>