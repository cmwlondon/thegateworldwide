<?php
    $references_categories = array(
        'first_category'    => 'First Column',
        'second_category'   => 'Second Column',
    )
?>

<?php foreach( $references_categories as $category_name => $category_title ) : ?>
    <div class="experience references_categories postbox">
        <div class="handlediv" title="Click to toggle"></div>
        <h3><?php _e( $category_title ); ?></h3>
        <div class="inside" style="position: relative">
            <?php while($metabox->have_fields_and_multi($category_name . '_references')): ?>
                <?php $mb->the_group_open(); ?>
                    <div class="misc-pub-section">
                        <table>
                                <tr>
                                    <?php $metabox->the_field('text'); ?>
                                    <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></td>
                                </tr>
                                <tr>
                                    <th><?php _e('Category'); ?></th>
                                    <th><?php _e('Category'); ?></th>
                                </tr>
                                <td>
                                    <?php $metabox->the_field('first_category'); ?>
                                    <textarea rows="5" cols="30" name="<?php $mb->the_name(); ?>"><?php $mb->the_value(); ?></textarea></td>
                                </td>
                                <td>
                                    <?php $metabox->the_field('second_category'); ?>
                                    <textarea rows="5" cols="30" name="<?php $mb->the_name(); ?>"><?php $mb->the_value(); ?></textarea></td>
                                </td>
                            <tr>
                            <tr>
                                <td><a href="#" class="dodelete button"><?php _e('Remove Reference'); ?></a></td>
                            </tr>
                        </table>
                    </div>
                <?php $mb->the_group_close(); ?>
            <?php endwhile; ?>
            <p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-<?php echo $category_name; ?>_references button"><?php _e('Add Reference'); ?></a></p>
        </div>
    </div>
<?php endforeach; ?>
<div class="clear"></div>