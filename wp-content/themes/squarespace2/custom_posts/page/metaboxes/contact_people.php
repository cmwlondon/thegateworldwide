<?php
$settings = array(
	array(
		"field" => "cpl_name",
		"label" => "Name"
	),
	array(
		"field" => "cpl_role",
		"label" => "Role"
	),
	array(
		"field" => "cpl_phone",
		"label" => "Phone"
	),
	array(
		"field" => "cpl_email",
		"label" => "Email"
	),
	array(
		"field" => "cpl_link",
		"label" => "Link"
	),
	array(
		"field" => array('cpl_image', 'cpl_image_id', 'cpl_image_w', 'cpl_image_h'),
		"label" => "Photo"
	),
	array(
		"field" => "cpr_name",
		"label" => "Name"
	),
	array(
		"field" => "cpr_role",
		"label" => "Role"
	),
	array(
		"field" => "cpr_phone",
		"label" => "Phone"
	),
	array(
		"field" => "cpr_email",
		"label" => "Email"
	),
	array(
		"field" => "cpr_link",
		"label" => "Link"
	),
	array(
		"field" => array('cpr_image', 'cpr_image_id', 'cpr_image_w', 'cpr_image_h'),
		"label" => "Photo"
	)
);
$pickers = array();

foreach ($settings AS $index => $item) :
	if ($item['label'] != 'Photo') :
?>
<div class="customField mediumtext">
	<label for="<?php echo $item['field']; ?>"><?php echo $item['label']; ?></label>
	<input type="text" id="<?php echo $item['field']; ?>" name="<?php echo $item['field']; ?>" value="<?php echo $values[$item['field']]; ?>">
</div>
<?php
else :
	$pickers[] = "'.".$item['field'][0]."_Picker'";
?>
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label><?php echo $item['label']; ?></label>
		<input type="hidden" id="<?php echo $item['field'][0]; ?>" name="<?php echo $item['field'][0]; ?>" value="<?php echo $values[ $item['field'][0] ]; ?>">
		<input type="hidden" id="<?php echo $item['field'][1]; ?>" name="<?php echo $item['field'][1]; ?>" value="<?php echo $values[ $item['field'][1] ]; ?>" class="ident">
		<input type="hidden" id="<?php echo $item['field'][2]; ?>" name="<?php echo $item['field'][2]; ?>" value="<?php echo $values[ $item['field'][2] ]; ?>" class="width">
		<input type="hidden" id="<?php echo $item['field'][3]; ?>" name="<?php echo $item['field'][3]; ?>" value="<?php echo $values[ $item['field'][2] ]; ?>" class="height">
		<a href="" class="<?php echo $item['field'][0]; ?>_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values[$item['field'][0]]; ?>" <?php if ($values[$item['field'][0]] == '' || $values[$item['field'][0]] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values[$item['field'][0]] == '' || $values[$item['field'][0]] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values[$item['field'][0]]; ?></p>
	</div>
</div>
<?php
	endif;
endforeach;
?>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
<?php
	foreach ($pickers AS $picker) :
?>
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery(<?php echo $picker; ?>)
    }));
<?php
	endforeach;
?>
});
</script>
