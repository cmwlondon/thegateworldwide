<?php
/**
 * General page template - Used for default template pages such as Terms and Conditions, Accessibility and View Sitemap
 *
 * @package WordPress
 * @subpackage thegate_local
 */

get_header(); ?>

<body>
    <!-- wrapper -->
    <div class="sub-page" id="wrapper">
        <div class="wrapper-holder">
            <div id="main">
                <div class="main-holder">
                    <div id="content">
                       <h2><?php the_title(); ?></h2>
            			<?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>

	<?php get_footer(); ?>