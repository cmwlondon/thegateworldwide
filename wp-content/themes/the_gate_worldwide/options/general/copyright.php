<?php
/**
 * Contact Options in the Settings
 */

add_action( 'admin_init', 'copyright_options_init' );
/* Register our settings. Add the settings section, and settings fields */
function copyright_options_init()
{
    add_settings_section(
        'copyright_section',
        '',
        '',
        'copyright-settings'
    );

    add_settings_field(
        'copyright_text',
        'Copyright text',
        'copyright_editor',
        'copyright-settings',
        'copyright_section'
    );

    register_setting(
        'copyright-settings',
        'copyright-settings',
        'clean_copyright_transients'
    );
}

add_action( 'admin_menu', 'copyright_options_add_page' );
/* Add sub page to the Settings Menu */
function copyright_options_add_page()
{
    add_options_page(
            'Copyright',
            'Copyright',
            'manage_options',
            'copyright-settings',
            'copyright_options_page' );
}

/* Callbacks function */
global $value;
$value = get_option( 'copyright-settings' );

function copyright_editor()
{
    global $value;

    wp_editor($value, 'copyright-settings');
}

function copyright_options_page()
{
?>
    <div class="wrap">
        <div class="icon32" id="icon-options-general"><br /></div>
        <h2><?php _e( 'Footer Copyright' ); ?></h2>
        <form action="options.php" method="post">

            <?php settings_fields( 'copyright-settings' ); ?>

            <?php do_settings_sections( 'copyright-settings' ); ?>

            <?php submit_button(); ?>

        </form>
    </div>
<?php
}


function clean_copyright_transients($input)
{
    for ($i=2; $i <=5; $i++) {
        switch_to_blog($i);

        delete_transient('copyright_text');
    }

    switch_to_blog(1);

    return $input;
}
