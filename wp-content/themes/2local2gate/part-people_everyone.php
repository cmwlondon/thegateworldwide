<?php
/**
 * Template Name: People Everyone
 *
 * @package WordPress
 * @subpackage TheGate
 */

global  $everyone_page,
        $everyone,
        $individual_page_permalink,
        $seed,
        $lp_leaders,
        $lp_followers;

/* Redirect to 404 if opened directly #3342 */
if( basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME) )
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

if($everyone_page) : ?>
    <div class="peopleBlock londonPeople">
        <h2><?php echo apply_filters('the_title', $everyone_page->post_title); ?></h2>

        <ul class="peopleGrid">
            <?php foreach ($lp_followers as $person) : ?>
                <li>
                    <div>
                        <a href="<?php echo $individual_page_permalink; ?>?<?php echo http_build_query(array('slug' => $person['slug'], 'e' => true, 'o' => $seed), '', '&amp;'); ?>" class="photo" data-slug="<?php echo $person['slug']; ?>">
                            <div class="border"><?php echo $person['small']; ?></div>
                        </a>
                        <p class="job"><?php echo $person['position']; ?></p>
                        <p class="name"><?php echo $person['first_name']; ?> <?php echo $person['last_name']; ?></p>
                    </div>
                </li>
            <?php endforeach; ?>
         </ul>
        </div>
<?php endif;
