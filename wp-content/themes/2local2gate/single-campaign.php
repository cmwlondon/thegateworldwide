<?php
/**
 * Single Campaign Page (Individual Campaign Page)
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $related_posts;

the_post();
$related_posts = get_related_posts(3, array('post'), true);
$client = get_client();
$campaign_elements = get_campaign_elements();

wp_enqueue_script('share_popup');
wp_enqueue_script('fancybox');
wp_enqueue_script('anything_slider');
wp_enqueue_script('isotope_grid');
wp_enqueue_script('masonry_grid');

$previous_post = get_previous_post();
$next_post = get_next_post();

$is_london = !substr_compare(trim(site_url(), '/'), 'london', -6, 6);

get_header();
?>
    <!-- visual -->
</header>
<!-- main -->
<div id="main">
    <!--
    <ul class="featured-prev-next">
        <?php if (!empty( $next_post )): ?>
            <li class="previous"><a href="<?php echo get_permalink( $next_post->ID ); ?>"></a></li>
        <?php endif; ?>
        <li class="mobile-carousel-msg"><span><?php _e('WORK <br /> BY CLIENT', 'Single Campaign Page'); ?></span></li>
        <?php if (!empty( $previous_post )): ?>
            <li class="next"><a href="<?php echo get_permalink( $previous_post->ID ); ?>"></a></li>
        <?php endif; ?>
    </ul>
    <div class="work-slider-wrapper">
        <ul class="work-individual">
            <li>
                <a href="#" class="work-fancybox-fake"><?php echo (has_post_thumbnail()) ? get_the_post_thumbnail($post->ID, 'homepage-thumbnail') : get_placeholder_image('homepage-thumbnail'); ?></a>
            </li>
        </ul>
        <div class="visual">
        </div>
    </div>
    -->
    <div class="main-holder">
        <!-- content -->
        <div id="content" class="street-campaign">

            <div id="values" class="outerBox">
                <div class="innerPadding">
                <h2><?php the_title(); ?></h2>

                        <div style="clear:both;">
                            <?php
                                $subheader = get_post_meta($post->ID, 'campaign_subtitle', true);
                                if ( $subheader != '' ) :
                            ?>
                            <div class="titleCol">
                                <h3><?php echo $subheader; ?></h3>
                            </div>
                            <?php endif; ?>
                            <div class="campaignItems">
                                <?php
                                $majorItem = array_shift($campaign_elements);

                                $showmp4 = false;
                                $mp4_id = 0;
                                $mp4File = '';
                                $mp4scale = 1.0;
                                $items = get_post_meta($majorItem->ID, 'campaign_element_mp4', true);
                                if ($items) {
                                    if (array_key_exists('document', $items)) {
                                        $showmp4 = true;
                                        $mp4_id = $items['document']; // get post_id of attached video file
                                        $mp4post = get_post_meta($mp4_id, '_wp_attached_file', false); // get path to attached video file (within uploads directory)
                                        $mp4File = wp_upload_dir()['baseurl']."/".$mp4post[0]; // get full URL for video file
                                        $mp4scale = ( $items['scale'] != null ) ? $items['scale'] : 1.0;
                                        $mp4boxwidth = ( $items['boxwidth'] != null ) ? $items['boxwidth'] : 'random';
                                    }
                                }

                                $imageData = wp_get_attachment_image_src( get_post_thumbnail_id($majorItem->ID), 'medium-thumbnail');
                                $thumbnailURL   = $imageData[0];
                                $campaign_element_url = get_permalink($majorItem->ID);

                                if(class_exists('MultiPostThumbnails'))
                                {
                                    $check_main_img = MultiPostThumbnails::has_post_thumbnail($majorItem->post_type, 'main_image', $majorItem->ID, 'full');
                                    $main_img_url   = MultiPostThumbnails::get_post_thumbnail_url($majorItem->post_type, 'main_image', $majorItem->ID, 'full');
                                }
                                ?>
                                <div class="majorItem">
                                    <figure>
                                        <?php if ($showmp4 && !$key) : ?>
                                            <div class="videoFrame">
                                                <video autoplay preload poster="<?php echo $check_main_img ? $main_img_url : get_placeholder_image_src('full'); ?>" style="width:100%;">
                                                    <source type="video/mp4" src="<?php echo $mp4File; ?>"> 
                                                </video>
                                            </div>
                                        <?php else : ?>
                                            <?php echo (has_post_thumbnail($majorItem->ID)) ? get_the_post_thumbnail($majorItem->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
                                        <?php endif; ?>
                                    </figure>
                                </div>

                                <div class="gutter-sizer"></div>
                                <div class="grid-sizer"></div>

                                <ul class="isotopeList campaignElementList">
                                    <li class="itemTemplate">
                                        <div class="holder">
                                            <a class="overlayLink"></a>
                                            <figure></figure>
                                            <header>
                                                <h3></h3>
                                                <h4></h4>
                                                <p class="excerpt"></p>
                                                <p class="category"></p>
                                            </header>
                                        </div>
                                    </li>

                                    <?php foreach ($campaign_elements as $key => $campaign_element) : ?>
                                        <?php
                                            $showmp4 = false;
                                            $mp4_id = 0;
                                            $mp4File = '';
                                            $mp4scale = 1.0;
                                            $items = get_post_meta($campaign_element->ID, 'campaign_element_mp4', true);
                                            if ($items) {
                                                if (array_key_exists('document', $items)) {
                                                    $showmp4 = true;
                                                    $mp4_id = $items['document']; // get post_id of attached video file
                                                    $mp4post = get_post_meta($mp4_id, '_wp_attached_file', false); // get path to attached video file (within uploads directory)
                                                    $mp4File = wp_upload_dir()['baseurl']."/".$mp4post[0]; // get full URL for video file
                                                    $mp4scale = ( $items['scale'] != null ) ? $items['scale'] : 1.0;
                                                    $mp4boxwidth = ( $items['boxwidth'] != null ) ? $items['boxwidth'] : 'random';
                                                }
                                            }

                                            $imageData = wp_get_attachment_image_src( get_post_thumbnail_id($campaign_element->ID), 'medium-thumbnail');
                                            $thumbnailURL   = $imageData[0];
                                            $campaign_element_url = get_permalink($campaign_element->ID);
                                            /*
                                            $video = get_post_meta($campaign_element->ID, 'campaign_element_video', true);

                                            $swf = get_post_meta($campaign_element->ID, 'campaign_element_swf', true);
                                            $check_swf = !empty($swf['document']) ? wp_get_attachment_url((int)$swf['document']) : '';
                                            $swf_link = isset($swf['addit_url']) && !empty($swf['addit_url']) ? $swf['addit_url'] : '#';
                                            */
                                            if(class_exists('MultiPostThumbnails'))
                                            {
                                                $check_main_img = MultiPostThumbnails::has_post_thumbnail($campaign_element->post_type, 'main_image', $campaign_element->ID, 'full');
                                                $main_img_url   = MultiPostThumbnails::get_post_thumbnail_url($campaign_element->post_type, 'main_image', $campaign_element->ID, 'full');
                                            }

                                            /* <?php if (!$key) : ?> major<?php endif; ?> */
                                        ?>
                                        <li class="dynamic appended<?php if ($showmp4) : ?> videoPost<?php endif; ?>">
                                            <div class="holder">
                                                <!-- <a class="overlayLink" href="<?php echo $campaign_element_url; ?>"><span style="text-indent:-9999em;"><?php echo apply_filters('the_title', $campaign_element->post_title); ?></span></a> -->
                                                <figure>
                                                    <?php if ($showmp4 && !$key) : ?>
                                                        <div class="videoFrame">
                                                            <video autoplay preload poster="<?php echo $check_main_img ? $main_img_url : get_placeholder_image_src('full'); ?>" style="width:100%;">
                                                                <source type="video/mp4" src="<?php echo $mp4File; ?>"> 
                                                            </video>
                                                        </div>
                                                        <!--
                                                        <a href="<?php echo $check_main_img ? $main_img_url : get_placeholder_image_src('full'); ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="video" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>" data-vide-source="<?php echo $mp4File; ?>">
                                                            <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
                                                        </a>
                                                        -->
                                                    <?php else : ?>
                                                        <!--
                                                        <a href="<?php echo $check_main_img ? $main_img_url : get_placeholder_image_src('full'); ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="image" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                                            
                                                            <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
                                                        </a>
                                                        -->
                                                        <!-- <a href="<?php echo $campaign_element_url; ?>"> -->
                                                            <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
                                                        <!-- </a> -->
                                                    <?php endif; ?>
                                                </figure>
                                                <!--
                                                <header>
                                                    <h3><?php echo apply_filters('the_title', $campaign_element->post_title); ?></h3>
                                                </header>
                                                -->
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>            
                            </div>
                            <div class="textCol">
                                <!-- <?php echo (has_post_thumbnail($client->ID)) ? get_the_post_thumbnail($client->ID, 'client-logo') : get_placeholder_image('client-logo'); ?> -->
                                <?php echo apply_filters('the_content', $post->post_content); ?>
                            </div>
                        </div>

                </div>
            </div>



            <div class="article-info-container article-info-container-mob">
                <ul class="article-info">
                    <li>
                        <?php get_template_part('part', 'social_buttons'); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php get_template_part('part', 'related_items'); ?>
</div>

<?php get_footer(); ?>