<?php
/*
$values = array(
	"ab_title" => "",
	"ab_banner" => "",
	"ab_banner_id" => "",
	"ab_banner_w" => "",
	"ab_banner_h" => ""	
);
$fieldSettings = array(
	"titleField" => "ab_title",
	"imagePrefix" => "ab_banner"
);
*/
?>

<div class="customField mediumtext">
	<label for="<?php echo $fieldSettings['titleField']; ?>">Title</label>
	<input type="text" id="<?php echo $fieldSettings['titleField']; ?>" name="<?php echo $fieldSettings['titleField']; ?>" value="<?php echo $values[$fieldSettings['titleField']]; ?>">
</div>
 
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Banner image</label>
		<input type="hidden" id="<?php echo $fieldSettings['imagePrefix']; ?>" name="<?php echo $fieldSettings['imagePrefix']; ?>" value="<?php echo $values[$fieldSettings['imagePrefix']]; ?>">
		<input type="hidden" id="<?php echo $fieldSettings['imagePrefix']; ?>_id" name="<?php echo $fieldSettings['imagePrefix']; ?>_id" value="<?php echo $values[$fieldSettings['imagePrefix'].'_id']; ?>" class="ident">
		<input type="hidden" id="<?php echo $fieldSettings['imagePrefix']; ?>_w" name="<?php echo $fieldSettings['imagePrefix']; ?>_w" value="<?php echo $values[$fieldSettings['imagePrefix'].'_w']; ?>" class="width">
		<input type="hidden" id="<?php echo $fieldSettings['imagePrefix']; ?>_h" name="<?php echo $fieldSettings['imagePrefix']; ?>_h" value="<?php echo $values[$fieldSettings['imagePrefix'].'_h']; ?>" class="height">
		<a href="" class="<?php echo $fieldSettings['imagePrefix']; ?>_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values[$fieldSettings['imagePrefix']]; ?>" <?php if ($values[$fieldSettings['imagePrefix']] == '' || $values[$fieldSettings['imagePrefix']] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values[$fieldSettings['imagePrefix']] == '' || $values[$fieldSettings['imagePrefix']] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values[$fieldSettings['imagePrefix']]; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.<?php echo $fieldSettings['imagePrefix']; ?>_Picker')
    }));
});
</script>
