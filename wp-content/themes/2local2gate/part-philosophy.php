<?php
/**
 * Template Name: About Philosophy
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $philosophy_page;


/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

if($philosophy_page) : ?>
<li id="philosophy" class="gray">
    <h2><?php echo apply_filters('the_title', $philosophy_page->post_title); ?></h2>
    <a href="#" class="opener"><?php echo apply_filters('the_title', get_post_meta($philosophy_page->ID, 'menu_label', true)); ?></a>
    <div class="slide">
        <figure class="alignleft border">
            <?php echo (has_post_thumbnail($philosophy_page->ID)) ? get_the_post_thumbnail($philosophy_page->ID, 'about-philosophy') : get_placeholder_image('about-philosophy'); ?>
        </figure>
        <div class="text">
            <h3><?php echo get_post_meta($philosophy_page->ID, 'about_subtitle_subtitle', true); ?></h3>
            <?php echo apply_filters('the_content', $philosophy_page->post_content); ?>
        </div>
    </div>
</li>
<?php endif;