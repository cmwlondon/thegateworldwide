<?php
/**
 * Front page
 *
 * @package WordPress
 * @subpackage thegate_worldwide
 */

global $sites_details;

$sites_details_meta = $sites_details->the_meta();
$regex = '/^(asia|new_york|london|edinburgh)_(.+)/';

$london = get_site_details('london');
$asia = get_site_details('asia');
$edinburgh = get_site_details('edinburgh');
$new_york = get_site_details('new_york');

foreach ($sites_details_meta as $key => $value)
{
    if(preg_match($regex, $key, $match))
    {
        $array_name = $match[1];

        if(!isset($$array_name)) $$array_name = array();

        $$array_name = array_merge($$array_name, array($match[2] => $value));
    }
}

$active_menu_item = rand(1, 4);

the_post();

?>
<?php get_header();?>
<body>
    <!-- wrapper -->
    <div id="wrapper" class="home">
        <div class="wrapper-holder">
            <!-- header -->
            <header id="header">
                <div class="header-area">
                    <div class="header-holder">
                        <h1 class="logo"><a href="#"><?php _e('The Gate'); ?></a></h1>
                        <div class="container">
                            <!-- nav -->
                            <nav id="nav" class="anchor-nav">
                                <ul>
                                    <li <?php echo (1 == $active_menu_item) ? 'class="active"' : ''; ?>>
                                        <a href="<?php echo $asia['url']; ?>" data-scroll-to="asia" class="asia"><?php _e('ASIA'); ?></a>
                                        <strong class="direction">
                                            <span><?php echo isset($asia['coordinates_lattitude_degrees']) ? $asia['coordinates_lattitude_degrees'] : ''; ?> </span>
                                            <span><?php echo isset($asia['coordinates_lattitude_minutes']) ? $asia['coordinates_lattitude_minutes'] : ''; ?> </span>
                                            <?php echo isset($asia['coordinates_lattitude_direction']) ? $asia['coordinates_lattitude_direction'] : ''; ?> /
                                            <span><?php echo isset($asia['coordinates_longitude_degrees']) ? $asia['coordinates_longitude_degrees'] : ''; ?></span>
                                            <span><?php echo isset($asia['coordinates_longitude_minutes']) ? $asia['coordinates_longitude_minutes'] : ''; ?></span>
                                            <?php echo isset($asia['coordinates_longitude_direction']) ? $asia['coordinates_longitude_direction'] : ''; ?>
                                        </strong>
                                    </li>
                                    <li <?php echo (2 == $active_menu_item) ? 'class="active"' : ''; ?>>
                                        <a href="<?php echo $edinburgh['url']; ?>" class="edinburgh"><?php _e('EDINBURGH'); ?></a>
                                        <strong class="direction">
                                            <span><?php echo isset($edinburgh['coordinates_lattitude_degrees']) ? $edinburgh['coordinates_lattitude_degrees'] : ''; ?> </span>
                                            <span><?php echo isset($edinburgh['coordinates_lattitude_minutes']) ? $edinburgh['coordinates_lattitude_minutes'] : ''; ?> </span>
                                            <?php echo isset($edinburgh['coordinates_lattitude_direction']) ? $edinburgh['coordinates_lattitude_direction'] : ''; ?> /
                                            <span><?php echo isset($edinburgh['coordinates_longitude_degrees']) ? $edinburgh['coordinates_longitude_degrees'] : ''; ?></span>
                                            <span><?php echo isset($edinburgh['coordinates_longitude_minutes']) ? $edinburgh['coordinates_longitude_minutes'] : ''; ?></span>
                                            <?php echo isset($edinburgh['coordinates_longitude_direction']) ? $edinburgh['coordinates_longitude_direction'] : ''; ?>
                                        </strong>
                                    </li>
                                    <li <?php echo (3 == $active_menu_item) ? 'class="active"' : ''; ?>>
                                        <a href="<?php echo $london['url']; ?>" class="london"><?php _e('LONDON'); ?></a>
                                        <strong class="direction">
                                            <span><?php echo isset($london['coordinates_lattitude_degrees']) ? $london['coordinates_lattitude_degrees'] : ''; ?> </span>
                                            <span><?php echo isset($london['coordinates_lattitude_minutes']) ? $london['coordinates_lattitude_minutes'] : ''; ?> </span>
                                            <?php echo isset($london['coordinates_lattitude_direction']) ? $london['coordinates_lattitude_direction'] : ''; ?> /
                                            <span><?php echo isset($london['coordinates_longitude_degrees']) ? $london['coordinates_longitude_degrees'] : ''; ?></span>
                                            <span><?php echo isset($london['coordinates_longitude_minutes']) ? $london['coordinates_longitude_minutes'] : ''; ?></span>
                                            <?php echo isset($london['coordinates_longitude_direction']) ? $london['coordinates_longitude_direction'] : ''; ?>
                                        </strong>
                                    </li>
                                    <li <?php echo (4 == $active_menu_item) ? 'class="active"' : ''; ?>>
                                        <a href="<?php echo $new_york['url']; ?>" data-scroll-to="new-york" class="ny"><?php _e('NEW YORK'); ?></a>
                                        <strong class="direction">
                                            <span><?php echo isset($new_york['coordinates_lattitude_degrees']) ? $new_york['coordinates_lattitude_degrees'] : ''; ?> </span>
                                            <span><?php echo isset($new_york['coordinates_lattitude_minutes']) ? $new_york['coordinates_lattitude_minutes'] : ''; ?> </span>
                                            <?php echo isset($new_york['coordinates_lattitude_direction']) ? $new_york['coordinates_lattitude_direction'] : ''; ?> /
                                            <span><?php echo isset($new_york['coordinates_longitude_degrees']) ? $new_york['coordinates_longitude_degrees'] : ''; ?></span>
                                            <span><?php echo isset($new_york['coordinates_longitude_minutes']) ? $new_york['coordinates_longitude_minutes'] : ''; ?></span>
                                            <?php echo isset($new_york['coordinates_longitude_direction']) ? $new_york['coordinates_longitude_direction'] : ''; ?>
                                        </strong>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- visual -->
                <div class="visual">
                    <div class="visual-holder">
                        <img class="asia <?php echo (1 == $active_menu_item) ? 'active' : ''; ?>" src="<?php echo (isset($asia['images']) && ($images = explode(',', $asia['images'])) && ($img_details = wp_get_attachment_image_src( $images[array_rand($images)], 'city-landscape'))) ? current($img_details) : get_placeholder_image_src('city-landscape'); ?>" alt="" width="2000" height="1230" <?php echo (1 == $active_menu_item) ? 'style="opacity: 1;"' : ''; ?>/>
                        <img class="edinburgh <?php echo (2 == $active_menu_item) ? 'active' : ''; ?>" src="<?php echo (isset($edinburgh['images']) && ($images = explode(',', $edinburgh['images'])) && ($img_details = wp_get_attachment_image_src( $images[array_rand($images)], 'city-landscape'))) ? current($img_details) : get_placeholder_image_src('city-landscape'); ?>" alt="" width="2000" height="1230" <?php echo (2 == $active_menu_item) ? 'style="opacity: 1;"' : ''; ?>/>
                        <img class="london <?php echo (3 == $active_menu_item) ? 'active' : ''; ?>" src="<?php echo (isset($london['images']) && ($images = explode(',', $london['images'])) && ($img_details = wp_get_attachment_image_src( $images[array_rand($images)], 'city-landscape'))) ? current($img_details) : get_placeholder_image_src('city-landscape'); ?>" alt="" width="2000" height="1230" <?php echo (3 == $active_menu_item) ? 'style="opacity: 1;"' : ''; ?>/>
                        <img class="ny <?php echo (4 == $active_menu_item) ? 'active' : ''; ?>" src="<?php echo (isset($new_york['images']) && ($images = explode(',', $new_york['images'])) && ($img_details = wp_get_attachment_image_src( $images[array_rand($images)], 'city-landscape'))) ? current($img_details) : get_placeholder_image_src('city-landscape'); ?>" alt="" width="2000" height="1230" <?php echo (4 == $active_menu_item) ? 'style="opacity: 1;"' : ''; ?>/>
                    </div>
                </div>
            </header>
            <!-- main -->
            <div id="main" style="visibility: hidden; height: 0">
                <div class="main-holder">
                    <div id="content">
                        <!-- heading -->
                        <h2 class="heading border small"><?php echo get_the_content(); ?></h2>
                        <!-- block-in -->
                        <section id="asia" class="block-in">
                            <!-- place -->
                            <aside class="place">
                                <a href="<?php echo $asia['url']; ?>" class="link"><span><span><?php _e('ASIA'); ?></span></span></a>
                                <div class="link-area ww-social">
                                    <?php if(isset($asia['contact_url']) && trim($asia['contact_url']) !== '') : ?>
                                        <a href="<?php echo $asia['contact_url']; ?>">
                                        	<div class="ww-social-but ww-e"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($asia['linkedin_url']) && trim($asia['linkedin_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $asia['linkedin_url']; ?>">
                                        	<div class="ww-social-but ww-l"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($asia['twitter_url']) && trim($asia['twitter_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $asia['twitter_url']; ?>">
                                        	<div class="ww-social-but ww-t"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($asia['facebook_url']) && trim($asia['facebook_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $asia['facebook_url']; ?>">
                                        	<div class="ww-social-but ww-f"></div>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </aside>
                            <div class="text">
                                <!-- carousel -->
                                <div class="carousel">
                                    <div class="mask">
                                        <div class="slideset">
                                            <?php foreach ($asia['posts'] as $key => $post) : ?>
                                                <?php if((int)$key % 2 == 0) : ?>
                                                    <div class="slide">
                                                <?php endif; ?>
                                                    <article class="column">
                                                        <div class="holder">
                                                            <figure>
                                                                <a href="<?php echo $post->permalink; ?>">
                                                                    <?php echo $post->thumb; ?>
                                                                </a>
                                                                <strong class="title"><?php echo $post->category; ?></strong>
                                                            </figure>
                                                            <header>
                                                                <h3><?php echo $post->post_title; ?></h3>
                                                                <?php if($post->post_type === 'post') : ?>
                                                                    <h4><?php echo the_gate_format_date($post->post_date); ?></h4>
                                                                <?php endif; ?>
                                                                <span class="client">
                                                                    <?php echo isset($post->client) ? $post->client : ''; ?>
                                                                </span>
                                                            </header>
                                                            <p><?php echo $post->post_excerpt; ?></p>
                                                        </div>
                                                    </article>
                                                <?php if(((int)$key % 2 != 0) || $key + 1 == count($asia['posts'])) : ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <a class="btn-prev" href="#">Previous</a>
                                    <a class="btn-next" href="#">Next</a>
                                </div>
                            </div>
                        </section>
                        <!-- block-in -->
                        <section id="edinburgh" class="block-in">
                            <!-- place -->
                            <aside class="place">
                                <a href="<?php echo $edinburgh['url']; ?>" class="link"><span><span><?php _e('EDINBURGH'); ?></span></span></a>
                                <div class="link-area ww-social">
                                	  <?php if(isset($edinburgh['contact_url']) && trim($edinburgh['contact_url']) !== '') : ?>
                                        <a href="<?php echo $edinburgh['contact_url']; ?>">
                                        	<div class="ww-social-but ww-e"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($edinburgh['linkedin_url']) && trim($edinburgh['linkedin_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $edinburgh['linkedin_url']; ?>">
                                        	<div class="ww-social-but ww-l"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($edinburgh['twitter_url']) && trim($edinburgh['twitter_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $edinburgh['twitter_url']; ?>">
                                        	<div class="ww-social-but ww-t"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($edinburgh['facebook_url']) && trim($edinburgh['facebook_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $edinburgh['facebook_url']; ?>">
                                        	<div class="ww-social-but ww-f"></div>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </aside>
                            <div class="text">
                                <!-- carousel -->
                                <div class="carousel">
                                    <div class="mask">
                                        <div class="slideset">
                                            <?php foreach ($edinburgh['posts'] as $key => $post) : ?>
                                                <?php if((int)$key % 2 == 0) : ?>
                                                    <div class="slide">
                                                <?php endif; ?>
                                                    <article class="column">
                                                        <div class="holder">
                                                            <figure>
                                                                <a href="<?php echo $post->permalink; ?>">
                                                                    <?php echo $post->thumb; ?>
                                                                </a>
                                                                <strong class="title"><?php echo $post->category; ?></strong>
                                                            </figure>
                                                            <header>
                                                                <h3><?php echo $post->post_title; ?></h3>
                                                                <?php if($post->post_type === 'post') : ?>
                                                                    <h4><?php echo the_gate_format_date($post->post_date); ?></h4>
                                                                <?php endif; ?>
                                                                <span class="client">
                                                                    <?php echo isset($post->client) ? $post->client : ''; ?>
                                                                </span>
                                                            </header>
                                                            <p><?php echo $post->post_excerpt; ?></p>
                                                        </div>
                                                    </article>
                                                <?php if(((int)$key % 2 != 0) || $key + 1 == count($edinburgh['posts'])) : ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <a class="btn-prev" href="#">Previous</a>
                                    <a class="btn-next" href="#">Next</a>
                                </div>
                            </div>
                        </section>
                        <!-- block-in -->
                        <section id="london" class="block-in">
                            <!-- place -->
                            <aside class="place">
                                <a href="<?php echo $london['url']; ?>" class="link"><span><span><?php _e('london'); ?></span></span></a>
                                <div class="link-area ww-social">
                                	<?php if(isset($london['contact_url']) && trim($london['contact_url']) !== '') : ?>
                                        <a href="<?php echo $london['contact_url']; ?>">
                                        	<div class="ww-social-but ww-e"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($london['linkedin_url']) && trim($london['linkedin_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $london['linkedin_url']; ?>">
                                        	<div class="ww-social-but ww-l"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($london['twitter_url']) && trim($london['twitter_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $london['twitter_url']; ?>">
                                        	<div class="ww-social-but ww-t"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($london['facebook_url']) && trim($london['facebook_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $london['facebook_url']; ?>">
                                        	<div class="ww-social-but ww-f"></div>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </aside>
                            <div class="text">
                                <!-- carousel -->
                                <div class="carousel">
                                    <div class="mask">
                                        <div class="slideset">
                                            <?php foreach ($london['posts'] as $key => $post) : ?>
                                                <?php if((int)$key % 2 == 0) : ?>
                                                    <div class="slide">
                                                <?php endif; ?>
                                                    <article class="column">
                                                        <div class="holder">
                                                            <figure>
                                                                <a href="<?php echo $post->permalink; ?>">
                                                                    <?php echo $post->thumb; ?>
                                                                </a>
                                                                <strong class="title"><?php echo $post->category; ?></strong>
                                                            </figure>
                                                            <header>
                                                                <h3><?php echo $post->post_title; ?></h3>
                                                                <?php if($post->post_type === 'post') : ?>
                                                                    <h4><?php echo the_gate_format_date($post->post_date); ?></h4>
                                                                <?php endif; ?>
                                                                <span class="client">
                                                                    <?php echo isset($post->client) ? $post->client : ''; ?>
                                                                </span>
                                                            </header>
                                                            <p><?php echo $post->post_excerpt; ?></p>
                                                        </div>
                                                    </article>
                                                <?php if(((int)$key % 2 != 0) || $key + 1 == count($london['posts'])) : ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <a class="btn-prev" href="#">Previous</a>
                                    <a class="btn-next" href="#">Next</a>
                                </div>
                            </div>
                        </section>
                        <!-- block-in -->
                        <section id="new-york" class="block-in">
                            <!-- place -->
                            <aside class="place">
                                <a href="<?php echo $new_york['url']; ?>" class="link"><span><span><?php _e('NEW YORK'); ?></span></span></a>
                                <div class="link-area ww-social">
                                	  <?php if(isset($new_york['contact_url']) && trim($new_york['contact_url']) !== '') : ?>
                                        <a href="<?php echo $new_york['contact_url']; ?>">
                                        	<div class="ww-social-but ww-e"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($new_york['linkedin_url']) && trim($new_york['linkedin_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $new_york['linkedin_url']; ?>">
                                        	<div class="ww-social-but ww-l"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($new_york['twitter_url']) && trim($new_york['twitter_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $new_york['twitter_url']; ?>">
                                        	<div class="ww-social-but ww-t"></div>
                                        </a>
                                    <?php endif; ?>
                                    <?php if(isset($new_york['facebook_url']) && trim($new_york['facebook_url']) !== '') : ?>
                                        <a target="_blank" href="<?php echo $new_york['facebook_url']; ?>">
                                        	<div class="ww-social-but ww-f"></div>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </aside>
                            <div class="text">
                                <!-- carousel -->
                                <div class="carousel">
                                    <div class="mask">
                                        <div class="slideset">
                                            <?php foreach ($new_york['posts'] as $key => $post) : ?>
                                                <?php if((int)$key % 2 == 0) : ?>
                                                    <div class="slide">
                                                <?php endif; ?>
                                                    <article class="column">
                                                        <div class="holder">
                                                            <figure>
                                                                <a href="<?php echo $post->permalink; ?>">
                                                                    <?php echo $post->thumb; ?>
                                                                </a>
                                                                <strong class="title"><?php echo $post->category; ?></strong>
                                                            </figure>
                                                            <header>
                                                                <h3><?php echo $post->post_title; ?></h3>
                                                                <?php if($post->post_type === 'post') : ?>
                                                                    <h4><?php echo the_gate_format_date($post->post_date); ?></h4>
                                                                <?php endif; ?>
                                                                <span class="client">
                                                                    <?php echo isset($post->client) ? $post->client : ''; ?>
                                                                </span>
                                                            </header>
                                                            <p><?php echo $post->post_excerpt; ?></p>
                                                        </div>
                                                    </article>
                                                <?php if(((int)$key % 2 != 0) || $key + 1 == count($new_york['posts'])) : ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <a class="btn-prev" href="#">Previous</a>
                                    <a class="btn-next" href="#">Next</a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
<?php get_footer(); ?>

