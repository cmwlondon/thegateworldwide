<?php while($mb->have_fields_and_multi('field')): ?>
    <?php $mb->the_group_open(); ?>
    <div class="contact_field misc-pub-section">
        <p>
            <?php $mb->the_field('type'); ?>
            <h4><?php _e('Type:'); ?></h4>
            <input type="radio" name="<?php $mb->the_name(); ?>" value="text_input" <?php echo $mb->is_value('text_input')?' checked="checked"':''; ?>/>
            <label><?php _e('Text input'); ?></label><br />
            <input type="radio" name="<?php $mb->the_name(); ?>" value="textarea" <?php echo $mb->is_value('textarea')?' checked="checked"':''; ?>/>
            <label><?php _e('Textarea'); ?></label><br />
        </p>

        <p>
            <?php $mb->the_field('title'); ?>
            <label><strong><?php _e('Title:'); ?></strong></label><br />
            <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
        </p>

        <p>
            <?php $mb->the_field('id'); ?>
            <label><strong><?php _e('Unique ID:'); ?></strong></label><br />
            <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
            <p style="font-size: 11px; color: #AAA">
                <?php _e('Please do not use "name", "hour", "date" or "minute" for ID as they are reserved for WordPress'); ?>
            </p>
        </p>

        <p>
            <h4><?php _e('Validation:'); ?></h4>
            <?php $mb->the_field('validate_against_is_required'); ?>
            <input type="checkbox" name="<?php $mb->the_name(); ?>" value="1" <?php $mb->the_checkbox_state('1'); ?>/>
            <label><?php _e('Is Required'); ?></label><br />
            <?php $mb->the_field('validate_against_is_email'); ?>
            <input type="checkbox" name="<?php $mb->the_name(); ?>" value="1" <?php $mb->the_checkbox_state('1'); ?>/>
            <label><?php _e('Is Email'); ?></label><br />
        </p>

        <p>
            <h4><?php _e('Error Message:'); ?></h4>
            <?php $mb->the_field('error_message'); ?>
            <textarea name="<?php $mb->the_name(); ?>" style="max-width: 100%;"><?php $mb->the_value(); ?></textarea>
        </p>

        <p>
            <a href="#" class="dodelete button"><?php _e('Remove Field'); ?></a>
        </p>
    </div>

    <?php $mb->the_group_close(); ?>
<?php endwhile; ?>
<div class="clear"></div>
<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-field button"><?php _e('Add Field'); ?></a></p>
