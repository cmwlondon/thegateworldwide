<?php
/*
Template Name: contact

@package WordPress
@subpackage stackbartlett
*/
$personLeft = array();
$personRight = array();
$plFields = array('cpl_name', 'cpl_role', 'cpl_phone', 'cpl_email', 'cpl_link', 'cpl_image', 'cpl_image_id', 'cpl_image_w', 'cpl_image_h');
$prFields = array('cpr_name', 'cpr_role', 'cpr_phone', 'cpr_email', 'cpr_link', 'cpr_image', 'cpr_image_id', 'cpr_image_w', 'cpr_image_h');
$metadata = get_post_meta($post->ID);

foreach ($plFields AS $field) {
    $personLeft[$field] = $metadata[$field][0];
}
foreach ($prFields AS $field) {
    $personRight[$field] = $metadata[$field][0];
}
$plPhoto = wp_get_attachment_image_src($personLeft['cpl_image_id'],'full')[0];
$prPhoto = wp_get_attachment_image_src($personRight['cpr_image_id'],'full')[0];

// 'contact_gate_header', 'contact_gate_phone', 'contact_gate_address', 'contact_gate_latitude', 'contact_gate_longitude'
$address = array(
	"header" => $metadata['contact_gate_header'][0],
	"phone" => $metadata['contact_gate_phone'][0],
	"address" => $metadata['contact_gate_address'][0],
	"latitude" => $metadata['contact_gate_latitude'][0],
	"longitude" => $metadata['contact_gate_longitude'][0]
);

$nextPageID = $metadata['contact_nextpage'][0];
$nextPageURL = get_permalink($nextPageID);
$nextPage = get_page_by_postid($nextPageID)[0];

get_header();
?>
	</div>

	<div id="content" class="site-content contactus">

		<div class="double contacts">
			<article class="column left">
				<div class="car"><img alt="<?php echo $personLeft['cpl_name']; ?>" data-width="<?php echo $personLeft['cpl_image_w']; ?>" data-height="<?php echo $personLeft['cpl_image_h']; ?>" src="<?php echo $plPhoto; ?>"></div>
				<div class="tb">
					<h2 itemprop="name"><?php echo $personLeft['cpl_name']; ?></h2>
					<h3 itemprop="role"><?php echo $personLeft['cpl_role']; ?></h3>
					<h4 itemprop="telephone"><?php echo $personLeft['cpl_phone']; ?></h4>
					<a href="mailto:<?php echo $personLeft['cpl_email']; ?>"><?php echo strtoupper($personLeft['cpl_email']); ?></a>
				</div>
			</article>

			<article class="column right">
				<div class="car"><img alt="<?php echo $personRight['cpr_name']; ?>" data-width="<?php echo $personRight['cpr_image_w']; ?>" data-height="<?php echo $personRight['cpr_image_h']; ?>" src="<?php echo $prPhoto; ?>"></div>
				<div class="tb">
					<h2 itemprop="name"><?php echo $personRight['cpr_name']; ?></h2>
					<h3 itemprop="role"><?php echo $personRight['cpr_role']; ?></h3>
					<h4 itemprop="telephone"><?php echo $personRight['cpr_phone']; ?></h4>
					<a href="mailto:<?php echo $personRight['cpr_email']; ?>"><?php echo strtoupper($personRight['cpr_email']); ?></a>
				</div>
			</article>
		</div>

		<div class="single stackaddress">
			<article class="column">
				<div class="hr"><hr></div>
				<h2><?php echo $address['header']; ?></h2>
				<h3 itemprop="telephone"><?php echo $address['phone']; ?></h3>
				<address><?php echo $address['address']; ?></address>
			</article>
		</div>
		
		<div class="single map">
			<article class="column">
				<!-- <div class="car"><img alt="" data-width="1920" data-height="1080" src="<?php echo $theme_root; ?>/img/test/contact/map.jpg"></div> -->
				<div id="mapbox" data-latitude="<?php echo $address['latitude']; ?>" data-longitude="<?php echo $address['longitude']; ?>"></div>
			</article>
		</div>

		<div class="text nextlink">
			<p>Next: <a href="<?php echo $nextPageURL; ?>"><?php echo strtoupper($nextPage->post_title); ?></a></p>
		</div>

<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
