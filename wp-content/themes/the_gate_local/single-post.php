<?php
/**
 * Single Post Page (Individual News Page)
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $related_posts;

the_post();

$related_posts = get_related_posts(3, array('post', 'campaign', 'campaign_element'), true);

$news_listing_page = get_first_page_by_template('full-news.php');
$news_listing_page_permalink = ($news_listing_page) ? get_permalink($news_listing_page->ID) : false;

$featured_news_page = get_first_children_page_by_template('part-news_featured.php', $news_listing_page);

$post_category = get_post_category($post->ID);

$previous_post = get_previous_post(true);
$next_post = get_next_post(true);

$is_london = get_current_blog_id() === 4 ? true : false;
$has_gallery = (
                    MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'second-image', $post->ID) ||
                    MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'third-image', $post->ID) ||
                    MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fourth-image', $post->ID) ||
                    MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fifth-image', $post->ID)
                );

wp_enqueue_script('fancybox_pack');
wp_enqueue_script('share_popup');
get_header();

?>
    <!-- visual -->
    <?php if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'background-image', $post->ID)) : /* If background mage is set show it */?>
        <div class="visual news-backgrd" style="background: url('<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'background-image', $post->ID); ?>') repeat scroll 0 0 transparent;">
    <?php elseif(has_post_thumbnail($featured_news_page->ID)) : /* If not get the featured image of the featured news page */?>
        <div class="visual news-backgrd" style="background: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($featured_news_page->ID));?>') repeat scroll 0 0 transparent;">
    <?php else : /* show the default hardcoded background */?>
        <div class="visual news-backgrd">
    <?php endif; ?>
        <div class="featured-prev-next-wrap">
            <ul class="featured-prev-next">
                <?php if (!empty( $previous_post )): ?>
                    <li class="prev"><a href="<?php echo get_permalink( $previous_post->ID ); ?>"></a></li>
                <?php endif; ?>
                <li class="mobile-carousel-msg"><span><?php _e('News', 'Single News Page'); ?></span></li>
                <?php if (!empty( $next_post )): ?>
                    <li class="next"><a href="<?php echo get_permalink( $next_post->ID ); ?>"></a></li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="main-holder">
            <div class="news-gallery">
                <?php if($has_gallery) : ?>
                    <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo (has_post_thumbnail($post->ID)) ? get_thumbnail_src($post->ID, 'news-large') : get_placeholder_image_src('news-large'); ?>">
                        <?php echo (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'news-large') : get_placeholder_image('news-large'); ?>
                    </a>
                    <ul>
                        <?php if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'second-image', $post->ID)) : ?>
                            <li>
                                <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'second-image', $post->ID,  'news-large'); ?>">
                                    <?php echo MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'second-image', $post->ID,  'news-small'); ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'third-image', $post->ID)) : ?>
                            <li>
                                <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'third-image', $post->ID,  'news-large'); ?>">
                                    <?php echo MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'third-image', $post->ID,  'news-small'); ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fourth-image', $post->ID)) : ?>
                            <li>
                                <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'fourth-image', $post->ID,  'news-large'); ?>">
                                    <?php echo MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'fourth-image', $post->ID,  'news-small'); ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fifth-image', $post->ID)) : ?>
                            <li>
                                <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'fifth-image', $post->ID,  'news-large'); ?>">
                                    <?php echo MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'fifth-image', $post->ID,  'news-small'); ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                <?php else : ?>
                    <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo (has_post_thumbnail($post->ID)) ? get_thumbnail_src($post->ID, 'news-full-width') : get_placeholder_image_src('news-full-width'); ?>">
                        <?php echo (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'news-full-width', array('class' => 'full-width')) : get_placeholder_image('news-full-width', null, array('full-width')); ?>
                    </a>
                <?php endif; ?>
            </div>
            <div class="article-head">
                <ul class="dropdown-menu">
                    <li id="show-category1" class="headlink">
                        <div class="dropdown-title-wrap">
                            <span class="dropdown-label"><span><?php _e('View By'); ?></span><?php _e(' Category'); ?>:</span>
                            <div href="#" class="dropdown-title"><?php echo $post_category; ?></div>
                        </div>
                        <?php $news_categories = get_categories(array('parent' => 0)); ?>
                        <ul class="dropdown-submenu">
                            <?php foreach ($news_categories as $category) : ?>
                                <li><a href="<?php if($news_listing_page_permalink) echo $news_listing_page_permalink . '?' . http_build_query(array('category' => $category->term_id), '', '&amp;'); ?>" data-category="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                </ul>
                <h4><?php echo $post_category; ?></h4>
                <h2><?php the_title(); ?></h2>
                <h3><?php echo the_gate_format_date($post->post_date); ?> <?php _e('by', 'Single News Page'); ?> <?php echo get_post_meta($post->ID, 'fake_author', true); ?></h3>
            </div>
        </div> <!--.main-holder end -->
    </div>
</header>

<!-- main -->
<div id="main">
    <div class="main-holder">
        <!-- content -->
        <div id="content">
            <article class="article-body">
                <?php the_content(); ?>
            </article>
            <aside>
            <ul class="dropdown-menu">
                <li id="show-category2" class="headlink">
                    <div class="dropdown-title-wrap">
                        <span class="dropdown-label"><span><?php _e('View By', 'Single News Page'); ?></span><?php _e(' Category', 'Single News Page'); ?>:</span>
                        <div class="dropdown-title"><?php echo $post_category; ?></div>
                    </div>
                    <ul class="dropdown-submenu">
                        <?php foreach ($news_categories as $category) : ?>
                            <li><a href="<?php if($news_listing_page_permalink) echo $news_listing_page_permalink . '?' . http_build_query(array('category' => $category->term_id), '', '&amp;'); ?>" data-category="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
            <ul class="article-info">
                <li>
                    <?php get_template_part('part', 'social_buttons'); ?>
                </li>
                <?php if(!$is_edinburgh && get_post_meta($post->ID, 'fake_views_check', true)): ?>
                <li class="views-counter">
                	<img class="do-not-open-in-modal" src="<?php bloginfo('template_directory'); ?>/assets/images/news-views.png" width="30">
                   	<span><?php echo do_shortcode('[post_view]'); ?></span>
                </li>
                <?php endif; ?>
                <li>
                    <span><?php _e('POSTED', 'Single News Page'); ?></span>
                    <?php echo the_gate_format_date($post->post_date); ?>
                </li>
                <?php if($is_london) : ?>
                    <?php $get_cat_name = get_the_category(); $cat_name = $get_cat_name[0]->slug; ?>
                    <?php if( $cat_name === 'davetrottblog' ) : ?>
                        <li class="subscribe-button-wrap">
                            <div class="createsend-button" style="height:27px;display:inline-block;" data-listid="r/F5/475/7EA/79D38A7BF5F3C84B"></div>
                            <script type="text/javascript">(function () { var e = document.createElement('script'); e.type = 'text/javascript'; e.async = true; e.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://btn.createsend1.com/js/sb.min.js?v=2'; e.className = 'createsend-script'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(e, s); })();</script>
                        </li>
                    <?php else : ?>
                        <li class="subscribe-button-wrap">
                            <div class="createsend-button" style="height:27px;display:inline-block;" data-listid="r/DD/937/411/AD9065175910703C"></div>
                            <script type="text/javascript">(function () { var e = document.createElement('script'); e.type = 'text/javascript'; e.async = true; e.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://btn.createsend1.com/js/sb.min.js?v=2'; e.className = 'createsend-script'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(e, s); })();</script>
                        </li>
                    <?php endif; ?>
               <?php endif; ?>
            </ul>
            </aside>
        </div>
    </div>
    <?php get_template_part('part', 'related_items'); ?>
    <!-- .connected-items end -->
</div>
<!--.main-holder end -->
<!-- connected-items -->
<?php get_footer(); ?>