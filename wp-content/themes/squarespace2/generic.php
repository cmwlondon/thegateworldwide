<?php
/*
Template Name: generic

@package WordPress
@subpackage stackbartlett
*/
$defaultFooter = false;

$thumbnailID = get_post_thumbnail_id($post->ID);
$imageData = wp_get_attachment_image_src( $thumbnailID, 'full');
$headerImage = $imageData[0];

get_header();
?>
	</div>

	<div id="content" class="site-content generic">
		<div class="fullscreen">
			<div class="headerBackground"></div>
		</div>

		<article class="module">
			<div class="singleColumn">
				<h1><?php echo $post->post_title; ?></h1>
				<?php echo $post->post_content; ?>
			</div>

		</article>

<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
