$(function(){
    $('#show-more').on('click', function(e) {
        e.preventDefault();

        var offset = $('.list li').length;

        if(!$(this).hasClass('hide')) {
            showMore({offset: offset, numberposts: 9, post_type: ['post', 'campaign', 'campaign_element']});
        }
    })
});

function showMore(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'show_more'}),
        beforeSend  : function() {
            /* Disable button */
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
                var listClasses = [];

                /* Append new posts */
                $.each(response.posts, function(key, post) {
                    var listElement = $('.list li').first().clone();
                    var rowClass = 'row-' + post.row;
                    listClasses.push(rowClass);

                    $(listElement).attr('class', rowClass);

                    if(post.category) {
                        if($('figure .title', listElement).length > 0) {
                            $('figure .title', listElement).text(post.category);
                        }
                        else {
                            $('figure', listElement).append($('<strong/>').addClass('title').text(post.category));
                        }
                    }
                    else {
                        $('figure .title', listElement).remove();
                    }

                    post.thumbnail = post.thumbnail.replace('<img','<img onload="this.setAttribute(\'data-loaded\',\'yep\')" data-loaded="nope"');
                    if(post.post_type === 'campaign_element' && post.campaign) {
                        $('figure a', listElement).attr('href', post.campaign).html(post.thumbnail);
                    } else {
                        $('figure a', listElement).attr('href', post.url).html(post.thumbnail);
                    }

                    $('header h3', listElement).html(post.title);

                    if(post.client && post.post_type === 'campaign_element') {
                        $('header span', listElement).text(post.client);
                    }

                    if(post.post_type === 'post') {
                        $('header h4', listElement).html(post.date);
                    }

                    $('.holder', listElement).height('auto');

                    if($('p', listElement).length > 0) {
                        var excerpts = $('p', listElement);
                        excerpts.remove();
                    }
                    $('header', listElement).after($(post.excerpt));

                    $('a.more', listElement).attr('href', post.url);

                    listElement.appendTo($('.list'));
                });

                /* Set equal hight for list items in same row */
                setTimeout(function(){
                    if($('[data-loaded="nope"]').length != 0){
                        setTimeout(arguments.callee, 250);
                        return;
                    }
                    $.setEqualHeightByClassWithImages($.unique(listClasses));
                }, 100)

                /* Enable button if posts are as much as expected */
                if(response.posts.length == data.numberposts)
                $('#show-more').removeClass('hidden');
            }
            else {
                /* Enable button */
                $('#show-more').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            /* Enable button */
            $('#show-more').removeClass('hidden');
        }
    });

}
