<?php
/**
 * @package Wordpress Multisite
 * @subpackage The Gate Local
 */

/**
 * Register post type Campaign element
 *
 */

function thegate_local_campaign_element_init()
{
    $labels = array(
        'name'                => _x( 'Campaign elements', 'post type general name' ),
        'singular_name'       => _x( 'Campaign element', 'post type singular name' ),
        'add_new'             => _x( 'Add New Campaign element', 'campaign_element' ),
        'add_new_item'        => __( 'Add New Campaign element'),
        'edit_item'           => __( 'Edit Campaign element' ),
        'new_item'            => __( 'New Campaign element' ),
        'all_items'           => __( 'All Campaign elements' ),
        'view_item'           => __( 'View Campaign element' ),
        'search_items'        => __( 'Search Campaign element' ),
        'not_found'           => __( 'No Campaign element found' ),
        'not_found_in_trash'  => __( 'No Campaign element found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Campaign elements'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => 'campaign-elements',
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/campaign_element/img/campaign_element.png',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'campaign_element', $args );
}
add_action( 'init', 'thegate_local_campaign_element_init' );

/* Styling for the custom post type icon */
add_action( 'admin_head', 'add_campaign_element_large_icon' );

function add_campaign_element_large_icon()
{
    ?>
    <style type="text/css" media="screen">
        #icon-edit.icon32-posts-campaign_element {background: url(<?php bloginfo('template_url') ?>/post_types/campaign_element/img/campaign_element32x32.png) no-repeat;}
    </style>
<?php
}

/* Add filter to ensure the text office, or office, is displayed when user updates a office */
add_filter( 'post_updated_messages', 'codex_campaign_element_updated_messages' );
function codex_campaign_element_updated_messages( $messages ) {
    global $post, $post_ID;

    $messages['campaign_element'] = array(
        0 => '', /* Unused. Messages start at index 1.*/
        1 => sprintf( __( 'campaign element updated. <a href="%s">View campaign element</a>' ), esc_url( get_permalink( $post_ID ) ) ),
        2 => __( 'Custom field updated.' ),
        3 => __( 'Custom field deleted.' ),
    );
    return $messages;
}

/* Custom Metaboxes */
/*
$campaign_element_video = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_video',
    'title'     => 'Video details',
    'types'     => array( 'campaign_element' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/video_details.php',
    'autosave'  => true,
));
*/
/*
$campaign_element_swf = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_swf',
    'title'     => 'SWF',
    'types'     => array( 'campaign_element' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/swf.php',
    'autosave'  => true,
));
*/

$campaign_element_mp4 = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_mp4',
    'title'     => 'MP4 Video',
    'types'     => array( 'campaign_element' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/mp4.php',
    'autosave'  => true,
));

$campaign_element_feature_title = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_feature_title',
    'title'     => 'Feature Title',
    'types'     => array( 'campaign_element' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/feature_title.php',
    'autosave'  => true,
));

$campaign_element_feature_subtitle = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_feature_subtitle',
    'title'     => 'Feature Subtitle',
    'types'     => array( 'campaign_element' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/feature_subtitle.php',
    'autosave'  => true,
));

$campaign_element_mp4 = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_mp4',
    'title'     => 'MP4 Video',
    'types'     => array( 'campaign_element' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/mp4.php',
    'autosave'  => true,
));

$campaign_element_linkto_campaign = new WPAlchemy_MetaBox( array(
    'id'        => 'campaign_element_linkto_campaign',
    'title'     => 'Campaigns',
    'types'     => array( 'campaign_element' ),
    'context'   => 'side',
    'priority'  => 'default',
    'template'  => get_template_directory() . '/post_types/campaign_element/metaboxes/campaign.php',
    'autosave'  => true,
));

add_filter( 'manage_edit-campaign_element_columns', 'add_new_campaign_element_columns' );
function add_new_campaign_element_columns( $campaign_element_columns )
{
    $new_columns['cb']              = '<input type="checkbox" />';
    $new_columns['id']              = __( 'ID' );
    $new_columns['thumbnail']       = _x( 'Thumbnail', 'column name' );
    $new_columns['title']           = _x( 'Campaign element', 'column name' );
    // $new_columns['video']           = _x( 'Video', 'column name' );
    $new_columns['video']           = _x( 'Video', 'column name' );
    $new_columns['client']          = _x('Client');
    $new_columns['date']            = __( 'Date' );

    return $new_columns;
}

/* Add sortable columns */
add_filter('manage_edit-campaign_element_sortable_columns', 'add_sortable_columns_to_campaign_element');
function add_sortable_columns_to_campaign_element($columns)
{
    $columns['id'] = 'id';

    return $columns;
}

add_action( 'manage_campaign_element_posts_custom_column', 'manage_campaign_element_columns', 10, 2 );

function manage_campaign_element_columns( $column_name, $id )
{
    global $post, $campaign_element_video, $campaign_element_mp4, $campaigns_purchaser;
    $campaigns_purchaser->the_meta();
    // $campaign_element_video->the_meta();
    $campaign_element_mp4->the_meta();

    switch( $column_name )
    {
        case 'id':
            echo $post->ID;
            break;

        case 'thumbnail':
            echo get_the_post_thumbnail( $post->ID, array( 80, 60 ) );
            break;

        case 'client':
                $client_id = $campaigns_purchaser->get_the_value('client');
                echo get_the_title($client_id);
            break;

        case 'video':
            echo $campaign_element_mp4->get_the_value( 'document' ) ? __( 'Yes' ) : __( 'No');
            break;
        /*
        case 'video':
            // echo $campaign_element_video->get_the_value( 'video_embedcode' ) ? __( 'Yes' ) : __( 'No');
            break;
        */
    }
}


/* Add metabox for secondary images */
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Main Image',
            'id' => 'main_image',
            'post_type' => 'campaign_element'
        )
    );
}