<?php
    $personal_information_details = array(
        'first_name' => array('label' => 'First Name'),
        'last_name' => array('label' => 'Last Name'),
        'position' => array('label' => 'Position'),
        'phones' => array('label' => 'Phones', 'multi' => true, 'add_button_text' => 'Add Phone', 'remove_button_text' => 'Remove Phone'),
        'faxes' => array('label' => 'Faxes', 'multi' => true, 'add_button_text' => 'Add Fax', 'remove_button_text' => 'Remove Fax'),
        'emails' => array('label' => 'Emails', 'multi' => true, 'add_button_text' => 'Add Email', 'remove_button_text' => 'Remove Email'),
    );
?>
<div class="personal_information box">
    <table>
        <tbody>
            <?php foreach ($personal_information_details as $personal_information_detail_key => $personal_information_detail_array) : ?>
                    <?php if(isset($personal_information_detail_array['multi']) && $personal_information_detail_array['multi']) : ?>
                        <tr>
                            <th valign="top" align="left">
                                <label><?php _e($personal_information_detail_array['label']); ?></label>
                            </th>
                            <td>
                                <?php while($mb->have_fields_and_multi($personal_information_detail_key)): ?>
                                    <?php $mb->the_group_open($personal_information_detail_key); ?>
                                        <div>
                                            <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" style="width: 300px;"/>
                                            <a href="#" class="dodelete button"><?php isset($personal_information_detail_array['remove_button_text']) ? _e($personal_information_detail_array['remove_button_text']) : _e('Remove'); ?></a>
                                        </div>
                                    <?php $mb->the_group_close($personal_information_detail_key); ?>
                                <?php endwhile; ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <a href="#" class="docopy-<?php echo $personal_information_detail_key; ?> button"><?php isset($personal_information_detail_array['remove_button_text']) ? _e($personal_information_detail_array['add_button_text']) : _e('Add'); ?></a>
                            </td>
                        </tr>
                    <?php else : ?>
                        <tr>
                            <?php $mb->the_field($personal_information_detail_key); ?>
                            <th valign="top" align="left">
                                <label for="<?php echo $mb->id . '_' . $mb->name; ?>"><?php _e($personal_information_detail_array['label']); ?></label>
                            </th>
                            <td>
                                <input id="<?php echo $mb->id . '_' . $mb->name; ?>" type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" style="width: 300px;"/>
                            </td>
                        </tr>
                    <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>