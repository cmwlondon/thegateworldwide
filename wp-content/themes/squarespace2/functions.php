<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<?php
function my_enqueue($hook) {

    // wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/library/modernizr-2.6.2.min.js', array(), '1.0.0', true  );
    // jQuery and jQuery UI aleady included
    wp_enqueue_script('jquery-ui-draggable','jquery-ui-droppable','jquery-ui-sortable');
    wp_enqueue_script('admin', get_template_directory_uri() . '/js/admin.js', array(), '1.0.0', true );

    wp_localize_script( 'admin', 'tgl_ajax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );

    wp_enqueue_style('adminstyles', get_template_directory_uri() . '/admin.css');
}
add_action( 'admin_enqueue_scripts', 'my_enqueue' );

/* WPALchemy metaboxes */

/* basic setup */
include_once 'metaboxes/setup.php';

/* custom post types */
include('custom_posts/work/work.php');
include('custom_posts/people/people.php');

include('custom_posts/page/page.php');
include('custom_posts/post/post.php');

include('functions/page.php');

/* functions related to custom posts */
include('functions/work.php');
include('functions/people.php');

if ( ! function_exists( 'squarespace_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function squarespace_setup() {
	// header and footer navigation menus
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'squarespace' ),
		'footer'  => __( 'Footer Links Menu', 'squarespace' )
	) );

	// 'featured image' post image
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1920, 1080, true );

}
endif; // squarespace_setup
add_action( 'after_setup_theme', 'squarespace_setup' );

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function squarespace_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'squarespace_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function squarespace_scripts() {
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/library/modernizr-2.6.2.min.js', array(), '1.0.0', true  );
	wp_enqueue_script( 'ieflexboxfix', get_template_directory_uri() . '/js/library/flexibility.js', array(), '1.0.0', true  );
	// wp_enqueue_script( 'jquery_validate', get_template_directory_uri() . '/js/library/jquery.validate.min.js', array(), '1.0.0', true  );
	// wp_enqueue_script( 'jquery_mobile_custom', get_template_directory_uri() . '/js/library/jquery.mobile.custom.min.js', array(), '1.0.0', true  );


	// Load our main stylesheet.
	wp_enqueue_style( 'squarespace-style', get_stylesheet_uri() );
	wp_enqueue_style( 'squarespace-svg1', get_template_directory_uri().'/svg/social-accounts.svg' );
	wp_enqueue_style( 'squarespace-svg2', get_template_directory_uri().'/svg/ui-icons.svg' );

	// load custom fonts
	wp_enqueue_script( 'google-webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js', array( 'jquery' ), '20150330', true );
	wp_enqueue_script( 'font-loader', get_template_directory_uri() . '/js/font_loader.js', array( 'jquery' ), '20150330', true );

	wp_enqueue_script( 'squarespace-objects', get_template_directory_uri() . '/js/objects.js', array( 'jquery' ), '20150330', true );
	wp_enqueue_script( 'squarespace-map', get_template_directory_uri() . '/js/map.js', array( 'jquery' ), '20150330', true );
	wp_enqueue_script( 'squarespace-vimeo', 'https://player.vimeo.com/api/player.js', array( 'jquery' ), '20150330', true );

	wp_enqueue_script( 'squarespace-system', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );

	$thisTemplateFile = basename( get_page_template() );
	switch ( $thisTemplateFile ) {
		case "home.php" : {
			// $eventlist = array('font', 'video', 'image');
			$eventlist = array('font', 'image');
		} break;
		case "about.php" : {
			$eventlist = array('font', 'image');
		} break;
		case "contact.php" : {
			$eventlist = array('font', 'map', 'image');
		} break;
		case "culture.php" : {
			$eventlist = array('font', 'image');
		} break;
		case "news.php" : {
			$eventlist = array('font');
		} break;
		case "ourwork.php" : {
			$eventlist = array('font', 'image');
		} break;
		case "people.php" : {
			$eventlist = array('font',  'image');
		} break;

		case "page.php" : {
			$eventlist = array('font', 'image');
		} break;

		default : {
			$eventlist = array('font', 'image');
		} break;
	}
	wp_localize_script( 'squarespace-system', 'squarespace2_events', array( "events" => $eventlist, "template" => $thisTemplateFile ));
	wp_localize_script( 'squarespace-system', 'squarespace2_template', array( "location" => get_template_directory_uri() ));

	/*
	wp_enqueue_script( 'squarespace2_ajax_request', get_template_directory_uri() . '/js/ajax.js', array( 'jquery' ) );
	wp_localize_script( 'squarespace2_ajax_request', 'squarespace2_ajax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ), 'current_blog' => $current_blog->blog_id ) );
	*/


}
add_action( 'wp_enqueue_scripts', 'squarespace_scripts' );

// add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}

// ensure all tags are included in queries
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');


function get_item_ids($items){
    $ids = array();
    foreach($items as $item) {
        $ids[] = $item->ID;
    }
    return $ids;
}

function buildTaglist($postid) {
	$posttags = get_the_tags($postid);

	$taglist = [];
	if ($posttags) {
	    foreach($posttags AS $tag) {
	        $taglist[] = $tag->name;
	    }
	}

	return array(
		"array" => $taglist,
		"string" => implode( ', ', $taglist)
	);
}

function do_query($query_parameters) {
	$loop = new WP_Query( $query_parameters );
	$posts = $loop->get_posts();
	wp_reset_query();

	return $posts;
}

function synchronise($order, $all, $postid, $metakey) {
	$allIDS = get_item_ids($all);

	$newOrderItems = array();
	$removedOrderItems = array();

	if ($order != '') {
		// remove items from order list which have been removed from all items
		foreach ( $order AS $key => $id) {
	        if (!in_array($id, $allIDS)) {
	        	$removedOrderItems[] = array (
	        		'key' => $key,
	        		'id' => $id
	        	);
	        }
		}

		$removeReversed = array_reverse($removedOrderItems);
		foreach ( $removeReversed AS $removeItem) {
			array_splice($order, $removeItem['key'], 1);
		}

		// save updated order list
	    update_post_meta($post_id, $metakey, implode(',',$order) );

	    // find products which haven't been assigned to a product page
	    // array_diff() gets values which are in only one of the two arrays, but preserves the keys from the $order array
	    // array_values() resets the item keys so that they run from 0
		$unassigned = array_values(array_diff($allIDS, $order));

		return array(
			'assigned' => $order,
			'unassigned' => $unassigned
		);
	} else {
		return array(
			'assigned' => array(),
			'unassigned' => $allIDS
		);
	}
}

function generic_ajax($response){
	header( "Content-Type: application/json" );
	echo json_encode($response);	

	wp_die();
}

/* AJAX back-end functions definitions */
// http://solislab.com/blog/5-tips-for-using-ajax-in-wordpress/
add_action( 'wp_ajax_nopriv_get_pages', 'get_pagelist' );
add_action( 'wp_ajax_get_pages', 'get_pagelist' );

// ajax action : 'get_pages'

function get_pagelist(){

	generic_ajax(array('time' => time()));
}

add_action( 'wp_ajax_nopriv_get_people', 'get_people_ajax' );
add_action( 'wp_ajax_get_people', 'get_people_ajax' );

// ajax action : 'get_people'

function get_people_ajax(){
	$people = get_all_people();
	$peopleprime = array();

	foreach($people As $person) {
		$person_id = $person->ID;

		$metadata = $metadata = get_post_meta($person_id);

		$peopleprime[] = array(
			'forename' => $metadata['p_forename'][0],
			'surname' => $metadata['p_surname'][0],
			'role' => $metadata['p_role'][0],
			'section' => $metadata['p_section'][0],
			'email' => $metadata['p_email'][0],
			'linkedin' => $metadata['p_linkedin'][0],
			'phone' => $metadata['p_phone'][0],
			'mobile' => $metadata['p_mobile'][0],
			'portrait' => $metadata['p_portrait'][0]
		);
	}
					
	generic_ajax( $peopleprime );
}



/* ajax call function template START */

	add_action( 'wp_ajax_nopriv_template', 'template_ajax' ); // front end (no privilege) access
	add_action( 'wp_ajax_template', 'template_ajax' ); // wp admin only access

	// ajax action : 'template'

	function template_ajax(){
		// insert your data into $data for JSON response 
		// $_POST[] or $_REQUEST[]
		$data = array(
			"action" => "template",
			"response" => "OK"
		);

		generic_ajax( $data );
	}

/* ajax call function template END */

?>