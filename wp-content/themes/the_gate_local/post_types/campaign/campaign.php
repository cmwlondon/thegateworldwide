<?php
/**
 * Campaign
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Creation of the custom posttype */
add_action( 'init', 'create_campaign_post_type');
function create_campaign_post_type()
{
    $labels = array(
        'name' => __('Campaigns'),
        'singular_name' => __('Campaign'),
        'add_new' => __('Add New Campaign'),
        'add_new_item' => __('Add New Campaign'),
        'edit_item' => __('Edit Campaign'),
        'new_item' => __('New Campaign'),
        'all_items' => __('All Campaigns'),
        'view_item' => __('View Campaign'),
        'search_items' => __('Search Campaigns'),
        'not_found' =>  __('No campaigns found'),
        'not_found_in_trash' => __('No campaigns found in Trash'),
        'parent_item_colon' => __(''),
        'menu_name' => __('Campaigns')
    );

    register_post_type( 'campaign',
            array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'show_in_nav_menus' => true,
            'has_archive' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 27,
            'menu_icon' => get_bloginfo('template_directory') . '/post_types/campaign/img/campaign_small.png',
            'supports' => array('title', 'excerpt', 'editor', 'thumbnail'),
            'hierarchical' => false,
            'rewrite' => array('slug' => 'campaigns'),
            'taxonomies' => array('post_tag'),
        )
    );
 }

/* hook into the init action to add taxonomy when it fires */
add_action( 'init', 'create_campaign_type_taxonomy' );

function create_campaign_type_taxonomy()
{
    /* Add new taxonomy, make it hierarchical (like categories) */
    $labels = array(
        'name'          => __( 'Media Types' ),
        'singular_name' => __( 'Media Type' ),
        'search_items'  => __( 'Search Media Types' ),
        'all_items'     => __( 'All Media Types' ),
        'edit_item'     => __( 'Edit Media Type' ),
        'update_item'   => __( 'Update Media Type' ),
        'add_new_item'  => __( 'Add Media Type' ),
        'new_item_name' => __( 'New Media Type' ),
        'menu_name'     => __( 'Media Types' ),
    );

    register_taxonomy('media_type', array('campaign', 'campaign_element'), array(
        'hierarchical'  => true,
        'public'        => false,
        'show_tagcloud' => false,
        'labels'        => $labels,
        'show_ui'       => true,
        'query_var'     => true,
        'rewrite'       => array( 'slug' => 'media-type' ),
    ));
}

/* Styling for the custom post type icon */
add_action( 'admin_head', 'add_campaign_large_icon' );

function add_campaign_large_icon()
{
?>
    <style type="text/css" media="screen">
        #icon-edit.icon32-posts-campaign {background: url(<?php bloginfo('template_url') ?>/post_types/campaign/img/campaign_large.png) no-repeat;}
    </style>
<?php
}

/* add filter to ensure the text thas Pizza is updated, is displayed when user updates a document */
add_filter( 'post_updated_messages', 'set_campaign_updated_messages' );

function set_campaign_updated_messages( $messages )
{
  global $post, $post_ID;

  $messages['campaign'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Campaign updated. <a href="%s">View Campaign</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Campaign updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Campaign restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Campaign published. <a href="%s">View Campaign</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Campaign saved.'),
    8 => sprintf( __('Campaign submitted. <a target="_blank" href="%s">Preview Campaign</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Campaign scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Campaign</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Campaign draft updated. <a target="_blank" href="%s">Preview Campaign</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}


/* Manage Post Columns */
add_filter('manage_edit-campaign_columns', 'add_new_campaign_columns');

function add_new_campaign_columns($columns)
{
    $new_columns['cb'] = $columns['cb'];
    $new_columns['id'] = __('ID');
    $new_columns['title'] = $columns['title'];
    $new_columns['thumb'] = __('Image');
    $new_columns['featured'] = __('Featured');
    $new_columns['client'] = __('Client');
    $new_columns['media'] = __('Media');
    $new_columns['date'] = $columns['date'];

    return $new_columns;
}

add_action('manage_campaign_posts_custom_column', 'manage_campaign_columns');
function manage_campaign_columns($column)
{
    global $post, $campaigns_subtitle, $campaigns_purchaser;

    switch ($column)
    {
        case 'id':

            echo $post->ID;
            break;

        case 'thumb':

            echo get_the_post_thumbnail($post->ID, array(80, 60));
            break;

        case 'media':

            $media_type_objects = get_the_terms($post->ID, 'media_type');
            $media_types = array();
            if(is_array($media_type_objects))
            {
                foreach ($media_type_objects as $obj) {
                    $media[] = $obj->name;
                }

                echo implode(', ', $media);
            }

            break;
            echo 'media';
            break;

        case 'client':

            $meta = $campaigns_purchaser->the_meta();
            $client = isset($meta[$column]) ? get_post($meta[$column]) : false;
            echo ($client && $client->post_type == "client") ?  '<a href="' . get_edit_post_link( $client->ID ) . '" target="_blank">' . $client->post_title . '</a>' : __('No client set');

            break;

        case 'featured':
            $tags = wp_get_post_tags($post->ID, array('fields' => 'names'));

            $filtered_tags = array();

            foreach ($tags as $tag)
            {
                $filtered_tags[] = apply_filters('the_category', $tag);
            }
            echo (in_array('featured', $filtered_tags)) ? __('Yes') : __('No');
            break;
    }
}

/* Add sortable columns */
add_filter('manage_edit-campaign_sortable_columns', 'add_sortable_columns_to_campaign');
function add_sortable_columns_to_campaign($columns)
{
    $columns['client'] = 'client';
    $columns['id'] = 'id';

    return $columns;
}

/* Set the needed query var if some of the custom sortable columns is sorted by */
add_filter( 'request', 'campaign_columns_orderby' );
function campaign_columns_orderby( $vars )
{
    $custom_meta_sortable_columns = array('client');

    if ( isset( $vars['orderby'] ) && in_array($vars['orderby'], $custom_meta_sortable_columns))
    {
        $vars = array_merge( $vars, array(
            'meta_key' => $vars['orderby'],
            'orderby' => 'meta_value'
        ));
    }

    return $vars;
}


/* Add contextual help for the campaigns */
add_action( 'contextual_help', 'campaign_contextual_help', 10, 3 );
function campaign_contextual_help( $contextual_help, $screen_id, $screen )
{
    if ( 'edit-campaign' == $screen->id )
    {
         $contextual_help = '<h2>Campaigns</h2>
         <p>You can see the list of campaigns on this page in reverse chronological order - the latest added appear first. Of course you are able to sort the list by any sortable column.</p>
         <p>You can filter the list by client, by featured or by date.</p>
         <p>You can view/edit the details of every campaign by clicking on its name, or you can perform bulk actions using the dropdown menu and selecting multiple items.</p>';
    }
    elseif ( 'campaign' == $screen->id )
    {
         $contextual_help = '<h2>Editing Campaign</h2>
         <p>This page allows you to view/modify a campaign details. </p>';
    }
    return $contextual_help;
}

add_action( 'restrict_manage_posts', 'manage_campaign_list_filters' );
function manage_campaign_list_filters()
{
    global $typenow, $wp_query;

    if ($typenow == 'campaign') {

        $selected = (isset($wp_query->query['tag']) && $wp_query->query['tag'] == 'featured') ? 'selected="selected"' : '';

        echo '<select name="tag" id="tag" class="postform">';
        echo '<option value="">Show All</option>';
        echo '<option value="featured" ' . $selected . '>Only Featured</option>';
        echo '</select>';


        $selected_term = false;
        if(isset( $wp_query->query['media_type'] ))
        {
            $selected_term = get_term_by( 'slug', $wp_query->query['media_type'], 'media_type' );
        }
        $selected_term = ($selected_term) ? $selected_term->term_id : '';

        wp_dropdown_categories( array(
            'show_option_all'   => 'Show All Media',
            'taxonomy'          => 'media_type',
            'name'              => 'media_type',
            'orderby'           => 'name',
            'selected'          => $selected_term,
            'hierarchical'      => false,
            'depth'             => 3,
            'show_count'        => false,
            'hide_empty'        => true,
        ) );


        $clients = get_posts(array(
            'post_type' => 'client'
        ));


        echo '<select name="campaign_client" id="client" class="postform">';
        echo '<option value="">All Clients</option>';
        foreach ($clients as $client)
        {
            $selected = (isset($wp_query->query['meta_key']) && $wp_query->query['meta_key'] == 'campaign_client' && $wp_query->query['meta_value'] == $client->ID) ? 'selected="selected"' : '';
            echo '<option value="' . $client->ID . '" ' . $selected . '>' . $client->post_title . '</option>';
        }
        echo '</select>';

    }
}

/* Make the functionalty to filter by office */
add_filter( 'request', 'filter_by_client' );
function filter_by_client( $vars )
{
    if( isset( $vars['media_type'] ) && $vars['media_type'] && is_numeric( $vars['media_type'] ) )
    {
        $term = get_term_by( 'id', $vars['media_type'], 'media_type' );
        $vars['media_type'] = $term->slug;
    }

    if ( isset( $vars['campaign_client'] ) && $vars['campaign_client'] ) {
        $vars = array_merge( $vars, array(
            'meta_key' => 'campaign_client',
            'meta_value' => $vars['campaign_client']
        ) );
    }
    return $vars;
}

add_filter( 'query_vars', 'add_client_to_campaign_query_vars' );
function add_client_to_campaign_query_vars($query_vars)
{
    $query_vars[] = 'campaign_client';

    return $query_vars;
}


/* Custom metaboxes */


/* Register Campaigns Metabox 'Subtitle' */
$campaigns_subtitle = new WPAlchemy_MetaBox(array
(
    'id'        => 'subtitle',
    'title'     => __('Subtitle'),
    'types'     => array('campaign'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'high',
    'template'  => get_template_directory() . '/post_types/campaign/metaboxes/subtitle.php',
    'autosave'  => true,
    'lock'      => WPALCHEMY_LOCK_AFTER_POST_TITLE,
    'prefix'    => 'campaign_',
));

/* Register Campaigns Metabox 'Client' */
$campaigns_purchaser = new WPAlchemy_MetaBox(array
(
    'id'        => 'purchaser',
    'title'     => __('Client'),
    'types'     => array('campaign', 'campaign_element'),
    'context'   => 'side',
    'priority'  => 'low',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'template'  => get_template_directory() . '/post_types/campaign/metaboxes/purchaser.php',
    'autosave'  => true,
    'prefix'    => 'campaign_',
));

/* Register Campaigns Metabox 'Campaign Elements' */
$campaigns_campaign_elements = new WPAlchemy_MetaBox(array
(
    'id'        => 'campaign_elements',
    'title'     => __('Campaign Elements'),
    'types'     => array('campaign'),
    'context'   => 'normal',
    'priority'  => 'low',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'template'  => get_template_directory() . '/post_types/campaign/metaboxes/campaign_elements.php',
    'autosave'  => true,
    'prefix'    => 'campaign_',
    'save_filter' => 'clear_duplicate_elements',
));

/* Register Page Metabox 'Related Items' */
$related_posts = new WPAlchemy_MetaBox(array
(
    'id'        => 'related_posts',
    'title'     => __('Related Items'),
    'types'     => array('post', 'campaign', 'campaign_element'),
    'context'   => 'side',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'low',
    'template'  => get_template_directory() . '/post_types/campaign/metaboxes/related_posts.php',
    'autosave'  => true,
    'prefix'    => 'related_posts_',
));


function clear_duplicate_elements($meta, $post_id)
{
    if(isset($meta['portfolio_pieces']) && is_array($meta['portfolio_pieces']))
    {
        $meta['portfolio_pieces'] = array_map('unserialize', array_unique(array_map('serialize', $meta['portfolio_pieces'])));
    }

    return $meta;
}
