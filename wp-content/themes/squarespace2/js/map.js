var map,
  icon,
  icon2,
  iconSize = 0.1,
  marker,
	mapStyle = [],
	mapStart = { // centred on stack office 90TCR
		"lat" : 51.5220083, 
		"long" : -0.1359654
	};

var mapStyling = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
];


(function () {
	"use strict";
})();

/*
need to wait till google maps library has loaded
*/
/* -------------------------- */
function initMap() {
  // 'contact' styled google map instance
    if (jQuery('#mapbox').length !== 0) {
      var mapCenterLocation,
        mapOptions = {
          // center: mapCenterLocation,
          zoom: 16,
          disableDefaultUI: true,
          mapTypeId: "MSQ",
          mapTypeControlOptions: {mapTypeIds: ["MSQ"]}
      };

      mapStart.lat = jQuery('#mapbox').attr('data-latitude');
      mapStart.long = jQuery('#mapbox').attr('data-longitude');

      mapCenterLocation = new google.maps.LatLng( mapStart.lat, mapStart.long);
      mapOptions.center = mapCenterLocation;

      map = new google.maps.Map(document.getElementById("mapbox"), mapOptions);

      var styledMapType1 = new google.maps.StyledMapType(mapStyling, {"name":"MSQ"});
      map.mapTypes.set("MSQ", styledMapType1);

      icon = {
        "url" : squarespace2_template.location + "/svg/solid.svg",
        "anchor" : new google.maps.Point(40,60),
        "scaledSize" : new google.maps.Size(79.2,79.2)
      };

      marker = new google.maps.Marker({
          "position" : mapCenterLocation,
          "map" : map,
          "draggable" : false,
          "icon" : icon,
          "zIndex" : -20
      });

      map.addListener('tilesloaded', function() {
        // jQuery('#content').removeClass('maploading');
        thisPageEventHandler.signal("map");
      });

      jQuery(window).resize(function(){
          google.maps.event.trigger(map, "resize");
          var mapCenterLocation = new google.maps.LatLng( mapStart.lat, mapStart.long);
          map.setCenter( mapCenterLocation ); 
          marker.setPosition(mapCenterLocation);
      });
    }
}
/* -------------------------- */
