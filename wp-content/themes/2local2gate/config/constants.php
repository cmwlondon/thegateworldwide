<?php 
/**
 * Theme specific constants
 * 
 * @package WordPress
 * @subpackage thegate_local
 */
 
/* Theme */
define('THEME_FULL_PATH', get_stylesheet_directory() );
define('THEME_ENVIRONMENT', 'development' ); // development|production

/* Use this for parent themes */
define('ASSETS_URL', get_bloginfo( 'template_url') . '/assets');
/* Use this for child themes */
/* define('ASSETS_URL', get_stylesheet_directory() . '/assets'); */

/* Assets */
define('ASSETS_JAVASCRIPTS_IN_FOOTER', true); 

?>