<?php
?>
<div class="customField mediumtext">
	<label for="hb_title">Banner Title</label>
	<input type="text" id="hb_title" name="hb_title" value="<?php echo $values['hb_title']; ?>">
</div>
 
<div class="customField mediumtext">
	<label for="hb_link">Banner Link</label>
	<input type="text" id="hb_link" name="hb_link" value="<?php echo $values['hb_link']; ?>">
</div>

<div class="customField mediumtext">
	<label for="hb_vimeo">Banner Vimeo url</label>
	<input type="text" id="hb_vimeo" name="hb_vimeo" value="<?php echo $values['hb_vimeo']; ?>">
</div>

<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Banner fallback image</label>
		<input type="hidden" id="hb_image" name="hb_image" value="<?php echo $values['hb_image']; ?>">
		<input type="hidden" id="hb_image_id" name="hb_image_id" value="<?php echo $values['hb_image_id']; ?>" class="ident">
		<input type="hidden" id="hb_image_w" name="hb_image_w" value="<?php echo $values['hb_image_w']; ?>" class="width">
		<input type="hidden" id="hb_image_h" name="hb_image_h" value="<?php echo $values['hb_image_h']; ?>" class="height">
		<a href="" class="hb_image_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['hb_image']; ?>" <?php if ($values['hb_image'] == '' || $values['hb_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['hb_image'] == '' || $values['hb_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['hb_image']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.hb_image_Picker')
    }));
});
</script>

