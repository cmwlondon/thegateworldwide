<div class="hide-if-no-js">
    <div class="swf-details postbox">
        <div class="handlediv" title="Click to toggle"></div>
        <h3><?php _e('Attached File Details'); ?></h3>
        <div class="inside">
            <table>
                <tr>
                    <th><?php _e('File'); ?></th>
                    <td>
                        <div id="document-preview" class="document-preview-container">
                            <?php $metabox->the_field('document'); ?>
                            <?php $attachment_url = wp_get_attachment_url((int)$metabox->get_the_value()); ?>
                            <?php $attachment_name = basename(get_attached_file((int)$metabox->get_the_value())); ?>

                            <input type="hidden" class="document-id" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>"/>
                            <a class="document-preview" href="<?php echo $attachment_url; ?>" target="_blank"><?php echo $attachment_name; ?></a>
                            <a class="set-document <?php echo ($attachment_url) ? 'hidden' : ''; ?>" id="set-document" href="#" data-uploader-title="Set File" data-uploader-button-text="Select File" data-attachment-id="<?php echo $metabox->get_the_value(); ?>"><?php _e('Set File'); ?></a>
                            <a class="remove-document <?php echo (!$attachment_url) ? 'hidden' : ''; ?>" id="remove-document" href="#"><?php _e('Remove File'); ?></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th><?php _e('Link'); ?></th>
                    <td>
                        <div id="addit_url" >
                            <?php $metabox->the_field('addit_url'); ?>
                            <input type="text" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>"/>
                        </div>
                    </td>
                </tr>

            </table>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(function() {
            var documentPreviewContainer = jQuery('#document-preview');

            jQuery('.remove-document', documentPreviewContainer).on('click', function(e) {
                e.preventDefault();
                jQuery(this).siblings('input').val('');
                jQuery(this).siblings('.document-preview').attr('href', '#').data('thumbnail-id', '').addClass('hidden');
                jQuery(this).siblings('.set-document').removeClass('hidden');
                jQuery(this).addClass('hidden');
            })

            new AthMediaModal({
                calling_selector: jQuery('.set-document', documentPreviewContainer),
                cb: function(attachment) {
                    jQuery('.document-id', documentPreviewContainer).val(attachment.id);
                    this.calling_selector.data('attachment-id', attachment.id).addClass('hidden');
                    jQuery('.document-preview', documentPreviewContainer).attr('href', attachment.url).html(attachment.name).removeClass('hidden');
                    jQuery('.remove-document', documentPreviewContainer).removeClass('hidden');
                },
                type: ''
            })
        })
    </script>
</div>