<?php
/**
 * Template Name: About
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $capabilities_page,
       $experience_page,
       $philosophy_page,
       $values_page;

the_post();

/* Get first children that has template "Capabilities" */
$capabilities_page = get_first_children_page_by_template('part-capabilities.php');
/* Get first children that has template "Experience" */
$experience_page = get_first_children_page_by_template('part-experience.php');
/* Get first children that has template "Philosophy" */
$philosophy_page = get_first_children_page_by_template('part-philosophy.php');
/* Get first child that has template "Values" */
$values_page = get_first_children_page_by_template('part-values.php');

get_header();

?>
    <!-- visual -->
    <div id="company" class="visual">
        <div class="visual-holder">
            <?php echo (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'homepage-thumbnail') : get_placeholder_image('homepage-thumbnail'); ?>
        </div>
        <?php $content = get_the_content(); $check_iframe = strpos($content, 'iframe') == true ? 'always-visible' : ''; ?>
        <div class="text <?php echo $check_iframe;?>">
            <h2><?php echo get_post_meta($post->ID, 'about_subtitle_subtitle', true); ?></h2>
            <?php the_content(); ?>
        </div>
    </div>
    <!-- menu -->
    <ul class="menu">
        <li class="active"><a href="#company"><?php echo get_post_meta($post->ID, 'menu_label', true); ?></a></li>
        <?php if($philosophy_page) : ?>
            <li><a href="#philosophy"><?php echo get_post_meta($philosophy_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($values_page) : ?>
            <li><a href="#values"><?php echo get_post_meta($values_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($capabilities_page) : ?>
            <li><a href="#capabilities"><?php echo get_post_meta($capabilities_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($experience_page) : ?>
            <li><a href="#experience"><?php echo get_post_meta($experience_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
    </ul>
</header>
<!-- main -->
<div id="main" style="display: none">
    <div class="main-holder">
        <!-- content -->
        <div id="content">
            <!-- simple-list -->
            <ul class="simple-list">
                <li id="about">
                    <div id="about-mob">
                        <a href="#" class="opener"><?php echo get_post_meta($post->ID, 'menu_label', true); ?></a>
                        <div class="slide">
                            <div class="text">
                                <h3><?php echo get_post_meta($post->ID, 'about_subtitle_subtitle', true); ?></h3>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </li>
                <?php get_template_part('part', 'philosophy'); ?>
                <?php get_template_part('part', 'values'); ?>
                <?php get_template_part('part', 'capabilities'); ?>
                <?php get_template_part('part', 'experience'); ?>
            </ul>
        </div>
    </div>
</div>
<?php

get_footer();