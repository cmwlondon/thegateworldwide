
$(function(){
    
    $('#sort-client li a').on('click', function(e) {
        e.preventDefault();

        var client = $(this).data('client');

        showFilteredCampaignsByClient({numberposts: -1, client: client});
    })
});

function showFilteredCampaignsByClient(data) {

    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'filter_campaigns_by_client'}),
        beforeSend  : function() {},
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {

                clientContainer.find('.dynamic').addClass('appended');

                var t1 = window.setTimeout(function (){
                    clientContainer.find('.dynamic').remove();
                    $.each(response.posts, function(key, post) {
                        var listElement = clientContainer.find('.itemTemplate').clone();

                        listElement
                        .removeClass('itemTemplate')
                        .addClass('appended')
                        .addClass('dynamic');

                        // post.date
                        // post.category
                        listElement.find ('a.overlayLink').attr({
                            "href" : post.url
                        });
                        listElement.find ('figure').append(post.thumbnail);
                        listElement.find ('header h3').html(post.title);
                        listElement.find ('header p').html(post.excerpt);

                        clientContainer.append(listElement);
                    });
                    
                    if(!response.hide_show_more)
                    $('#show-more').removeClass('hidden');
                    clientContainer.masonry( 'reloadItems' );
                    clientContainer.masonry( 'layout' );

                }, itemFadeDuration);

            }
            else {
            }

        },
        error       : function(xhrObj, status, error) {
        }
    });

}
