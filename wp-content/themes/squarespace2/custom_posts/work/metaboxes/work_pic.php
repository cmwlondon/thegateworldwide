<div class="customField mediumtext">
	<label for="<?php echo $settings['prefix']; ?>_alt">image alt</label>
	<input type="text" id="<?php echo $settings['prefix']; ?>_alt" name="<?php echo $settings['prefix']; ?>_alt" value="<?php echo $values[$settings['prefix'].'_alt']; ?>">
</div>
 
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Banner image</label>
		<input type="hidden" id="<?php echo $settings['prefix']; ?>_image" name="<?php echo $settings['prefix']; ?>_image" value="<?php echo $values[$settings['prefix'].'_image']; ?>">
		<input type="hidden" id="<?php echo $settings['prefix']; ?>_id" name="<?php echo $settings['prefix']; ?>_image_id" value="<?php echo $values[$settings['prefix'].'_image_id']; ?>" class="ident">
		<input type="hidden" id="<?php echo $settings['prefix']; ?>_image_w" name="<?php echo $settings['prefix']; ?>_image_w" value="<?php echo $values[$settings['prefix'].'_image_w']; ?>" class="width">
		<input type="hidden" id="<?php echo $settings['prefix']; ?>_image_h" name="<?php echo $settings['prefix']; ?>_image_h" value="<?php echo $values[$settings['prefix'].'_image_h']; ?>" class="height">
		<a href="" class="<?php echo $settings['prefix']; ?>_image_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values[$settings['prefix'].'_image']; ?>" <?php if ($values[$settings['prefix'].'_image'] == '' || $values[$settings['prefix'].'_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values[$settings['prefix'].'_image'] == '' || $values[$settings['prefix'].'_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values[$settings['prefix'].'_image']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.<?php echo $settings['prefix']; ?>_image_Picker')
    }));
});
</script>
