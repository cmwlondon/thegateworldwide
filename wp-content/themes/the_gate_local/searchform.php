<form class="form-search" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
    <fieldset>
        <input type="submit" value="<?php _e('Go', 'Search Form'); ?>" >
        <div class="text-holder">
            <input type="text" value="" name="s" />
        </div>
    </fieldset>
</form>