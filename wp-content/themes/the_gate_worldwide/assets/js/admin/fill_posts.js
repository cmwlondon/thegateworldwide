function fillPosts(element, blog_id) {

    var categoryDropdown    = jQuery(element),
        selectPost          = categoryDropdown.siblings('.landing-page-entry-post'),
        loader_img          = categoryDropdown.siblings('.loading-placeholder').first().find('img'),

        loading_animation = {
            on: function() {
                loader_img.removeClass('hidden');
            },
            off: function() {
                loader_img.addClass('hidden');
            }
        },
        category = categoryDropdown.val();
    if(category && category !== 'random') {
        jQuery.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
                action: 'get_posts',
                post_type: category,
                blog_id: blog_id
            },
            beforeSend: function() {
                /* Show Loader */
                loading_animation.on();
            },
            isLocal     : true,
            dataType    : 'json',
            async       : true,
            success: function(response) {
                if(response.status == 'success') {

                    var posts = response.posts;
                    var output = ['<option value="random">Random post</option>'];

                    jQuery.each(posts, function(key, value) {
                        output.push('<option value="'+ value.id +'" data-image="' + value.src + '">'+ value.name +'</option>');
                    });
                    selectPost.html(output);
                }
                loading_animation.off();
            },
            failed: function(e) {
                loading_animation.off();
            }
        });
    }
    else if(category == 'random') {
        var output = ['<option value="random">Random post</option>'];
        selectPost.html(output);
    }
    else {
        var output = ['<option value=""></option>'];
        selectPost.html(output);
    }
}