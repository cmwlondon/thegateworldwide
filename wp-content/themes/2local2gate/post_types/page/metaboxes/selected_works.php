<?php
$all_campaigns = get_posts(array('post_type' => 'campaign', 'status' => 'publish', 'numberposts' => -1));

while($metabox->have_fields_and_multi('entries')) :
$mb->the_group_open();
$metabox->the_field('selectedworks');
?>
<div class="selected_works">
    <select class="landing-page-entry-post" name="<?php $mb->the_name(); ?>" style="width: 250px;">
    	<option value=""><?php _e(''); ?></option>
        <?php foreach($all_campaigns as $campaign) : ?>
            <option value="<?php echo $campaign->id; ?>" <?php $mb->the_select_state($campaign->id); ?>><?php echo $campaign->post_title; ?> </option>
        <?php endforeach; ?>
    </select>
    <a href="#" class="dodelete delete-landing-page-entry button"><?php _e('Remove Entry'); ?></a>
</div>
<?php
$mb->the_group_close();
endwhile;
?>
<div class="clear"></div>
<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-entries button"><?php _e('Add Entry'); ?></a></p>
