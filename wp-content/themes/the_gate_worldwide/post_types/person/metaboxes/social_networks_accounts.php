<?php
    $social_networks = array(
        'facebook' => 'Facebook',
        'linked_in' => 'LinkedIn',
        'twitter' => 'Twitter',
    );
?>
<div class="social_networks_accounts box">
    <table>
        <tbody>
            <?php foreach ($social_networks as $social_network_name => $social_network_label) : ?>
                <tr>
                    <?php $mb->the_field($social_network_name); ?>
                    <th>
                        <img src="<?php echo get_bloginfo('template_url') . '/post_types/person/img/' . $mb->name . '.png'; ?>" title="<?php echo $mb->name; ?>" alt="<?php echo $mb->name; ?>" />
                    </th>
                    <td>
                        <label for="<?php echo $mb->id . '_' . $mb->name; ?>"><?php _e($social_network_label); ?></label>
                    </td>
                    <td>
                        <input id="<?php echo $mb->id . '_' . $mb->name; ?>" type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
                    </td>
                </tr>
            <?php endforeach; ?>
            	<tr>
                	<?php $mb->the_field('featured_articles'); ?>
                  	<th></th>
               	<td><label for ="">Featured Articles</label></td>
                	<td><input id="<?= "{$mb->id}_{$mb->name}" ?>" type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" /></td>
              	</tr>
        </tbody>
    </table>
</div>