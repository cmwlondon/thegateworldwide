<?php
/*
cpl_name
cpl_role
cpl_phone
cpl_email
cpl_link
cpl_image
cpl_image_id
cpl_image_w
cpl_image_h

cpr_name
cpr_role
cpr_phone
cpr_email
cpr_link
cpr_image
cpr_image_id
cpr_image_w
cpr_image_h

contact_gate_header
contact_gate_phone
contact_gate_address
contact_gate_lat
contact_gate_long

contact_nextpage
*/

function contact_people_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'contact_people_save_meta_box_data', 'contact_people_meta_box_nonce' );

    $fields = array('cpl_name', 'cpl_role', 'cpl_phone', 'cpl_email', 'cpl_link', 'cpl_image', 'cpl_image_id', 'cpl_image_w', 'cpl_image_h', 'cpr_name', 'cpr_role', 'cpr_phone', 'cpr_email', 'cpr_link', 'cpr_image', 'cpr_image_id', 'cpr_image_w', 'cpr_image_h');
    
    foreach ($fields AS $field) {
        $values[$field] = get_post_meta( $post->ID, $field, true );
    }

    require get_template_directory() . '/custom_posts/page/metaboxes/contact_people.php';
}

function contact_people_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['contact_people_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['contact_people_meta_box_nonce'], 'contact_people_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array('cpl_name', 'cpl_role', 'cpl_phone', 'cpl_email', 'cpl_link', 'cpl_image', 'cpl_image_id', 'cpl_image_w', 'cpl_image_h', 'cpr_name', 'cpr_role', 'cpr_phone', 'cpr_email', 'cpr_link', 'cpr_image', 'cpr_image_id', 'cpr_image_w', 'cpr_image_h');
    
    foreach($fields AS $field) {
        if ( isset( $_POST[$field] )) {
            $my_data = sanitize_text_field( $_POST[$field] );
            update_post_meta( $post_id, $field, $my_data );
        }
    }

}
add_action( 'save_post', 'contact_people_save_meta_box_data' );

/*
$fields= array('contact_gate_header', 'contact_gate_phone', 'contact_gate_address', 'contact_gate_latitude', 'contact_gate_longitude');
*/
function contact_map_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'contact_map_save_meta_box_data', 'contact_map_meta_box_nonce' );

    $fields = array('contact_gate_header', 'contact_gate_phone', 'contact_gate_address', 'contact_gate_latitude', 'contact_gate_longitude');
    
    foreach ($fields AS $field) {
        $values[$field] = get_post_meta( $post->ID, $field, true );
    }

    require get_template_directory() . '/custom_posts/page/metaboxes/contact_map.php';
}

function contact_map_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['contact_map_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['contact_map_meta_box_nonce'], 'contact_map_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array('contact_gate_header', 'contact_gate_phone', 'contact_gate_address', 'contact_gate_latitude', 'contact_gate_longitude');
    
    foreach($fields AS $field) {
        if ( isset( $_POST[$field] )) {
            $my_data = sanitize_text_field( $_POST[$field] );
            update_post_meta( $post_id, $field, $my_data );
        }
    }

}
add_action( 'save_post', 'contact_map_save_meta_box_data' );

function contact_nextpage_meta_box_callback( $post ) {
    wp_nonce_field( 'contact_nextpage_save_meta_box_data', 'contact_nextpage_meta_box_nonce' );

    $values = array(
        "contact_nextpage" => get_post_meta( $post->ID, 'contact_nextpage', true )
    );

    $fieldSettings = array(
        "nextpagefield" => "contact_nextpage"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_nextpage.php';
}

function contact_nextpage_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['contact_nextpage_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['contact_nextpage_meta_box_nonce'], 'contact_nextpage_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('contact_nextpage', 'contact_nextpage')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'contact_nextpage_save_meta_box_data' );
?>
