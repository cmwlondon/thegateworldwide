<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	</div>

	<div id="content" class="site-content latestnews">
		<div class="text newsitem">
			<h1><?php echo $post->post_title; ?></h1>
			<div class="hr"><hr></div>
			<h2><?php echo get_the_date('F j, Y', $post->ID); ?></h2>
			<div><p><?php echo nl2br($post->post_content); ?></p></div>
		</div>

		<div class="sharebuttons clearfix">
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($item->ID)); ?>" class="Share-buttons-item Share-buttons-item--social" target="_blank" data-service="facebook">
			<svg class="Share-buttons-item-icon" viewBox="0 0 64 64">
			<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $theme_root; ?>/svg/social-accounts.svg#facebook-icon"></use>
			</svg><span class="Share-buttons-item-label">Facebook</span><span class="Share-buttons-item-count">0</span>
			</a>
			<a href="https://twitter.com/intent/tweet?url=<?php echo urlencode(get_permalink($item->ID)); ?>&amp;text=" class="Share-buttons-item Share-buttons-item--social" target="_blank" data-service="twitter">
			<svg class="Share-buttons-item-icon" viewBox="0 0 64 64">
			<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $theme_root; ?>/svg/social-accounts.svg#twitter-icon"></use>
			</svg><span class="Share-buttons-item-label">Twitter</span>
			</a>
		      <!--<a href="#" class="Share-buttons-item Share-buttons-item--like" data-item-identifier="58c141a759cc68868297e6c9" data-like-count="0" data-service="squarespace-like">
		        <svg class="Share-buttons-item-icon" viewBox="0 0 64 64">
		          <g class="svg-icon">
		            <path d="M39.9,37.8c-7.3,4.8-7.7,6.9-7.9,7.8c-0.2-0.9-0.6-3-7.9-7.8c-16-10.6,1.4-25.4,7.9-11.7C38.5,12.4,55.9,27.3,39.9,37.8z"></path>
		          </g>
		        </svg><span class="Share-buttons-item-label">0 Likes</span>
		      </a>-->
		    

</div>
<?php get_footer(); ?>
