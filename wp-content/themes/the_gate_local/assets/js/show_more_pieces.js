$(function(){
    $('#show-more').on('click', function(e) {
        e.preventDefault();

        var offset = $('#by-media .list li').length;
        var category = $('#by-media .filtered').first().data('media') || '0';

        if(!$(this).hasClass('hidden')) {
            showMorePieces({offset: offset, taxonomy: 'media_type', numberposts: 6, category: category, post_type: ['campaign_element']});
        }
    })
});

function showMorePieces(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'show_more'}),
        beforeSend  : function() {
            /* Disable button */
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
                var listClasses = [];

                /* Append new posts */
                $.each(response.posts, function(key, post) {
                    var listElement = $('#by-media .list li').first().clone();
                    var rowClass = 'by-media-row-' + post.row;

                    listClasses.push(rowClass);

                    if(post.category) {
                        if($('figure .title', listElement).length > 0) {
                            $('figure .title', listElement).text(post.category);
                        }
                        else {
                            $('figure', listElement).append($('<strong/>').addClass('title').text(post.category));
                        }
                    }
                    else {
                        $('figure .title', listElement).remove();
                    }

                    $(listElement).attr('class', rowClass);
                    post.thumbnail = post.thumbnail.replace('<img','<img onload="this.setAttribute(\'data-loaded\',\'yep\')" data-loaded="nope"');
                    $('figure a', listElement).attr('href', post.url).html(post.thumbnail);
                    $('header h3', listElement).html(post.title);
                    $('header span', listElement).html(post.client);

                    $('.holder', listElement).height('auto');

                    if($('p', listElement).length > 0) {
                        var excerpts = $('p', listElement);
                        excerpts.remove();
                    }
                    $('header', listElement).after($(post.excerpt));

                    $('a.more', listElement).attr('href', post.url);

                    listElement.appendTo($('#by-media .list'));
                });

                /* Set equal hight for list items in same row */
                setTimeout(function(){
                    if($('[data-loaded="nope"]').length != 0){
                        setTimeout(arguments.callee, 250);
                        return;
                    }
                    $.setEqualHeightByClassWithImages($.unique(listClasses));
                }, 100)

                /* Enable button if posts are as much as expected */
                if(!response.hide_show_more)
                $('#show-more').removeClass('hidden');
            }
            else {
                /* Enable button */
                $('#show-more').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            /* Enable button */
            $('#show-more').removeClass('hidden');
        }
    });

}
