<?php
$theme_root = get_template_directory_uri();

$syncedItems = synchronise_people(explode(',', $values['ppl_order']), $post->ID, 'ppl_order');
$assigned = (count($syncedItems['assigned']) > 0 ) ? get_people_in_order($syncedItems['assigned']) : [];
$unassigned = (count($syncedItems['unassigned']) > 0 ) ? get_people_in_order($syncedItems['unassigned']) : [];

$dummy1State = (count($assigned) > 0) ? ' hidden' : '';
$dummy2State = (count($unassigned) > 0) ? ' hidden' : '';

?>

<div class="customField itemSorter pplSorter">
    <input type="hidden" id="ppl_order" name="ppl_order" value="<?php echo implode(',', $syncedItems['assigned']); ?>">

	<p>Assigned</p>
    <ul id="pplItemAssign"  class="slideConnected clearfix">
        <li class="clearfix dummy <?php echo $dummy1State; ?>" id="itemDummy1">
            <div class="portrait"><img alt="-" src="<?php echo $theme_root; ?>/img/work_dummy.png"></div>
            <h3>-</h3>
        </li>
	<?php
	foreach ( $assigned AS $pplItem) :
		$itemMetadata = get_post_meta($pplItem->ID);
        $title = $itemMetadata['p_name'][0];
        $role = $itemMetadata['p_role'][0];
        $portrait = [
            "id" => $itemMetadata['person_normal_id'][0],
            "image" => $itemMetadata['person_normal'][0],
            "width" => $itemMetadata['person_normal_w'][0],
            "height" => $itemMetadata['person_normal_h'][0]
        ];
	?>
	<li class="clearfix" id="<?php echo "pplItem_".$pplItem->ID; ?>"><div class="portrait"><img alt="<?php echo $title ?>" src="<?php echo $portrait['image']; ?>" data-width="<?php echo $portrait['width']; ?>" data-height="<?php echo $portrait['height']; ?>"></div><h3><?php echo "$title<br>$role"; ?></h3></li>
	<?php endforeach; ?>
	</ul>

	<p>Unassigned</p>
    <ul id="pplItemUnassign" class="slideConnected clearfix">
        <li class="clearfix dummy <?php echo $dummy2State; ?>" id="itemDummy2">
            <div class="portrait"><img alt="-" src="<?php echo $theme_root; ?>/img/work_dummy.png"></div>
            <h3>-</h3>
        </li>
	<?php
	foreach ( $unassigned AS $pplItem) :
		$itemMetadata = get_post_meta($pplItem->ID);
        $title = $itemMetadata['p_name'][0];
        $role = $itemMetadata['p_role'][0];
        $portrait = [
            "id" => $itemMetadata['person_normal_id'][0],
            "image" => $itemMetadata['person_normal'][0],
            "width" => $itemMetadata['person_normal_w'][0],
            "height" => $itemMetadata['person_normal_h'][0]
        ];
	?>
	<li class="clearfix" id="<?php echo "pplItem_".$pplItem->ID; ?>"><div class="portrait"><img alt="<?php echo $title ?>" src="<?php echo $portrait['image']; ?>" data-width="<?php echo $portrait['width']; ?>" data-height="<?php echo $portrait['height']; ?>"></div><h3><?php echo "$title<br>$role"; ?></h3></li>
	<?php endforeach; ?>
	</ul>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // make people order items sortable
        if (jQuery('#pplItemAssign').length != 0) {

            jQuery('#pplItemAssign, #pplItemUnassign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    var newOrderList = [];
                    jQuery('#pplItemAssign li').not(".dummy").each(function(i){
                        newOrderList.push( jQuery(this).attr("id").substr(8) );
                    });
                    console.log("stop1 %s", newOrderList.join());
                    jQuery('#ppl_order').val(newOrderList.join());
                }
            }).disableSelection();

            jQuery('#pplItemAssign').on('sortstop',function(event, ui) {
            	var realItems = jQuery(this).find('li').not(".dummy"),
            		realItemCount = realItems.length,
            		dummyItem = jQuery(this).find('li.dummy');
                
                console.log('assign: ' + realItemCount);

                if ( realItemCount == 0 ) {
					dummyItem.removeClass('hidden');
                } else {
					dummyItem.addClass('hidden');
                }
            });
            jQuery('#pplItemUnassign').on('sortstop',function(event, ui) {
            	var realItems = jQuery(this).find('li').not(".dummy"),
            		realItemCount = realItems.length,
            		dummyItem = jQuery(this).find('li.dummy');
                
                console.log('unassign: ' + realItemCount);

                if ( realItemCount == 0 ) {
					dummyItem.removeClass('hidden');
                } else {
					dummyItem.addClass('hidden');
                }
            });
        }
    });
</script>
