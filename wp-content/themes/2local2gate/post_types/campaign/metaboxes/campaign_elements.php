<?php
    global $campaigns_purchaser;

    $args = array(
        'numberposts' => -1,
        'post_type' => 'campaign_element',
        'orderby'         => 'post_date',
        'post_status' => 'publish',
        'suppress_filters' => false
    );

    if($campaigns_purchaser->get_the_value())
    {
        $args = array_merge($args, array('meta_key' => 'campaign_client', 'meta_value' => $campaigns_purchaser->get_the_value()));
    }
    $campaign_elements = get_posts($args);
?>
<div class="portfolio_pieces box">
    <p>
        <?php _e('Relate existing Campaign elements to this Campaign. The appearance in the individual campaign public page will be in the saame order as here.'); ?>
    </p>
    <?php while($mb->have_fields_and_multi('portfolio_pieces')): ?>
        <?php $mb->the_group_open('portfolio_pieces'); ?>
            <div>
                <select name="<?php $mb->the_name(); ?>" style="min-width: 150px; max-width: 70%">
                    <option value=""><?php _e('Select...'); ?></option>
                    <?php foreach ($campaign_elements as $campaign_element) : ?>
                        <option value="<?php echo $campaign_element->ID; ?>" <?php $mb->the_select_state($campaign_element->ID); ?>><?php echo $campaign_element->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
                <a href="#" class="dodelete button"><?php _e('Break Relation'); ?></a>
            </div>
        <?php $mb->the_group_close('portfolio_pieces'); ?>
    <?php endwhile; ?>
    <p>
        <a href="#" class="docopy-portfolio_pieces; ?> button"><?php _e('Relate another Portfolio Piece to this Campaign'); ?></a>
    </p>
</div>