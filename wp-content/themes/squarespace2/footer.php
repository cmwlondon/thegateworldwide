<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
global $current_blog;

$theme_root = get_template_directory_uri();
?>

	<?php // if ( $defaultFooter ) :  get_template_part( 'modules/advisor' ); endif ?>

<!-- closure point for div#content in all templates -->
	</div>
<!--  -->

	<footer id="colophon" class="site-footer">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="thegatelondon"><img alt="thegatelondon" src="<?php echo $theme_root; ?>/img/gate_london.png" class="logo"></a>
		<nav class="social clearfix">
			<a href="https://twitter.com/TheGateLondon" target="_blank" class="sqs-svg-icon--wrapper twitter">
			    <svg viewBox="0 0 32 32">
			      <use class="sqs-use--icon" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $theme_root; ?>/svg/social-accounts.svg#twitter-icon"></use>
			    </svg>
			</a>
			<a href="http://instagram.com/the_gate_london" target="_blank" class="sqs-svg-icon--wrapper instagram">
			    <svg viewBox="0 0 32 32">
			      <use class="sqs-use--icon" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $theme_root; ?>/svg/social-accounts.svg#instagram-icon"></use>
			    </svg>
			</a>
		</nav>
		<p>PART OF <a target="_blank" href="http://www.msqpartners.com" class="msq">MSQ PARTNERS</a>: <a target="_blank" href="https://thegateworldwide.com/asia/">ASIA</a>, <a target="_blank" href="https://thegateworldwide.com/edinburgh/">EDINBURGH</a>, <a target="_blank" href="https://thegateworldwide.com/ny/">NEW YORK</a></p>
	</footer>

</div>

<?php
wp_footer();
?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBvYQD3pGGYKrinw2QAXODQZ919slgNBY&amp;callback=initMap"></script>
</body>
</html>
