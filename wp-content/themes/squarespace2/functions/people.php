<?php
function get_key_contact($personID) {
    $query_parameters = array(
        'post_type' => 'person',
        'post_status' => 'publish',
        'p' => $personID,
        'posts_per_page' => 1
    );

	return do_query($query_parameters);
}

function get_people($mode = null) {
	// currently returns all 'person' posts in random order
    $query_parameters = array(
        'post_type' => 'person',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'rand'
    );

	return do_query($query_parameters);
}

function get_all_people() {
    $query_parameters = array(
        'post_type' => 'person',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' =>'ASC'
    );

	return do_query($query_parameters);
}

function get_people_in_order($order) {
    $query_parameters = array(
        'post_type' => 'person',
        'post_status' => 'publish', // only include published offices
        'posts_per_page' => -1,
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

function synchronise_people( $order, $postid, $metakey ) {
    $allPeople = get_all_people();

    return synchronise($order, $allPeople, $postid, $metakey);
}

function synchronise_team($order, $postid) {
	$allPeople = get_all_people();
	$allPeopleIDs = get_item_ids($allPeople);

	$newOrderItems = array();
	$removedOrderItems = array();

	// remove items from order list which have been removed from all items
	foreach ( $order AS $key => $id) {
        if (!in_array($id, $allPeopleIDs)) {
        	$removedOrderItems[] = array (
        		'key' => $key,
        		'id' => $id
        	);
        }
	}

	$removeReversed = array_reverse($removedOrderItems);
	foreach ( $removeReversed AS $removeItem) {
		array_splice($order, $removeItem['key'], 1);
	}

	// save updated order list
    update_post_meta($post_id, 'c_team', implode(',',$order) );

    // find unassigned items
	$unassigned = array_diff($allPeopleIDs, $order);

	return array(
		'assigned' => $order,
		'unassigned' => $unassigned
	);
}
?>
