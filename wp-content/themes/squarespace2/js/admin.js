/*
admin.js
*/

// console.log("admin.js - %s %s", tgl_ajax.ajaxUrl);

var file_frame,
	thisMediaPanel,
	multiImageChoosers = [],
	imagePickers = [];

(function () {
	"use strict";

    /* ------------------------------------------------ */
    // VideoPlayer
    /* ------------------------------------------------ */
    
    function VideoPlayer(parameters) {
        this.parameters = parameters;

        this.videoWrapper = parameters.videoWrapper;
        this.videoTag = parameters.videoTag;
        this.icon = parameters.icon;
        this.control = parameters.control;
        this.timeline = parameters.timeline;
        this.mode = parameters.mode;

        if (this.timeline) {
            this.elapsedTimeDisplay = this.timeline.find('.elapsed');
            this.timelineProgress = this.timeline.find('.inner');
            this.timelineMarker = this.timeline.find('.marker');
            this.videoDuration = this.timeline.find('.total');
        }

        this.init();
    }
    
    VideoPlayer.prototype = {
        "constructor" : VideoPlayer,
        "template" : function () {var that = this; },
        
        "init" : function () {
            var that = this;
    
            this.ready();
        },
        "init" : function () {
            var that = this;

            this.isFullMode = (this.mode === 'full');
            this.duration = 0;
            this.elapsed = 0;
            this.startTime = 0;
            this.offsetTime = 0;
            this.updater = null;
            this.isRunning = false;

            // bind events to control layer
            if (this.videoTag) {
                this.videoTag
                .bind('ended', function () {
                    that.ended();
                });

                if (this.isFullMode) {
                    this.videoTag
                    .bind('timeupdate', function(){
                        that.updateAction2(this.currentTime);
                    });
                }

                this.control
                .bind('click', function(){
                    that.controlAction();
                });
            }
        },
        "loaded" : function (videoDOMNode) {
            var that = this;

            this.duration = videoDOMNode.duration;
            if (this.isFullMode) {
                this.videoDuration.text(this.formatTime(this.duration));
            }
        },
        "controlAction" : function () {
            var that = this;

            if (this.isRunning) {
                this.pause();
            } else {
                this.play();
            }
            this.isRunning = !this.isRunning;
            this.icon.toggleClass('pause');
        },
        "play" : function () {
            var that = this;
            /*
            if (this.isFullMode) {
                this.startTime = new Date().getTime();
                this.updater = window.setInterval(function(){
                    that.updateAction();
                }, 250);

            }
            */
            this.videoTag.get(0).play();
        },
        "pause" : function () {
            var that = this;

            /*
            if (this.isFullMode) {
                window.clearInterval(this.updater);
                this.offsetTime = this.elapsed;
            }
            */
            this.videoTag.get(0).pause();
        },
        "ended" : function () {
            var that = this;

            if (this.isFullMode) {
                window.clearInterval(this.updater);
                this.offsetTime = 0;
            }
            that.isRunning = !that.isRunning;
        },
        "updateAction" : function () {
            var that = this,
                percent;
            this.elapsed = (new Date().getTime() - this.startTime) + this.offsetTime;
            this.updateTimeline(this.elapsed / 1000);
        },
        "updateAction2" : function (elapsedTime) {
            var that = this;

            this.updateTimeline(elapsedTime);

        },
        "updateTimeline" : function (elapsedTime) {
            var that = this,
                percent;

            percent = Math.floor((elapsedTime / this.duration) * 100);

            this.elapsedTimeDisplay.text(this.formatTime(elapsedTime));
            this.timelineProgress
            .css({
                "width" : percent +"%"
            });
            this.timelineMarker
            .css({
                "left" : percent +"%"
            });
        },
        "removeVideo" : function () {
            var that = this;

            this.videoTag.get(0).pause();
            this.videoTag.remove();

            if (this.isFullMode) {
                this.updateTimeline(0);
                this.videoDuration.text(this.formatTime(this.duration));
            }
        },
        "insertVideo" : function (videoURL) {
            var that = this;

            var newVideoTag = jQuery('<video></video>')
            .attr({
                "preload" : true
            });

            var newVideoSource = jQuery("<source></source>")
            .attr({
                "type" : "video/mp4",
                "src" : videoURL
            })
            newVideoTag.append(newVideoSource);
            this.videoWrapper.append(newVideoTag);


            window.setTimeout(function(){
                that.videoTag = that.videoWrapper.find('video');
                that.videoTag
                .bind('loadeddata', function () {
                    that.loaded(this);
                })
                .bind('ended', function () {
                    that.ended();
                });

                if (that.isFullMode) {
                    that.videoTag
                    .bind('timeupdate', function(){
                        that.updateAction2(this.currentTime);
                    });
                }

                that.control
                .unbind('click')
                .bind('click', function(){
                    that.controlAction();
                });

            }, 10);
        },
        "refreshVideo" : function (node) {
            var that = this;

            this.videoTag = node;
        },
        "formatTime" : function (time) {
            // format supplied time value into SS.HH
            // SS - seconds
            // HH hundredths of a second
            var seconds = Math.floor(time);
            var hundredths = Math.floor((time - seconds) * 100);
            hundredths = (hundredths < 10) ? "0" + hundredths : hundredths;
            return seconds + "." + hundredths;
        }
    };

    window.VideoPlayer = VideoPlayer;

	/* -------------- */
	// MediaPanel
	/* -------------- */
	/*
	add media selector functionality to metabox fields, primarily for images/video
	*/
	function MediaPanel(parameters) {
		this.parameters = parameters;

		this.trigger = parameters.trigger;

		this.frame;
		this.start();
	}
	
	MediaPanel.prototype = {
		"constructor" : MediaPanel,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;

			this.trigger.live('click', function( event ){
				event.preventDefault();
				that.openMedia(jQuery(this));
			});
		},
		"openMedia" : function (doctrigger) {
			var that = this;

			this.parentBox = doctrigger.parents('.imageSelector').eq(0);
			this.target = this.parentBox.find('input').eq(0);
			this.imagepath = this.parentBox.find('p.imagepath');
			that.preview = this.parentBox.find('.imageThumbnail');

			this.picid = false;
			this.width = false;
			this.height = false;
			if (this.parentBox.find('input.ident').length != 0) {
				this.picid = this.parentBox.find('input.ident').eq(0);	
			}
			
			if (this.parentBox.find('input.width').length != 0) {
				this.width = this.parentBox.find('input.width').eq(0);	
			}
			if (this.parentBox.find('input.height').length != 0) {
				this.height = this.parentBox.find('input.height').eq(0);	
			}

			if ( this.frame ) {
				this.frame.open();
				return;
			}

			this.frame = wp.media.frames.file_frame = wp.media({
				"title" : jQuery( this ).data( 'uploader_title' ),
				"button" : {
					text: jQuery( this ).data( 'uploader_button_text' ),
				},
				"multiple" : false  
			});

			this.frame.on( 'select', function() {

				that.attachment = that.frame.state().get('selection').first().toJSON();
				// attachment.id
				// attachment.sizes: full, large, medium, thumbnail
				that.target.val(that.attachment.sizes.full.url);

				if (that.picid) { that.picid.val(that.attachment.id); }
				
				if (that.width) { that.width.val(that.attachment.width); }
				if (that.height) { that.height.val(that.attachment.height); }

				jQuery(that.imagepath)
				.animate({"opacity" : 0}, 500, function(){
				});

				jQuery(that.preview)
				.animate({"opacity" : 0}, 500, function(){
					jQuery(this).load(function(){
						jQuery(that.imagepath).text(that.attachment.sizes.full.url);
						jQuery(this).animate({"opacity" : 1}, 500, function(){
						});
						jQuery(that.imagepath).animate({"opacity" : 1}, 500, function(){
						});
					}).attr({"src" : that.attachment.sizes.full.url});
				});
			});

			this.frame.open();

		}
	}

	window.MediaPanel = MediaPanel;

	/* -------------- */
	// DocPicker
	/* -------------- */
	/*
	add media selector functionality to metabox fields, primarily for PDF/documents
	*/
	function DocPicker(parameters) {
		this.parameters = parameters;

		this.trigger = parameters.trigger;

		this.frame;
		this.start();
	}
	
	DocPicker.prototype = {
		"constructor" : DocPicker,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;

			this.trigger.live('click', function( event ){
				event.preventDefault();
				that.openMedia(jQuery(this));
			});
		},
		"openMedia" : function (doctrigger) {
			var that = this,
				docTriggerParent = doctrigger.parents('.docSelector').eq(0);

			this.targetLink = docTriggerParent.find('input.link');
			this.targetID = docTriggerParent.find('input.id');
			this.targetData = docTriggerParent.find('input.data');
			this.filepath = docTriggerParent.find('.docLinkPath');

			if ( this.frame ) {
				this.frame.open();
				return;
			}

			this.frame = wp.media.frames.file_frame = wp.media({
				"title" : jQuery( this ).data( 'uploader_title' ),
				"button" : {
					text: jQuery( this ).data( 'uploader_button_text' ),
				},
				"multiple" : false  
			});

			this.frame.on( 'select', function() {

				that.attachment = that.frame.state().get('selection').first().toJSON();
				// get attachment data
				/*
				id
				filesizeHumanReadable
				filesizeInBytes
				mime
				subtype
				*/

				/*
				var data = [
					that.attachment.id,
					that.attachment.filesizeHumanReadable,
					that.attachment.filesizeInBytes,
					that.attachment.mime,
					that.attachment.subtype
				];
				that.targetData.val( JSON.stringify(data).replace(/"/g, '\\"') );
				*/
				that.targetLink.val(that.attachment.url);

				var data = that.attachment.id + "," + that.attachment.filesizeHumanReadable + "," + that.attachment.filesizeInBytes + "," + that.attachment.mime + "," + that.attachment.subtype;
				that.targetData.val(data);

				jQuery(that.filepath).text(that.attachment.url);
				jQuery(that.filepath).animate({"opacity" : 1}, 500, function(){
				});

			});

			this.frame.open();

		}
	}

	window.DocPicker = DocPicker;

	/* -------------- */
	// VideoPicker
	/* -------------- */
	/*
	add media selector functionality to metabox fields, primarily for images/video
	*/
	function VideoPicker(parameters) {
		this.parameters = parameters;

		this.trigger = parameters.trigger;
		this.videoWidget = parameters.videoWidget;
		this.buildVideoTag = parameters.buildVideoTag;

		this.frame;
		this.start();
	}
	
	VideoPicker.prototype = {
		"constructor" : VideoPicker,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;

			this.trigger.live('click', function( event ){
				event.preventDefault();
				that.doAction(jQuery(this));
			});
		},
		"doAction" : function (doctrigger) {
			var that = this,
				actionRegex = /(add|remove)/,
				result;

			this.docTriggerParent = doctrigger.parents('.videoSelector').eq(0);
			this.videoLink = this.docTriggerParent.find('input.id');
			this.videoWrapper = this.docTriggerParent.find('.vpWrapperFull');

			var result = actionRegex.exec(doctrigger.attr('class'));
			if (result) {

				switch (result[0]) {
					case "add" : {
						this.addVideo();
					} break;
					case "remove" : {
						this.removeVideo();
					} break;
				}

			}
			return;
		},
		"addVideo" : function () {
			var that = this;

			if ( this.frame ) {
				this.frame.open();
				return;
			}

			this.frame = wp.media.frames.file_frame = wp.media({
				"title" : jQuery( this ).data( 'uploader_title' ),
				"button" : {
					text: jQuery( this ).data( 'uploader_button_text' ),
				},
				"multiple" : false  
			});

			this.frame.on( 'select', function() {

				that.attachment = that.frame.state().get('selection').first().toJSON();
				// get attachment data
				/*
				id
				filesizeHumanReadable
				filesizeInBytes
				mime
				subtype
				*/

				if (that.buildVideoTag) {
					jQuery(that.videoWrapper).animate({"opacity" : 0}, 500, function(){
						var data = [
							that.attachment.id,
							that.attachment.filesizeHumanReadable,
							that.attachment.filesizeInBytes,
							that.attachment.mime,
							that.attachment.subtype
						];

						that.videoWidget.removeVideo();
						that.videoWidget.insertVideo(that.attachment.url);
						that.videoLink.val(that.attachment.url);

						jQuery(that.videoWrapper).animate({"opacity" : 1}, 500, function(){
						});
					});
				} else {
					that.videoTitle = that.docTriggerParent.find('.namespan');
					that.videoTitle.text(that.attachment.url)
					that.videoLink.val(that.attachment.url);
				}
			});

			this.frame.open();

		},
		"removeVideo" : function () {
			var that = this;

			jQuery(that.videoWrapper).animate({"opacity" : 0}, 500, function(){
				that.videoWidget.removeVideo();
				that.videoLink.val('');
			});

		}
	}

	window.VideoPicker = VideoPicker;

	function MultiImageChooser(parameters) {
        this.parameters = parameters;
        this.module = parameters.module;
        this.prefix = parameters.prefix;
        this.preexisting = parameters.preexisting;

        this.init();
    }
    
    MultiImageChooser.prototype = {
        "constructor" : MultiImageChooser,
        "template" : function () {var that = this; },
        
        "init" : function () {
            var that = this;

			this.addButtons = this.module.find('.addItem');
			this.removeButtons = this.module.find('.removeItem');
			this.itemBox = this.module.find('.items');
			this.template = this.module.find('.template');
			this.orderField = this.module.find('#' + this.prefix + '_order');
			this.nextIDField = this.module.find('#' + this.prefix + '_nextid');

			this.order = this.orderField.val();
			this.nextid = this.nextIDField.val();

			this.bindAddButtons();
			this.bindRemoveButtons();
			this.bindImagePickers();
			this.bindSorter();
        },
        "bindAddButtons" : function () {
            var that = this;

			this.addButtons.on('click', function(e){
				e.preventDefault();
				that.addItem();
			});
        },
        "bindRemoveButtons" : function () {
            var that = this;

			this.removeButtons.on('click', function(e){
				e.preventDefault();
				that.removeItem( jQuery(this).parents('.item') );
			});
        },
        "bindImagePickers" : function () {
            var that = this,
				clIndex = 0;

			do {
				imagePickers.push(new MediaPanel({
			        "trigger" : jQuery('.' + this.preexisting[clIndex] + '_Picker')
			    }));

				clIndex++;
			} while (clIndex < this.preexisting.length );
        },
        "bindSorter" : function () {
            var that = this,
            	listID = '#' + this.prefix + 'List';

		    jQuery(listID).sortable({
		    	/*
		        "receive" : function ( event, ui ) {
			    	console.log('receive');
		        },
		        "start" : function( event, ui ) {
		        	console.log('start');
		        },
		        "stop" : function( event, ui ) {
		        	console.log('stop');
		        }
		        */
		    }).disableSelection();

		    jQuery(listID)
		    .on('sortstop',function(event, ui) {
	        	that.updateOrder();
		    });

        },
        "addItem" : function () {
            var that = this,
            	newItem = this.template.clone(),
            	v = newItem.html(),
            	r = new RegExp(/NNXNN/, 'g'),
            	newID = this.prefix + "_" + this.nextid,
            	newPickerClass = '.' + newID + '_Picker';
    
	 		v = v.replace(r, this.nextid);
	 		newItem.html(v);
	 		newItem.attr({"id" : newID});
	 		newItem.removeClass('template');

	 		// bind remove button
	 		newItem.find('.removeItem').on('click', function (e) {
	 			e.preventDefault();
	 			that.removeItem( jQuery(this).parents('.item') );
	 		});

	 		// bind imagePicker
			imagePickers.push(new MediaPanel({
		        "trigger" : newItem.find(newPickerClass)
		    }));

	 		this.itemBox.append(newItem);

	 		this.nextid++;
	 		this.nextIDField.val(this.nextid);
    		this.updateOrder();
        },
        "removeItem" : function (thisItem) {
            var that = this;
    
    		thisItem.remove();
    		this.updateOrder();
        },
        "updateOrder" : function () {
            var that = this,
				items = [];

			this.itemBox.find('.item').each(function(i){
				items.push(jQuery(this).attr('id'));
			});
			this.orderField.val(items.join(','));
        }
    }

    window.MultiImageChooser = MultiImageChooser;

})();
