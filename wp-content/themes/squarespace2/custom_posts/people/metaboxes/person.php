<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label for="person_normal">Portrait image (normal)</label>
		<input type="hidden" id="person_normal" name="person_normal" value="<?php echo $values['person_normal']; ?>">
		<input type="hidden" id="person_normal_id" name="person_normal_id" value="<?php echo $values['person_normal_id']; ?>" class="ident">
		<input type="hidden" id="person_normal_width" name="person_normal_width" value="<?php echo $values['person_normal_w']; ?>" class="width">
		<input type="hidden" id="person_normal_height" name="person_normal_height" value="<?php echo $values['person_normal_h']; ?>" class="height">
		<a href="" class="personNormalPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['person_normal']; ?>" <?php if ($values['person_normal'] == '' || $values['person_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['person_normal'] == '' || $values['person_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['person_normal']; ?></p>
	</div>
</div>

<div class="customField mediumtext">
	<label for="person_forename">name</label>
	<input type="text" id="person_name" name="person_name" value="<?php echo $values['name']; ?>">
</div>


<div class="customField mediumtext">
	<label for="person_role">Role</label>
	<input type="text" id="person_role" name="person_role" value="<?php echo $values['role']; ?>">
</div>

<div class="customField wpeditor">
    <p>Bio</p>
    <?php
        $settings = array(
            'textarea_name' => 'person_bio',
            'editor_class'  => 'intro_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => true
        );
        wp_editor( html_entity_decode($values['bio']), 'introwp', $settings );
        // wp_editor( $values['ac1_text'], 'introwp', $settings );
    ?>
</div>

<div class="customField mediumtext">
	<label for="person_link">Link</label>
	<input type="text" id="person_link" name="person_link" value="<?php echo $values['link']; ?>">
</div>



<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.personNormalPicker')
    }));
});
</script>
