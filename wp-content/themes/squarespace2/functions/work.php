<?php
/*
functions relating to custom post type 'work'

get_all_work()
get_filtered_work($tags,$mode)
get_work_by_order($order)
get_related_work($tags,$postid)
synchronise_work($order, $tags, $mode, $postid, $metakey)
*/

// get_all_products()
function get_all_work($postid = null) {
	if (is_null($postid)) {
		// no post id passed in to function, get *ALL* products
	    $query_parameters = array(
	        'post_type' => 'work',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	} else {
		// get all products except product identified by $postid
	    $query_parameters = array(
			'post__not_in' => array($postid),	    	
	        'post_type' => 'work',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );

	}

	return do_query($query_parameters);
}

function get_work_by_id($postid = null) {
    $query_parameters = array(
		'post__in' => array($postid),	    	
        'post_type' => 'work',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' =>'ASC'
    );

	return do_query($query_parameters);
}

// get_products_by_order($order)
function get_work_by_order($order) {
    $query_parameters = array(
        'post_type' => 'work',
        'post_status' => 'publish',
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

// synchronise_products($order, $tags, $postid, $metakey)
function synchronise_work( $order, $postid, $metakey ) {
	$filteredWork = get_all_work();

	return synchronise($order, $filteredWork, $postid, $metakey);
}

/* ajax call function template START */
/*
	add_action( 'wp_ajax_nopriv_template', 'template_ajax' ); // front end (no privilege) access
	add_action( 'wp_ajax_template', 'template_ajax' ); // wp admin only access

	// ajax action : 'template'

	function template_ajax(){
		// insert your data into $data for JSON response 
		// $_POST[] or $_REQUEST[]
		$data = array(
			"action" => "template",
			"response" => "OK"
		);

		generic_ajax( $data );
	}
*/
/* ajax call function template END */

add_action( 'wp_ajax_nopriv_get_all_work', 'get_all_work_ajax' ); // front end (no privilege) access
add_action( 'wp_ajax_get_all_work', 'get_all_work_ajax' ); // wp admin only access

// ajax action : 'get_all_products'

function get_all_work_ajax(){
	$workid = $_REQUEST['postid'];
	$work = get_all_work($postid); // pass in current product id to exclude this product from list of all products

	$workPrime = array();
	$workIDs = array();

	foreach($work AS $workItem) {
		$productIDs[] = $workItem->ID; // build list of product ids
		// $productMetaData = get_post_meta($product->ID);

		// get tags for each product
		$taglist = buildTaglist($workItem->ID);

		$workPrime[] = array(
			"id" => $workItem->ID,
			"title" => $workItem->post_title,
			"tags" => $taglist['string']
		);
	}
	

	$data = array(
		"action" => "get_all_work",
		"postid" => $postid,
		"products" => $workPrime,
		"ids" => $workIDs,
		"response" => "OK"
	);

	generic_ajax( $data );
}
?>