<?php
?>

<div class="customField mediumtext">
	<label for="ac1_header">Copy block 1 header</label>
	<input type="text" id="ac1_header" name="ac1_header" value="<?php echo $values['ac1_header']; ?>">
</div>

<div class="customField wpeditor">
    <p>copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'ac1_text',
            'editor_class'  => 'intro_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => true
        );
        wp_editor( html_entity_decode($values['ac1_text']), 'introwp', $settings );
        // wp_editor( $values['ac1_text'], 'introwp', $settings );
    ?>
</div>
