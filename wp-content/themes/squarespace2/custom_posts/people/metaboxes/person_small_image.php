<?php
/*
office map image select
*/
?>

<div class="customField longtext imageField">
<label for="personImageSmall" class="mediaTrigger_small"><?php echo _e( 'Key contact image', 'myplugin_textdomain' ); ?></label>
<input type="text" id="personImageSmall" name="personImageSmall" value="<?php echo esc_attr( $value ); ?>" />  
<img id="personImageSmall_image" alt="" src="<?php echo esc_attr( $value ); ?>">

<script type="text/javascript">
var thisMediaPanel_small;

jQuery(document).ready(function(){

	thisMediaPanel_small = new MediaPanel({
		"trigger" : jQuery('.mediaTrigger_small'),
		"target" : jQuery('#personImageSmall'),
		"preview" : jQuery('#personImageSmall_image')
	})
});
</script>


</div>
