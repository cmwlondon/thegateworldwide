$(function(){
    $('#sort-media li a').on('click', function(e) {
        e.preventDefault();

        var media = $(this).data('media');

        showFilteredWorkByMedia({numberposts: 6, media: media});
    })
});

function showFilteredWorkByMedia(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'filter_work_by_media'}),
        beforeSend  : function() {
            /* Disable show more button */
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            var listClasses = [];

            if(response.status == 'success') {
                /* Replace posts */
                var patternElement = $('#by-media .list li').first();
                var list = $('#by-media .list').clone();

                list.html('');

                $.each(response.posts, function(key, post) {
                    var listElement = patternElement.clone();
                    var rowClass = 'by-media-row-' + post.row;

                    listClasses.push(rowClass);

                    $(listElement).attr('class', rowClass);
                    if(post.media) {
                        if($('.title', listElement).length > 0) {
                            $('.title', listElement).text(post.media);
                        }
                        else {
                            $('figure', listElement).append($('<strong/>').addClass('title').text(post.media));
                        }
                    }
                    else {
                        $('.title', listElement).remove();
                    }

                    post.thumbnail = post.thumbnail.replace('<img','<img onload="this.setAttribute(\'data-loaded\',\'yep\')" data-loaded="nope"');
                    $('figure a', listElement).attr('href', post.url).html(post.thumbnail);
                    $('header h3', listElement).html(post.title);
                    $('header span', listElement).html(post.client);
                    $('.holder', listElement).height('auto');

                    if($('p', listElement).length > 0) {
                        var excerpts = $('p', listElement);
                        excerpts.remove();
                    }
                    $('header', listElement).after($(post.excerpt));

                    $('a.more', listElement).attr('href', post.url);

                    list.append(listElement);
                });

                $('#by-media .list').html(list.html());

                /* Enable button if posts are as much as expected */
                if(!response.hide_show_more)
                $('#show-more').removeClass('hidden');

                /* Set equal hight for list items in same row */
                setTimeout(function(){
                    if($('[data-loaded="nope"]').length != 0){
                        setTimeout(arguments.callee, 250);
                        return;
                    }
                    $.setEqualHeightByClassWithImages($.unique(listClasses));
                }, 100)

            }
            else {
                /* Enable button */
                $('#show-more').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            /* Enable button */
            $('#show-more').removeClass('hidden');
        }
    });

}
