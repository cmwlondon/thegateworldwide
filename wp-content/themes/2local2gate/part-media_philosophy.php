<?php
/**
 * Template Name: Media Philosophy
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $media_philosophy_page;


/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

if($media_philosophy_page) : ?>
<div class="outerBox">
    <div class="innerPadding">
        <div style="clear:both;">
            <div class="titleCol"><h3><?php echo get_post_meta($media_philosophy_page->ID, 'about_subtitle_subtitle', true); ?></h3></div>
            <figure class="imageCol"><?php echo (has_post_thumbnail($media_philosophy_page->ID)) ? get_the_post_thumbnail($media_philosophy_page->ID, 'about-philosophy') : get_placeholder_image('about-philosophy'); ?></figure>
            <div class="textCol">
                <?php echo apply_filters('the_content', $media_philosophy_page->post_content); ?>
            </div>

            <figure class="alignRight"></figure>
            <br style="clear:both;">
        </div>
    </div>
</div>
<?php endif;