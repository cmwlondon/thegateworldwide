<?php
/**
 * Template Name: News Featured
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $featured_news_page,
       $featured_news;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}
?>

<?php if($featured_news_page && !empty($featured_news)) : ?>
<li id="featured" class="gray featured-news" <?php if(has_post_thumbnail($featured_news_page->ID)) : ?> style="background: url('<?php echo get_thumbnail_src($featured_news_page->ID); ?>') repeat scroll 3486px 0 transparent;" <?php endif; ?>>
    <div class="featured-news-wrap">
        <div class="featured-news-side">
            <h2><?php echo apply_filters('the_title', $featured_news_page->post_title); ?></h2>
            <div class="featured-info">
                <?php $count = 0; ?>
                <?php foreach ($featured_news as $key => $single_news) : ?>
                    <div class="<?php if(!$count) echo 'first'; ?> featured-info-inner" data-id="<?php echo ++$count; ?>" data-count="<?php if(!$is_edinburgh && get_post_meta($single_news->ID, 'fake_views_check', true)){ echo do_shortcode('[post_view id='.$single_news->ID.']'); } else { ?>-1<?php } ?>">
                        <h3><?php echo apply_filters('the_title', $single_news->post_title); ?></h3>
                        <h4><?php _e('By', 'News Listing Page'); ?> <span><?php echo get_post_meta($single_news->ID, 'fake_author', true); ?></span></h4>
                        <?php echo apply_filters('the_excerpt', $single_news->post_excerpt); ?>
                        <a href="<?php echo get_permalink($single_news->ID); ?>" class="featured-read-more"><?php _e('Read more', 'News Listing Page'); ?>&hellip;</a>
                    </div>
                <?php endforeach; ?>
                <div class="featured-controls"></div>
                <?php if(!$is_edinburgh): ?>
                <div class="views-counter" style="display: none">
                	<img class="do-not-open-in-modal" src="<?php bloginfo('template_directory'); ?>/assets/images/news-views.png" width="20">
                   	<span></span>
                </div>
                <?php endif; ?>
            </div>
        </div><!-- .featured-news-side -->
        <div class="slider-expand-wrapper">
            <ul class="featured-news-slider">
                <?php foreach ($featured_news as $key => $single_news) : ?>
                    <li>
                        <a href="<?php echo get_permalink($single_news->ID); ?>"><?php echo (has_post_thumbnail($single_news->ID)) ? get_the_post_thumbnail($single_news->ID, 'featured-news') : get_placeholder_image('featured-news'); ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="visual">
            </div>
        </div><!-- .slider-expand-wrapper -->
    </div><!-- .featured-news-wrap -->
</li>
<?php endif; ?>