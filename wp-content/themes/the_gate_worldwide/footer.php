<?php
    $offices = get_offices_information();
    $copyright_text = get_copyright_text();

    /* Do not show footer on authentication page */
    if(!defined('WP_INSTALLING') || !WP_INSTALLING) :
?>
        <!-- footer -->
        <footer id="footer">
            <div class="footer-holder">
                <div class="footer-frame">
                    <!-- slide-area -->
                    <div class="slide-area">
                        <!-- slide -->
                        <div class="slide">
                            <a href="#" class="opener"><?php _e('close'); ?></a>
                            <div class="columns-holder">
                                <!-- link-area -->
                                <?php wp_nav_menu(array('theme_location' => 'footer_left', 'container' => '', 'menu_class' => 'link-area', 'depth' => 1)); ?>
                                <!-- area -->
                                <div class="area">
                                    <div class="columns-place">
                                        <?php foreach ($offices as $office) : ?>
                                            <div class="column">
                                                <h3><?php echo apply_filters('the_title', $office->post_title); ?></h3>
                                                <?php if(trim($office->phone) !== '') : ?>
                                                    <p><?php echo $office->phone; ?></p>
                                                <?php endif; ?>
                                                <?php if(trim($office->contact_url) !== '') : ?>
                                                    <p><a href="<?php echo $office->contact_url; ?>"><?php _e('Contact Us'); ?></a></p>
                                                <?php endif; ?>
                                                <?php if(trim($office->map_url) !== '') : ?>
                                                    <p><a href="<?php echo $office->map_url; ?>"><?php _e('Map'); ?></a></p>
                                                <?php endif; ?>
                                                <?php if(trim($office->work_url) !== '') : ?>
                                                    <p><a href="<?php echo $office->work_url; ?>" class="link"><?php _e('View our work'); ?></a></p>
                                                <?php endif; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <!-- text -->
                                <div class="text">
                                    <!-- link-area -->
                                    <?php wp_nav_menu(array('theme_location' => 'footer_right', 'container' => '', 'menu_class' => 'link-area', 'depth' => 1)); ?>
                                    <?php echo $copyright_text; ?>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="opener"><span class="text-open"><?php _e('MORE'); ?></span><span class="text-close"><?php _e('CLOSE'); ?></span> <span class="ico"></span><?php _e(' INFO'); ?></a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <?php endif; ?>
    <?php wp_footer();?>
</body>
</html>