<?php global $related_posts; ?>

<?php if(!empty($related_posts)) : ?>
    <div class="connected-items">
      <div class="connected-items-holder">
            <h2><?php _e('Related items', 'Related Items'); ?></h2>
            <ul class="list">
                <?php foreach ($related_posts as $key => $related_post) : ?>
                    <li class="related-items-row-<?php echo ceil(($key + 1) / 3); ?>">
                        <div class="holder">
                            <figure>
                                <a href="<?php echo get_permalink($related_post->ID); ?>">
                                    <?php echo (has_post_thumbnail($related_post->ID)) ? get_the_post_thumbnail($related_post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?>
                                </a>
                                <?php $post_category = get_the_category($related_post->ID); ?>
                                <?php if(!empty($post_category) && is_object(current($post_category))) : ?>
                                    <strong class="title"><?php echo current($post_category)->cat_name; ?></strong>
                                <?php endif; ?>
                            </figure>
                            <header>
                                <h3><?php echo apply_filters('the_title', $related_post->post_title); ?></h3>
                                <?php if($related_post->post_type == 'campaign_element') : ?>
                                    <?php $client_name = get_post_meta($related_post->ID, 'campaign_client', true); ?>
                                    <span class="client"><?php echo get_the_title($client_name); ?></span>
                                <?php endif; ?>
                            </header>
                            <?php echo apply_filters('the_excerpt', $related_post->post_excerpt); ?>
                            <a class="more" href="#"><?php _e('READ MORE', 'Related Items'); ?></a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif;