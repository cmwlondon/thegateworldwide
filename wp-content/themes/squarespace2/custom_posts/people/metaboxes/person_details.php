<?php
/*
forename
surname
role
section
email
linkedin
phone
mobile
image1 (large)
image2 (small)
*/
?>
    <div class="personDetails postbox">
        <div class="handlediv" title="Click to toggle"></div>
        <div class="inside">

        	<?php
        	$metabox->the_field('forename');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Forename</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>
        	<?php
        	$metabox->the_field('surname');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Surname</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>
        	<?php
        	$metabox->the_field('role');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Role</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>
        	<?php
        	$metabox->the_field('section');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Section</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>

        	<?php
        	$metabox->the_field('email');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Email</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>
        	<?php
        	$metabox->the_field('linkedin');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Linkedin</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>
        	<?php
        	$metabox->the_field('phone');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Phone</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>
        	<?php
        	$metabox->the_field('mobile');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Mobile</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>
        	<?php
        	$metabox->the_field('main_image');
        	$mainImage = $metabox->get_the_value();
			$mainImageName = $metabox->get_the_name();
        	?>
        	<div class="customField imageSelector">
        		<label for="<?php echo $mainImageTarget; ?>">Main image</label>
        		<input type="hidden" id="<?php echo $mainImageTarget; ?>" name="<?php echo $mainImageName; ?>" value="<?php echo $mainImage; ?>">
        		<a href="" class="mediaTrigger_large action">Add/Update image</a>
        		<img class="imageThumbnail" alt="" src="<?php echo $mainImage; ?>" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>>
        		<p class="imagepath" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $mainImage; ?></p>
        	</div>
        </div>
		<script type="text/javascript">
		var thisMediaPanel_large;

		jQuery(document).ready(function(){

			thisMediaPanel_large = new MediaPanel({
				"trigger" : jQuery('.mediaTrigger_large')
			});

		});
		</script>

    </div>
