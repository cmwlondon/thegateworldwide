<?php
/**
 * Functions and Definitions
 *
 * @package WordPress Multisite
 * @subpackage The Gate Local theme
 */

error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT);

/**
 * Constants
 */
include_once 'config/constants.php';

/**
 * Metaboxes
 */
include_once 'includes/metaboxes/setup.php';

/**
 * Utilities
 */
include_once 'includes/utility.php';
include_once 'includes/validation_utility.php';
include_once 'includes/mail_utility.php';
include_once 'includes/people_utility.php';
include_once 'includes/highlight_utility.php';

/**
 * oAuth Library
 */
include_once 'includes/twitteroauth/twitteroauth.php';


/**
 * Custom Post Types
 */

include('post_types/about_us/about_us.php');
include('post_types/campaign_element/campaign_element.php');
include('post_types/campaign/campaign.php');
include('post_types/client/client.php');
include('post_types/value/value.php');
include('post_types/media_value/media_value.php');
include('post_types/page/page.php');
include('post_types/post/post.php');

/**
 * Options
 */
 include('options/general/office.php');

/**
 * Custom Widgets
 */
include('widgets/twitter.php');

flush_rewrite_rules();
/**
 * Remove standard image sizes so that these sizes are not
 * created during the Media Upload process
 *
 * Tested with WP 3.2.1
 *
 * Hooked to intermediate_image_sizes_advanced filter
 * See wp_generate_attachment_metadata( $attachment_id, $file ) in wp-admin/includes/image.php
 *
 * @param $sizes, array of default and added image sizes
 * @return $sizes, modified array of image sizes
 * @author Ade Walker http://www.studiograsshopper.ch
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function thegate_local_setup() {
    /* This theme uses post thumbnails */
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'homepage-thumbnail', 2000, 1230, true );
    add_image_size( 'about-philosophy', 427, 427, true );

    // add_image_size( 'about-value', 382, 382, true );
    add_image_size( 'about-value', 600, 383, true );
    add_image_size( 'about-main', 600, 383, true );

    add_image_size( 'about-experience', 415, 415, true );
    add_image_size( 'worldwide-thumb', 288, 255, true );
    add_image_size( 'client-logo', 280, 101, true );
    add_image_size( 'medium-thumbnail', 291, 227, true );
    add_image_size( 'featured-news', 693, 470, true );
    add_image_size( 'person-small', 88, 88, true );
    add_image_size( 'person-medium', 131, 131, true );
    add_image_size( 'person-large', 334, 334, true );
    add_image_size( 'campaign-element-small', 265, 209, true );
    add_image_size( 'campaign-element-large', 600, 383, true );
    add_image_size( 'news-large', 657, 385, true );
    add_image_size( 'news-full-width', 768, 385, true );
    add_image_size( 'news-small', 104, 91, true );
    add_image_size( 'main-image', 854, 519, false);
    add_image_size( 'admin-thumb', 100, 78, true );
    add_image_size( 'contact-local', 999, 619, true);
    add_image_size( 'search-thumb', 107, 84, true);

    /* Register menu areas */
    register_nav_menus( array(
        'primary'       => __( 'Primary Navigation'),
        'footer'        => __( 'Footer Navigation'),
        'footer_left'   => __( 'Footer Left Navigation'),
        'footer_right'  => __( 'Footer Right Navigation'),
    ) );

}
add_action( 'after_setup_theme', 'thegate_local_setup' );

/**
 * Register widgetized areas
 *
 * @uses register_sidebar
 */
function thegate_local_widgets_init() {
    /* Twitter Feed Widget Area - Displayed on front-page */
    register_sidebar( array(
        'name'          => __( 'Twitter Feed'),
        'id'            => 'twitter-widget-area',
        'description'   => __( 'Twitter Feed widget area'),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'before_text'   => '',
        'after_text'    => ''
    ) );
}
add_action( 'widgets_init', 'thegate_local_widgets_init' );


/* ########################## Filter Functions ############################## */


/* Make filter function available through ajax call */
add_action('wp_ajax_filter_work_by_state', 'filter_work_by_state_ajax');
add_action('wp_ajax_filter_work_by_media', 'filter_work_by_media');
add_action('wp_ajax_nopriv_filter_work_by_media', 'filter_work_by_media');

/* Ajax Function that handles filtering work by client */
function filter_work_by_state($term = null) {
    $query = array(
        'post_type' => 'campaign',
        'status' => 'publish',
        'numberposts' => -1
    );

    // evo / revo
    $state_filter = array(
        'meta_key'   => 'campaign_state',
        'meta_value' => $term,
        'meta_compare' => '==='
    );

    if (null !== $term) {
        $query = array_merge($query, $state_filter);
    }
    $stateposts = get_posts($query);

    /*
    $posts = array();

    foreach ($stateposts AS $post) {

        $url = get_permalink($post->ID);
        $thumbnail = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail');
        $title = apply_filters('the_title', $post->post_title);

        $posts[] = array(
            'url' => $url,
            'thumbnail' => $thumbnail,
            'title' => $title,
        );        
    }
    */

    /*
        <a class="overlayLink" href="<?php echo get_permalink($campaign->ID); ?>"></a>
        <figure>
            <?php echo (has_post_thumbnail($campaign->ID)) ? get_the_post_thumbnail($campaign->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?>
        </figure>
        <header>
            <h3><?php echo apply_filters('the_title', $campaign->post_title); ?></h3>
            <h4><?php echo apply_filters('the_client', $campaign->post_client); ?></h4>
            <p><?php echo apply_filters('the_excerpt', $campaign->post_excerpt); ?></p>
            <p><?php echo apply_filters('the_media', $campaign->post_media); ?></p>
        </header>
    */
    $posts = $stateposts;
    return $posts;
}

function filter_work_by_state_ajax() {
    if ( isset($_POST['state'])) {
        emit_json(filter_work_by_state($_POST['state']));
    } else {
        emit_json(filter_work_by_state());
    }
}

function emit_json($posts) {
    echo json_encode(array('status' => 'success', 'posts' => $posts));
}

function filter_work_by_media()
{
    if(isset($_POST['media']) && isset($_POST['numberposts']))
    {
        $args = array(
            'numberposts' => (int)$_POST['numberposts'] + 1,
            'offset' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => array('campaign_element'),
            'post_status' => 'publish',
            'suppress_filters' => 0 /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
        );


        if((int)$_POST['media'])
        {
            /* Filter by media */
            $args = array_merge($args, array('tax_query' => array(
                    array(
                         'taxonomy' => 'media_type',
                         'field' => 'id',
                         'terms' => array((int)$_POST['media']),
                         'operator' => 'IN'
                    )
                )
            ));
        }
        $posts = get_posts($args);
        $hide_show_more = count($posts) < ((int)$_POST['numberposts'] + 1);

        if(!$hide_show_more) array_pop($posts);

        $result = array();

        /* Reformat result */
        foreach ($posts as $key => $post)
        {
            if((int)$_POST['media'])
            {
                $media = get_term((int)$_POST['media'], 'media_type');
                $media = $media->name;
            }
            else
            {
                $media = get_the_terms($post->ID, 'media_type');
                if($media && is_array($media))
                {
                    $media = current($media)->name;
                }
            }
            $url = get_permalink($post->ID);
            $thumbnail = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail');
            $title = apply_filters('the_title', $post->post_title);
            $get_client = get_post_meta($post->ID, 'campaign_client', true);
            $client = isset($get_client) ? get_the_title($get_client) : false;
            $excerpt = apply_filters('the_excerpt', $post->post_excerpt);
            $row = ceil(($key + 1) / 3);
			$mrow = ceil(($key + 1) / 2);
            $query = parse_url($url, PHP_URL_QUERY);

            $result[] = array(
                'url' => $url,
                'thumbnail' => $thumbnail,
                'title' => $title,
                'client'=> $client,
                'excerpt' => $excerpt,
                'date' => the_gate_format_date($post->post_date),
                'media' => apply_filters('the_category', $media), /* Apply filter in order to remove suffixes added by WPML */
                'row' => $row,
				'mrow' => $mrow
            );
        }

        if(is_array($posts))
        {
            echo json_encode(array('status' => 'success', 'posts' => $result, 'hide_show_more' => $hide_show_more));
        }
        else
        {
            echo json_encode(array('status' => 'failure'));
        }
    }
    else
    {
        echo json_encode(array('status' => 'failure'));
    }

    exit;
}

/* Make filter function available through ajax call */
add_action('wp_ajax_filter_campaigns_by_client', 'filter_campaigns_by_client');
add_action('wp_ajax_nopriv_filter_campaigns_by_client', 'filter_campaigns_by_client');

/* Ajax Function that handles filtering work by client */
function filter_campaigns_by_client()
{
    if(isset($_POST['client']) && isset($_POST['numberposts']))
    {
        $args = array(
            'numberposts' => (int)$_POST['numberposts'],
            'offset' => 0,
            'meta_key' => 'campaign_client',
            'meta_value' => $_POST['client'],
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'campaign',
            'post_status' => 'publish',
            'suppress_filters' => 0 /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
        );

        $posts = get_posts($args);

        $result = array();
        /* Reformat result */
        foreach ($posts as $key => $post)
        {
            $url = get_permalink($post->ID);
            $thumbnail = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail');
            $title = apply_filters('the_title', $post->post_title);
            $excerpt = apply_filters('the_excerpt', $post->post_excerpt);
            $row = ceil(($key + 1) / 3);
			$mrow = ceil(($key + 1) / 2);
            $query = parse_url($url, PHP_URL_QUERY);

            $result[] = array(
                'url' => $url,
                'thumbnail' => $thumbnail,
                'title' => $title,
                'excerpt' => $excerpt,
                'date' => the_gate_format_date($post->post_date), /* Apply filter in order to remove suffixes added by WPML */
                'row' => $row,
				'mrow' => $mrow
            );
        }

        if(is_array($posts))
        {
            echo json_encode(array('status' => 'success', 'posts' => $result));
        }
        else
        {
            echo json_encode(array('status' => 'failure'));
        }
    }
    else
    {
        echo json_encode(array('status' => 'failure'));
    }

    exit;
}

/* Make filter function available through ajax call */
add_action('wp_ajax_filter_news_by_category', 'filter_news_by_category');
add_action('wp_ajax_nopriv_filter_news_by_category', 'filter_news_by_category');

/* Ajax Function that handles filter dropdowns */
function filter_news_by_category()
{
    if(isset($_POST['category']) && isset($_POST['numberposts']))
    {
        $args = array(
            'numberposts' => (int)$_POST['numberposts'] + 1,
            'offset' => 0,
            'category' => $_POST['category'],
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => 0 /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
        );

        $posts = get_posts($args);
        $hide_show_more = count($posts) < ((int)$_POST['numberposts'] + 1);

        if(!$hide_show_more) array_pop($posts);

        $result = array();

        /* Reformat result */
        foreach ($posts as $key => $post)
        {
            $url = get_permalink($post->ID);
            $thumbnail = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail');
            $title = apply_filters('the_title', $post->post_title);
            $excerpt = apply_filters('the_excerpt', $post->post_excerpt);
            $category = (int)$_POST['category'] ? get_category($_POST['category'])->name : current(get_the_category($post->ID))->name;
            $row = ceil(($key + 1) / 3);
			 $mrow = ceil(($key + 1) / 2);
            $query = parse_url($url, PHP_URL_QUERY);
            $url = ($query) ? $url . '&category=' . urlencode((int)$_POST['category']) : $url . '?category=' . urlencode((int)$_POST['category']);
			 $postCount = get_post_meta($post->ID, 'fake_views_check', true) ? do_shortcode('[post_view id='.$post->ID.']') : -1;

            $result[] = array(
                'url' => $url,
                'thumbnail' => $thumbnail,
                'title' => $title,
                'excerpt' => $excerpt,
                'date' => the_gate_format_date($post->post_date),
                'category' => apply_filters('the_category', $category), /* Apply filter in order to remove suffixes added by WPML */
                'row' => $row,
				 'mrow' => $mrow,
				 'postCount' => $postCount
            );
        }

        if(is_array($posts))
        {
            echo json_encode(array('status' => 'success', 'posts' => $result, 'hide_show_more' => $hide_show_more));
        }
        else
        {
            echo json_encode(array('status' => $status));
        }
    }
    else
    {
        echo json_encode(array('status' => 'failure'));
    }

    exit;
}


/* ########################## Show More Functions ############################## */


/* Make show more function available through ajax call */
add_action('wp_ajax_show_more', 'show_more');
add_action('wp_ajax_nopriv_show_more', 'show_more');

/* Ajax Function that handles show more buttons */
function show_more()
{
    if(isset($_POST['offset']) && isset($_POST['numberposts']) && isset($_POST['post_type']))
    {
        $taxonomy = isset($_POST['taxonomy']) ? $_POST['taxonomy'] : 'category';
        $category = isset($_POST['category']) ? $_POST['category'] : 0;

        $args = array(
            'numberposts' => (int)$_POST['numberposts'] + 1,
            'offset' => (int)$_POST['offset'],
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => $_POST['post_type'],
            'post_status' => 'publish',
            'suppress_filters' => 0, /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
        );

        if($category) $args = array_merge($args, array('tax_query' => array(
                array(
                     'taxonomy' => $taxonomy,
                     'field' => 'id',
                     'terms' => array((int)$category),
                     'operator' => 'IN'
                )
            ))
        );

        $posts = get_posts($args);
        $hide_show_more = count($posts) < ((int)$_POST['numberposts'] + 1);

        if(!$hide_show_more) array_pop($posts);

        $result = array();

        /* Reformat result */
        foreach ($posts as $key => $post)
        {
            $row            = ceil(((int)$_POST['offset'] + $key + 1) / 3);
			$mrow 			 = ceil(((int)$_POST['offset'] + $key + 1) / 2);
            $category       = get_post_category($post->ID);
            $url            = get_permalink($post->ID);
            $thumbnail      = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail');
            $title          = apply_filters('the_title', $post->post_title);
            $excerpt        = apply_filters('the_excerpt', $post->post_excerpt);
            $get_client     = get_post_meta($post->ID, 'campaign_client', true);
            $client         = isset($get_client) ? get_the_title($get_client) : false;
            $get_campaign   = get_post_meta($post->ID, 'campaign_element_linkto_campaign', true);
            $campaign       = isset($get_campaign['associate_campaign']) ? get_permalink($get_campaign['associate_campaign']) : false;
            $result[]       = array(
                'url'       => $url,
                'thumbnail' => $thumbnail,
                'title'     => $title,
                'excerpt'   => $excerpt,
                'client'    => $client,
                'campaign'  => $campaign,
                'date'      => the_gate_format_date($post->post_date),
                'post_type' => $post->post_type,
                'category'  => apply_filters('the_category', $category), /* Apply filter in order to remove suffixes added by WPML */
                'row'       => $row,
				'mrow' => $mrow,
				'postCount' => get_post_meta($post->ID, 'fake_views_check', true) ? do_shortcode('[post_view id='.$post->ID.']') : -1
            );
        }

        if(is_array($posts))
        {
            echo json_encode(array('status' => 'success', 'posts' => $result, 'hide_show_more' => $hide_show_more));
        }
        else
        {
            echo json_encode(array('status' => 'failure'));
        }
    }
    else
    {
        echo json_encode(array('status' => 'failure'));
    }

    exit;
}



/* Ajax Function that handles show more button on landing page */
add_action('wp_ajax_get_landing_page_posts_ajax', 'get_landing_page_posts_ajax');
add_action('wp_ajax_nopriv_get_landing_page_posts_ajax', 'get_landing_page_posts_ajax');

function get_landing_page_posts_ajax()
{
    if(isset($_POST['offset']) && isset($_POST['numberposts']))
    {
        $post_ids_to_exclude = isset($_POST['exclude_ids']) ? $_POST['exclude_ids'] : array();

        $posts = get_landing_page_posts($_POST['offset'], $_POST['numberposts'], $post_ids_to_exclude);

        /* Reformat result */
        foreach ($posts as $key => $post)
        {

            // $video = get_post_meta($post->ID, 'campaign_element_video', true);

            // WORKING mechanism for getting attached mp4 file URL
            $showmp4 = false;
            $mp4_id = 0;
            $mp4File = '';
            $items = get_post_meta($post->ID, 'campaign_element_mp4', true);
            if ($items) {
                if (array_key_exists('document', $items)) {
                    $showmp4 = true;
                    $mp4_id = $items['document']; // get post_id of attached video file
                    $mp4post = get_post_meta($mp4_id, '_wp_attached_file', false); // get path to attached video file (within uploads directory)
                    $mp4File = wp_upload_dir()['baseurl']."/".$mp4post[0]; // get full URL for video file
                    $mp4scale = ( $items['scale'] != null ) ? $items['scale'] : 1.0;
                    $mp4boxwidth = ( $items['boxwidth'] != null ) ? $items['boxwidth'] : 'random';
                }
            }

            /*
            $swf = get_post_meta($post->ID, 'campaign_element_swf', true);
            $swf_link = isset($swf['addit_url']) && !empty($swf['addit_url']) ? $swf['addit_url'] : '#';
            */

            $imageData = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium-thumbnail');

            // $row            = ceil(((int)$_POST['offset'] + $key + 1) / 3);
            $category       = get_post_category($post->ID);
            $url            = get_permalink($post->ID);
            $thumbnail      = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail');
            // $thumbnailURL   = (has_post_thumbnail($post->ID)) ? get_post_thumbnail_url($post->ID, 'medium-thumbnail') : '-';
            // $thumbnailURL   = get_post_thumbnail_id($post->ID);
            $thumbnailURL   = $imageData[0];
            $title          = apply_filters('the_title', $post->post_title);
            $excerpt        = apply_filters('the_excerpt', $post->post_excerpt);
            $get_client     = get_post_meta($post->ID, 'campaign_client', true);
            $client         = isset($get_client) ? get_the_title($get_client) : false;
            $get_campaign   = get_post_meta($post->ID, 'campaign_element_linkto_campaign', true);
            $campaign       = isset($get_campaign['associate_campaign']) ? get_permalink($get_campaign['associate_campaign']) : false;

            $feature_title = get_post_meta($post->ID, 'campaign_element_feature_title', true);
            $feature_subtitle = get_post_meta($post->ID, 'campaign_element_feature_subtitle', true);

            $result[]       = array(
                'id'        => $post->ID,
                'url'       => $url,
                'thumbnail' => $thumbnail,
                'thumbnailURL' => $thumbnailURL,
                'title'     => $title,
                'excerpt'   => $excerpt,
                'client'    => $client,
                'campaign'  => $campaign,
                'date'      => the_gate_format_date($post->post_date),
                'post_type' => $post->post_type,
                'category'  => apply_filters('the_category', $category), /* Apply filter in order to remove suffixes added by WPML */
                // 'row'       => $row,
                // 'video'     => $video,
                // 'swf'       => $swf,
                // 'swflink'   => $swf_link,
                'mp4'       => $mp4_id,
                'mp4file'   => $mp4File,
                'mp4scale' => $mp4scale,
                'mp4boxwidth' => $mp4boxwidth,
                'feature_title' => $feature_title[0],
                'feature_subtitle' => $feature_subtitle[0]
            );
        }
		
		echo json_encode(array('status' => 'success', 'posts' => $result, 'exclude' => $post_ids_to_exclude, 'template_path' => get_bloginfo('template_directory') ));
		
    }
    else
    {
        echo json_encode(array('status' => 'failure', 'error' => 'no numberposts or offset provided'));
    }

    exit;
}

function get_landing_page_posts($offset = 0, $numberposts = 9, $post_ids_to_exclude = array())
{
    $front_page_id = get_option('page_on_front');
    $predefined_posts_details = get_post_meta($front_page_id, 'landing_page_entries', true);

    /* Add predefined posts to the exclude argument */
    if(is_array($predefined_posts_details))
    {
        foreach ($predefined_posts_details as $value)
        {
            if(isset($value['post_id']) && $value['post_id'] !== 'random')
            {
                $post_ids_to_exclude[] = (int)$value['post_id'];
            }
        }
    }

    $posts = array();

    for ($i=$offset; $i < $offset + $numberposts; $i++) {
        $post = null;

        $args = array(
            'posts_per_page'   => 1,
            'exclude'          => implode(',', $post_ids_to_exclude),
            'suppress_filters' => false
        );
        if(isset($predefined_posts_details[$i]))
        {
            if($predefined_posts_details[$i]['category'] === 'random')
            {
                /* Get a random post from all needed posttypes */
                $args = array_merge($args, array(
                    'post_type' => array('post', 'campaign', 'campaign_element'),
                    'orderby'   => 'rand'
                ));

                $post_array = get_posts($args);
                $post = (is_array($post_array)) ? current($post_array) : $post_array;
            }
            else
            {
                if($predefined_posts_details[$i]['post_id'] === 'random')
                {
                    $args = array_merge($args, array(
                        'post_type' => $predefined_posts_details[$i]['category'],
                        'orderby'   => 'rand'
                    ));

                    $post_array = get_posts($args);
                    $post = (is_array($post_array)) ? current($post_array) : $post_array;
                }
                else
                {
                    $post = get_post((int)$predefined_posts_details[$i]['post_id']);
                }
            }
        }

        if($post)
        {
            $posts[] = $post;
            $post_ids_to_exclude[] = $post->ID;
        }
        else
        {
            /* Get random post if no predefined post is available */
            $args = array(
                'posts_per_page'    => 1,
                // 'post_type'         => array('campaign_element', 'post', 'campaign'),
                'post_type'         => array('campaign_element', 'campaign'),
                'orderby'           => 'rand',
                'exclude'           => implode(',', $post_ids_to_exclude),
                'suppress_filters'  => false
            );

            $post_array = get_posts($args);
            $post = (is_array($post_array)) ? current($post_array) : $post_array;

            if($post)
            {
                $posts[] = $post;
                $post_ids_to_exclude[] = $post->ID;
            }
        }
    }

    return $posts;
}

/* Make send_email_ajax function available through ajax call */
add_action('wp_ajax_send_email', 'send_email_ajax');
add_action('wp_ajax_nopriv_send_email', 'send_email_ajax');

/* Ajax Function that handles sending emails */
function send_email_ajax()
{
    if(isset($_POST['post_id']) && isset($_POST['params']))
    {
        $is_mail_sent = send_email($_POST['post_id'], $_POST['params']);
        $status = ($is_mail_sent) ? 'success' : 'failure';

        echo json_encode(array('status' => $status));
    }
    else
    {
        echo json_encode(array('status' => 'failure'));
    }

    exit;
}

function send_email($post_id, $fields)
{
    $receivers_meta = get_post_meta($post_id, 'contact_emails_emails', true);
    $receivers = !empty($receivers_meta) ? $receivers_meta : array();

    if(isset($_POST['params']['office']))
    {
        $get_receivers = get_office_email_by_id($_POST['params']['office']);
        if(is_array($receivers))
        {
            $receivers = array_merge($receivers, array($get_receivers));
        }
    }

    $main_office_mail = get_office_email_by_id(get_option('office'));
    $receivers = array_merge($receivers, array($main_office_mail));
    $fields_meta = get_post_meta($post_id, 'contact_fields_field');
    $fields_meta = (is_array($fields_meta)) ? current($fields_meta) : false;

    $mail_fields = array();
    if(is_array($fields_meta))
    {
        foreach ($fields_meta as $number => $field)
        {
            if($field['id'] && $field['title'] && isset($fields[$field['id']]))
            {
                $mail_fields[$field['title']] = $fields[$field['id']];
            }
        };
    }

    $mail = new MailHelper($mail_fields, $receivers);
    return $mail->send();
}

/* Function that returns the first page that has the given page template or null if  there is no such a page */
function get_first_page_by_template($template)
{
    $args = array(
        'numberposts'     => 1,
        'offset'          => 0,
        'meta_key'        => '_wp_page_template',
        'post_type'       => 'page',
        'meta_value'      => $template,
        'post_status'     => 'publish',
        'suppress_filters' => 0 /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
    );

    $pages = get_posts($args);

    return (isset($pages) && is_array($pages)) ? current($pages) : null;
}

/* Function that return the first children page of the current post that has the given page template or null if  there is no such a page */
function get_first_children_page_by_template($template, $post = null)
{
    if(!$post)
    {
        global $post;
    }

    if($post->ID)
    {
        $args = array(
            'numberposts'     => 1,
            'offset'          => 0,
            'meta_key'        => '_wp_page_template',
            'post_type'       => 'page',
            'post_parent'     => $post->ID,
            'post_status'     => 'publish',
            'suppress_filters' => 0 /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
        );

        $pages = get_posts(array_merge($args, array('meta_value' => $template)));
    }

    return (isset($pages) && is_array($pages)) ? current($pages) : null;
}

/* Function that returns reordered array using given seed */
function reorder_array_by_seed($array, $seed)
{
    mt_srand($seed);

    $order = array_map(create_function('$val', 'return mt_rand();'), range(1, count($array)));

    if(count($array))
    {
        array_multisort($order, $array);
    }

    return $array;
}

/**
 * Function that returns all posts from given posttype tagged as featured
 *
 * @param (string/array) $post_type
 *
 * @return array
 */
function get_latest_featured_posts($post_type, $numberposts)
{
    $tag = 'featured';
    if(function_exists('icl_register_string'))
    {
        global $sitepress;
        if($sitepress->get_default_language() !== ICL_LANGUAGE_CODE)
        {
            $tag .= '-' . ICL_LANGUAGE_CODE;
        }
    }

    $args = array(
        'numberposts'     => $numberposts,
        'offset'          => 0,
        'post_type'       => $post_type,
        'tag'             => $tag,
        'post_status'     => 'publish',
        'suppress_filters' => 0 /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
    );

    return get_posts($args);
}

/* Function that returns the last n posts */
function get_recent_news($count, $offset = 0, $category = 0)
{
    $args = array(
        'numberposts'     => (int)$count,
        'offset'          => (int)$offset,
        'post_type'       => 'post',
        'category'        => -24, (int)$category, // exclude 'about us' category from news page
        'post_status'     => 'publish',
        'suppress_filters' => 0 /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
    );

    return get_posts($args);
}

/* get items in 'About us' post category used in full-about.php, single-post.php */
function get_about_us_items1($postid)
{
    /*
    $args = array(
        'post_type'       => 'post',
        'category'        => 24, // include only 'about us' category items
        'post_status'     => 'publish',
        'suppress_filters' => 0
    );
    return get_posts($args);
    */

    $about_us_selected_items = get_post_meta($postid, 'about_us_items', true);

    $marked_items = array();

    foreach ($about_us_selected_items as $item) {
        $marked_items[] = $item['selectedaboutus'];
    }

    $about_items = new WP_Query(array(
        'post_type' => 'post',
        'post__in'  => $marked_items,
        'orderby'   => 'post__in'
    ));

    $about_items->get_posts();
    return $about_items;

}

function get_about_us_items2($postid, $mode = 'grid'){
    $items = array();

    // get list of custom posts of type 'about us items' ordered by arbitrary order defined in 'about' page admin panel
    switch ($mode) {
        case "grid" : {

            // display selected items first, then display the rest in random order
            $grid = array();
            $marked_items_list = array();

            // marked items - used to control the order in which items are displayed in the 'about us' page
            $about_us_selected_items = get_post_meta($postid, 'about_us_items', true);


            // get post id(s) of marked item(s)
            foreach ($about_us_selected_items as $item) {
                $marked_items_list[] = $item['markedaboutus'];
            }

            // check to see there are any items in the marked item list
            // if not do not perform the query as it will return ALL about_us items for an empty $marked_items_list
            if (count($marked_items_list) > 0) {
                // get marked item(s)
                $marked_items = new WP_Query(array(
                    'post_type' => array('about_us'),
                    'post__in'  => $marked_items_list,
                    'orderby'   => 'post__in'
                ));
                $marked_items->get_posts();
                $grid = array_merge($grid, $marked_items->posts);
            }

            // get the rest of the items in random order
            $args = array(
                'post_type' => array('about_us'),
                'orderby'   => 'rand',
                'exclude' => $marked_items_list,
                'posts_per_page' => -1
            );
            $random_items = get_posts($args);
            $grid = array_merge($grid, $random_items);

            return $grid;
        } break;
        case "list" : {
            // get all items, ordered by ID, highest first
            $args = array(
                'post_type' => array('about_us'),
                'orderby'   => 'ID'
            );
            $items = get_posts($args);

            return $items;

        } break;
    }
}

/* Function that returns the last n campaign elements */
function get_recent_campaign_elements($count, $offset = 0, $category = 0)
{
    $args = array(
        'numberposts'     => (int)$count,
        'offset'          => (int)$offset,
        'post_type'       => 'campaign_element',
        'category'        => (int)$category,
        'post_status'     => 'publish',
        'suppress_filters' => 0 /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
    );

    return get_posts($args);
}


/* Function that returns the first n related posts */
function get_related_posts($max_posts, $posttypes)
{
    global $post;

    $function_body = 'return (in_array(get_post_type($post_id), explode(",", "' . implode(',', $posttypes) . '")) && $post_id !== ' . $post->ID . ' && (int)$post_id > 0);';

    $related_posts_ids = explode(',', str_replace(' ', '', get_post_meta($post->ID, 'related_posts_ids', true)));

    /* Filter out posts that are not from the allowed posttypes */
    $filtered_ids = array_filter($related_posts_ids, create_function('$post_id', $function_body));
    if($max_posts > -1) $filtered_ids = array_slice($filtered_ids, 0, $max_posts);

    if(is_array($filtered_ids))
    {
        $related_posts = array_map('get_post', $filtered_ids);
    }

    return $related_posts;
}


/* Function that returns the campaign client post */
function get_client()
{
    global $post;

    $client_id = get_post_meta($post->ID, 'campaign_client', true);

    return ((int)$client_id) ? get_post((int)$client_id) : null;
}

/* Function that returns the campaign elements with image */
function get_campaign_elements()
{
    global $post;

    $elements_ids = get_post_meta($post->ID, 'campaign_portfolio_pieces', true);
    if(!is_array($elements_ids) || empty($elements_ids))
    {
        return array();
    }

    $elements = array_map('get_post', $elements_ids);

    return $elements;
}
function filter_campaigns_by_campaign_element($campaign_id)
{
    $elements_ids = get_post_meta($campaign_id, 'campaign_portfolio_pieces', true);
    if(!is_array($elements_ids) || empty($elements_ids))
    {
        return array();
    }

    $elements = array_map('get_post', $elements_ids);

    foreach( $elements as $key => $element)
    {
        return $element->ID;
    }

}
/* Function that gets offices info from shared pool */
function get_offices_information()
{

    $current_language = (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE) ? ICL_LANGUAGE_CODE : 'en';
    $current_language = ($current_language !== 'en') ? $current_language : '';
    $transient_name = ($current_language === '') ? 'offices_information' : 'offices_information_' . $current_language;

    /* Get cached result */
    $transient = get_transient($transient_name);

    if($transient)
    {
        return $transient;
    }
    else
    {
        switch_to_blog(1);
        $args = array(
            'numberposts' => -1,
            'offset' => 0,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'post_type' => 'office',
            'post_status' => 'publish',
            'suppress_filters' => 0
        );

        $offices = get_posts($args);
        $offices_by_id = array();

        foreach ($offices as $office)
        {
            $office->post_title = (get_post_meta($office->ID, 'office_title_' . $current_language, true)) ? get_post_meta($office->ID, 'office_title_' . $current_language, true) : $office->post_title;
            $office->contact_url = get_post_meta($office->ID, 'office_contact_url', true);
            $office->map_url = get_post_meta($office->ID, 'office_map_url', true);
            $office->work_url = get_post_meta($office->ID, 'office_work_url', true);
            $office->phone = get_post_meta($office->ID, 'office_office_phone', true);
            $office->address = get_post_meta($office->ID, 'office_office_address_' . $current_language, true);
            $office->address = (!$office->address) ? get_post_meta($office->ID, 'office_office_address', true) : $office->address;
            $office->office_floor = get_post_meta($office->ID, 'contact_office_floor_' . $current_language, true);
            $office->office_floor = (!$office->office_floor) ? get_post_meta($office->ID, 'contact_office_floor', true) : $office->office_floor;
            $office->street = get_post_meta($office->ID, 'contact_office_street_' . $current_language, true);
            $office->street = (!$office->street) ? get_post_meta($office->ID, 'contact_office_street', true) : $office->street;
            $office->post_code = get_post_meta($office->ID, 'contact_office_post_code_' . $current_language, true);
            $office->post_code = (!$office->post_code) ? get_post_meta($office->ID, 'contact_office_post_code', true) : $office->post_code;
            $office->email = get_post_meta($office->ID, 'contact_office_email', true);
            $office->latitude = get_post_meta($office->ID, 'contact_latitude', true);
            $office->longitude = get_post_meta($office->ID, 'contact_longitude', true);

            $offices_by_id[$office->ID] = $office;
			
        }
        restore_current_blog();

        /* Cache result */
        set_transient($transient_name, $offices_by_id);
    }


    return $offices_by_id;
}

function get_office_email_by_id($office_id)
{
    $offices = get_offices_information();

    foreach($offices as $key => $office)
    {
        if( $key == $office_id)
        {
            return $office->email;
        }
    }
}

/* Function that gets copyright text from shared pool */
function get_copyright_text()
{
    /* Get cached result */
    $transient = get_transient('copyright_text');

    if($transient)
    {
        return $transient;
    }
    else
    {
        switch_to_blog(1);
        $copyright_text = apply_filters('the_content', get_option('copyright-settings'));
        restore_current_blog();

        /* Cache result */
        set_transient('copyright_text', $copyright_text);
    }

    return $copyright_text;
}

function get_clients_latest_campaigns()
{
    $clients = get_posts(array('post_type' => 'client', 'status' => 'publish', 'numberposts' => -1));

    $campaigns = array();

    foreach ($clients as $client)
    {
        $client_latest_campaign = get_posts(array(
            'numberposts' => 1,
            'post_type' => 'campaign',
            'status' => 'publish',
            'meta_key' => 'campaign_client',
            'meta_value' => $client->ID,
            'suppress_filters' => 0,
        ));

        if(!empty($client_latest_campaign))
        {
            $campaigns[$client->ID] = current($client_latest_campaign);
        }
    }

    return $campaigns;
}

function get_media_latest_work()
{
    $media_types = get_terms('media_type', array('taxonomy' => 'media_type'));

    $work = array();
    foreach ($media_types as $media_type)
    {
        $media_type_latest_work = get_posts(array(
            'numberposts' => 1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'media_type',
                    'field' => 'id',
                    'terms' => $media_type->term_id
                )
            ),
            'post_type' => 'campaign_element',
            'status' => 'publish',
            'suppress_filters' => 0,
        ));

        if(!empty($media_type_latest_work))
        {
            $work[$media_type->term_id] = current($media_type_latest_work);
        }
    }

    return $work;
}

function get_media_types_containing_campaign_elements()
{
    $args = array(
        'hide_empty' => 0,
    );

    $media_types = get_terms('media_type', $args);

    return array_filter($media_types, create_function('$type', 'return get_posts(array(
            \'numberposts\' => 1,
            \'tax_query\' => array(
                array(
                    \'taxonomy\' => \'media_type\',
                    \'field\' => \'id\',
                    \'terms\' => $type->term_id
                )
            ),
            \'post_type\' => \'campaign_element\',
            \'status\' => \'publish\',
            \'suppress_filters\' => 0,
        ));')
    );
}


function get_thumbnail_src($post_id, $size = 'full')
{
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), $size);

    return ($thumb && is_array($thumb)) ? $thumb['0'] : '';
}

function get_placeholder_image_src($size)
{
    if(is_array($size))
    {
        if(isset($size[0]) && isset($size[1]))
        {
            $width = (int)$size[0];
            $heigth = (int)$size[1];
        }
    }
    else
    {
        if(in_array($size, get_intermediate_image_sizes()))
        {
            global $_wp_additional_image_sizes;
            if (isset($_wp_additional_image_sizes[$size])) {
                $width = intval($_wp_additional_image_sizes[$size]['width']);
                $height = intval($_wp_additional_image_sizes[$size]['height']);
            } else {
                $width = get_option($size.'_size_w');
                $height = get_option($size.'_size_h');
            }
        }
    }

    if(isset($width) && isset($height))
    {
        return 'http://placehold.it/' . $width . 'x' . $height;
    }

    return null;
}
function get_placeholder_image($size, $alt = 'Image', $classes = false)
{
    $classes = ($classes && is_array($classes)) ? 'class="' . implode(' ', $classes) . '"' : '';

    if(is_array($size))
    {
        if(isset($size[0]) && isset($size[1]))
        {
            $width = (int)$size[0];
            $heigth = (int)$size[1];
        }
    }
    else
    {
        if(in_array($size, get_intermediate_image_sizes()))
        {
            global $_wp_additional_image_sizes;
            if (isset($_wp_additional_image_sizes[$size])) {
                $width = intval($_wp_additional_image_sizes[$size]['width']);
                $height = intval($_wp_additional_image_sizes[$size]['height']);
            } else {
                $width = get_option($size.'_size_w');
                $height = get_option($size.'_size_h');
            }
        }
    }

    if(isset($width) && isset($height))
    {
        return '<img ' . $classes . ' src="http://placehold.it/' . $width . 'x' . $height . '" width="' . $width . '" height="' . $height . '" alt="' . $alt . '"/>';
    }

    return null;
}

function get_wrapper_classes($post)
{
    if (is_search() || isset($_GET['s']))
    {
        $classes = array('sub-page', 'search-wrap', 'contact-wrap');

        return implode(' ', $classes);
    }
    if (is_404())
    {
        $classes = array('sub-page','error-page');

        return implode(' ', $classes);
    }

    if( !isset($post) || !$post )

    return 'sub-page';

    $classes_by_templates = array(
        'full-about.php' => 'about-us',
        'full-media.php' => 'about-us',
        'full-people.php' => 'people',
        'full-work.php' => 'work',
        'full-news.php' => 'news-landing',
        'full-contact.php' => 'contact-wrap',
    );

    $classes_by_post_type = array(
        'campaign' => 'work individual-campaign',
        'campaign_element' => 'work indport-piece',
        'post' => 'news-article',
    );

    $template = (!is_404()) ? basename(get_page_template($post->ID)) : false;

    $current_url = home_url(add_query_arg(array()));
    if(strpos($current_url, 'people-individual'))
    {
        $classes = array('sub-page','people-ind');

        return implode(' ', $classes);
    }

    $classes = array();

    if (!is_front_page())
        $classes[] = 'sub-page';
    if(in_array($template, array_keys($classes_by_templates)))
        $classes[] = $classes_by_templates[$template];
    if(in_array($post->post_type, array_keys($classes_by_post_type)))
        $classes[] = $classes_by_post_type[$post->post_type];
    return implode(' ', $classes);
}

/* Function that returns either the first set category for the post or the first media type or false if none is set */
function get_post_category($post_id)
{
    $post_type = get_post_type($post_id);

    if($post_type == 'post')
    {
        $post_category = get_the_category($post_id);

        return apply_filters('the_category', current($post_category)->name);
    }
    else
    {
        $post_category = get_the_terms($post_id, 'media_type');
        if(is_array($post_category))
        {
           return apply_filters('the_category', current($post_category)->name);
        }
        else
        {
            return false;
        }

    }
}
/* Function that returns either the first set category for the post or the first media type or false if none is set */
function get_post_category_object($post_id)
{
    $post_type = get_post_type($post_id);

    if($post_type == 'post')
    {
        $post_category = get_the_category($post_id);

        return current($post_category);
    }
    else
    {
        $post_category = get_the_terms($post_id, 'media_type');
        if(is_array($post_category))
        {
            return current($post_category);
        }
        else
        {
            return false;
        }

    }
}

function add_br($text)
{
    $text = nl2br($text);
    $text = preg_replace('/(<br *\/?>\s*)+/i', '<br />', $text);

    return $text;
}


function get_office_contact_info_by_id($id)
{
    $offices = get_offices_information();

    return (isset($offices[$id])) ? $offices[$id] : false;
}

/* Make search page display all found entries */
function change_wp_search_size($query) {
    /* Make sure that you are on search page */
    if ( $query->is_search )
        /* -1 to show all post */
        $query->query_vars['posts_per_page'] = -1;

    return $query;
}
add_filter('pre_get_posts', 'change_wp_search_size');

/* When searching an empty string WP does not sends you to searchpage */
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}
add_filter( 'request', 'my_request_filter' );

function admin_thumb($id)
{
    $image = wp_get_attachment_image_src($id, 'admin-thumb', true);
    ?>
    <li><img src="<?php echo $image[0]; ?>" width="100" height="75" /><a href="#" class="the-gate-gallery-remove" data-id="<?php echo $id; ?>"><?php echo __('Remove'); ?></a></li>
    <?php
}

add_action('wp_ajax_get_posts', 'get_posts_by_post_type');
function get_posts_by_post_type()
{
    if( isset($_POST['post_type']))
    {
        $get_posts = get_posts( array(
            'post_type'         => $_POST['post_type'],
            'numberposts'       => -1,
            'suppress_filters'  => 0 )
        );

        $posts = array();
        foreach( $get_posts as $key => $post )
        {
            $featured_image = get_thumbnail_src( $post->ID, array(150, 150) );
            $posts[$key]['id']    = $post->ID;
            $posts[$key]['name']  = $post->post_title . " ({$post->post_name})" ;
            $posts[$key]['src']   = $featured_image ? $featured_image[0] : 0;
        }

        if(is_array($posts))
        {
            echo json_encode(array('status' => 'success', 'posts' => $posts));
        }
        else
        {
            echo json_encode(array('status' => 'failure'));
        }

    }
    else
    {
        echo json_encode(array('status' => 'failure'));
    }

    exit;
}

/* Strips Alt Text from Images (so no back-end rubbish) */
function thumbnail_remove_img_title($atts) {
	unset($atts['alt']);
	return $atts;
}
add_filter('wp_get_attachment_image_attributes','thumbnail_remove_img_title',1,1);

/**
 * Include Theme config
 */
include('config/admin.php');
include('config/public.php');

/* bio_linked_articles: part-people_bio.php

    <?php if(isset($person['featured_articles']) && trim($person['featured_articles']) !== ''): ?>
        <?php
            // Get Blog ID for person
            switch_to_blog(1);
            $office_site = get_post_meta(intval($person['office_id']), 'office_site_association', true);
            restore_current_blog();
            
            // Switch to Blog before Query
            $switch = switch_to_blog($office_site);
            $featured_articles = explode(',', str_replace(' ', '', $person['featured_articles']));
            $filtered_ids = array_filter($featured_articles, create_function('$post_id', 'return get_post_type($post_id) === "post";'));
            $featured_articles = is_array($filtered_ids) ? array_map('get_post', $filtered_ids) : false;
        ?>  
        <?php if($featured_articles): ?>
        <ul class="author-featured-articles">
            <?php foreach($featured_articles as $article): ?>
            <li>
                <a href="<?= get_permalink($article->ID); ?>" target="_blank">
                    <?php
                        if(($article_thumb = get_post_thumbnail_id($article->ID)) !== ""){
                            $thumb_src = wp_get_attachment_image_src($article_thumb);
                            $thumb_src = $thumb_src[0];
                        }
                        else{
                            $thumb_src = get_placeholder_image_src(array(70,70));
                        }
                    ?>
                    <img style="width: 70px !important;" width="70" src="<?= $thumb_src ?>">
                    <header><?= $article->post_title ?></header>
                    <div class="author-featured-info">
                        <time><?php echo the_gate_format_date($article->post_date); ?></time>
                        <?php if(shortcode_exists('post_view') && get_post_meta($article->ID, 'fake_views_check', true)): ?>
                        <div class="views-counter">
                            <img style="width: 20px !important;" width="20" src="<?php bloginfo('template_directory'); ?>/assets/images/news-views.png">
                            <span><?php echo do_shortcode("[post_view id={$article->ID}]"); ?></span>
                        </div>
                        <?php endif; ?>
                    </div>
                </a>
            </li>
            <?php endforeach; ?>
            <li class="more-by-me">
                <a href="<?= get_site_url()."?s=Articles+and+Blogs+by+{$person['first_name']}+{$person['last_name']}" ?>" target="_blank">
                    <p>VIEW MORE ARTICLES</p><span></span>
                </a>
            </li>
        </ul>
        <?php endif; ?>
        <?php if($switch) restore_current_blog(); ?>
    <?php endif; ?>

*/
?>