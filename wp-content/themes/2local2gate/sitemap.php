<?php
/**
 * Template Name: Sitemap
 *
 * @package WordPress
 * @subpackage thegate_local
 */

the_post();
get_header(); ?>

</header>
<!-- main -->
<div id="main">
    <div class="main-holder sitemap">
        <div id="content">
            <h2><?php the_title(); ?></h2>
            <style type="text/css">
				h2{ margin: 0 0 10px !important; }
				.sitemap a{ font: 400 16px/21px "proxima-nova",Arial,Helvetica,Verdana,sans-serif; text-decoration: none; }
				.sitemap a:hover{ font-weight:800; }
			</style>
            <ul>
            	<li><a href="/">Worldwide</a></li>
           		<li><a href="/asia">Asia</a></li>
           		<li><a href="/edinburgh">Edinburgh</a></li>
           		<li><a href="/london">London</a></li>
           		<li><a href="/ny">Home</a></li>
           		<li><a href="/ny/about">About</a></li>
           		<li><a href="/ny/work">Work</a>
               		<ul>
                    	<li><strong>-- By Client:</strong></li>
                    	<?php $campaigns = get_clients_latest_campaigns(); ?>
                       <?php foreach($campaigns as $campaign): ?>
                       	<li><a href="/ny/campaign/<?= $campaign->post_name ?>"><?= $campaign->post_title ?></a></li> 
                       <?php endforeach; ?>
                       <li><strong>-- By Media:</strong></li>
                       <?php $elements = get_recent_campaign_elements(-1); ?>
                       <?php foreach($elements as $element): ?>
                       	<li><a href="/ny/campaign_element/<?= $element->post_name ?>"><?= $element->post_title ?></a></li>
                       <?php endforeach; ?>
                   </ul>
               </li>
               <li><a href="/ny/people">People</a>
               		<ul>
                   		<?php $people = new PeopleHelper(); ?>
                       <?php $lp = get_first_children_page_by_template('part-people_listview.php'); ?>
                       <?php $peopleList = $people->get_list('listview', $lp->ID); ?>
                       <?php $baseURL = get_permalink(get_page_by_path('people/people-individual')); ?>
                       <?php foreach($peopleList as $person): ?>
                       	<li>
                        		<?php
                                	$url = "{$baseURL}?";
									$url .= http_build_query(array('slug' => $person['slug'], 'e' => true), '', '&amp;'); 
								?>
                        		<a href="<?= $url ?>"><?= $person['first_name'].' '.$person['last_name'] ?></a>
                        	</li>
                       <?php endforeach; ?>
                   </ul>
               </li>
               <li>
               		<a href="/ny/news">News</a>
                   <ul>
                   		<?php $allNews = get_recent_news(-1); ?>
                       <?php foreach($allNews as $news): ?>
                       	<li><a href="<?= get_permalink($news->ID) ?>"><?= $news->post_title ?></a></li>
                       <?php endforeach; ?>
                   </ul>
               </li>
               <li><a href="/ny/news/?category=16">Blog</a></li>
               <li><a href="/ny/news/?category=14">Press</a></li>
               <li><a href="/ny/news/?category=1">Published By-Lines</a></li>
               <li><a href="/ny/contact">Contact</a></li>
               <li><a href="/ny/sitemap">Sitemap</a></li>
            </ul>
            <?php /*the_content();*/ ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
