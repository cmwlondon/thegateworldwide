<?php
/**
 * Template Name: Media Values
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $media_values_page;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

/* Get Values */
$values = get_posts(array(
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'numberposts' => -1,
    'post_type' => array('media_value'),
    'suppress_filters' => 0, /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
));

if($media_values_page) : ?>



<div id="values" class="outerBox">
    <div class="innerPadding">
    <h2><?php echo apply_filters('the_title', $media_values_page->post_title); ?></h2>
        <?php foreach ($values as $value) : ?>
            <div style="clear:both;">
                <div class="titleCol"><h3><?php echo apply_filters('the_title', $value->post_title); ?></h3></div>
                <figure class="imageCol"><?php echo (has_post_thumbnail($value->ID)) ? get_the_post_thumbnail($value->ID, 'about-value') : get_placeholder_image('about-value'); ?></figure>
                <div class="textCol">
                    <?php echo apply_filters('the_content', $value->post_content); ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>


<?php endif;