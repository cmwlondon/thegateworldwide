<?php
/*
awards list module
/custom_posts/awards/
custom post type: 'awards'
functions.php -> get_awards();

arranged into 'pages' of four awards each
[1 2 3 4] [5 6]
*/
$awards = get_awards();
$awardCount = count($awards);
?>
		<article class="generalContent awards">
			<h2>Recent Awards</h2>
			<div class="frame">

				<section class="awardPages carousel <?php if ($awardCount < 5) { echo "nocarousel"; } ?> <?php if ($awardCount < 4) echo "awards".$awardCount; ?>">
					<article class="page active">

					<?php
					foreach($awards AS $key => $award) :
						$award_id = $award->ID;
						$metadata = $metadata = get_post_meta($award_id);

						$award_title = $award->post_title;
					    $award_image = $metadata['a_image'][0];
					    $award_year = $metadata['a_year'][0];
					    $award_text = $metadata['a_award'][0];
					    $award_category = $metadata['a_category'][0];

						if ($key > 0 && $key % 4 == 0) :
					?>
					</article>

					<article class="page">
					<?php 
						endif;
					?>
						<div class="award <?php echo "award" . $key; ?>">
							<div class="pictureFrame">
								<img alt="<?php echo  $award_title; ?>" class="logo" src="<?php echo $award_image; ?>">
							</div>
							<div class="text">
								<p class="date"><?php echo $award_year; ?></p>
								<h3><?php echo $award_text; ?></h3>
								<h4><?php echo $award_category; ?></h4>
							</div>
						</div>
					<?php
						endforeach;
					?>
					</article>

					<div class="controls"></div>
					<br class="cb">
				</section>

			</div>
		</article>
