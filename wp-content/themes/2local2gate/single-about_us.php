<?php
/**
 * Single About us item Page (Individual About us item Page)
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $related_posts;

the_post();

$about_listing_page = get_first_page_by_template('full-about.php');
$about_listing_page_permalink = ($about_listing_page) ? get_permalink($about_listing_page->ID) : false;

$items = get_post_meta($post->ID, 'about_us_main_image_thumbnail_id', true);
$file = get_post_meta($items, '_wp_attached_file', true);
$uploads = wp_upload_dir();
$mainImageURL = $uploads["baseurl"]."/".$file;

get_header();
?>
    <!-- visual -->
</header>
<div id="main">
    <div class="outerBox">
        <div class="innerPadding">
            <a href="#" class="title"><?php the_title(); ?></a>
            <h1><?php the_title(); ?></h1>
            <div style="clear:both;">
                <div class="titleCol" style="display:none;"><h3></h3></div>
                <figure class="imageCol"><img src="<?php echo $mainImageURL; ?>" alt="<?php the_title(); ?>"></figure>
                <div class="textCol">
                    <?php
                    $about_us_selected_items = get_post_meta($postid, 'about_us_items', true);

                    echo "<pre>".print_r($about_us_selected_items,true)."</pre>";
                    ?>
                    <?php the_content(); ?>
                    <a href="<?php echo $about_listing_page_permalink; ?>">Back</a>
                </div>

                <figure class="alignRight"></figure>
                <br style="clear:both;">
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>