var PageTransitions = (function() {

	var $main = $( '#pt-main' ),
		$pages = $main.children( 'div.pt-page' ),
		$iterate = $( '#iterateEffects' ),
		$returnPage1 = $( '#returnPage1' ),
		$returnPage2 = $( '#returnPage2' ),
		$returnPage3 = $( '#returnPage3' ),
		animcursor = 1,
		pagesCount = $pages.length,
		current = 0,
		isAnimating = false,
		endCurrPage = false,
		endNextPage = false,
		animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		// animation end event name
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		// support css animations
		support = Modernizr.cssanimations;
	
	function init() {

		$pages.each( function() {
			var $page = $( this );
			$page.data( 'originalClassList', $page.attr( 'class' ) );
		} );

		$pages.eq( current ).addClass( 'pt-page-current' );
	
		$iterate.on( 'mousedown', function() {
			if ($pages.hasClass( 'pt-page-1 pt-page-current' )) {
				if( isAnimating ) {
					return false;
				}
				if( animcursor > 2 ) {
					animcursor = 1;
				}
				nextPage( animcursor );
				++animcursor;
			} 
		} );
		$returnPage1.on( 'mousedown', function() {
			if ($pages.hasClass( 'pt-page-2 pt-page-current' )) {
				if( isAnimating ) {
					return false;
				}
				if( animcursor > 2 ) {
					animcursor = 1;
				}
				nextPage( animcursor );
				++animcursor;
			}
			
		} );
		$returnPage2.on( 'mousedown', function() {
			if ($pages.hasClass( 'pt-page-2 pt-page-current' )) {
				if( isAnimating ) {
					return false;
				}
				if( animcursor > 2 ) {
					animcursor = 1;
				}
				nextPage( animcursor );
				++animcursor;
			} 
		} );
		$returnPage3.on( 'mousedown', function() {
			if ($pages.hasClass( 'pt-page-2 pt-page-current' )) {
				if( isAnimating ) {
					return false;
				}
				if( animcursor > 2 ) {
					animcursor = 1;
				}
				nextPage( animcursor );
				++animcursor;
			}
			
		} );
	}

	function nextPage( animation ) {

		if( isAnimating ) {
			return false;
		}

		isAnimating = true;
		
		var $currPage = $pages.eq( current );

		if( current < pagesCount - 1 ) {
			++current;
		}
		else {
			current = 0;
		}

		var $nextPage = $pages.eq( current ).addClass( 'pt-page-current' ),
			outClass = '', inClass = '';

		switch( animation ) {
			case 1:
				outClass = 'pt-page-moveToLeft';
				inClass = 'pt-page-moveFromRight';
				break;
			case 2:
				outClass = 'pt-page-moveToRight';
				inClass = 'pt-page-moveFromLeft';
				break;
		}

		$currPage.addClass( outClass ).on( animEndEventName, function() {
			$currPage.off( animEndEventName );
			endCurrPage = true;
			if( endNextPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		$nextPage.addClass( inClass ).on( animEndEventName, function() {
			$nextPage.off( animEndEventName );
			endNextPage = true;
			if( endCurrPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		if( !support ) {
			onEndAnimation( $currPage, $nextPage );
		}

	}

	function onEndAnimation( $outpage, $inpage ) {
		endCurrPage = false;
		endNextPage = false;
		resetPage( $outpage, $inpage );
		isAnimating = false;
	}

	function resetPage( $outpage, $inpage ) {
		$outpage.attr( 'class', $outpage.data( 'originalClassList' ) );
		$inpage.attr( 'class', $inpage.data( 'originalClassList' ) + ' pt-page-current' );
	}

	init()

	return { init : init };
	

})();