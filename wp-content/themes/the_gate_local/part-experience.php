<?php
/**
 * Template Name: About Experience
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $experience_page, $experience_references;

wp_enqueue_script('fancybox_pack');
/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}
if($experience_page) : ?>
<li id="experience">
    <h2><?php echo apply_filters('the_title', get_the_title($experience_page->ID)); ?></h2>
    <a href="#" class="opener"><?php echo apply_filters('the_title', get_post_meta($experience_page->ID, 'menu_label', true)); ?></a>
    <div class="slide">
        <figure class="alignright border"><?php echo (has_post_thumbnail($experience_page->ID)) ? get_the_post_thumbnail($experience_page->ID, 'about-experience') : get_placeholder_image('about-experience'); ?></figure>
        <div class="text indent">
            <h3><?php echo get_post_meta($experience_page->ID, 'about_subtitle_subtitle', true); ?></h3>
            <?php echo apply_filters('the_content', $experience_page->post_content); ?>
            <div class="roll-wrap">
                <ul class="roll">
                    <?php $first_category_references = get_post_meta($experience_page->ID, 'experience_references_first_category_references', true); ?>
                    <?php if(is_array($first_category_references)) : ?>
                        <?php $count = 1; foreach ($first_category_references as $reference) :
                            $first_list_category = !empty($reference['first_category']) ? add_br($reference['first_category']) : '';
                            $second_list_category = !empty($reference['second_category']) ? add_br($reference['second_category']) : '';
                        ?>
                            <?php if(!empty($reference['text']) && trim($reference['text']) !== '') : ?>
                                <li>
                                    <?php if(!empty($reference['first_category']) || !empty($reference['second_category'])): ?>
                                        <a class="fancybox" rel="fancybox" href="#left-cat<?php echo $count; ?>"><?php echo $reference['text']; ?></a>
                                        <div id="left-cat<?php echo $count; ?>" class="modal-window">
                                            <span class="top-close"></span>
                                            <h3><?php _e('Category experience:'); ?></h3>
                                            <h2><?php echo $reference['text']; ?></h2>
                                            <div>
                                                <?php echo $first_list_category ?>
                                            </div>
                                            <div>
                                                <?php echo $second_list_category ?>
                                            </div>
                                            <span class="bottom-close"></span>
                                        </div>
                                    <?php else : ?>
                                        <p><?php echo $reference['text']; ?></p>
                                    <?php endif; ?>
                                </li>
                            <?php endif; ?>
                        <?php $count++; endforeach; ?>
                    <?php endif; ?>
                </ul>
                <ul class="roll">
                    <?php $second_category_references = get_post_meta($experience_page->ID, 'experience_references_second_category_references', true); ?>
                    <?php if(is_array($second_category_references)) : ?>
                        <?php $count = 1; foreach ($second_category_references as $reference) :
                            $first_list_category = !empty($reference['first_category']) ? add_br($reference['first_category']) : '';
                            $second_list_category = !empty($reference['second_category']) ? add_br($reference['second_category']) : '';
                        ?>
                            <?php if(!empty($reference['text']) && trim($reference['text']) !== '') : ?>
                                <li>
                                    <?php if(!empty($reference['first_category']) || !empty($reference['second_category'])): ?>
                                        <a class="fancybox" rel="fancybox" href="#right-cat<?php echo $count; ?>"><?php echo $reference['text']; ?></a>
                                        <div id="right-cat<?php echo $count; ?>" class="modal-window">
                                            <span class="top-close"></span>
                                            <h3><?php _e('Category experience:'); ?></h3>
                                            <h2><?php echo $reference['text']; ?></h2>
                                            <div>
                                                <?php echo $first_list_category; ?>
                                            </div>
                                            <div>
                                                <?php echo $second_list_category; ?>
                                            </div>
                                            <span class="bottom-close"></span>
                                        </div>
                                    <?php else : ?>
                                        <p><?php echo $reference['text']; ?></p>
                                    <?php endif; ?>
                                </li>
                            <?php endif; ?>
                        <?php $count++; endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</li>
<?php endif;