<?php
/**
 * Front page
 *
 * @package WordPress
 * @subpackage 2local2gate
 */
the_post();

// $homepage_images_ids = array_filter(explode(',', get_post_meta($post->ID, 'gallery_thumbs', true)), 'wp_get_attachment_image');
// $random_image_id = (!empty($homepage_images_ids)) ? $homepage_images_ids[array_rand($homepage_images_ids)] : false;
$get_content = $post->post_content;

/*
$posts = get_landing_page_posts(0, 20);
// Check if there are more than 9 posts published
$hide_show_more = count($posts) < 10;
// Remove the last post
if(!$hide_show_more)
{
    array_pop($posts);
}
*/

// get selected home boxes
// functions.php line 656
$home_boxes =  get_home_boxes($post->ID);

wp_enqueue_script('show_more_landing_page');
wp_enqueue_script('simply_scroll');
wp_enqueue_script('isotope_grid');
wp_enqueue_script('masonry_grid');
wp_enqueue_script('froogaloop_min');

get_header();?>

    <!-- visual -->
    <!-- 
    <div class="visual">
        <div class="visual-holder">
            <?php echo ($random_image_id) ? wp_get_attachment_image($random_image_id, 'homepage-thumbnail') : get_placeholder_image('homepage-thumbnail'); ?>
        </div>
        <div class="scroll-hint">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/scroll-btn.png" alt="" width="40" height="41">
        </div>
    </div>
-->
<script type="text/javascript">
</script>
</header>
<!-- main -->
<div id="main" class="office-home" style="margin-top:0px;display: none">
    <div class="main-holder">
        <div id="content">
            <h2 class="heading"><?php the_title(); ?></h2>

            <div class="list-area">
                <div class="gutter-sizer"></div>
                <div class="grid-sizer"></div>
                <ul class="isotopeList homeList">
                    <!--
                    <li class="itemTemplate">
                        <div class="holder">
                            <a class="overlayLink"></a>
                            <figure>
                                <div class="videoFrame videoNotReady"></div>
                            </figure>
                            <header>
                                <h3></h3>
                                <p></p>
                            </header>
                        </div>
                    </li>
                    -->
                    <?php
		    
		    $sections = array();
		    
		    
                    foreach ($home_boxes AS $box) {
			     $videoClass = $box['hasvideo'] ? ' class="hasVideo"' : ' class="hasVideo"';
			     $videoAnchor = $box['hasvideo'] ? '<a class="overlayLink" data-video="'.$box['video'].'"></a>' : '';
			     
			     $html = 
				     '<li' . $videoClass . ' id="homebox' . $box['id'] . '">'."\n".
						'<div class="holder">'."\n".
							$videoAnchor."\n".
							'<figure class="tilebackground">'."\n".
								'<img alt="" src="' . $box['tilebackground']. '">'."\n".
							'</figure>'."\n".
							'<header>'."\n".
								'<h3>'.$box['title'].'</h3>'."\n".
								'<p>' .$box['copy'] . '</p>'."\n".
							'</header>'."\n".
							'<figure class="tile">'."\n".
								'<img alt="" src="'.$box['tile'].'">'."\n".
							'</figure>'."\n".
						'</div>'."\n".
					'</li>';
			     
			     $tag = $box['tag'];
			     $sections[$tag] = $sections[$tag] ? : '';
			     $sections[$tag] .= $html;
		    }

		    echo implode('',$sections);
                    ?>
                 </ul>

                <!--
                <div class="show-more-work">
                    <a id="show-more-landing-posts" data-exclude-ids="<?php echo implode(',', $exclude_ids); ?>" <?php echo ($hide_show_more) ? 'class="hidden"' : ''; ?> href="#"><p><?php _e('SHOW MORE', 'Local Home Page'); ?></p><span></span></a>
                </div>
                -->
            </div>
        </div>
    </div>
</div>

<div class="shroud"></div>
<div class="overlay" id="videoPlayer">
    <div class="container">
        <a href="" class="closer">Close</a>
        <div class="videoWrapper">
            <!--
            <video preload="yes" width="600" height="400" controls onloadeddata="hbLoaded(this);">
                <source src="http://local.thegateworldwide.com/london/wp-content/uploads/sites/4/2016/02/PDSA.m4v" type="video/mp4"></source>
            </video>
            -->
        </div>
    </div>
</div>

<?php
    /* Set js variable containing path to ajax handling php script */
    wp_localize_script( 'show_more_landing_page', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ), 'excludeIds' => $exclude_ids, 'items' => $items, 'formerPath' => get_bloginfo('template_directory') .'/assets/images/former_408_229.gif'  ) );
    get_footer();
?>
