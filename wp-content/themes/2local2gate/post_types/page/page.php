<?php

/* Remove content support for "page" post type in the page's template is "capabilities" - Fixes #3237 */

/* add_action('init', 'remove_capabilities_content');

function remove_capabilities_content()
{
    $post_id = WPAlchemy_MetaBox::_get_post_id();
    $post_template = get_post_meta($post_id,'_wp_page_template',TRUE);

    if($post_template == 'capabilities')
    {
        remove_post_type_support( 'page', 'editor' );
    }
}

*/

/* Custom Meta Boxes */

$front_page_id = get_option('page_on_front');
$front_page_id = ($front_page_id) ? $front_page_id : -1;

/* Register Page Metabox 'References' */
$capabilities_references = new WPAlchemy_MetaBox(array
    (
    'id'        => 'capabilities_references',
    'title'     => __('References'),
    'types'     => array('page'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'low',
    'template'  => get_template_directory() . '/post_types/page/metaboxes/capabilities_references.php',
    'autosave'  => true,
    'prefix'    => 'capabilities_references_',
    'include_template' => array('part-capabilities.php', 'part-media_capabilities.php'),
    'save_filter' => 'gate_capabilities_references_save_filter',
));

/* Register Page Metabox 'Menu Label' */
$submenu_label = new WPAlchemy_MetaBox(array
    (
    'id'        => 'submenu_label',
    'title'     => __('Menu Label'),
    'types'     => array('page'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'low',
    'template'  => get_template_directory() . '/post_types/page/metaboxes/menu_label.php',
    'autosave'  => true,
    'prefix'    => 'menu_',
    'lock'      => WPALCHEMY_LOCK_AFTER_POST_TITLE,
));

/* Register Page Metabox 'Top Text' only on capabilities template */
$capabilities_top_text = new WPAlchemy_MetaBox(array
    (
    'id'        => 'capabilities_top_text',
    'title'     => __('Top Text'),
    'types'     => array('page'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'low',
    'template'  => get_template_directory() . '/post_types/page/metaboxes/capabilities_top_text.php',
    'autosave'  => true,
    'prefix'    => 'capabilities_top_text_',
    'include_template' => array('part-capabilities.php', 'part-media_capabilities.php'),
    'lock'      => WPALCHEMY_LOCK_AFTER_POST_TITLE,
    'hide_editor' => TRUE,
));
/* Register Page Metabox 'Subtitle'  only on defined templates */
$about_subtitle = new WPAlchemy_MetaBox(array
    (
    'id'        => 'about_subtitle',
    'title'     => __('Subtitle'),
    'types'     => array('page'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'low',
    'template'  => get_template_directory() . '/post_types/page/metaboxes/subtitle.php',
    'autosave'  => true,
    'prefix'    => 'about_subtitle_',
    'include_template' => array('part-capabilities.php', 'part-media_capabilities.php', 'full-about.php', 'full-media.php', 'part-experience.php', 'part-media_experience.php', 'part-philosophy.php', 'part-media_philosophy.php'),
    'lock'      => WPALCHEMY_LOCK_AFTER_POST_TITLE,
));

/* Register Page Metabox 'Bottom Text'  only on capabilities template */
$capabilities_bottom_text = new WPAlchemy_MetaBox(array
    (
    'id'        => 'capabilities_bottom_text',
    'title'     => __('Bottom Text'),
    'types'     => array('page'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'low',
    'template'  => get_template_directory() . '/post_types/page/metaboxes/capabilities_bottom_text.php',
    'autosave'  => true,
    'prefix'    => 'capabilities_bottom_text_',
    'include_template' => array('part-capabilities.php', 'part-media_capabilities.php'),
    'lock'      => WPALCHEMY_LOCK_AFTER_POST_TITLE,
    'hide_editor' => TRUE,
));

/* Register Page Metabox 'References' */
$experience_references = new WPAlchemy_MetaBox(array
    (
    'id'                => 'experience_references',
    'title'             => __( 'References' ),
    'types'             => array( 'page' ),
    'context'           => 'normal',
    'mode'              => WPALCHEMY_MODE_EXTRACT,
    'priority'          => 'low',
    'template'          => get_template_directory() . '/post_types/page/metaboxes/experience_references.php',
    'autosave'          => true,
    'prefix'            => 'experience_references_',
    'include_template'  => array( 'part-experience.php', 'part-media_experience.php' ),
    'save_filter'       => 'gate_capabilities_references_save_filter',
));

/* Register Page Metabox 'Contact field' */
$contact_fields = new WPAlchemy_MetaBox(array
    (
    'id'                => 'contact_fields',
    'title'             => __( 'Contact Fields' ),
    'types'             => array( 'page' ),
    'context'           => 'normal',
    'mode'              => WPALCHEMY_MODE_EXTRACT,
    'priority'          => 'low',
    'template'          => get_template_directory() . '/post_types/page/metaboxes/contact_fields.php',
    'autosave'          => true,
    'prefix'            => 'contact_fields_',
    'include_template'  => array( 'part-local.php' ),
    'hide_editor' => TRUE,
));

/* Register Page Metabox 'Contact Emails' */
$contact_emails = new WPAlchemy_MetaBox(array
    (
    'id'                => 'contact_emails',
    'title'             => __( 'Contact Emails' ),
    'types'             => array( 'page' ),
    'context'           => 'normal',
    'mode'              => WPALCHEMY_MODE_EXTRACT,
    'priority'          => 'low',
    'template'          => get_template_directory() . '/post_types/page/metaboxes/contact_emails.php',
    'autosave'          => true,
    'prefix'            => 'contact_emails_',
    'include_template'  => array( 'part-local.php' ),
));

/* Register Page Metabox 'Contact Messages' */
$contact_messages = new WPAlchemy_MetaBox(array
    (
    'id'                => 'contact_messages',
    'title'             => __( 'Contact Messages' ),
    'types'             => array( 'page' ),
    'context'           => 'normal',
    'mode'              => WPALCHEMY_MODE_EXTRACT,
    'priority'          => 'low',
    'template'          => get_template_directory() . '/post_types/page/metaboxes/contact_messages.php',
    'autosave'          => true,
    'prefix'            => 'contact_messages_',
    'include_template'  => array( 'part-local.php' ),
));

/* Register Page Metabox 'Leadership allowed offices' */
$leadership_allowed_offices = new WPAlchemy_MetaBox(array
    (
    'id'                => 'leadership_allowed_offices',
    'title'             => __( 'Offices to be presented' ),
    'types'             => array( 'page' ),
    'context'           => 'normal',
    'mode'              => WPALCHEMY_MODE_EXTRACT,
    'priority'          => 'low',
    'template'          => get_template_directory() . '/post_types/page/metaboxes/leadership_allowed_offices.php',
    'autosave'          => true,
    'prefix'            => 'leadership_allowed_offices_',
    'include_template'  => array( 'part-people_leadership.php' ),
    'hide_editor' => TRUE,
    'save_filter'       => 'clear_duplicates',
    'save_action'       => 'clear_leaders_transients'
));

/* Register Page Metabox 'Filter People By Office' */
$filter_people = new WPAlchemy_MetaBox(array
    (
    'id'                => 'filter_people',
    'title'             => __( 'Filter People By Office' ),
    'types'             => array( 'page' ),
    'context'           => 'normal',
    'mode'              => WPALCHEMY_MODE_EXTRACT,
    'priority'          => 'low',
    'template'          => get_template_directory() . '/post_types/page/metaboxes/filter_people.php',
    'autosave'          => true,
    'prefix'            => 'filter_people_',
    'include_template'  => array( 'part-people_everyone.php', 'part-people_listview.php' ),
    'hide_editor' => TRUE,
    'save_filter'       => 'clear_duplicates',
    'save_action'       => 'clear_everyone_and_listview_transients'
));

/* Register Page Metabox Gallery for front page */

/*
$homepage_gallery = new WPAlchemy_MetaBox( array(
    'id'        => 'homepage_gallery',
    'title'     => 'Homepage Images',
    'types'     => array( 'page' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'template'  => get_template_directory() . '/post_types/page/metaboxes/frontpage_gallery.php',
    'include_post_id' => $front_page_id,
    'autosave'  => true,
));
*/

/* Register Page Metabox 'PostOnLandingPage' */
/*
$landing_page_posts = new WPAlchemy_MetaBox(array
(
'id'        => 'posts',
'title'     => __('Posts on Landing Page'),
'types'     => array('page'),
'context'   => 'normal',
'priority'  => 'default',
'mode'      => WPALCHEMY_MODE_EXTRACT,
'autosave'  => true,
'include_post_id' => $front_page_id,
'prefix'    => 'landing_page_',
'template'  => get_template_directory() . '/post_types/page/metaboxes/landing_page_entries.php',
));
*/
$landing_page_posts = new WPAlchemy_MetaBox(array
(
'id'        => 'homeboxes',
'title'     => __('Displayed Items'),
'types'     => array('page'),
'context'   => 'normal',
'priority'  => 'high',
'mode'      => WPALCHEMY_MODE_EXTRACT,
'autosave'  => true,
'include_post_id' => 2582, // stories page #2582
'prefix'    => 'landing_page_',
'template'  => get_template_directory() . '/post_types/page/metaboxes/landing_page_homeboxes.php',
));

/* london people dropdown used to select leadership */
$london_leadership = new WPAlchemy_MetaBox(array
(
'id'        => 'londonleaders',
'title'     => __('London Leadership'),
'types'     => array('page'),
'include_post_id' => 50, // only show this item in the admin page for the 'people' page
'context'   => 'normal',
'priority'  => 'high',
'mode'      => WPALCHEMY_MODE_EXTRACT,
'autosave'  => true,
'prefix'    => 'london_leadership_',
'template'  => get_template_directory() . '/post_types/page/metaboxes/london_leadership.php',
));

// work page selected campaigns
$selected_works = new WPAlchemy_MetaBox(array
(
'id'        => 'selectedworks',
'title'     => __('Selected works'),
'types'     => array('page'),
'include_post_id' => 67, // only show this item in the admin page for the 'work' page
'context'   => 'normal',
'priority'  => 'high',
'mode'      => WPALCHEMY_MODE_EXTRACT,
'autosave'  => true,
'prefix'    => 'selected_works_',
'template'  => get_template_directory() . '/post_types/page/metaboxes/selected_works.php',
));

$selected_about_us = new WPAlchemy_MetaBox(array(
    'id'        => 'markedaboutus',
    'title'     => __('Selected About Us Items'),
    'types'     => array('page'),
    'include_post_id' => 12, // only show this item in the admin page for the 'work' page
    'context'   => 'normal',
    'priority'  => 'high',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'autosave'  => true,
    'prefix'    => 'about_us_',
    'template'  => get_template_directory() . '/post_types/page/metaboxes/about_us_items.php',
));

/* Sanitize URL's */
function add_http( &$item, $key )
{
    if( $key == 'url' )
    {
        $validation = preg_match( '/^(http|https):\/\/([a-z0-9-]\.)*/i', $item );

        if( !$validation && trim( $item ) !== '' )
        {
            $item = 'http://' . $item;
        }
    }
}

function clear_duplicates( $meta, $post_id )
{
    if(isset($meta['offices']) && is_array($meta['offices']))
    {
        $meta['offices'] = array_map('unserialize', array_unique(array_map('serialize', $meta['offices'])));
    }

    return $meta;
}

function gate_capabilities_references_save_filter( $meta, $post_id )
{
    if( $meta )
    {
        array_walk_recursive( $meta, 'add_http' );
    }

    return $meta;
}

function clear_everyone_and_listview_transients()
{
    delete_transient('everyone_info');
    delete_transient('listview');
}

function clear_leaders_transients()
{
    delete_transient('leaders_info');
}
