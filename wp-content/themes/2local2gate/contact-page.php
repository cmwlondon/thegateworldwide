<?php
/**
 * Template name: Contact-2016
 *
 * @package WordPress
 * @subpackage 2local2gate
 */
get_header();
?>

<div id="contact-page" class="container">
	<div class="row">
		<div class="col-md-12">
			<span id="success" class="hidden">Thank you for contacting us. We will get back to you shortly.</span>
			<span id="fail" class="hidden">Please try again later.</span>
		</div>
	</div>

	<div class="row">
		<div class="form col-md-12">
			<form class="contact" action="http://thegate-stackworks.rhcloud.com/london/contact/" method="POST">
				<div class="row">
					<div class="col-md-12 text-right">
<!--						<input type="submit" name="send" value="" class="glyphicon glyphicon-send" />-->
				
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 smallFields">
						<div class="contact-field ">
                                                    <input class="textsend-color" id="username" data-defaultvalue="NAME" type="text" placeholder="NAME" name="username">
                                                       
							<span class="error-msg"><span class="left-arrow"></span><span class="text">Please enter your name</span></span>
						</div>
						<div class="contact-field ">
                                                    <input class="textsend-color" id="email" data-defaultvalue="EMAIL" type="text" placeholder="EMAIL" name="email">

							<span class="error-msg"><span class="left-arrow"></span><span class="text">Please enter your email</span></span>
						</div>
					</div>
					<div class="col-md-6 textBoxWrapper">
						<div class="contact-field text-right">
                                                    <button><span class="glyphicon glyphicon-send"></span></button>
                                                    <textarea class="textarea-override textsend-color" id="message" data-defaultvalue="MESSAGE" cols="40" rows="8" name="message" placeholder="MESSAGE"></textarea>

							<span class="error-msg"><span class="left-arrow"></span><span class="text">Please enter your message</span></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="contact-success box-edit">
						<p>THANK-YOU FOR YOUR ENQUIRY</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="story_boxes">
        <div class="col-md-4 padnone">
        	<div class="clip"><div><img src="<?php bloginfo('template_directory') . _e('/assets/images/box1.jpg') ?>" width="100%" /></div></div>
        	<p class="transform">Watch our <span>TRANSFORMATION</span> stories</p>
        	<a href="<?php echo site_url() ?>/stories/#transformation"></a>
        </div>
        <div class="col-md-4 padnone">
    		<div class="clip"><div><img src="<?php bloginfo('template_directory') . _e('/assets/images/box2.jpg') ?>" width="100%" /></div></div>
			<p class="invigorate">Watch our <span>REINVIGORATION</span> stories</p>        	
        	<a href="<?php echo site_url() ?>/stories/#reinvigoration"></a>
        </div>
        <div class="col-md-4 padnone">
			<div class="clip"><div><img src="<?php bloginfo('template_directory') . _e('/assets/images/box3.jpg') ?>" width="100%" /></div></div>
			<p class="initiate">Watch our <span>INITIATION</span> stories</p>        	
			<a href="<?php echo site_url() ?>/stories/#initiation"></a>
        </div>
</div>


<?php
get_footer();
