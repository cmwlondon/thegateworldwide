<?php
$slideshowPrefix = 'cs1';
$itemArray = explode(',', $values[$slideshowPrefix.'_order']);
$itemString = '';
foreach ( $itemArray AS $itemArrayItem ) {
	$itemString = ($itemString != '') ? $itemString.',' : $itemString;
	$itemString = $itemString . "'$itemArrayItem'";
}

/*
useless buggy php a single *ESCAPED* quote in a serialized string breaks unserialize()
https://davidwalsh.name/php-serialize-unserialize-issues
//to safely serialize
$safe_string_to_store = base64_encode(serialize($multidimensional_array));

//to unserialize...
$array_restored_from_db = unserialize(base64_decode($encoded_serialized_string));
*/
$csSlides = unserialize(base64_decode($values[$slideshowPrefix.'_items']));


?>

<div class="customField itemChooser <?php echo $slideshowPrefix; ?>SlideItems">
	<p>Add a single slide for a static image, multiple slides for a slideshow. Use the 'delay' parameter to set how long each slide is displayed for. Drag and drop slides to set the order in which they are displayed.</p>
	<input type="hidden" id="<?php echo $slideshowPrefix; ?>_order" name="<?php echo $slideshowPrefix; ?>_order" value="<?php echo $values[$slideshowPrefix . '_order'] ?>">
	<input type="hidden" id="<?php echo $slideshowPrefix; ?>_nextid" name="<?php echo $slideshowPrefix; ?>_nextid" value="<?php if ($values[$slideshowPrefix . '_nextid'] == '' ) { echo '1'; } else { echo $values[$slideshowPrefix . '_nextid']; } ?>">

	<div class="item clearfix template">
		<div class="sizer">
			<label><span>alt</span><input type="text" id="<?php echo $slideshowPrefix; ?>_NNXNN_alt" name="<?php echo $slideshowPrefix; ?>_NNXNN_alt" value=""></label>
			<label><span>delay (in milliseconds)</span><input type="text" id="<?php echo $slideshowPrefix; ?>_NNXNN_delay" name="<?php echo $slideshowPrefix; ?>_NNXNN_delay" value=""></label>
			<div class="customField imageSelector">
				<input type="hidden" id="<?php echo $slideshowPrefix; ?>_NNXNN_image" name="<?php echo $slideshowPrefix; ?>_NNXNN_image" value="">
				<input type="hidden" id="<?php echo $slideshowPrefix; ?>_NNXNN_image_id" name="<?php echo $slideshowPrefix; ?>_NNXNN_image_id" value="" class="ident">
				<input type="hidden" id="<?php echo $slideshowPrefix; ?>_NNXNN_image_w" name="<?php echo $slideshowPrefix; ?>_NNXNN_image_w" value="" class="width">
				<input type="hidden" id="<?php echo $slideshowPrefix; ?>_NNXNN_image_h" name="<?php echo $slideshowPrefix; ?>_NNXNN_image_h" value="" class="height">
				<a href="" class="cs1_NNXNN_Picker picker action"><img class="imageThumbnail" alt="" src="" style="opacity:0;"></a>
			</div>
			<a href="#" class="removeItem">Remove item</a>
		</div>
	</div>

	<a href="#" class="addItem">Add new item</a>


	<div class="items" id="<?php echo $slideshowPrefix; ?>List">

	<?php
		foreach ( $csSlides AS $csSlide ) :
		$ip = wp_get_attachment_image_src($csSlide['picid'],'full');

	?>
		<div class="item clearfix" id="<?php echo stripslashes($csSlide['id']); ?>">
			<div class="sizer">
				<label><span>alt</span><input type="text" id="<?php echo $csSlide['id']; ?>_alt" name="<?php echo $csSlide['id']; ?>_alt" value="<?php echo $csSlide['alt']; ?>"></label>
				<label><span>delay (in milliseconds)</span><input type="text" id="<?php echo $csSlide['id']; ?>_delay" name="<?php echo $csSlide['id']; ?>_delay" value="<?php echo $csSlide['delay']; ?>"></label>
				<div class="customField imageSelector">
					<input type="hidden" id="<?php echo $csSlide['id']; ?>_image" name="<?php echo $csSlide['id']; ?>_image" value="<?php echo $ip[0]; ?>">
					<input type="hidden" id="<?php echo $csSlide['id']; ?>_image_id" name="<?php echo $csSlide['id']; ?>_image_id" value="<?php echo $csSlide['picid']; ?>" class="ident">
					<input type="hidden" id="<?php echo $csSlide['id']; ?>_image_w" name="<?php echo $csSlide['id']; ?>_image_w" value="<?php echo $csSlide['width']; ?>" class="width">
					<input type="hidden" id="<?php echo $csSlide['id']; ?>_image_h" name="<?php echo $csSlide['id']; ?>_image_h" value="<?php echo $csSlide['height']; ?>" class="height">
					<a href="" class="<?php echo $csSlide['id']; ?>_Picker picker action"><img class="imageThumbnail" alt="" src="<?php echo $ip[0]; ?>"></a>
				</div>
				<a href="#" class="removeItem">Remove item</a>
			</div>
		</div>
	<?php endforeach; ?>

	</div>
	<a href="#" class="addItem">Add new item</a>

</div>

<script type="text/javascript">
var imagePickers = [],
	clientLogoItemTemplate,
	clientLogoItemsBox,
	orderfield,
	newIDfield,
	countfield,
	items,
	prefix = '<?php echo $slideshowPrefix; ?>',
	preexisting = [<?php echo $itemString; ?>];

jQuery(document).ready(function(){
	clientLogoItemTemplate = jQuery('.' + prefix + 'SlideItems .template');
	clientLogoItemsBox = jQuery('.' + prefix + 'SlideItems .items');
	orderfield = jQuery('#' + prefix + '_order');
	newIDfield = jQuery('#' + prefix + '_nextid');

	// bind image picker to pre-existing items
	var clIndex = 0;
	do {
		imagePickers.push(new MediaPanel({
	        "trigger" : jQuery('.' + preexisting[clIndex] + '_Picker')
	    }));

		clIndex++;
	} while (clIndex < preexisting.length );

	// bind 'remove item' action to pre-existing items
 	jQuery('.items .removeItem').on('click',function(e){
 		e.preventDefault();
 		var itemToDelete = jQuery(this).parents('.item'),
 			itemToDeleteID = itemToDelete.attr('id');

 		itemToDelete.remove();

		items = [];
		jQuery('.item').not('.template').each(function(i){
			items.push(jQuery(this).attr('id'));
		});
		orderfield.val(items.join(','));

 	});

 	// bind and new item action
 	jQuery('.' + prefix + 'SlideItems .addItem').on('click',function(e){
 		e.preventDefault();
 		console.log('a');

 		newIDfield.css({"border" : "1px solid red"});

 		var newID = newIDfield.val();

 		var newItem = clientLogoItemTemplate.clone();

 		var v = newItem.html(),
 			r = new RegExp(/NNXNN/, 'g');
 		v = v.replace(r, newID);

 		newItem.html(v);

 		newItem.attr({"id" : prefix + "_" + newID});
 		newItem.removeClass('template');

 		// bind remove item action to new items
	 	newItem.find('.removeItem').on('click',function(e){
	 		e.preventDefault();
	 		var itemToDelete = jQuery(this).parents('.item'),
	 			itemToDeleteID = itemToDelete.attr('id')

	 		itemToDelete.remove();

			items = [];
			jQuery('.item').not('.template').each(function(i){
				items.push(jQuery(this).attr('id'));
			});
			orderfield.val(items.join(','));

	 	});

	 	// bind image picker to new items
		imagePickers.push(new MediaPanel({
	        "trigger" : jQuery('.' + prefix + '_' + newID + '_Picker')
	    }));

		clientLogoItemsBox.append(newItem);
		newIDfield.val( (newID * 1) + 1 );

		items = [];
		jQuery('.item').not('.template').each(function(i){
			items.push(jQuery(this).attr('id'));
		});
		orderfield.val(items.join(','));
 	});

	// set up item order sorter
    jQuery('#' + prefix + 'List').sortable({
        "receive" : function ( event, ui ) {
	    	console.log('receive');
        },
        "start" : function( event, ui ) {
        	console.log('start');
        },
        "stop" : function( event, ui ) {
        	console.log('stop');
        }
    }).disableSelection();

    jQuery('#' + prefix + 'List').on('sortstop',function(event, ui) {
		items = [];
		jQuery('.item').not('.template').each(function(i){
			items.push(jQuery(this).attr('id'));
		});
		orderfield.val(items.join(','));
    });
});
</script>
