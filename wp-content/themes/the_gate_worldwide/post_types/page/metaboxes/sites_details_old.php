<?php
    $sites = array(
        'new_york'  => 'New York',
        'london'    => 'London',
        'edinburgh' => 'Edinburgh',
        'asia'      => 'Asia',
    )
?>

<?php foreach ($sites as $site_key => $site_name) : ?>
    <div class="site_details postbox">
        <div class="handlediv" title="Click to toggle"></div>
        <h3><?php _e($site_name); ?></h3>
        <div class="inside" style="position: relative">

            <div class="misc-pub-section hide-if-no-js">
                <h4><?php _e($site_name . ' Image On Worldwide Page'); ?></h4>
                <?php
                    $metabox->the_field($site_key . '_images');
                    $images = $metabox->get_the_value();
                ?>
                <input class="the-gate-gallery-upload-button" data-uploader_title="Upload Image" data-uploader_button_text="Select" class="upload_button button" type="button" value="<?php echo __('Upload Image'); ?>" rel="" />
                <input class="the-gate-gallery-attachments-ids" type="hidden" value="<?php $metabox->the_value(); ?>" name="<?php $mb->the_name(); ?>" />
                <div class="the-gate-gallery-container">
                    <ul class="the-gate-gallery-thumbs clearfix"><?php
                        $images = explode(',', $images);
                        if (is_array($images) && count($images) > 0) {
                            foreach ($images as $id) {
                                if((int)$id)
                                {
                                    echo admin_thumb((int)$id);
                                }
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="misc-pub-section">
                    <h4><?php _e('Coordinates: '); ?></h4>

                    <div class="postbox" style="width: 100%;">
                        <h3><?php _e('Set Lattitude'); ?></h3>
                        <div class="inside">
                            <?php $metabox->the_field($site_key . '_coordinates_lattitude_degrees'); ?>
                            <label for="<?php echo $site_key; ?>-coordinates-lattitude-degrees"><?php _e('Lattitude Degrees'); ?></label>
                            <p><input id="<?php echo $site_key; ?>-coordinates-lattitude-degrees" type="number" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" /></p>

                            <?php $metabox->the_field($site_key . '_coordinates_lattitude_minutes'); ?>
                            <label for="<?php echo $site_key; ?>-coordinates-lattitude-minutes"><?php _e('Lattitude Minutes'); ?></label>
                            <p><input id="<?php echo $site_key; ?>-coordinates-lattitude-minutes" type="number" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" /></p>

                            <?php $metabox->the_field($site_key . '_coordinates_lattitude_direction'); ?>
                            <label for="<?php echo $site_key; ?>-coordinates-lattitude-direction"><?php _e('Lattitude Degrees'); ?></label>
                            <p>
                                <select id="<?php echo $site_key; ?>-coordinates-lattitude-direction" name="<?php $mb->the_name(); ?>">
                                    <option value="N" <?php $mb->the_select_state('N'); ?>><?php _e('N'); ?></option>
                                    <option value="S" <?php $mb->the_select_state('S'); ?>><?php _e('S'); ?></option>
                                </select>
                            </p>
                        </div>
                    </div>

                    <div class="postbox" style="width: 100%">
                        <h3><?php _e('Set Longitude'); ?></h3>
                        <div class="inside">
                            <?php $metabox->the_field($site_key . '_coordinates_longitude_degrees'); ?>
                            <label for="<?php echo $site_key; ?>-coordinates-longitude-degrees"><?php _e('Longitude Degrees'); ?></label>
                            <p><input id="<?php echo $site_key; ?>-coordinates-longitude-degrees" type="number" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" /></p>

                            <?php $metabox->the_field($site_key . '_coordinates_longitude_minutes'); ?>
                            <label for="<?php echo $site_key; ?>-coordinates-longitude-minutes"><?php _e('Longitude Minutes'); ?></label>
                            <p><input id="<?php echo $site_key; ?>-coordinates-longitude-minutes" type="number" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" /></p>

                            <?php $metabox->the_field($site_key . '_coordinates_longitude_direction'); ?>
                            <label for="<?php echo $site_key; ?>-coordinates-longitude-direction"><?php _e('Longitude Degrees'); ?></label>
                            <p>
                                <select id="<?php echo $site_key; ?>-coordinates-longitude-direction" name="<?php $mb->the_name(); ?>">
                                    <option value="E" <?php $mb->the_select_state('E'); ?>><?php _e('E'); ?></option>
                                    <option value="W" <?php $mb->the_select_state('W'); ?>><?php _e('W'); ?></option>
                                </select>
                            </p>
                        </div>
                    </div>
            </div>

            <div class="misc-pub-section">
                <?php $metabox->the_field($site_key . '_contact_url'); ?>
                <label for="<?php echo $site_key; ?>-contact-url"><?php _e('URL for contact: '); ?></label>
                <p><input id="<?php echo $site_key; ?>-contact-url" type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" style="width: 100%" placeholder="http://" /></p>
            </div>

            <div class="misc-pub-section">
                <?php $metabox->the_field($site_key . '_follow_url'); ?>
                <label for="<?php echo $site_key; ?>-follow-url"><?php _e('URL for follow: '); ?></label>
                <p><input id="<?php echo $site_key; ?>-follow-url" type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" style="width: 100%" placeholder="http://" /></p>
            </div>

        </div>
    </div>
<?php endforeach; ?>

<div class="clear"></div>
<script>
    jQuery(function() {
        <?php foreach ($sites as $site_key => $site_name) : ?>
            jQuery('#remove-<?php echo $site_key; ?>-thumb').on('click', function(e) {
                e.preventDefault();
                jQuery(this).siblings('input').val('');
                jQuery(this).siblings('img').attr('src', '<?php echo get_bloginfo('template_url') . '/post_types/page/img/preview.png'; ?>').data('thumbnail-id', '');
                jQuery(this).addClass('hidden');
            })

            new MediaModal({
                calling_selector: '#<?php echo $site_key; ?>-thumb-preview',
                cb: function(attachment) {
                    jQuery('#<?php echo $site_key; ?>-thumb-id').val(attachment.id);
                    jQuery('#<?php echo $site_key; ?>-thumb-preview').data('thumbnail-id', attachment.id).attr('src', attachment.url);
                    jQuery('#remove-<?php echo $site_key; ?>-thumb').removeClass('hidden');
                }
            })
        <?php endforeach; ?>
    })
</script>