if( typeof Validation == "undefined" ){

    var Validation = function(fieldsList) {
        if(fieldsList && fieldsList instanceof Array) {
            this.fields = fieldsList;
        }
        else {
            throw new TypeError('fieldList should be an array');
        }

        this.isValid = true;

        return this;
    };

    Validation.prototype.validate = function() {
        var self = this;

        for(var field = 0, fieldCount = self.fields.length; field < fieldCount; field++){
            /* Clear Errors */

            self.fields[field].errors = [];
            for(var property = 0, propertyCount = self.fields[field].properties.length; property < propertyCount; property++){
                if(self.fields[field].properties[property].indexOf('isSameAs') == 0) {
                    if(!self.isSameAs(self.fields[field], self.fields[field].properties[property].slice(8))) {
                        self.isValid = false;
                        break;
                    };
                }
                else {
                    if(!self[self.fields[field].properties[property]].apply(self, [self.fields[field]])) {
                        self.isValid = false;
                        break;
                    };
                }
            };
        };

        return self.isValid;
    };

    Validation.prototype.isValidName = function(field) {
        var self = this;
        var element = jQuery('[name="' + field.name + '"]');

        field.value = element.val();
        if(!element || !/^[A-Z\s\-_\.\`‒–—―]+$/i.test(field.value)) {
            field.errors.push('isValidName');

            return;
        }

        return true;
    };

    Validation.prototype.isSameAs = function(field, otherField) {
        var self = this;
        var element = jQuery('[name="' + field.name + '"]');
        var otherElement = jQuery('[name="' + otherField);

        field.value = element.val();
        if(field.value !== otherElement.val()) {
            field.errors.push('isSameAs');

            return;
        }

        return true;
    };

    Validation.prototype.isEmail = function(field) {
        var self = this;
        var element = jQuery('[name="' + field.name + '"]'),
            re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        field.value = element.val();
        if(!element || !re.test(field.value)) {
            field.errors.push('isEmail');

            return;
        }

        return true;
    };

    Validation.prototype.isRequired = function(field) {
        var self = this;
        var element = jQuery('[name="' + field.name + '"]');

        field.value = element.val();
        if(!element || !field.value || !jQuery.trim(field.value) || field.value == element.data('defaultvalue')) {
            field.errors.push('isRequired');

            return;
        }

        return true;
    };

    Validation.prototype.isValidPhoneNumber = function(field) {
        var self = this;
        var element = jQuery('[name="' + field.name + '"]');

        field.value = element.val();
        if(!element || !/^\d+$/.test(field.value.replace(/\s/g, '')) || field.value.length < 5 || new RegExp(/\s{2,}/g).test($.trim(field.value))) {
            field.errors.push('isValidPhoneNumber');

            return;
        }

        return true;
    };

    Validation.prototype.isChecked = function(field) {
        var self = this;
        var element = jQuery('[name="' + field.name + '"]');

        field.value = element.val();
        if(!element || !element.prop('checked')) {
            field.errors.push('isChecked');

            return;
        }

        return true;
    };

    Validation.prototype.filterName = function(name) {
        name = name.split('_');
        name = $.map(name, function(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        })

        return name.join(' ');
    };
}
