<?php
/**
 * Front page
 *
 * @package WordPress
 * @subpackage thegate_local
 */
the_post();

$homepage_images_ids = array_filter(explode(',', get_post_meta($post->ID, 'gallery_thumbs', true)), 'wp_get_attachment_image');
$random_image_id = (!empty($homepage_images_ids)) ? $homepage_images_ids[array_rand($homepage_images_ids)] : false;
$get_content = $post->post_content;

$posts = get_landing_page_posts(0, 10);
/* Check if there are more than 9 posts published */
$hide_show_more = count($posts) < 10;
/* Remove the last post */
if(!$hide_show_more)
{
    array_pop($posts);
}

wp_enqueue_script('show_more_landing_page');
wp_enqueue_script( 'simply_scroll' );

/* Set js variable containing path to ajax handling php script */
wp_localize_script( 'show_more_landing_page', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );

get_header();?>

    <!-- visual -->
    <div class="visual">
        <div class="visual-holder">
            <?php echo ($random_image_id) ? wp_get_attachment_image($random_image_id, 'homepage-thumbnail') : get_placeholder_image('homepage-thumbnail'); ?>
        </div>
        <div class="scroll-hint">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/scroll-btn.png" alt="" width="40" height="41">
        </div>
    </div>
</header>
<!-- main -->
<div id="main" class="office-home" style="display: none">
    <div class="main-holder">
        <div id="content">
            <h2 class="heading"><?php the_title(); ?></h2>
            <?php
                /* Remove wpautop filter for the content, display the content of the page and add it again */
                remove_filter( 'the_content', 'wpautop' );
                add_filter( 'the_content', 'add_br' , 12 );
            ?>
            <?php if($get_content) : ?>
                <p class="intro"><?php echo apply_filters('the_content', $get_content); ?></p>
            <?php endif; ?>
            <?php
                remove_filter( 'the_content', 'add_br' );
                add_filter( 'the_content', 'wpautop' );
            ?>
            <section class="block">
                <?php dynamic_sidebar( 'Twitter Feed' ); ?>
            </section>
            <div class="list-area">
                <ul class="list">
                    <?php $exclude_ids = array(); ?>
                    <?php foreach ($posts as $key => $post) : $exclude_ids[] = $post->ID; ?>
                        <li class="row-<?php echo ceil(($key + 1) / 3); ?>">
                            <div class="holder">
                                <figure>
                                    <?php if( $post->post_type === 'campaign_element') : ?>
                                        <?php $associate_campaign = get_post_meta($post->ID, 'campaign_element_linkto_campaign', true); ?>
                                        <?php if(!empty($associate_campaign['associate_campaign'])) : ?>
                                            <a href="<?php echo get_permalink($associate_campaign['associate_campaign']); ?>">
                                                <?php echo has_post_thumbnail($post->ID) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?>
                                            </a>
                                        <?php else : ?>
                                            <a href="<?php echo get_permalink($post->ID); ?>">
                                                <?php echo has_post_thumbnail($post->ID) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?>
                                            </a>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <a href="<?php echo get_permalink($post->ID); ?>">
                                            <?php echo has_post_thumbnail($post->ID) ? get_the_post_thumbnail($post->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?>
                                        </a>
                                    <?php endif; ?>
                                    <?php $post_category = get_post_category($post->ID); ?>
                                    <?php if($post_category) : ?>
                                        <strong class="title"><?php echo $post_category; ?></strong>
                                    <?php endif; ?>
                                </figure>
                                <header>
                                    <h3><?php echo apply_filters('the_title', $post->post_title); ?></h3>
                                    <span class="client">
                                        <?php if($post->post_type === 'campaign_element') : ?>
                                        <?php $client_name = get_post_meta($post->ID, 'campaign_client', true);?>
                                            <?php echo get_the_title($client_name); ?>
                                        <?php endif;?>
                                    </span>
                                     <h4>
                                        <?php if($post->post_type === 'post' ) : ?>
                                           <?php echo the_gate_format_date($post->post_date); ?>
                                        <?php endif; ?>
                                    </h4>
                                </header>
                                <?php echo apply_filters('the_excerpt', $post->post_excerpt); ?>
                                <a href="<?php echo get_permalink($post->ID); ?>" class="more"><?php _e('READ MORE', 'Local Home Page'); ?></a>
                            </div>
                        </li>

                    <?php endforeach; ?>
                </ul>
                        </ul>
                <div class="show-more-work">
                    <a id="show-more-landing-posts" data-exclude-ids="<?php echo implode(',', $exclude_ids); ?>" <?php echo ($hide_show_more) ? 'class="hidden"' : ''; ?> href="#"><p><?php _e('SHOW MORE', 'Local Home Page'); ?></p><span></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
