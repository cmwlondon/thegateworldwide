<!DOCTYPE HTML>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php if (is_page_template('full-people_individual.php')) : ?>
			<title><?php echo ucwords(str_replace('-', ' ', esc_attr($_GET['slug']))) . ' - '; ?><?php bloginfo('name'); ?></title>
		<?php else : ?>
			<title><?php wp_title('|', true, 'right'); ?></title>
		<?php
		endif;

		wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css');
		wp_enqueue_style('bootstrap-theme', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap-theme.min.css');
		wp_enqueue_style('2016-redesign', get_template_directory_uri() . '/assets/css/2016-redesign.css');
		?>
		<link rel="stylesheet" media="all" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css"> 
		<link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<script>
			// Load the IFrame Player API code asynchronously.
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/player_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		</script>
		<script type="text/javascript" src="//use.typekit.net/tpt2fek.js"></script>
		<script type="text/javascript">try {
				Typekit.load();
			} catch (e) {
			}</script>
		<!--- Specific function for i8 browser and under --->
		<script type="text/javascript">
			function showHideIE8(elementid) {
				if (document.getElementById(elementid).style.display == '') {
					document.getElementById(elementid).style.display = 'none';
				} else {
					document.getElementById(elementid).style.display = '';
				}
			}
		</script>
		<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/ie8andunder.css" media="screen" />
		<![endif]-->
		<!--- ////////// --->

		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/ie8andunder.css" media="screen" />

		<?php
		/* Scripts */
		wp_enqueue_script('jquery');
		//wp_enqueue_script('jquery_main');
		//wp_enqueue_script('init');
		wp_enqueue_script('2016-redesign', get_template_directory_uri() . '/assets/js/2016-redesign.js', array(), false, true);
		wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array(), false, true);
		wp_enqueue_script( 'validation_helper',  ASSETS_URL . '/js/libs/validation_helper.js', array(), false, true);
		wp_enqueue_script('contact', ASSETS_URL . '/js/contact.js', array(), false, true);
		wp_head();

		/* Create a var in js that will contain site theme url in order to be able to load images through js */
		wp_localize_script('init', 'siteDetails', array('url' => get_bloginfo('template_url')));
		?>
	</head>
	<body class='<?php _e('page-id-' . get_the_ID()) ?>'>

		<!--- Browser update box  --->
		<!--[if lt IE 9]>
		    <div id="div_1035677" >
			<div class="ie8message" align="center">
			<div id="ie8mscopy">
			<div id="ie8closebutton"><a href="#" onclick="showHideIE8('div_1035677');"><img src="<?php bloginfo('template_directory'); ?>/assets/images/ie8closebutton.jpg"></a></div>
			<div id="ie8mscopybox">
			<p>This site will work for 95.6% of the world's web browsers.<br>
			 If it doesn't work for you, it might be time to update your technology.<br>
			 <a href="http://browsehappy.com/" class="ie8updatedlink">Click here to get started.</a></p>
			</div>
			</div>
		       </div>
		       </div>
		 <![endif]-->
		<!--- ////////// ---> 
		<!-- Navigation -->
			<?php
				$thisPage = get_post($post_id);
				$thisPageName = $thisPage->post_name;
				$compassClass = ($thisPageName == 'home') ? ' open' : '';
				$menuClass = ($thisPageName == 'home') ? ' open' : '';
			?>
		<header>
			<div class="compass<?php echo $compassClass; ?>"><div class="clip"><img alt="" src="<?php bloginfo('template_directory') . _e('/assets/images/compass.png') ?>"></div></div>
			<!--
			<button class="hamburger"><img src="<?php bloginfo('template_directory') . _e('/assets/images/burgermenu_04.png') ?>"></button>
			<button class="cross"><img src="<?php bloginfo('template_directory') . _e('/assets/images/burgermenu_close.png') ?>"></button>
			-->
		</header>

		<div class="menu<?php echo $compassClass; ?>">
			<ul>
				<a href="<?php echo site_url() ?>"><li>Home</li></a>
				<a href="<?php echo site_url() ?>/about"><li>About</li></a>
				<a href="<?php echo site_url() ?>/stories"><li>Stories</li></a>
				<a href="<?php echo site_url() ?>/predatory-thinking"><li>Predatory<br>Thinking</li></a>
				<a href="<?php echo site_url() ?>/fullservice"><li>Full Service</li></a>
				<a href="<?php echo site_url() ?>/contact-us"><li>Contact</li></a>
			</ul>
		</div>
		<div class="thegate-logo" id="logo">
                    <a href="<?php echo site_url() ?>"><img src="<?php bloginfo('template_directory') . _e('/assets/images/logo_02.png') ?>" class="logo-mob-width" id="test" alt="<?php _e('The Gate'); ?>" /></a>
		</div>

		<!-- Intro Header -->
		<header class="intro">
			<div class="intro-body">
				<div class="titleContainer"><h1 class="brand-heading">HOME OF <br/>PREDATORY <br/>THINKING</h1></div>

				<div class="container">
					<div class="row"  style="display:none;">
						<div class="col-md-8 center-block">
							<h1 class="brand-heading">HOME OF PREDATORY THINKING</h1>
						</div>
					</div>
				</div>
			</div>
		</header>

