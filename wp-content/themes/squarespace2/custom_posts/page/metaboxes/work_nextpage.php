		<?php
		$pages = get_all_pages();
		$output = '';
		// $values['w_nextpage']
		foreach ($pages AS $page) {
			$state = ( $values['w_nextpage'] == $page->ID ) ? 'selected' : ''; 
			$output = $output . "<option $state value=\"".$page->ID."\">".strtoupper($page->post_title)."</option>\n";
		}

		?>

		<div class="customField nextChooser">
			<label>Destination for 'Next' link
			</label>
			<select name="w_nextpage" id="w_nextpage">
				<option value="-">Select Page</option>
				<?php echo $output; ?>
			</select>
		</div>
