<?php
/**
 * Template Name: People Everyone
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $everyone_page,
       $everyone,
       $individual_page_permalink,
       $seed;

/* Redirect to 404 if opened directly #3342 */
if( basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME) )
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

$veryone_by_15 = array_chunk($everyone, 15, true);
if($everyone_page) : ?>
    <li id="everyone" class="everyone visual">
    <div class="main-holder">
        <h2><?php echo apply_filters('the_title', $everyone_page->post_title); ?></h2>
        <?php if(count($veryone_by_15) > 1) : ?>
            <ul class="featured-prev-next">
                <li class="prev"><a href="#"></a></li>
                <li class="next"><a href="#"></a></li>
            </ul>
        <?php endif; ?>
        <div class="people-sldier-wrap">
            <ul class="people-slider">
                <?php foreach ($veryone_by_15 as $single_page_list) : ?>
                    <li>
                        <ul class="peoples">
                            <?php foreach ($single_page_list as $person) : ?>
                                <li>
                                    <div class="people">
                                        <a href="<?php echo $individual_page_permalink; ?>?<?php echo http_build_query(array('slug' => $person['slug'], 'e' => true, 'o' => $seed), '', '&amp;'); ?>" class="photo">
                                            <div class="border"><?php echo $person['small_thumbnail']; ?></div>
                                        </a>
                                        <span class="job"><?php echo $person['position']; ?></span>
                                        <span class="name"><?php echo $person['first_name']; ?> <?php echo $person['last_name']; ?></span>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                         </ul>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        </div>
    </li><!--li#everyone-->
<?php endif;
