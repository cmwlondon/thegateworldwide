$(function(){
    $('#sort-client li a').on('click', function(e) {
        e.preventDefault();

        var client = $(this).data('client');

        showFilteredCampaignsByClient({numberposts: -1, client: client});
    })
});

function showFilteredCampaignsByClient(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'filter_campaigns_by_client'}),
        beforeSend  : function() {},
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
                 var listClasses = [];

                /* Replace posts */
                var patternElement = $('#by-client .list li').first();
                var list = $('#by-client .list').clone();

                list.html('');

                $.each(response.posts, function(key, post) {
                    var listElement = patternElement.clone();
                    var rowClass = 'by-client-row-' + post.row;

                    listClasses.push(rowClass);

                    $(listElement).attr('class', rowClass);
                    post.thumbnail = post.thumbnail.replace('<img','<img onload="this.setAttribute(\'data-loaded\',\'yep\')" data-loaded="nope"');
                    $('figure a', listElement).attr('href', post.url).html(post.thumbnail);
                    $('header h3', listElement).html(post.title);
                    $('.holder', listElement).height('auto');

                    if($('p', listElement).length > 0) {
                        var excerpts = $('p', listElement);
                        excerpts.remove();
                    }
                    $('header', listElement).after($(post.excerpt));

                    $('a.more', listElement).attr('href', post.url);

                    list.append(listElement);
                });

                $('#by-client .list').html(list.html());

                /* Set equal height for list items in same row */
                setTimeout(function(){
                    if($('[data-loaded="nope"]').length != 0){
                        setTimeout(arguments.callee, 250);
                        return;
                    }
                    $.setEqualHeightByClassWithImages($.unique(listClasses));
                }, 100)

            }
            else {
            }

        },
        error       : function(xhrObj, status, error) {
        }
    });

}
