<?php
/**
 * Search page
 *
 * @package WordPress
 * @subpackage thegate_local
 */
global $wp_query;

$total_results = $wp_query->found_posts;
$posts = $wp_query->posts;
$search_query = get_search_query();
$people = new PeopleHelper();
$everyone_page = get_page_id_by_slug('people/everyone'); /* ID 127 */
$people = $people->get_list('everyone', $everyone_page, null, true);
$people = array_filter($people, create_function('$person', 'return stripos($person["content"], "' . $search_query . '") !== false || stripos($person["first_name"], "' . $search_query . '") !== false || stripos($person["last_name"], "' . $search_query . '") !== false || stripos($person["position"], "' . $search_query . '") !== false;'));
$individual_person_page_permalink = get_permalink(get_first_page_by_template('full-people_individual.php'));
?>
<?php get_header();?>
    <!-- search -->
    <div class="wide-search-box-wrap">
        <div class="wide-search-box">
            <form id="searchform" class="form-search" action="<?php echo home_url( '/' ); ?>" method="get" role="search">
                <fieldset>
                    <div class="text-holder">
                        <input type="text" value="" name="s" />
                    </div>
                </fieldset>
                <input type="submit" class="btn-search" value="<?php _e('Go', 'Search Results Page'); ?>" >
            </form>
        </div>
    </div>
    <!-- visual -->
    <div class="visual">
        <div class="visual-holder">
            <img src="<?php bloginfo('template_url'); ?>/assets/images/content/ny-landing-small.jpg" width="2000" height="675" alt="search page">
        </div>
    </div>
</header>
    <!-- main -->
    <div id="main">
        <div class="main-holder">
            <!-- content -->
            <div id="content">
                <a class="title" href="#"><?php _e('Search Results', 'Search Results Page'); ?></a>
                <h2><?php echo $total_results + count($people); ?> <?php _e('SEARCH RESULTS', 'Search Results Page'); ?> <span><?php _e('FOR', 'Search Results Page'); ?> <strong><?php echo $search_query; ?></strong></span></h2>
                <ul class="simple-list">
                    <li id="most-recent" class="results-wrap most-recent">
                        <ul class="search-results">
                            <?php foreach ($people as $person) : ?>
                                <li>
                                    <a class="result-desc" href="<?php echo trim($individual_person_page_permalink, '/') . '?' . http_build_query(array('slug' => $person['slug'], 'e' => 1)); ?>">
                                        <?php echo '<img class="result-img" width="107" height="84" alt="' . $person['title'] . '" src="' . get_img_tag_src($person['search_thumbnail']) . '">'; ?>
                                        <h3><?php echo apply_filters('the_title', $person['first_name'] . ' ' . $person['last_name']); ?></h3>
                                        <?php echo apply_filters('the_excerpt', str_highlight(util_truncate_string(strip_tags($person['content']), 117, '...'), $search_query) . ' <span class="date">' . $person['position'] . '</span>'); ?>
                                        <span class="read-more"><?php _e('Read more', 'Search Results Page'); ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                            <?php foreach ($posts as $post) : ?>
                                <li>
                                    <a class="result-desc" href="<?php echo get_permalink($post->ID); ?>">
                                        <?php echo has_post_thumbnail($post->ID) ? get_the_post_thumbnail($post->ID, 'search-thumb', array('class' => 'result-img')) : get_placeholder_image('search-thumb', $post->post_title, array('result-img')); ?>
                                        <h3><?php echo apply_filters('the_title', $post->post_title); ?></h3>
                                        <?php echo apply_filters('the_excerpt', str_highlight($post->post_excerpt, $search_query) . ' <span class="date">' . the_gate_format_date($post->post_date) . '</span>'); ?>
                                        <span class="read-more"><?php _e('Read more', 'Search Results Page'); ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?php get_footer();?>