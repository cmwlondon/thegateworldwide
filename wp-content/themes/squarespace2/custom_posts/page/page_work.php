<?php


function work_items_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'work_items_save_meta_box_data', 'work_items_meta_box_nonce' );

    $values['wi_order'] = get_post_meta( $post->ID, 'wi_order', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/work_items.php';
}

function work_items_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_items_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_items_meta_box_nonce'], 'work_items_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('wi_order', 'wi_order')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'work_items_save_meta_box_data' );

function work_nextpage_meta_box_callback( $post ) {
    wp_nonce_field( 'work_nextpage_save_meta_box_data', 'work_nextpage_meta_box_nonce' );

    $values = array(
        "w_nextpage" => get_post_meta( $post->ID, 'w_nextpage', true )
    );

    $fieldSettings = array(
        "nextpagefield" => "w_nextpage"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_nextpage.php';
}

function work_nextpage_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_nextpage_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_nextpage_meta_box_nonce'], 'work_nextpage_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('w_nextpage', 'w_nextpage')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'work_nextpage_save_meta_box_data' );

?>