<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
global $current_blog;
global $theme_root;
global $defaultFooter;

$theme_root = get_template_directory_uri();
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.ico">
	<meta name="theme-color" content="#ffffff">
	<!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Abel:400,700,300,300i,600,700i,800|Open+Sans:400,700,300,300i,600,700i,800"> -->
	
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site loading">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyfifteen' ); ?></a>

	<div class="pageHeader">
		<header class="clearfix">
			<h1><?php echo $post->post_title; ?></h1>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="thegatelondon"><img alt="thegatelondon" src="<?php echo $theme_root; ?>/img/gate_london.png" class="logo"></a>
			
			<div class="menuWrapper clearfix">
				<a href="" id="hamburgermenu" class="button"><svg viewBox="0 0 64 64" width="64" height="64">
			      <use class="" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $theme_root; ?>/svg/ui-icons.svg#hamburger-icon--even"></use>
			    </svg></a>
				<div class="menuMover">
					<a href="" class="closeMenu button"><svg viewBox="0 0 64 64" width="64" height="64">
			      <use class="" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $theme_root; ?>/svg/ui-icons.svg#close-icon"></use>
			    </svg></a>
					<?php if ( has_nav_menu( 'primary' ) ) : ?>
						<nav id="site-navigation" class="main-navigation" role="navigation">
							<?php
								// Primary navigation menu.
								wp_nav_menu( array(
									'menu_class'     => 'nav-menu',
									'theme_location' => 'primary',
								) );
							?>
						</nav><!-- .main-navigation -->
					<?php endif; ?>
					<nav class="footerLinks">
						<?php
							// footer links
							wp_nav_menu( array(
								'menu_class'     => 'nav-menu',
								'theme_location' => 'footer',
							) );
						?>
					</nav>

				</div>
			</div>
		</header>
