<?php
/**
 * Template Name: People Leadership
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $leadership_page,
       $leaders,
       $individual_page_permalink;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

if($leadership_page) : ?>
<li id="leadership" class="leadership visual">
    <div class="main-holder">
        <h2><?php echo apply_filters('the_title', $leadership_page->post_title); ?></h2>
        <?php $counter = 1; $leaders_count = count($leaders); ?>
        <?php foreach ($leaders as $office => $current_office_leaders) : ?>
            <?php if($counter < 4 || $counter > 5) : ?>
                <div class="by-office-place-wrap <?php echo ($counter < 4) ? 'big' : 'small'; ?><?php if(!$counter) echo ' first'; ?><?php if($counter === $leaders_count) echo ' last'; ?>">
            <?php endif; ?>
                    <h3><?php echo ($counter > 2 && $counter < 6) ? 'Asia - ' .  $office : $office; ?></h3>
                    <ul class="by-office-place">
                        <?php foreach ($current_office_leaders as $leader) : ?>
                            <li>
                                <div class="people">
                                    <a href="<?php echo $individual_page_permalink; ?>?<?php echo http_build_query(array('slug' => $leader['slug'], 'e' => false), '', '&amp;'); ?>" class="photo">
                                        <div class="border"><?php echo $leader['small_thumbnail']; ?></div>
                                    </a>
                                    <span class="job"><?php echo $leader['position']; ?></span>
                                    <span class="name"><?php echo $leader['first_name']; ?> <?php echo $leader['last_name']; ?></span>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
            <?php if($counter < 3 || $counter > 4) : ?>
                </div>
            <?php endif; ?>
            <?php $counter++ ?>
        <?php endforeach; ?>
    </div>
</li><!--li#leadership-->
<?php endif;
