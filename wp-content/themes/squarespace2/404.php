<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	</div>

	<div id="content" class="site-content page404">
		<div class="text">
			<h1>Oops! That page can&rsquo;t be found.</h1>
			<p>It looks like nothing was found at this location.</p>
		</div>


<?php get_footer(); ?>
