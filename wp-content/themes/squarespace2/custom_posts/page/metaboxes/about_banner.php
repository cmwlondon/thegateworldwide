<?php
?>

<div class="customField mediumtext">
	<label for="ab_title">Banner Title</label>
	<input type="text" id="ab_title" name="ab_title" value="<?php echo $values['ab_title']; ?>">
</div>
 
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Banner image</label>
		<input type="hidden" id="ab_banner" name="ab_banner" value="<?php echo $values['ab_banner']; ?>">
		<input type="hidden" id="ab_banner_id" name="ab_banner_id" value="<?php echo $values['ab_banner_id']; ?>" class="ident">
		<input type="hidden" id="ab_banner_w" name="ab_banner_w" value="<?php echo $values['ab_banner_w']; ?>" class="width">
		<input type="hidden" id="ab_banner_h" name="ab_banner_h" value="<?php echo $values['ab_banner_h']; ?>" class="height">
		<a href="" class="ab_banner_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['ab_banner']; ?>" <?php if ($values['ab_banner'] == '' || $values['ab_banner'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['ab_banner'] == '' || $values['ab_banner'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ab_banner']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.ab_banner_Picker')
    }));
});
</script>
