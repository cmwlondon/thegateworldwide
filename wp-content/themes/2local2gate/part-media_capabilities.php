<?php
/**
 * Template Name: Media Capabilities
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $media_capabilities_page;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

if($media_capabilities_page) : ?>
<div id="capabilities" class="gray">
    <h2><?php echo apply_filters('the_title', get_the_title($media_capabilities_page->ID)); ?></h2>
    <a href="#" class="opener"><?php echo apply_filters('the_title', get_post_meta($media_capabilities_page->ID, 'menu_label', true)); ?></a>
    <div class="slide">
        <figure class="alignleft large border"><?php echo (has_post_thumbnail($media_capabilities_page->ID)) ? get_the_post_thumbnail($media_capabilities_page->ID, 'about-value') : get_placeholder_image('about-value'); ?></figure>
        <div class="text">
            <h3 class="mob"><?php echo get_post_meta($media_capabilities_page->ID, 'about_subtitle_subtitle', true); ?></h3>
            <p><?php echo get_post_meta($media_capabilities_page->ID, 'capabilities_top_text_top_text', true); ?></p>
            <h3 class="desc"><?php echo get_post_meta($media_capabilities_page->ID, 'about_subtitle_subtitle', true); ?></h3>
            <p><?php echo get_post_meta($media_capabilities_page->ID, 'capabilities_bottom_text_bottom_text', true); ?></p>
        </div>
        <div class="columns-area">
            <div class="column">
                <h4><?php echo get_post_meta($media_capabilities_page->ID, 'capabilities_references_first_category_title', true); ?></h4>
                <ul>
                    <?php $first_category_references = get_post_meta($media_capabilities_page->ID, 'capabilities_references_first_category_references', true);
                    if(is_array($first_category_references) && current($first_category_references)) :
                        foreach ($first_category_references as $first_category_reference) : ?>
                            <?php if(isset($first_category_reference['text']) && trim($first_category_reference['text']) !== '') : ?>
                                <li><?php echo $first_category_reference['text']; ?></li>
                            <?php endif; ?>
                        <?php endforeach;
                    endif; ?>
                </ul>
            </div>
            <div class="column">
                <h4><?php echo get_post_meta($media_capabilities_page->ID, 'capabilities_references_second_category_title', true); ?></h4>
                <ul>
                    <?php $second_category_references = get_post_meta($media_capabilities_page->ID, 'capabilities_references_second_category_references', true);
                    if(is_array($second_category_references) && current($second_category_references)) :
                        foreach ($second_category_references as $second_category_reference) : ?>
                            <?php if(isset($second_category_reference['text']) && trim($second_category_reference['text']) !== '') : ?>
                                <li><?php echo $second_category_reference['text']; ?></li>
                            <?php endif; ?>
                        <?php endforeach;
                    endif; ?>
                </ul>
            </div>
            <div class="column">
                <h4><?php echo get_post_meta($media_capabilities_page->ID, 'capabilities_references_third_category_title', true); ?></h4>
                <ul>
                    <?php $third_category_references = get_post_meta($media_capabilities_page->ID, 'capabilities_references_third_category_references', true);
                    if(is_array($third_category_references) && current($third_category_references)) :
                        foreach ($third_category_references as $third_category_reference) : ?>
                            <?php if(isset($third_category_reference['text']) && trim($third_category_reference['text']) !== '') : ?>
                                <li><?php echo $third_category_reference['text']; ?></li>
                            <?php endif; ?>
                        <?php endforeach;
                    endif; ?>
                </ul>
            </div>
            <div class="column">
                <h4><?php echo get_post_meta($media_capabilities_page->ID, 'capabilities_references_fourth_category_title', true); ?></h4>
                <ul>
                    <?php $fourth_category_references = get_post_meta($media_capabilities_page->ID, 'capabilities_references_fourth_category_references', true);
                    if(is_array($fourth_category_references) && current($fourth_category_references)) :
                        foreach ($fourth_category_references as $fourth_category_reference) : ?>
                            <?php if(isset($fourth_category_reference['text']) && trim($fourth_category_reference['text']) !== '') : ?>
                                <li><?php echo $fourth_category_reference['text']; ?></li>
                            <?php endif; ?>
                        <?php endforeach;
                    endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php endif;