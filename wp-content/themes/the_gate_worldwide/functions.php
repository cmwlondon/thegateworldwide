<?php
/**
 * Functions and Definitions
 *
 * @package WordPress Multisite
 * @subpackage The Gate Wordwide
 */

/**
 * Constants
 */
include_once 'config/constants.php';

/**
 * Metaboxes
 */
include_once 'includes/metaboxes/setup.php';

/**
 * Utilities
 */
include_once 'includes/utility.php';

/**
 * Custom Post Types
 */
include('post_types/person/person.php');
include('post_types/office/office.php');
include('post_types/page/page.php');

/**
 * Options
 */
 include('options/general/copyright.php');

/**
 * Custom Widgets
 */
// include('widgets/phone_reminder.php');

/**
 * Remove standard image sizes so that these sizes are not
 * created during the Media Upload process
 *
 * Tested with WP 3.2.1
 *
 * Hooked to intermediate_image_sizes_advanced filter
 * See wp_generate_attachment_metadata( $attachment_id, $file ) in wp-admin/includes/image.php
 *
 * @param $sizes, array of default and added image sizes
 * @return $sizes, modified array of image sizes
 * @author Ade Walker http://www.studiograsshopper.ch
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function thegate_worldwide_setup() {

     /* This theme styles the visual editor with editor-style.css to match the theme style. */
    add_editor_style();

    /* This theme uses post thumbnails */
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'worldwide-thumb', 288, 255, true );
    add_image_size( 'person-small', 88, 88, true );
    add_image_size( 'person-medium', 131, 131, true );
    add_image_size( 'person-large', 334, 334, true );
    add_image_size( 'city-landscape', 2000, 1230, true );
    add_image_size( 'admin-thumb', 100, 78, true );
    add_image_size( 'search-thumb', 107, 84, true);

    /* Add default posts and comments RSS feed links to head */
    add_theme_support( 'automatic-feed-links' );

    // Register menus
    register_nav_menus( array(
        'primary'       => __( 'Primary Navigation', 'thegate_worldwide' ),
        'footer'        => __( 'Footer navigation', 'thegate_worldwide' ),
        'footer_left'   => __( 'Footer Left Navigation'),
        'footer_right'  => __( 'Footer Right Navigation'),
    ) );

}
add_action( 'after_setup_theme', 'thegate_worldwide_setup' );

/**
 * Register widgetized areas
 *
 * @uses register_sidebar
 */
function thegate_worldwide_widgets_init() {

    /* Global Widget Area - Displayed on all pages */
    register_sidebar( array(
        'name'          => __( 'Global', 'thegate_worldwide' ),
        'id'            => 'global-widget-area',
        'description'   => __( 'Global widget area', 'thegate_worldwide' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'before_text'   => '',
        'after_text'    => ''
    ) );

}
add_action( 'widgets_init', 'thegate_worldwide_widgets_init' );

function get_offices_information()
{
    $args = array(
        'numberposts' => -1,
        'offset' => 0,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_type' => 'office',
        'post_status' => 'publish',
        'suppress_filters' => true
    );

    $offices = get_posts($args);

    foreach ($offices as $office)
    {
        $office->contact_url = get_post_meta($office->ID, 'office_contact_url', true);
        $office->map_url = get_post_meta($office->ID, 'office_map_url', true);
        $office->work_url = get_post_meta($office->ID, 'office_work_url', true);
        $office->address = get_post_meta($office->ID, 'office_office_address', true);
        $office->phone = get_post_meta($office->ID, 'office_office_phone', true);
    }

    return $offices;
}

function get_copyright_text()
{
    return apply_filters('the_content', get_option('copyright-settings'));
}

function get_placeholder_image_src($size)
{
    if(is_array($size))
    {
        if(isset($size[0]) && isset($size[1]))
        {
            $width = (int)$size[0];
            $heigth = (int)$size[1];
        }
    }
    else
    {
        if(in_array($size, get_intermediate_image_sizes()))
        {
            global $_wp_additional_image_sizes;
            if (isset($_wp_additional_image_sizes[$size])) {
                $width = intval($_wp_additional_image_sizes[$size]['width']);
                $height = intval($_wp_additional_image_sizes[$size]['height']);
            } else {
                $width = get_option($size.'_size_w');
                $height = get_option($size.'_size_h');
            }
        }
    }

    if(isset($width) && isset($height))
    {
        return 'http://placehold.it/' . $width . 'x' . $height;
    }

    return null;
}

function get_placeholder_image($size, $alt = 'Image', $classes = false)
{
    $classes = ($classes && is_array($classes)) ? 'class="' . implode(' ', $classes) . '"' : '';

    if(is_array($size))
    {
        if(isset($size[0]) && isset($size[1]))
        {
            $width = (int)$size[0];
            $heigth = (int)$size[1];
        }
    }
    else
    {
        if(in_array($size, get_intermediate_image_sizes()))
        {
            global $_wp_additional_image_sizes;
            if (isset($_wp_additional_image_sizes[$size])) {
                $width = intval($_wp_additional_image_sizes[$size]['width']);
                $height = intval($_wp_additional_image_sizes[$size]['height']);
            } else {
                $width = get_option($size.'_size_w');
                $height = get_option($size.'_size_h');
            }
        }
    }

    if(isset($width) && isset($height))
    {
        return '<img ' . $classes . ' src="http://placehold.it/' . $width . 'x' . $height . '" width="' . $width . '" height="' . $height . '" alt="' . $alt . '"/>';
    }

    return null;
}

function admin_thumb($id)
{
    $image = wp_get_attachment_image_src($id, 'admin-thumb', true);
    ?>
    <li><img src="<?php echo $image[0]; ?>" width="100" height="75" /><a href="#" class="the-gate-gallery-remove" data-id="<?php echo $id; ?>"><?php echo __('Remove'); ?></a></li>
    <?php
}

function filter_out_not_english_posts($post)
{
	if(!function_exists('wpml_get_language_information'))
	{
		return true;
	}
	$language_info = wpml_get_language_information($post->ID);

	if($language_info['different_language']) return false;

	return true;
}

/* Function that returns either the first set category for the post or the first media type or false if none is set */
function get_post_category($post_id)
{
    $post_type = get_post_type($post_id);

    if($post_type == 'post')
    {
        $post_category = get_the_category($post_id);

        return apply_filters('the_category', current($post_category)->name);
    }
    else
        $post_category = get_the_terms($post_id, 'media_type');
        if(is_array($post_category))
        {
            return apply_filters('the_category', current($post_category)->name);
        }
        else
        {
            return 'work';
        }
    {

    }
}

add_action('wp_ajax_get_posts', 'get_posts_by_post_type');
function get_posts_by_post_type()
{
    if( isset($_POST['post_type']) && isset($_POST['blog_id']))
    {
        switch_to_blog((int)$_POST['blog_id']);

        $post_ids_to_exclude = array();
        if($_POST['blog_id'] == 3)
        {
            /* Exclude non english posts */
            global $wpdb;

            $query = "SELECT `element_id` FROM `wp_3_icl_translations` WHERE `language_code` NOT IN ( 'en') AND (`element_type` = 'post_post' OR `element_type` = 'post_campaign' OR `element_type` = 'post_campaign_element')";
            $result = $wpdb->get_results($query);

            foreach ($result as $value) {
                $post_ids_to_exclude[] = (int)$value->element_id;
            }
        }

        $get_posts = get_posts( array(
            'post_type'         => $_POST['post_type'],
            'numberposts'       => -1,
            'exclude'          => implode(',', $post_ids_to_exclude),
            'suppress_filters'  => true )
        );

        if(get_option('icl_sitepress_settings') && $_POST['blog_id'] == 3)
        {
            $get_posts = array_filter($get_posts, 'filter_out_not_english_posts');
        }

        $posts = array();
        foreach( $get_posts as $key => $post )
        {
            $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(150, 150) );
            $posts[$key]['id']    = $post->ID;
            $posts[$key]['name']  = $post->post_title . " ({$post->post_name})" ;
            $posts[$key]['src']   = $featured_image ? $featured_image[0] : 0;
        }

        if(is_array($posts))
        {
            echo json_encode(array('status' => 'success', 'posts' => $posts));
        }
        else
        {
            echo json_encode(array('status' => 'failure'));
        }

        switch_to_blog(1);
    }
    else
    {
        echo json_encode(array('status' => 'failure'));
    }

    exit;
}


function get_site_details($site_name)
{
    $sites_mapping = array(
        'new_york'  => 2,
        'asia'      => 3,
        'london'    => 4,
        'edinburgh' => 5
    );

    $front_page_id = get_option('page_on_front');
    $predefined_posts_details = get_post_meta($front_page_id, "{$site_name}-entries", true);
    $post_ids_to_exclude = array();
    /* Add predefined posts to the exclude argument */
    foreach ($predefined_posts_details as $value)
    {
        if(isset($value['post_id']) && $value['post_id'] !== 'random')
        {
            $post_ids_to_exclude[] = (int)$value['post_id'];
        }
    }

    $posts = array();

    switch_to_blog($sites_mapping[$site_name]);


    if($site_name === 'asia')
    {
        /* Exclude non english posts */
        global $wpdb;

        $query = "SELECT `element_id` FROM `wp_3_icl_translations` WHERE `language_code` NOT IN ( 'en') AND (`element_type` = 'post_post' OR `element_type` = 'post_campaign' OR `element_type` = 'post_campaign_element')";
        $result = $wpdb->get_results($query);

        foreach ($result as $value) {
            $post_ids_to_exclude[] = (int)$value->element_id;
        }
    }

    for ($i=0; $i < 10; $i++) {
        $post = null;


        $args = array(
            'posts_per_page'   => 1,
            'exclude'          => implode(',', $post_ids_to_exclude),
            'suppress_filters' => true
        );
        if(isset($predefined_posts_details[$i]))
        {
            if($predefined_posts_details[$i]['category'] === 'random')
            {
                /* Get a random post from all needed posttypes */
                $args = array_merge($args, array(
                    'post_type' => array('post', 'campaign', 'campaign_element'),
                    'orderby'   => 'rand'
                ));

                $post_array = get_posts($args);
                $post = (is_array($post_array)) ? current($post_array) : $post_array;
            }
            else
            {
                if($predefined_posts_details[$i]['post_id'] === 'random')
                {
                    $args = array_merge($args, array(
                        'post_type' => $predefined_posts_details[$i]['category'],
                        'orderby'   => 'rand'
                    ));

                    $post_array = get_posts($args);
                    $post = (is_array($post_array)) ? current($post_array) : $post_array;
                }
                else
                {
                    $post = get_post((int)$predefined_posts_details[$i]['post_id']);
                }
            }
        }

        if($post)
        {
            $post->thumb = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'worldwide-thumb') : get_placeholder_image('worldwide-thumb');
            if($post->post_type === 'campaign_element' && ($parent_campaign = get_post_meta($post->ID, 'campaign_element_linkto_campaign', true)) && is_array($parent_campaign))
            {
                $post->permalink = @get_post_permalink((int)$parent_campaign['associate_campaign'], false, false);
            }
            else
            {
                $post->permalink = @get_post_permalink($post->ID, false, false);
            }
            $post->category = get_post_category($post->ID);
            if($post->post_type === 'campaign_element')
            {
                $client_name = get_post_meta($post->ID, 'campaign_client', true);
                $post->client = ($client_name) ? get_the_title($client_name) : '';
            }
            $posts[] = $post;
            $post_ids_to_exclude[] = $post->ID;
        }
        else
        {
            /* Get random post if no predefined post is available */
            $args = array(
                'posts_per_page'    => 1,
                'post_type'         => array('post', 'campaign', 'campaign_element'),
                'orderby'           => 'rand',
                'exclude'           => implode(',', $post_ids_to_exclude),
                'suppress_filters'  => true
            );

            $post_array = get_posts($args);
            $post = (is_array($post_array)) ? current($post_array) : $post_array;

            if($post)
            {
                $post->thumb = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'worldwide-thumb') : get_placeholder_image('worldwide-thumb');
                if($post->post_type === 'campaign_element' && ($parent_campaign = get_post_meta($post->ID, 'campaign_element_linkto_campaign', true)) && is_array($parent_campaign))
                {
                    $post->permalink = @get_post_permalink((int)$parent_campaign['associate_campaign'], false, false);
                }
                else
                {
                    $post->permalink = @get_post_permalink($post->ID, false, false);
                }
                $post->category = get_post_category($post->ID);
                if($post->post_type === 'campaign_element')
                {
                    $client_name = get_post_meta($post->ID, 'campaign_client', true);
                    $post->client = ($client_name) ? get_the_title($client_name) : '';
                }

                $posts[] = $post;
                $post_ids_to_exclude[] = $post->ID;
            }
        }

    }

    $url = get_option('siteurl');

    switch_to_blog(1);

    return array('posts' => $posts, 'url' => $url);
}




/**
 * Include Theme config
 */
include('config/admin.php');
include('config/public.php');

?>