<?php
function get_all_pages($postid = null) {

    if (is_null($postid)) {
        // no post id passed in to function, get *ALL* pages
        $query_parameters = array(
            'post_type' => 'page',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'ID',
            'order' =>'ASC'
        );
    } else {
        // get all products except page identified by $postid
        $query_parameters = array(
            'post__not_in' => array($postid),           
            'post_type' => 'page',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'ID',
            'order' =>'ASC'
        );

    }
	return do_query($query_parameters);
}

function get_page_by_postid($postid) {
    $query_parameters = array(
		'post__in' => array($postid),	    	
        'post_type' => 'page',
        'post_status' => 'publish',
        'posts_per_page' => 1
    );

	return do_query($query_parameters);
}
?>