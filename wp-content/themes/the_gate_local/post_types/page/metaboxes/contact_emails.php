<div class="contact_emails box">
    <span><?php _e( 'Add email to which every response will be sent:' ); ?></span><br />
    <?php while( $mb->have_fields_and_multi('emails') ): ?>
    <?php $mb->the_group_open(); ?>
        <div class="email">
            <input type="text" style="width: 50%;" name="<?php echo $mb->the_name(); ?>" value="<?php echo $mb->the_value(); ?>"/>
            <a href="#" class="dodelete button right"><?php _e( 'Remove Email' ); ?></a>
            <br class="clear" />
        </div><!-- end survey answer -->
    <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>
    <p style="margin-bottom:0px; padding-top:0px;"><a href="#" class="docopy-emails button"><?php _e( 'Add Email' ); ?></a></p>
</div><!-- end survey answers -->
<br class="clear" />
