<div class="clearfix multiImages">
	<p>(Thumbnail image displayed on 'work' landing page.)</p>
	<div class="customField imageSelector multiImage">
		<input type="hidden" id="work_thumbnail" name="work_thumbnail" value="<?php echo $values['work_thumbnail']; ?>">
		<input type="hidden" id="work_thumbnail_id" name="work_thumbnail_id" value="<?php echo $values['work_thumbnail_id']; ?>" class="ident">
		<input type="hidden" id="work_thumbnail_w" name="work_thumbnail_w" value="<?php echo $values['work_thumbnail_w']; ?>" class="width">
		<input type="hidden" id="work_thumbnail_h" name="work_thumbnail_h" value="<?php echo $values['work_thumbnail_h']; ?>" class="height">
		<a href="" class="work_thumbnail_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['work_thumbnail']; ?>" <?php if ($values['work_thumbnail'] == '' || $values['work_thumbnail'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['work_thumbnail'] == '' || $values['work_thumbnail'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['work_thumbnail']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
	imagePickers.push(new MediaPanel({"trigger" : jQuery('.work_thumbnail_Picker')}));
});
</script>
