<?php
/**
 * 404 Page
 *
 * @package WordPress
 * @subpackage TheGate local
 */
get_header();
?>
<body class="sub-page error-page">
    <!-- wrapper -->
    <div id="wrapper" class="sub-page">
        <div class="wrapper-holder">
            <!-- header -->
            <header id="header">
                <div class="header-area">
                    <div class="header-holder">
                        <div class="logo-area">
                            <h1 class="logo"><a href="<?php bloginfo('url'); ?>"><?php _e('The Gate'); ?></a></h1>
                        </div>
                        <!-- button-area -->
                        <div class="button-area">
                            <a href="#" class="open"><?php _e('menu'); ?></a>
                            <a href="javascript:javascript:history.go(-1)" class="back"><?php _e('back'); ?></a>
                        </div>
                        <div class="container nav-popup">
                            <!-- nav -->
                            <nav id="nav">
                                <ul>
                                    <li><a href="<?php echo network_site_url(); ?>asia"><?php _e('ASIA'); ?></a></li>
                                    <li><a href="<?php echo network_site_url(); ?>edinburgh"><?php _e('EDINBURGH'); ?></a></li>
                                    <li><a href="<?php echo network_site_url(); ?>london"><?php _e('LONDON'); ?></a></li>
                                    <li><a href="<?php echo network_site_url(); ?>ny"><?php _e('NEW YORK'); ?></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- visual -->
                <div class="visual">
                </div>
            </header>
                <div class="main-holder" style="padding-top: 100px;">
                   <p><?php _e( 'Woops! The page you are looking for doesn&#39;t currently exist.' ); ?></p>
                   <p><?php _e( '很抱歉！您访问的网页不存在。' ); ?></p>
                </div>
            </div>
         <!--.main-holder end -->
<?php get_footer();?>