<?php
/*
custom post type: work

title
intro_subhead
intro_text
banner: image / videobutton

b2_header
b2_text

pic1 : image

b3_header
b3_text

pic2 : image

b4_header
b4_text

*/

function work_init()
{
    $labels = array(
        'name'                => _x( 'Work', 'post type general name' ),
        'singular_name'       => _x( 'Work', 'post type singular name' ),
        'add_new'             => _x( 'Add New Work', 'work' ),
        'add_new_item'        => __( 'Add New Work'),
        'edit_item'           => __( 'Edit Work' ),
        'new_item'            => __( 'New Work' ),
        'all_items'           => __( 'All Work' ),
        'view_item'           => __( 'View Work' ),
        'search_items'        => __( 'Search Work' ),
        'not_found'           => __( 'No Work found' ),
        'not_found_in_trash'  => __( 'No Work found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Work'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        'supports'           => array( 'title', 'thumbnail', 'tags'),
        'taxonomies' => array('post_tag'),
        'register_meta_box_cb' => 'add_work_metaboxes',

        // remove '/blog/' from beginning of custom posts
        // set custom post permalink to '/work/%postname%' in admin / settings / permalinks
        'rewrite' => array(
            'with_front' => false,
            'slug'       => 'ourwork'
        )
    );

    register_post_type( 'work', $args );
}
add_action( 'init', 'work_init' );

/*
// add custom columns to the admin award 'browse' page and define column headings
function award_columns_head($defaults) {
    $date = $defaults['date'];  // save the date column
    unset($defaults['date']);

    $tags = $defaults['tags'];  // save the date column
    unset($defaults['tags']);

    $defaults['award_image'] = 'Image';
    $defaults['award_award'] = 'Award';
    $defaults['award_category'] = 'category';
    $defaults['award_year'] = 'Year';

    $defaults['tags'] = $tags;
    $defaults['date'] = $date;

    return $defaults;
}

// define what is displayed in each column
function award_columns_content($column_name, $post_ID) {
    $metadata = $metadata = get_post_meta($post_ID);

    $award_image = $metadata['a_image'][0];
    $year = $metadata['a_year'][0];
    $award = $metadata['a_award'][0];
    $category = $metadata['a_category'][0];

    switch($column_name) {
        case "award_image" : {
            echo "<img class=\"listAwardImage\" alt=\"\" src=\"$award_image\">";
        } break;
        case "award_year" : {
            echo $year;
        } break;
        case "award_award" : {
            echo $award;
        } break;
        case "award_category" : {
            echo $category;
        } break;
    }
}
add_filter('manage_award_posts_columns', 'award_columns_head');
add_action('manage_award_posts_custom_column', 'award_columns_content', 1, 2);
*/

/* define custom metabox */
function add_work_metaboxes(){

        add_meta_box(
            'myplugin_work_title',
            __( 'Work page title', 'myplugin_textdomain' ),
            'work_title_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_copy1',
            __( 'Intro copy', 'myplugin_textdomain' ),
            'work_copy1_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_banner',
            __( 'Banner image/video', 'myplugin_textdomain' ),
            'work_banner_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_copy2',
            __( 'Copy 2', 'myplugin_textdomain' ),
            'work_copy2_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_pic1',
            __( 'Image 1', 'myplugin_textdomain' ),
            'work_pic1_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_copy3',
            __( 'Copy 3', 'myplugin_textdomain' ),
            'work_copy3_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_pic2',
            __( 'Image 2', 'myplugin_textdomain' ),
            'work_pic2_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_copy4',
            __( 'Copy 4', 'myplugin_textdomain' ),
            'work_copy4_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_nextpage',
            __( 'Next work item', 'myplugin_textdomain' ),
            'work_nextwork_meta_box_callback',
            'work',
            'normal'
        );

        add_meta_box(
            'myplugin_work_thumbnail',
            __( 'Work thumbnail', 'myplugin_textdomain' ),
            'work_thumbnail_meta_box_callback',
            'work',
            'normal'
        );
}
// add_action( 'add_meta_boxes_quote', 'add_quote_metaboxes' );

function work_title_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'work_title_meta_box_data', 'work_title_meta_box_nonce' );

    $values['work_title'] = get_post_meta( $post->ID, 'work_title', true );

    require get_template_directory() . '/custom_posts/work/metaboxes/work_title_meta.php';
}

function work_title_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_title_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_title_meta_box_nonce'], 'work_title_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('work_title', 'work_title')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'work_title_meta_box_data' );

// first copy block
function work_copy1_meta_box_callback( $post ) {
    wp_nonce_field( 'work_copy1_save_meta_box_data', 'work_copy1_meta_box_nonce' );

    $values = array(
        "wc1_header" => get_post_meta( $post->ID, 'wc1_header', true ),
        "wc1_text" => get_post_meta( $post->ID, 'wc1_text', true )
    );
    
    $fieldSettings = array(
        "headerfield" => "wc1_header",
        "headerlabel" => "header",
        "textfield" => "wc1_text",
        "textwpeditclass" => "wcopy1_wpedit",
        "textwpeditid" => "wcopy1"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_copy.php';
}

function work_copy1_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_copy1_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_copy1_meta_box_nonce'], 'work_copy1_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('wc1_header', 'wc1_header'), // form element name/id, metakey name
        array('wc1_text', 'wc1_text')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'work_copy1_save_meta_box_data' );

function work_banner_meta_box_callback( $post ) {
    wp_nonce_field( 'work_banner_save_meta_box_data', 'work_banner_meta_box_nonce' );

    $values = array(
        "banner_type" => get_post_meta( $post->ID, 'banner_type', true ),
        "wb_option_alt" => get_post_meta( $post->ID, 'wb_option_alt', true ),
        "wb_option_vimeo" => get_post_meta( $post->ID, 'wb_option_vimeo', true ),
        "wb_option_image" =>  get_post_meta( $post->ID, 'wb_option_image', true ),
        "wb_option_image_id" =>  get_post_meta( $post->ID, 'wb_option_image_id', true ),
        "wb_option_image_w" =>  get_post_meta( $post->ID, 'wb_option_image_w', true ),
        "wb_option_image_h" =>  get_post_meta( $post->ID, 'wb_option_image_h', true )
    );

    $settings['prefix'] = 'wb_option';

    require get_template_directory() . '/custom_posts/work/metaboxes/work_banner.php';
}
function work_banner_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_banner_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_banner_meta_box_nonce'], 'work_banner_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }


    switch ( $_POST['banner_type'] ) {
        case "video" : {
            $fields = array(
                array('banner_type', 'banner_type'),
                array('wb_option_vimeo', 'wb_option_vimeo'),
                array('wb_option_image', 'wb_option_image'),
                array('wb_option_image_id', 'wb_option_image_id'),
                array('wb_option_image_', 'wb_option_image_'),
                array('wb_option_image_', 'wb_option_image_')
            );
        } break;
        case "image" : {
            $fields = array(
                array('banner_type', 'banner_type'),
                array('wb_option_alt', 'wb_option_alt'),
                array('wb_option_image', 'wb_option_image'),
                array('wb_option_image_id', 'wb_option_image_id'),
                array('wb_option_image_', 'wb_option_image_'),
                array('wb_option_image_', 'wb_option_image_')
            );
        } break;
    }

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'work_banner_save_meta_box_data' );

function work_copy2_meta_box_callback( $post ) {
    wp_nonce_field( 'work_copy2_save_meta_box_data', 'work_copy2_meta_box_nonce' );

    $values = array(
        "wc2_header" => get_post_meta( $post->ID, 'wc2_header', true ),
        "wc2_text" => get_post_meta( $post->ID, 'wc2_text', true )
    );
    
    $fieldSettings = array(
        "headerfield" => "wc2_header",
        "headerlabel" => "header",
        "textfield" => "wc2_text",
        "textwpeditclass" => "wcopy2_wpedit",
        "textwpeditid" => "wcopy2"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_copy.php';
}

function work_copy2_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_copy2_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_copy2_meta_box_nonce'], 'work_copy2_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('wc2_header', 'wc2_header'), // form element name/id, metakey name
        array('wc2_text', 'wc2_text')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'work_copy2_save_meta_box_data' );

function work_copy3_meta_box_callback( $post ) {
    wp_nonce_field( 'work_copy3_save_meta_box_data', 'work_copy3_meta_box_nonce' );

    $values = array(
        "wc3_header" => get_post_meta( $post->ID, 'wc3_header', true ),
        "wc3_text" => get_post_meta( $post->ID, 'wc3_text', true )
    );
    
    $fieldSettings = array(
        "headerfield" => "wc3_header",
        "headerlabel" => "header",
        "textfield" => "wc3_text",
        "textwpeditclass" => "wcopy3_wpedit",
        "textwpeditid" => "wcopy3"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_copy.php';
}

function work_copy3_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_copy3_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_copy3_meta_box_nonce'], 'work_copy3_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('wc3_header', 'wc3_header'), // form element name/id, metakey name
        array('wc3_text', 'wc3_text')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'work_copy3_save_meta_box_data' );

function work_copy4_meta_box_callback( $post ) {
    wp_nonce_field( 'work_copy4_save_meta_box_data', 'work_copy4_meta_box_nonce' );

    $values = array(
        "wc4_header" => get_post_meta( $post->ID, 'wc4_header', true ),
        "wc4_text" => get_post_meta( $post->ID, 'wc4_text', true )
    );
    
    $fieldSettings = array(
        "headerfield" => "wc4_header",
        "headerlabel" => "header",
        "textfield" => "wc4_text",
        "textwpeditclass" => "wcopy4_wpedit",
        "textwpeditid" => "wcopy4"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_copy.php';
}

function work_copy4_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_copy4_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_copy4_meta_box_nonce'], 'work_copy4_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('wc4_header', 'wc4_header'), // form element name/id, metakey name
        array('wc4_text', 'wc4_text')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'work_copy4_save_meta_box_data' );

function work_pic1_meta_box_callback( $post ) {
    wp_nonce_field( 'work_pic1_save_meta_box_data', 'work_pic1_meta_box_nonce' );

    $valueKeys = array('wp1_alt', 'wp1_image', 'wp1_image_id', 'wp1_image_w', 'wp1_image_h');
    $values = array();
    foreach ( $valueKeys AS $field ) {
        $values[$field] = get_post_meta( $post->ID, $field, true );
    }

    $settings['prefix'] = 'wp1';

    require get_template_directory() . '/custom_posts/work/metaboxes/work_pic.php';
}

function work_pic1_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_pic1_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_pic1_meta_box_nonce'], 'work_pic1_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('wp1_alt', 'wp1_alt'),
        array('wp1_image', 'wp1_image'),
        array('wp1_image_id', 'wp1_image_id'),
        array('wp1_image_w', 'wp1_image_w'),
        array('wp1_image_h', 'wp1_image_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'work_pic1_save_meta_box_data' );

function work_pic2_meta_box_callback( $post ) {
    wp_nonce_field( 'work_pic2_save_meta_box_data', 'work_pic2_meta_box_nonce' );

    $valueKeys = array('wp2_alt', 'wp2_image', 'wp2_image_id', 'wp2_image_w', 'wp2_image_h');
    $values = array();
    foreach ( $valueKeys AS $field ) {
        $values[$field] = get_post_meta( $post->ID, $field, true );
    }

    $settings['prefix'] = 'wp2';

    require get_template_directory() . '/custom_posts/work/metaboxes/work_pic.php';
}

function work_pic2_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_pic2_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_pic2_meta_box_nonce'], 'work_pic2_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('wp2_alt', 'wp2_alt'),
        array('wp2_image', 'wp2_image'),
        array('wp2_image_id', 'wp2_image_id'),
        array('wp2_image_w', 'wp2_image_w'),
        array('wp2_image_h', 'wp2_image_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'work_pic2_save_meta_box_data' );

function work_nextwork_meta_box_callback( $post ) {
    wp_nonce_field( 'work_nextwork_save_meta_box_data', 'work_nextwork_meta_box_nonce' );

    $values = array(
        "work_nextwork" => get_post_meta( $post->ID, 'work_nextwork', true )
    );

    $fieldSettings = array(
        "nextpagefield" => "work_nextwork"
    );

    require get_template_directory() . '/custom_posts/work/metaboxes/generic_nextwork.php';
}

function work_nextwork_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_nextwork_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_nextwork_meta_box_nonce'], 'work_nextwork_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('work_nextwork', 'work_nextwork')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'work_nextwork_save_meta_box_data' );

function work_thumbnail_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'work_thumbnail_meta_box_data', 'work_thumbnail_meta_box_nonce' );

    $values['work_thumbnail'] = get_post_meta( $post->ID, 'work_thumbnail', true );
    $values['work_thumbnail_w'] = get_post_meta( $post->ID, 'work_thumbnail_w', true );
    $values['work_thumbnail_h'] = get_post_meta( $post->ID, 'work_thumbnail_h', true );
    $values['work_thumbnail_id'] = get_post_meta( $post->ID, 'work_thumbnail_id', true );

    require get_template_directory() . '/custom_posts/work/metaboxes/work_thumbnail_meta.php';
}

function work_thumbnail_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['work_thumbnail_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['work_thumbnail_meta_box_nonce'], 'work_thumbnail_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('work_thumbnail','work_thumbnail'),
        array('work_thumbnail_w','work_thumbnail_w'),
        array('work_thumbnail_h','work_thumbnail_h'),
        array('work_thumbnail_id','work_thumbnail_id')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'work_thumbnail_meta_box_data' );
/*
*/
?>
