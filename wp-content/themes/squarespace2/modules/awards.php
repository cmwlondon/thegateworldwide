<?php
/*
awards list module
/custom_posts/awards/
custom post type: 'awards'
functions/awards.php
 -> get_awards();
 -> get_awards_in_order(order:array);
 -> synchronise_awards(order:array, postid:int, metakey:string)
*/
/* query variables
$awards_title
$awards_order
$pageid
$metakey
*/

$synced_awards = synchronise_awards( explode(',', $awards_order), $pageid, $metakey );
$awards = (count($synced_awards['assigned']) > 0) ? get_awards_in_order($synced_awards['assigned']) : [];
$awardCount = count($awards);
$clip = array(
	'width' => 213,
	'height' => 154,
	'aspectratio' => 154 / 213
);
if ($awardCount > 0) :
?>
		<article class="module awards">
			<div class="singleColumn">
				<h2><?php echo $awards_title; ?></h2>
			</div>
			<div class="frame">

				<section class="awardPages2" data-count="<?php echo $awardCount; ?>" style="opacity:1;">
					<article>

					<?php
					foreach($awards AS $key => $award) :
						$award_id = $award->ID;
						$metadata = $metadata = get_post_meta($award_id);

						$award_title = $award->post_title;
					    // $award_image = $metadata['a_image'][0];
					    $award_year = $metadata['a_year'][0];
					    $award_text = $metadata['a_award'][0];
					    $award_category = $metadata['a_category'][0];

					    // get dimensions of image
					    $attachment = wp_get_attachment_image_src($metadata['pic_id'][0],'full'); 
					    $awardpic = array(
					    	"url" => $attachment[0],
					    	"id" => $metadata['pic_id'][0],
					    	"width" => $attachment[1],
					    	"height" => $attachment[2]
					    );

					    // scale image to fit clipping box
						$picAspectRatio = $awardpic['height'] / $awardpic['width'];

						if ( $awardpic['width'] < $clip['width'] && $awardpic['height'] < $clip['height'] ) {
						    // centre image in clipping box without scaling
						    $marginLeft = floor($awardpic['width'] / 2);
						    $marginTop = floor($awardpic['height'] / 2);
						    $style = "left:50%;top:50%;margin-left:-{$marginLeft}px;margin-top:-{$marginTop}px;";

						} else {
							if ( $picAspectRatio > $clip['aspectratio']) {
								$new_w = $clip['width'];
								$scale = $new_w / $awardpic['width'];
								$new_h = floor($awardpic['height'] * $scale);
								$h_offset = floor($new_h / 2);
								$style = "left:0;top:50%;margin-top:-{$h_offset}px;width:{$new_w}px;height:{$new_h}px;";
							}

							if ( $picAspectRatio < $clip['aspectratio']) {
								$new_h = $clip['width'];
								$scale = $new_h / $awardpic['height'];
								$new_w = floor($awardpic['width'] * $scale);
								$w_offset = floor($new_w / 2);
								$style = "top:0;left:50%;margin-left:-{$w_offset}px;width:{$new_w}px;height:{$new_h}px;";
							}
						}
						
					?>
						<div class="award" id="<?php echo "award" . $key; ?>" data-id="<?php echo $award_id; ?>">
							<div class="pictureFrame">
								<div class="clip">
									<img style="<?php echo $style; ?>" alt="<?php echo  $award_title; ?>" class="logo" src="<?php echo $awardpic['url']; ?>">
								</div>
							</div>
							<div class="text">
								<p class="date"><?php echo $award_year; ?></p>
								<h3><?php echo $award_text; ?></h3>
								<h4><?php echo $award_category; ?></h4>
							</div>
						</div>
					<?php
					endforeach;
					?>
					</article>

					<div class="controls"></div>
					<br class="cb">
				</section>

			</div>
		</article>
<?php endif; ?>