<?php
/**
 * Template Name: People Individual
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $bio_page,
       $bio,
       $everyone_page,
       $everyone,
       $leadership_page,
       $leaders,
       $listview_page,
       $listview,
       $seed,
       $individual_page_permalink,
	   $viewEveryone;

/* If id is not set redirect to 404 page */
if(!isset($_GET['slug']) && $_GET['slug'])
{
    wp_redirect(home_url('404'), 303);
}

$individual_page_permalink = get_permalink();

/* Check on what was clicked to come on this page: on everyone or leaders */
$everyone_with_bio = (isset($_GET['e'])) ? $_GET['e'] : false;

/* Set post to be parent page in order to get children templates */
if(basename(get_page_template()) !== 'full-people.php' && $post->post_parent)
{
    $post = get_post($post->post_parent);
}

$viewEveryone = false;

$people = new PeopleHelper();

/* get the seed for the order of the people on the everyone carousel */
$seed = (isset($_GET['o']) && $_GET['o']) ? (int)$_GET['o'] : null;
/* Get first children that has template "People Everyone" */
$everyone_page = get_first_children_page_by_template('part-people_everyone.php');
/* Get everyone's list */
$everyone = $people->get_list('everyone', $everyone_page->ID, $seed, $everyone_with_bio);
$seed = $people->seed;

/* Get first children that has template "People Leadership" */
$leadership_page = get_first_children_page_by_template('part-people_leadership.php');
/* Get leadership's list of offices */
$leaders = $people->get_list('leadership', $leadership_page->ID, null, !$everyone_with_bio);

/* Get first children that has template "People Listview" */
$listview_page = get_first_children_page_by_template('part-people_listview.php');
$listview = $people->get_list('listview', $listview_page->ID);

/* Get first children that has template "People Bio" */
$bio_page = get_first_children_page_by_template('part-people_bio.php');
/* in Bio carousel show everyone or leaders depending on the way the user came to this page */
$bio = ($everyone_with_bio) ? $everyone : call_user_func_array('array_merge', $leaders);

get_header();

wp_enqueue_script('anything_slider');
wp_enqueue_script('tablesorter');
wp_enqueue_script('equal_heights');
wp_enqueue_script('modernizr');
wp_enqueue_script('dl_menu');
wp_enqueue_script('page_transitions');

?>
    <!-- menu -->
    <ul class="menu">
        <?php if($bio_page) : ?>
            <li class="active"><a id="returnPage3" href="#individual"><?php echo get_post_meta($bio_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($bio_page) : ?>
            <li><a id="returnPage1" href="#everyone"><?php echo get_post_meta($everyone_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($leadership_page) : ?>
            <li><a id="returnPage2" href="#leadership"><?php echo get_post_meta($leadership_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($listview_page) : ?>
            <li><a id="iterateEffects" href="#list-view"><?php echo get_post_meta($listview_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
    </ul>
</header>
<!-- main -->
<div id="main">
    <!-- content -->
    <div id="pt-main" class="pt-perspective">
        <div class="pt-page pt-page-1">
            <ul class="simple-list individual-mobile">
                <?php
                    get_template_part('part', 'people_bio');
                    get_template_part('part', 'people_everyone');
                    get_template_part('part', 'people_leadership');
                ?>
            </ul><!--.simple-list-->
        </div><!--.pt-page-1-->
        <?php get_template_part('part', 'people_listview'); ?>
    </div>
</div>
<?php get_footer(); ?>