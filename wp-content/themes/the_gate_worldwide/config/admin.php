<?php
/**
 * WP Theme Admin settings
 *
 * @package WordPress
 * @subpackage thegate_worldwide
 */


/**
 * Load required admin assets
 */
function thegate_worldwide_load_admin_assets()
{
    global $pagenow, $typenow;

    wp_register_style( 'thegate_worldwide_admin_css',  get_bloginfo( 'template_url' ) . '/assets/css/admin.css' );
    wp_enqueue_style( 'thegate_worldwide_admin_css' );

    wp_register_script( 'the_gate_media_modal', get_bloginfo( 'template_url' ) . '/assets/js/admin/libs/media_modal.js' );
    wp_enqueue_script( 'the_gate_media_modal' );

    wp_register_script( 'restrict_excerpt_size',  get_bloginfo( 'template_url' ) . '/assets/js/admin/restrict-excerpt-size.js' );
    wp_enqueue_script( 'restrict_excerpt_size' );

    wp_register_script( 'fill_posts',  get_bloginfo( 'template_url' ) . '/assets/js/admin/fill_posts.js' );
    wp_enqueue_script( 'fill_posts' );

    if('post.php' == $pagenow || 'post-new.php' == $pagenow)
    {
        wp_register_script( 'the_gate_media_modal', get_bloginfo( 'template_url' ) . '/assets/js/admin/libs/media_modal.js' );
        wp_enqueue_script( 'the_gate_media_modal' );

        wp_register_script( 'the_gate_gallery', get_bloginfo( 'template_url' ) . '/assets/js/admin/gallery.js' );
        wp_enqueue_script( 'the_gate_gallery' );
    }


}
 add_action( 'admin_init', 'thegate_worldwide_load_admin_assets' );


/**
 * Remove unused admin menus
 */
function thegate_worldwide_remove_admin_menus()
{
    if( defined('THEME_ENVIRONMENT') && THEME_ENVIRONMENT == 'production' )
    {
        global $menu, $submenu;

        // Rename comments to 'Comments & Testimonials'
        // $menu[25][0] .= '&amp; Testimonials';

        // Remove items //

        unset($menu[5]);// Posts
        unset($menu[10]);// Media
        unset($menu[15]);// Links
        unset($menu[25]);// Comments

        unset( $submenu['themes.php'][5] );// Themes
        unset( $submenu['themes.php'][10] );// Menus

        unset( $submenu['tools.php'][25] );// Delete Site

        unset( $submenu['options-general.php'][15] );// Settings -> Writing
        unset( $submenu['options-general.php'][20] );// Settings -> Reading
        unset( $submenu['options-general.php'][25] );// Settings -> Discussion
        unset( $submenu['options-general.php'][30] );// Settings -> Media
        unset( $submenu['options-general.php'][40] );// Settings -> Permalinks

    }
}
add_action( 'admin_menu', 'thegate_worldwide_remove_admin_menus' );

/**
 * Hide Wordpress original widgets
 */
function thegate_worldwide_remove_default_widgets()
{
    if( function_exists( 'unregister_sidebar_widget' ) )
    {
        $_wgt = array(
            'WP_Widget_Pages',
            'WP_Widget_Calendar',
            'WP_Widget_Archives',
            'WP_Widget_Links',
            'WP_Widget_Meta',
            'WP_Widget_Search',
            'WP_Widget_Text',
            'WP_Widget_Categories',
            'WP_Widget_Recent_Posts',
            'WP_Widget_Recent_Comments',
            'WP_Widget_RSS',
            'WP_Widget_Tag_Cloud',
            'WP_Nav_Menu_Widget',
        );

        foreach ( $_wgt as $v )
        {
            unregister_widget( $v );
        }
    }
}
add_action( 'widgets_init', 'thegate_worldwide_remove_default_widgets' );


/**
 * Remove Default Dashboard Widgets
 */
function thegate_worldwide_remove_dashboard_widgets()
{
    global $wp_meta_boxes;

    // Quick Press
    unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
}
add_action( 'wp_dashboard_setup', 'thegate_worldwide_remove_dashboard_widgets' );


/**
 * Add Dashboard Right Now Widget support for custom post types
 */
function ucc_right_now_content_table_end()
{
  $args = array( 'public' => true ,'_builtin' => false );
  $output = 'object';
  $operator = 'and';

  $post_types = get_post_types( $args , $output , $operator );

  foreach( $post_types as $post_type )
  {
    $num_posts = wp_count_posts( $post_type->name );
    $num = number_format_i18n( $num_posts->publish );
    $text = _n( $post_type->labels->singular_name, $post_type->labels->name , intval( $num_posts->publish ) );
    if ( current_user_can( 'edit_posts' ) )
    {
      $num = "<a href='edit.php?post_type={$post_type->name}'>{$num}</a>";
      $text = "<a href='edit.php?post_type={$post_type->name}'>{$text}</a>";
    }
    echo '<tr><td class="first b b-' . $post_type->name . '">' . $num . '</td>';
    echo '<td class="t ' . $post_type->name . '">' . $text . '</td></tr>';
  }

  $taxonomies = get_taxonomies( $args , $output , $operator );

  foreach( $taxonomies as $taxonomy )
  {
    $num_terms  = wp_count_terms( $taxonomy->name );
    $num = number_format_i18n( $num_terms );
    $text = _n( $taxonomy->labels->singular_name, $taxonomy->labels->name , intval( $num_terms ) );
    if ( current_user_can( 'manage_categories' ) )
    {
      $num = "<a href='edit-tags.php?taxonomy={$taxonomy->name}'>{$num}</a>";
      $text = "<a href='edit-tags.php?taxonomy={$taxonomy->name}'>{$text}</a>";
    }
    echo '<tr><td class="first b b-' . $taxonomy->name . '">' . $num . '</td>';
    echo '<td class="t ' . $taxonomy->name . '">' . $text . '</td></tr>';
  }

}
add_action( 'right_now_content_table_end' , 'ucc_right_now_content_table_end' );

/**
 * Custom WP_Editor Default Settings
 */
$wp_editor_default_settings = array(
    'media_buttons' => false,
	'wpautop' => 0,
    'quicktags' => false,
    'teeny' => true,
    'editor_class' => 'standalone_editor',
    'tinymce' => array(
        'theme_advanced_buttons1' => 'bold, italic, underline, forecolor,|, link, unlink,|, code',
        'force_p_newlines' => false,
        'force_br_newlines' => 1,
        'theme_adforced_root_blockvanced_buttons1' => '',
        'content_css' => get_stylesheet_directory_uri() . '/assets/css/editor_standalone.css',
    )
);
?>