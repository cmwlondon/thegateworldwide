<?php
/**
 * Validation Utility class
 */

class ValidationHelper
{
    public $errors;
    private $feedback_array;
    private $required_fields;
    private $error_messages;


    public function __construct($feedback_array, $required_fields)
    {
        $this->feedback_array = $feedback_array;
        $this->required_fields = $required_fields;
        $this->errors = array();
    }

    public function validate()
    {
        foreach ($this->required_fields as $name => $requirements)
        {
            if(isset($this->feedback_array[$name]))
            {
                /* Check all requirements for the field until one does not pass */
                if(is_array($requirements))
                {
                    foreach ($requirements as $requirement)
                    {
                        $result = true;

                        if(substr( $requirement, 0, 11 ) === "is_same_as_")
                        {
                            $result = $this->is_same_as($name, substr( $requirement, 11));
                        }
                        elseif(method_exists($this, $requirement))
                        {
                            $result = $this->{$requirement}($name);
                        }
                        /* If a requirement is not met do not check other requirements for this field */
                        if(!$result) break;
                    }
                }
            }
        }

        if(empty($this->errors))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function is_email($field_name)
    {
        $email = $this->feedback_array[$field_name];

        if($email && filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            return true;
        }

        $this->errors[$field_name] = __FUNCTION__;

        return false;
    }

    private function is_same_as($field_name, $other_field_name)
    {
        $field = $this->feedback_array[$field_name];
        $other_field = $this->feedback_array[$other_field_name];

        if($field && $other_field && $field == $other_field)
        {
            return true;
        }

        $this->errors[$field_name] = __FUNCTION__;

        return false;
    }

    private function is_required($field_name)
    {
        $field = $this->feedback_array[$field_name];

        if($field && trim($field) !== '')
        {
            return true;
        }

        $this->errors[$field_name] = __FUNCTION__;

        return false;
    }

    private function is_phone_number($field_name)
    {
        $field = $this->feedback_array[$field_name];

        if(strstr(trim($field), '  '))
        {
            $this->errors[$field_name] = __FUNCTION__;

            return false;
        }

        $field = preg_replace('/\s/', '', $field);

        if((int)$field && ctype_digit($field) && strlen($field) >= 5)
        {
            return true;
        }

        $this->errors[$field_name] = __FUNCTION__;

        return false;
    }

}
