<?php
/**
 * Media Value
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Creation of the custom posttype */
add_action( 'init', 'create_media_value_post_type');
function create_media_value_post_type()
{
    $labels = array(
        'name' => __('Media Values'),
        'singular_name' => __('Media Value'),
        'add_new' => __('Add New Media Value'),
        'add_new_item' => __('Add New Media Value'),
        'edit_item' => __('Edit Media Value'),
        'new_item' => __('New Media Value'),
        'all_items' => __('All Media Values'),
        'view_item' => __('View Media Value'),
        'search_items' => __('Search Media Values'),
        'not_found' =>  __('No media_values found'),
        'not_found_in_trash' => __('No media_values found in Trash'),
        'parent_item_colon' => __(''),
        'menu_name' => __('Media Values')
    );

    register_post_type( 'media_value',
            array(
            'labels' => $labels,
            'public' => false,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'show_ui' => true,
            'query_var' => true,
            'show_in_nav_menus' => true,
            'has_archive' => true,
            'show_in_menu' => 'edit.php?post_type=page',
            'show_in_admin_bar' => true,
            'menu_position' => 27,
            'menu_icon' => get_bloginfo('template_directory') . '/post_types/media_value/img/media_value_small.png',
            'supports' => array('title', 'editor', 'thumbnail'),
            'hierarchical' => true,
            'rewrite' => array('slug' => 'media_values'),
        )
    );
 }

/* Styling for the custom post type icon */
add_action( 'admin_head', 'add_media_value_large_icon' );

function add_media_value_large_icon()
{
?>
    <style type="text/css" media="screen">
        #icon-edit.icon32-posts-media_value {background: url(<?php bloginfo('template_url') ?>/post_types/media_value/img/media_value_large.png) no-repeat;}
    </style>
<?php
}

/* add filter to ensure the text thas Pizza is updated, is displayed when user updates a document */
add_filter( 'post_updated_messages', 'set_media_value_updated_messages' );

function set_media_value_updated_messages( $messages )
{
  global $post, $post_ID;

  $messages['media_value'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Media Value updated. <a href="%s">View Media Value</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Media Value updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Media Value restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Media Value published. <a href="%s">View Media Value</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Media Value saved.'),
    8 => sprintf( __('Media Value submitted. <a target="_blank" href="%s">Preview Media Value</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Media Value scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Media Value</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Media Value draft updated. <a target="_blank" href="%s">Preview Media Value</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
