<?php
/**
 * Front page
 *
 * @package WordPress
 * @subpackage 2local2gate
 */
get_header();
?>



<div class="container section-pad dogcoats">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-4 pull-left">
				<div>
					<img src="<?php bloginfo('template_directory') . _e('/assets/images/Agency-Website---master_04.jpg') ?>" />
				</div>
			</div>
			<div class="col-md-8">
                            <p class="count-font">The Gate is a <a class="number-link" href="<?php echo site_url() ?>/fullservice"><strong>full service</strong></a> advertising agency network.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 pull-left">
				<div>
					<img src="<?php bloginfo('template_directory') . _e('/assets/images/Agency-Website---master_07.jpg') ?>" />
				</div>
			</div>
			<div class="col-md-8">
                            <p class="count-font">We create <a class="number-link" href="<?php echo site_url() ?>/stories/#transformation"><strong>transformative ideas</strong></a>,  that help level the competitive playing field for challenger brands.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 pull-left">
				<div>
					<img src="<?php bloginfo('template_directory') . _e('/assets/images/Agency-Website---master_14.jpg') ?>" />
				</div>
			</div>
			<div class="col-md-8">
                            <p class="count-font">As challengers ourselves, we are naturally drawn to working with <a class="number-link" href="<?php echo site_url() ?>/stories"><strong>like-minded people and organisations.</strong></a></p>
			</div>
		</div>
	</div>

	<!-- RIGHT COL -->

	<div class="col-md-6">
		<div class="row">
			<div class="col-md-4 pull-left">
				<div>
					<img src="<?php bloginfo('template_directory') . _e('/assets/images/Agency-Website---master_06.jpg') ?>" />
				</div>
			</div>
			<div class="col-md-8">
                <p class="count-font">We use a set of principles we call <a class="number-link" href="<?php echo site_url() ?>/predatory-thinking"><strong>Predatory Thinking</strong></a> to deliver clever ideas to overcome challenging odds.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 pull-left">
				<div>
					<img src="<?php bloginfo('template_directory') . _e('/assets/images/Agency-Website---master_11.jpg') ?>" />
				</div>
			</div>
			<div class="col-md-8">
                 <p class="count-font">Because we believe that it’s healthy, fun and good  business to keep <a class="number-link" href="<?php echo site_url() ?>/about"><strong>challenging the status quo.</strong></a></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 pull-left">
				<div>
					<img src="<?php bloginfo('template_directory') . _e('/assets/images/Agency-Website---master_14-07.jpg') ?>" />
				</div>
			</div>
			<div class="col-md-8">
                 <p class="count-font">Every dog has its day so, <a class="number-link" href="<?php echo site_url() ?>/stories"><strong>grab an advantage</strong></a>, right out of the gate. </p>
			</div>
		</div>
	</div>
</div>
<div class="story_boxes clearfix">
        <div class="col-md-4 padnone">
        	<div class="clip"><div><img src="<?php bloginfo('template_directory') . _e('/assets/images/box1.jpg') ?>" width="100%" /></div></div>
        	<p class="transform">Watch our <span>TRANSFORMATION</span> stories</p>
        	<a href="<?php echo site_url() ?>/stories/#transformation"></a>
        </div>
        <div class="col-md-4 padnone">
    		<div class="clip"><div><img src="<?php bloginfo('template_directory') . _e('/assets/images/box2.jpg') ?>" width="100%" /></div></div>
			<p class="invigorate">Watch our <span>REINVIGORATION</span> stories</p>        	
        	<a href="<?php echo site_url() ?>/stories/#reinvigoration"></a>
        </div>
        <div class="col-md-4 padnone">
			<div class="clip"><div><img src="<?php bloginfo('template_directory') . _e('/assets/images/box3.jpg') ?>" width="100%" /></div></div>
			<p class="initiate">Watch our <span>INITIATION</span> stories</p>        	
			<a href="<?php echo site_url() ?>/stories/#initiation"></a>
        </div>
</div>
</div>


<?php
get_footer();
