<?php
// Include MetaBox Library
include_once( trailingslashit(THEME_FULL_PATH) . 'includes/wpalchemy/MetaBox.php' );
 
// Global styles for the meta boxes
function load_wpalchemy_assets()
{
    wp_enqueue_style( 'wpalchemy-metabox', get_bloginfo('template_url') . '/includes/metaboxes/meta.css' );
}
add_action( 'admin_init', 'load_wpalchemy_assets' );
/* eof */