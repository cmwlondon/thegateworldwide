<?php
/*
people/contacs list module
/custom_posts/people/
custom post type: 'person'
functions.php -> get_people();
*/
global $theme_root;

if ($team[2] != '') :
	$syncedTeam = synchronise_team(explode(',', $team[2]), $postid);
	$teamMembers = get_people_in_order($syncedTeam['assigned']); // 

?>
		<article class="generalContent management">
			<div class="singleColumn p1">
				<h2><?php echo $team[0]; ?></h2>
				<hr>
				<p><?php echo $team[1]; ?></p>
			</div>
			<div class="people">
				<!-- iterator: people-contacts -->

			<?php
			
			foreach($teamMembers AS $key => $person) :
				$person_id = $person->ID;
				$metadata = $metadata = get_post_meta($person_id);

				// $personDetails = unserialize($metadata['person_details'][0]);

				$forename = $metadata['p_forename'][0];
				$surname = $metadata['p_surname'][0];
				$role = $metadata['p_role'][0];
				$section = $metadata['p_section'][0];
				$email = $metadata['p_email'][0];
				$linkedin = $metadata['p_linkedin'][0];
				$phone = $metadata['p_phone'][0];
				$mobile = $metadata['p_mobile'][0];
				$portrait = $metadata['p_portrait'][0];
				
				$fullName = $forename . ' ' . $surname;
			?>
				<!-- item start -->
				<div class="person">
					<div class="interior">
						<div class="clipper"><img alt="<?php echo $fullName; ?>" src="<?php echo $portrait; ?>"></div>
						<div class="text">
							<h3><?php echo $section; ?></h3>
							<h4>Key contact:</h4>
							<p><?php echo $fullName; ?><br>
							<?php echo $role; ?></p>
							<p>Direct: <?php echo $phone; ?><br>
							Mobile: <?php echo $mobile; ?></p>
							<div class="actions"><a href="mailto:<?php echo $email; ?>" class="email">email</a><a href="<?php echo $linkedin; ?>" class="linkedin">linkedin</a></div>
						</div>
					</div>
				</div>
				<!-- item end -->
			<?php
				endforeach;
			?>


				<br class="cb">
			</div>
		</article>
<?php
endif;
?>		
