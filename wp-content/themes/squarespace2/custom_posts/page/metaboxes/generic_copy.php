<?php
/* generic wp edit text field */
/*
$fieldSettings = array(
    "headerlabel" => "",
    "headerfield" => "",
    "textfield" => "",
    "textwpeditclass" => "",
    "textwpeditid" => ""
)

$values = array(
    $fieldSettings["headerfield"] => "",
    $fieldSettings["textfield"] => ""
)
*/
?>

<div class="customField mediumtext">
	<label for="<?php echo $fieldSettings['headerfield']; ?>"><?php echo $fieldSettings['headerlabel']; ?></label>
	<input type="text" id="<?php echo $fieldSettings['headerfield']; ?>" name="<?php echo $fieldSettings['headerfield']; ?>" value="<?php echo $values[$fieldSettings['headerfield']]; ?>">
</div>

<div class="customField wpeditor">
    <p>copy</p>
    <?php
        $settings = array(
            'textarea_name' => $fieldSettings['textfield'],
            'editor_class'  => $fieldSettings['textwpeditclass'],
            'textarea_rows' => 6,
            'wpautop' => true
        );
        wp_editor( html_entity_decode($values[$fieldSettings['textfield']]), $fieldSettings['textwpeditid'], $settings );
        // wp_editor( $values['cc1_text'], 'introwp', $fieldSettings );
    ?>
</div>
