<?php
/**
 * Template Name: Stories
 *
 * @package WordPress
 * @subpackage 2local2gate
 */
get_header();
?>

<div class="container stories text-center">

<?php
// get selected home boxes
// functions.php line 656
$home_boxes = get_home_boxes($post->ID);
$sections = array();
foreach ($home_boxes AS $box) {
	$videoClass = $box['hasvideo'] ? ' hasVideo' : '';
	$startVideoAnchor = $box['hasvideo'] ? '<a href="#" class="overlayLink" data-toggle="modal" data-target="#stories__modal" data-video="' . $box['video'] . '">' : '';
	$endVideoAnchor = $box['hasvideo'] ? '</a>' : '';	
	$tag = $box['tag'];
	$sections[$tag] = $sections[$tag] ? : '';

	$html = '<div class="col-md-3 col-sm-6' . $videoClass . '" id="homebox' . $box['id'] . '">' . "\n" .
			$startVideoAnchor .
			'<img alt="" src="' . $box['tile'] . '">' . "\n" .
			'<span class="hover hide">' . "\n".
				'<img alt="" src="' . $box['tilebackground'] . '">' . "\n" .
				'<span class="desc">' . "\n" .
				'<h3>' . $box['title'] . '</h3>' . "\n" .
				'<p>' . $box['copy'] . '</p>' . "\n" .
				'</span>' . "\n" .
			'</span>' . "\n" .
			$endVideoAnchor.			
		'</div>';

	$sections[$tag] .= $html;
}

?>
	<!-- Heading -->
	<div class='row text-center center-block stories__header'>
		<div class="col-md-3 col-md-offset-2">
			<h2>
				<a href="#transformation">
					<small>Watch our</small><br /> TRANSFORMATION <br> <small>stories</small>
				</a>
			</h2>
		</div>
		<div class="col-md-3 stories__header--reinv">
			<h2>
				<a href="#reinvigoration">
					<small>Watch our</small><br /> REINVIGORATION <br> <small>stories</small>
				</a>
			</h2>
		</div>
		<div class="col-md-3 stories__header--initi">
			<h2>
				<a href="#initiation">
					<small>Watch our</small><br /> INITIATION <br> <small>stories</small>
				</a>
			</h2>
		</div>
	</div>
	
	<!-- Transformation -->
	<div id="transformation" class="row stories__row stories__row--trans">
		<h3 class=""><span>TRANSFORMATION <br> <small>Stories</small></span></h3>
		<?php _e($sections['transformation']) ?>
	</div>

	<!-- Reinvigoration -->
	<div id="reinvigoration" class="row  stories__row stories__row--reinv">
		<h3><span>REINVIGORATION <br> <small>Stories</small></span></h3>
		<?php _e($sections['reinvigoration']) ?>
	</div>

	<!-- Initiation -->
	<div id="initiation" class="row  stories__row stories__row--initi">
		<h3><span>INITIATION <br> <small>Stories</small></span></h3>
		<?php _e($sections['initiation']) ?>
	</div>
	
	
	<div class="modal fade" id="stories__modal" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body">
					<div class="embed-responsive embed-responsive-16by9">
						<video preload="yes" autoplay="yes" width="600" height="400" controls onloadeddata="">
							<source src="" type="video/mp4"></source>
						</video>
					</div>
					<a class="close-video" data-dismiss="modal" style="text-decoration:none">X</a>
				</div>
			</div>  
		</div>
	</div>

</div>


<?php
get_footer();
