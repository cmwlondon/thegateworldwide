<?php
$people = new PeopleHelper();
$everyone_page = get_first_children_page_by_template('part-people_everyone.php');
$everyone = $people->get_london_people('dropdown');

while($metabox->have_fields_and_multi('entries')) :
$mb->the_group_open();
$metabox->the_field('londonleaders');
?>
<div class="london_leadership">
    <select class="landing-page-entry-post" name="<?php $mb->the_name(); ?>" style="width: 250px;">
    	<option value=""><?php _e(''); ?></option>
        <?php foreach($everyone as $people) : ?>
            <option value="<?php echo $people["id"]; ?>" <?php $mb->the_select_state($people["id"]); ?>><?php echo $people['first_name']; ?> <?php echo $people['last_name']; ?></option>
        <?php endforeach; ?>
    </select>
    <a href="#" class="dodelete delete-landing-page-entry button"><?php _e('Remove Entry'); ?></a>
</div>
<?php
$mb->the_group_close();
endwhile;
?>
<div class="clear"></div>
<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-entries button"><?php _e('Add Entry'); ?></a></p>
