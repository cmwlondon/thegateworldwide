<?php
/*
Template Name: news

@package WordPress
@subpackage stackbartlett
*/

// get eight latest news posts
$query_parameters = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 8,
    'orderby' => array(
        'date'          => 'DESC'
    )
);

$xp = do_query($query_parameters);

?>
<?php get_header();?>
	</div>

	<div id="content" class="site-content latestnews">

		<div class="newslist">
			<ul>
<?php
foreach ($xp AS $item) :
$post_content = $item->post_content;
$pretty_date = get_the_date('F j, Y', $item->ID); // MONTH DD, YYYY 
?>
				<li>
					<h2><a href="<?php echo get_permalink($item->ID); ?>"><?php echo $item->post_title; ?></a></h2>
					<h3><?php echo $pretty_date; ?></h3>
					<hr>
				</li>
<?php endforeach; ?>
			</ul>
		</div> 

<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
