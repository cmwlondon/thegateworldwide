<?php
/*
custom post type: person

photo
name
role
bio
link: INSTAGRAM / TWITTER / LINKEDIN / YOUTUBE / none
    https://www.instagram.com
    https://twitter.com/
    https://www.linkedin.com/
    https://www.youtube.com/
*/

function squarespace2_people_init()
{
        $labels = array(
            'name'                => _x( 'People', 'post type general name' ),
            'singular_name'       => _x( 'Person', 'post type singular name' ),
            'add_new'             => _x( 'Add New Person', 'person' ),
            'add_new_item'        => __( 'Add New Person'),
            'edit_item'           => __( 'Edit Person' ),
            'new_item'            => __( 'New Person' ),
            'all_items'           => __( 'All People' ),
            'view_item'           => __( 'View Person' ),
            'search_items'        => __( 'Search People' ),
            'not_found'           => __( 'No People found' ),
            'not_found_in_trash'  => __( 'No People§ found in Trash' ),
            'parent_item_colon'   => '',
            'menu_name'           => 'People'
        );
        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'has_archive'        => 'People',
            'hierarchical'       => false,
            'menu_position'      => null,
            // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
            'supports'           => array( 'title', 'thumbnail'),
            'register_meta_box_cb' => 'add_person_metabox'
        );

        register_post_type( 'person', $args );

        /* add custom columns to the admin people 'browse' page */
        function person_columns_head($defaults) {
            $date = $defaults['date'];  // save the date column
            unset($defaults['date']);

            $defaults['_person_image'] = 'Portrait';
            $defaults['_person_role'] = 'Role';
            $defaults['date'] = $date;
            return $defaults;
        }

        /* define what is displayed in each column */ 
        function person_columns_content($column_name, $post_ID) {
            $metadata = $metadata = get_post_meta($post_ID);

            $role = $metadata['p_role'][0];
            $section = $metadata['p_section'][0];
            $portrait = $metadata['person_normal'][0];

            switch($column_name) {
                case "_person_image" : {
                    echo "<img class=\"listPortrait\" alt=\"\" src=\"$portrait\">";
                } break;
                case "_person_role" : {
                    echo $role;
                } break;
            }
        }
        add_filter('manage_person_posts_columns', 'person_columns_head');
        add_action('manage_person_posts_custom_column', 'person_columns_content', 1, 2);

}
add_action( 'init', 'squarespace2_people_init' );

/* define custom metabox */
function add_person_metabox() {

    $screens = array( 'person' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'myplugin_of',
            __( 'Person', 'myplugin_textdomain' ),
            'person_meta_box_callback',
            $screen,
            'normal'
        );

    }
}
// add_action( 'add_meta_boxes_quote', 'add_quote_metaboxes' );

function person_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'person_save_meta_box_data', 'person_meta_box_nonce' );

    $values['person_normal'] = get_post_meta( $post->ID, 'person_normal', true );
    $values['person_normal_id'] = get_post_meta( $post->ID, 'person_normal_id', true );
    $values['person_normal_w'] = get_post_meta( $post->ID, 'person_normal_w', true );
    $values['person_normal_h'] = get_post_meta( $post->ID, 'person_normal_h', true );
    $values['name'] = get_post_meta( $post->ID, 'p_name', true );
    $values['role'] = get_post_meta( $post->ID, 'p_role', true );
    $values['bio'] = get_post_meta( $post->ID, 'p_bio', true );
    $values['link'] = get_post_meta( $post->ID, 'p_link', true );
    
    require get_template_directory() . '/custom_posts/people/metaboxes/person.php';
}

function person_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['person_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['person_meta_box_nonce'], 'person_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('person_name', 'p_name'),
        array('person_role', 'p_role'),
        array('person_link', 'p_link'),
        array('person_normal', 'person_normal'),
        array('person_normal_id', 'person_normal_id'),
        array('person_normal_width', 'person_normal_w'),
        array('person_normal_height', 'person_normal_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

    if ( isset( $_POST['person_bio'] )) {
        $my_data = htmlentities( $_POST['person_bio'] );
        $my_data = $_POST['person_bio'];
        update_post_meta( $post_id, 'p_bio', $my_data );
    }
}
add_action( 'save_post', 'person_save_meta_box_data' );

?>
