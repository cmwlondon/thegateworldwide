<?php
?>
<div class="customField mediumtext">
	<label for="hw_title">Banner Title</label>
	<input type="text" id="hw_title" name="hw_title" value="<?php echo $values['hw_title']; ?>">
</div>
 
<div class="customField mediumtext">
	<label for="hw_link">Banner Link</label>
	<input type="text" id="hw_link" name="hw_link" value="<?php echo $values['hw_link']; ?>">
</div>
 
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Image</label>
		<input type="hidden" id="hw_image" name="hw_image" value="<?php echo $values['hw_image']; ?>">
		<input type="hidden" id="hw_image_id" name="hw_image_id" value="<?php echo $values['hw_image_id']; ?>" class="ident">
		<input type="hidden" id="hw_image_w" name="hw_image_w" value="<?php echo $values['hw_image_w']; ?>" class="width">
		<input type="hidden" id="hw_image_h" name="hw_image_h" value="<?php echo $values['hw_image_h']; ?>" class="height">
		<a href="" class="hw_image_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['hw_image']; ?>" <?php if ($values['hw_image'] == '' || $values['hw_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['hw_image'] == '' || $values['hw_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['hw_image']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.hw_image_Picker')
    }));
});
</script>
