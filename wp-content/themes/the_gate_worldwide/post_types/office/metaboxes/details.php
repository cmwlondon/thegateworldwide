<?php $mb->the_field( 'office_phone' ); ?>
<div class="office-box">
    <span><?php _e( 'Enter office phone number' ); ?></span><br/ >
    <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
</div>

<?php $mb->the_field( 'contact_url' ); ?>
<div class="office-box">
    <span><?php _e( 'Enter the url address for "Contact us" page.' ); ?></span><br/ >
    <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
</div>

<?php $mb->the_field( 'work_url' ); ?>
<div class="office-box">
    <span><?php _e( 'Enter the url address for "Work" page.' ); ?></span><br/ >
    <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
</div>

<?php $mb->the_field( 'map_url' ); ?>
<div class="office-box">
    <span><?php _e( 'Enter the url address for "Map" page.' ); ?></span><br/ >
    <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
</div>
<div class="office-box">
	<?php $mb->the_field('site_association'); ?>
    <label for="<?php $mb->the_name(); ?>"><?php _e("Site associated with this office: "); ?></label><br/>
    <select name="<?php $mb->the_name(); ?>" id="<?php $mb->the_name(); ?>" style="width: 100%">
    	<?php $blogs = wp_get_sites(); ?>
        <?php foreach ($blogs as $blog) : ?>
            <option value="<?php echo $blog['blog_id']; ?>" <?php $mb->the_select_state($blog['blog_id']); ?>>
                <?php echo $blog['domain'],$blog['path']; ?>
            </option>
        <?php endforeach; ?>
    </select>
</div>