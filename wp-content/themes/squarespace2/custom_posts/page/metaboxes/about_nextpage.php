		<?php
		$pages = get_all_pages();
		$output = '';

		foreach ($pages AS $page) {
			$state = ( $values['ab_nextpage'] == $page->ID ) ? 'selected' : ''; 
			$output = $output . "<option $state value=\"".$page->ID."\">".strtoupper($page->post_title)."</option>\n";
		}

		?>

		<div class="customField nextChooser">
			<label>Destination for 'Next' link
			</label>
			<select name="ab_nextpage" id="ab_nextpage">
				<option value="-">Select Page</option>
				<?php echo $output; ?>
			</select>
		</div>
