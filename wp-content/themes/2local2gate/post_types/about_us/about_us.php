<?php
/**
 * About us
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Creation of the custom posttype */
add_action( 'init', 'create_about_us_post_type');
function create_about_us_post_type()
{
    $labels = array(
        'name' => __('About Us items'),
        'singular_name' => __('About Us item'),
        'add_new' => __('Add New About Us item'),
        'add_new_item' => __('Add New About Us item'),
        'edit_item' => __('Edit About Us item'),
        'new_item' => __('New About Us item'),
        'all_items' => __('All About Us items'),
        'view_item' => __('View About Us item'),
        'search_items' => __('Search About Us items'),
        'not_found' =>  __('No About Us items found'),
        'not_found_in_trash' => __('No About Us items found in Trash'),
        'parent_item_colon' => __(''),
        'menu_name' => __('About Us items')
    );

    register_post_type( 'about_us',
            array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'show_in_nav_menus' => true,
            'has_archive' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 27,
            'menu_icon' => get_bloginfo('template_directory') . '/post_types/about_us/img/about_small.png',
            'supports' => array('title', 'excerpt', 'editor', 'thumbnail'),
            'hierarchical' => false,
            'rewrite' => array('slug' => 'about-us'),
            'taxonomies' => array('post_tag'),
        )
    );
}

/*
        $about_us_mp4 = new WPAlchemy_MetaBox( array(
            'id'        => 'about_us_mp4',
            'title'     => 'MP4 Video',
            'types'     => array( 'page' ),
            'context'   => 'normal',
            'priority'  => 'high',
            'template'  => get_template_directory() . '/post_types/about_us/metaboxes/test.php',
            'autosave'  => true,
        ));
*/

/* Add metabox for secondary images */
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Main Image',
            'id' => 'main_image',
            'post_type' => 'about_us'
        )
    );
}
?>