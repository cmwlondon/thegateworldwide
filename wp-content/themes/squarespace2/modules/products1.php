		<section class="homeProductList">
<?php
// bgh_individual_order
// individual
// business

$productFilter = array('individual');
$individual_products = synchronise_products( explode(',', $individual), $productFilter, $postid, 'bgh_individual_order' );
$highlights = (count($individual_products['assigned']) > 0) ? get_products_by_order($individual_products['assigned']) : []; // ;
?>
		<article class="generalContent quad">
			<?php 
			$arrangement = 'rightImage';
			foreach ($highlights AS $highlight) :
				$highlightMeta = get_post_meta($highlight->ID);
			?>
			<div class="doubleColumnWrapper bgindividual02 <?php echo $arrangement; ?>">
				<div class="doubleColumnItem imageBox">
					<a href="<?php echo get_permalink($highlight->ID); ?>" title="<?php echo $highlightMeta['list_title'][0]; ?>"><img alt="<?php echo $highlightMeta['list_title'][0]; ?>" src="<?php echo $highlightMeta['list_image'][0]; ?>"></a>
				</div>
				<div class="doubleColumnItem textBox">
					<h3><?php echo $highlightMeta['list_title'][0]; ?></h3>
					<p><?php echo $highlightMeta['list_copy'][0]; ?></p>
					<div class="buttonBox"><a href="<?php echo get_permalink($highlight->ID); ?>" class="lozengeButton buttonindividual07 caret caretWhite"><span>VIEW</span></a></div>
				</div>
				<br class="cb">
			</div>
			<?php 
				$arrangement = ($arrangement == 'rightImage') ? 'leftImage' : 'rightImage';
			endforeach;
			?>
		</article>

<?php
// bgh_business_order
$productFilter = array('business');
$business_products = synchronise_products( explode(',', $business), $productFilter, $postid, 'bgh_business_order' );
$highlights = (count($business_products['assigned']) > 0) ? get_products_by_order($business_products['assigned']) : []; // ;
?>
		<!-- product highlight boxes START -->
		<article class="generalContent quad ">
			<?php 
			$arrangement = 'rightImage';
			foreach ($highlights AS $highlight) :
				$highlightMeta = get_post_meta($highlight->ID);
			?>
			
			<div class="doubleColumnWrapper bgbusiness01 <?php echo $arrangement; ?>">
				<div class="doubleColumnItem imageBox">
					<a href="<?php echo get_permalink($highlight->ID); ?>" title="<?php echo $highlightMeta['list_title'][0]; ?>"><img alt="<?php echo $highlightMeta['list_title'][0]; ?>" src="<?php echo $highlightMeta['list_image'][0]; ?>"></a>
				</div>
				<div class="doubleColumnItem textBox">
					<h3><?php echo $highlightMeta['list_title'][0]; ?></h3>
					<p><?php echo $highlightMeta['list_copy'][0]; ?></p>
					<div class="buttonBox"><a href="<?php echo get_permalink($highlight->ID); ?>" class="lozengeButton buttonindividual07 caret caretWhite"><span>VIEW</span></a></div>
				</div>
				<br class="cb">
			</div>
			<?php 
				$arrangement = ($arrangement == 'rightImage') ? 'leftImage' : 'rightImage';
			endforeach;
			?>
		</article>
		<!-- product highlight boxes END -->
		</section>
