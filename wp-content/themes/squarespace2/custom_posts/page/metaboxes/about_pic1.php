<?php
?>

<div class="customField mediumtext">
	<label for="ap1_alt">image alt</label>
	<input type="text" id="ap1_alt" name="ap1_alt" value="<?php echo $values['ap1_alt']; ?>">
</div>
 
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Banner image</label>
		<input type="hidden" id="ap1_image" name="ap1_image" value="<?php echo $values['ap1_image']; ?>">
		<input type="hidden" id="ap1_image_id" name="ap1_image_id" value="<?php echo $values['ap1_image_id']; ?>" class="ident">
		<input type="hidden" id="ap1_image_w" name="ap1_image_w" value="<?php echo $values['ap1_image_w']; ?>" class="width">
		<input type="hidden" id="ap1_image_h" name="ap1_image_h" value="<?php echo $values['ap1_image_h']; ?>" class="height">
		<a href="" class="ap1_image_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['ap1_image']; ?>" <?php if ($values['ap1_image'] == '' || $values['ap1_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['ap1_image'] == '' || $values['ap1_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ap1_image']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.ap1_image_Picker')
    }));
});
</script>
