<?php switch_to_blog(1);

$args = array(
    'numberposts'     => -1,
    'offset'          => 0,
    'post_type'       => 'office',
    'post_status'     => 'publish',
    'suppress_filters' => true
);

$offices = get_posts($args); ?>

<p><?php _e('Choose people from which offices will appear on this page. (If non is chosen all people will appear)'); ?></p>

<?php while($mb->have_fields_and_multi('offices')): ?>
    <?php $mb->the_group_open(); ?>
    <div class="misc-pub-section">
        <?php $mb->the_field('office'); ?>
        <select name="<?php $mb->the_name(); ?>" style="width: 80%">
            <option value=""></option>
            <?php foreach ($offices as $office) : ?>
                <option value="<?php echo $office->ID; ?>"<?php $mb->the_select_state($office->ID); ?>><?php echo $office->post_title; ?></option>
            <?php endforeach; ?>
        </select>
        <a href="#" class="dodelete button"><?php _e('Remove Office'); ?></a>
    </div>
    <?php $mb->the_group_close(); ?>
<?php endwhile; ?>
<div class="clear"></div>
<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-offices button"><?php _e('Add Office'); ?></a></p>

<?php restore_current_blog();