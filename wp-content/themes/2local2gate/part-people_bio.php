    <?php
    /**
     * Template Name: People Bio
     *
     * @package WordPress
     * @subpackage TheGate
     */

    global $bio_page, $bio, $bio2, $viewEveryone;

    /* Redirect to 404 if opened directly #3342 */
    if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
    {
        if($post->post_parent)
        {
            wp_redirect(get_permalink($post->post_parent), 303);
        }
        else
        {
            wp_redirect(home_url('404'), 303);
        }
    }

    if($bio_page) : 
        $quickDisplay = "";
        foreach($bio as $key => $person){
            // echo "<p>".$person['slug']."</p>\n";
            if($person['slug'] == $_GET['slug']){
                $quickDisplay = $key;
                break;  
            }
        }
        if ($quickDisplay !== "") :
        // print_r($bio[$quickDisplay]);
    ?>
        <div class="mobileBio individual" data-slug="<?php echo $bio[$quickDisplay]['slug'] ?>">
            <?php printPerson($bio[$quickDisplay], true); ?>
        </div>
    <?php
        else :
    ?>
        <div class="peopleBlock hiddenBio">
            <ul>
                <?php foreach ($bio as $key => $person) : ?>
                    <li data-slug="<?php echo $person['slug']; ?>"><?php printPerson($person); ?></li>
                <?php endforeach; ?>

<?php foreach ($bio2 as $key1 => $agency) :
foreach ($agency as $key2 => $people) : ?>
<li data-slug="<?php echo $people['slug']; ?>"><?php printPerson($people); ?></li>
<?php endforeach; endforeach; ?>

            </ul>
        </div>
    <?php endif; ?>
    <?php endif;

	function printPerson($person, $sep = false){
	?>
	<div class="people-individual-page" data-page-title="<?php echo $person['first_name']; ?> <?php echo $person['last_name']; ?> - <?php bloginfo( 'name' ); ?>" <?php if($sep){ ?> style="display: table"<?php } ?>>
        <div class="main">
            <div class="photo">
                <div class="border"><?php echo $person['large_thumbnail']; ?></div>
            </div>
            <h4><?php echo $person['position']; ?></h4>
            <h3><?php echo $person['first_name']; ?> <?php echo $person['last_name']; ?></h3>
            <ul class="details">
                <li class="phone-numbers">
                    <?php if(isset($person['emails']) && is_array($person['emails'])) : ?>
                        <?php foreach ($person['emails'] as $email) : ?>
                            <p><?php echo __('E   ') . $email; ?></p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if(isset($person['phones']) && is_array($person['phones'])) : ?>
                        <?php foreach ($person['phones'] as $phone) : ?>
                            <p><?php echo __('T   ') . $phone; ?></p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if(isset($person['faxes']) && is_array($person['faxes'])) : ?>
                        <?php foreach ($person['faxes'] as $fax) : ?>
                            <p><?php echo __('F   ') . $fax; ?></p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </li>
            </ul>

 
        </div>
        <div class="additional">
            <h4><?php echo $person['position']; ?></h4>
            <h3><?php echo $person['first_name']; ?> <?php echo $person['last_name']; ?></h3>

            <div class="scrollOuter">
                <div class="arrows down">&gt;</div>
                <div class="arrows up">&lt;</div>
                <div class="scrollWindow">
                    <div class="scrollInner">
                        <?php echo apply_filters('the_content', $person['content']); ?>
                    </div>
                </div>
            </div>
            <ul class="social">
                <?php if(isset($person['pinterest_account']) && $person['pinterest_account']) : ?>
                    <li><a href="http://pinterest.com/<?php echo $person['pinterest_account']; ?>" class="icon-pinterest"><span><?php _e('Pinterestv', 'People Bio Page'); ?></span></a></li>
                <?php endif; ?>
                <?php if(isset($person['linked_in_account']) && $person['linked_in_account']) : ?>
                    <li><a href="<?php echo $person['linked_in_account']; ?>" target="_blank" class="icon-linkedin"><span><?php _e('Linkedin', 'People Bio Page'); ?></span></a></li>
                <?php endif; ?>
                <?php if(isset($person['twitter_account']) && $person['twitter_account']) : ?>
                    <li><a href="https://twitter.com/<?php echo $person['twitter_account']; ?>" target="_blank" class="icon-twitter"><span><?php _e('Twitter', 'People Bio Page'); ?></span></a></li>
                <?php endif; ?>
                <?php if(isset($person['facebook_account']) && $person['facebook_account']) : ?>
                    <li><a href="https://www.facebook.com/<?php echo $person['facebook_account']; ?>" target="_blank" class="icon-facebook"><span><?php _e('Facebook', 'People Bio Page'); ?></span></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
	<?php
	}