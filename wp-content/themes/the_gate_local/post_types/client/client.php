<?php
/**
 * @package Wordpress Multisite
 * @subpackage The Gate Local
 */

/**
 * Register post type Clients
 *
 */

  function thegate_local_client_init()
  {
    $labels = array(
        'name'                => _x( 'Clients', 'post type general name' ),
        'singular_name'       => _x( 'Client', 'post type singular name' ),
        'add_new'             => _x( 'Add New Client', 'client' ),
        'add_new_item'        => __( 'Add New Client'),
        'edit_item'           => __( 'Edit Client' ),
        'new_item'            => __( 'New Client' ),
        'all_items'           => __( 'All Clients' ),
        'view_item'           => __( 'View Client' ),
        'search_items'        => __( 'Search Client' ),
        'not_found'           => __( 'No Client found' ),
        'not_found_in_trash'  => __( 'No Client found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Clients'
    );
  $args = array(
        'labels'             => $labels,
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/client/img/client.png',
        'supports'           => array( 'title', 'thumbnail' )
  );

  register_post_type( 'client', $args );
}
add_action( 'init', 'thegate_local_client_init' );

/* Styling for the custom post type icon */
add_action( 'admin_head', 'add_client_large_icon' );

function add_client_large_icon()
{
?>
    <style type="text/css" media="screen">
        #icon-edit.icon32-posts-client {background: url(<?php bloginfo('template_url') ?>/post_types/client/img/client32x32.png) no-repeat;}
    </style>
<?php
}

/* Add filter to ensure the text office, or office, is displayed when user updates a office */
add_filter( 'post_updated_messages', 'codex_client_updated_messages' );
function codex_client_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['client'] = array(
    0 => '', /* Unused. Messages start at index 1.*/
    1 => sprintf( __( 'client updated. <a href="%s">View client</a>' ), esc_url( get_permalink( $post_ID ) ) ),
    2 => __( 'Custom field updated.' ),
    3 => __( 'Custom field deleted.' ),
  );
  return $messages;
}

add_filter( 'manage_edit-client_columns', 'add_new_client_columns' );
 function add_new_client_columns( $client_columns )
 {
    $new_columns['cb']              = '<input type="checkbox" />';
    $new_columns['thumbnail']       = _x( 'Thumbnail', 'column name' );
    $new_columns['title']           = _x( 'Client', 'column name' );
    $new_columns['date']            = __( 'Date' );

    return $new_columns;
 }
add_action( 'manage_client_posts_custom_column', 'manage_client_columns', 10, 2 );

 function manage_client_columns( $column_name, $id )
 {
     global $post;

    switch( $column_name )
    {
        case 'thumbnail':
            echo get_the_post_thumbnail( $post->ID, array( 100, 100 ) );
            break;
    }
}
?>