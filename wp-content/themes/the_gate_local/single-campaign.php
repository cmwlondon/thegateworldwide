<?php
/**
 * Single Campaign Page (Individual Campaign Page)
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $related_posts;

the_post();
$related_posts = get_related_posts(3, array('post'), true);
$client = get_client();
$campaign_elements = get_campaign_elements();

wp_enqueue_script('share_popup');
wp_enqueue_script('fancybox');
wp_enqueue_script('anything_slider');

$previous_post = get_previous_post();
$next_post = get_next_post();

$is_london = !substr_compare(trim(site_url(), '/'), 'london', -6, 6);

get_header();
?>
    <!-- visual -->
</header>
<!-- main -->
<div id="main">
    <ul class="featured-prev-next">
        <?php if (!empty( $next_post )): ?>
            <li class="previous"><a href="<?php echo get_permalink( $next_post->ID ); ?>"></a></li>
        <?php endif; ?>
        <li class="mobile-carousel-msg"><span><?php _e('WORK <br /> BY CLIENT', 'Single Campaign Page'); ?></span></li>
        <?php if (!empty( $previous_post )): ?>
            <li class="next"><a href="<?php echo get_permalink( $previous_post->ID ); ?>"></a></li>
        <?php endif; ?>
    </ul>
    <div class="work-slider-wrapper">
        <ul class="work-individual">
            <li>
                <a href="#" class="work-fancybox-fake"><?php echo (has_post_thumbnail()) ? get_the_post_thumbnail($post->ID, 'homepage-thumbnail') : get_placeholder_image('homepage-thumbnail'); ?></a>
            </li>
        </ul>
        <div class="visual">
        </div>
    </div>
    <div class="main-holder">
        <!-- content -->
        <div id="content" class="street-campaign">
            <h2><?php the_title(); ?></h2>
            <aside>
                <div class="campaign-aside-top">
                    <?php echo (has_post_thumbnail($client->ID)) ? get_the_post_thumbnail($client->ID, 'client-logo') : get_placeholder_image('client-logo'); ?>
                    <h3><?php echo get_post_meta($post->ID, 'campaign_subtitle', true); ?></h3>
                </div>
                <div class="campaign-aside-content">
                    <?php the_content(); ?>
                    <div class="article-info-container">
                    <ul class="article-info">
                        <li>
                            <?php get_template_part('part', 'social_buttons'); ?>
                        </li>
                    </ul>
                    </div>
                </div>
            </aside>
            <div class="campaign-content">
                <ul class="list mobile">
                    <?php foreach ($campaign_elements as $key => $campaign_element) : ?>
                        <?php
                        $video = get_post_meta($campaign_element->ID, 'campaign_element_video', true);
                        $swf = get_post_meta($campaign_element->ID, 'campaign_element_swf', true);
                        $check_swf = !empty($swf['document']) ? wp_get_attachment_url((int)$swf['document']) : '';
                        $swf_link = isset($swf['addit_url']) && !empty($swf['addit_url']) ? $swf['addit_url'] : '#';
                        if(class_exists('MultiPostThumbnails'))
                        {
                            $check_main_img = MultiPostThumbnails::has_post_thumbnail($campaign_element->post_type, 'main_image', $campaign_element->ID, 'full');
                            $main_img_url   = MultiPostThumbnails::get_post_thumbnail_url($campaign_element->post_type, 'main_image', $campaign_element->ID, 'full');
                        }
                        ?>
                        <li class="row-<?php echo floor($key / 2); ?>">
                            <div class="holder">
                                <figure>
                                    <?php if($video && isset($video['video_embedlink']) && !empty($video['video_embedlink'])) : ?>
                                        <a href="<?php echo $video['video_embedlink']; ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="iframe" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                            <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-small') : get_placeholder_image('campaign-element-small'); ?>
                                        </a>
                                    <?php elseif($swf && !empty($swf['document'])) : ?>
                                        <a href="<?php echo $check_swf ? $check_swf : get_placeholder_image_src('full'); ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="swf" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                            <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-small') : get_placeholder_image('campaign-element-small'); ?>
                                        </a>
                                    <?php else : ?>
                                        <a href="<?php echo $check_main_img ? $main_img_url : get_placeholder_image_src('full'); ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="image" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                            <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-small') : get_placeholder_image('campaign-element-small'); ?>
                                        </a>
                                    <?php endif; ?>
                                    <?php $post_category = get_post_category($campaign_element->ID); ?>
                                    <?php if($post_category) : ?>
                                        <strong class="title"><?php echo $post_category; ?></strong>
                                    <?php endif; ?>
                                </figure>
                                <header>
                                    <h3><?php echo apply_filters('the_title', $campaign_element->post_title); ?></h3>
                                </header>
                                <?php echo apply_filters('the_excerpt', $campaign_element->post_excerpt); ?>

                                <div class="hidden modal-title">
                                    <?php if($is_london) : /* Add title only on London site */ ?>
                                        <?php echo apply_filters('the_title', $campaign_element->post_title); ?>
                                    <?php endif; ?>
                                </div>

                                <div class="hidden modal-description">
                                    <?php if($is_london) : /* Add description only on London site */ ?>
                                        <?php echo apply_filters('the_content', $campaign_element->post_content); ?>
                                    <?php endif; ?>
                                </div>

                                <a class="more" href="#"><?php _e('READ MORE', 'Single Campaign Page'); ?></a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <ul class="list desktop">
                    <?php foreach ($campaign_elements as $key => $campaign_element) : ?>
                        <?php
                            $video = get_post_meta($campaign_element->ID, 'campaign_element_video', true);
                            $swf = get_post_meta($campaign_element->ID, 'campaign_element_swf', true);
                            $check_swf = !empty($swf['document']) ? wp_get_attachment_url((int)$swf['document']) : '';
                            $swf_link = isset($swf['addit_url']) && !empty($swf['addit_url']) ? $swf['addit_url'] : '#';
                            if(class_exists('MultiPostThumbnails'))
                            {
                                $check_main_img = MultiPostThumbnails::has_post_thumbnail($campaign_element->post_type, 'main_image', $campaign_element->ID, 'full');
                                $main_img_url   = MultiPostThumbnails::get_post_thumbnail_url($campaign_element->post_type, 'main_image', $campaign_element->ID, 'full');
                            }
                        ?>
                        <?php if(!$key) : ?>
                            <li class="campaign-big">
                                <?php if($video && isset($video['video_embedlink']) && !empty($video['video_embedlink'])) : ?>
                                    <a href="<?php echo $video['video_embedlink']; ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="iframe" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                        <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
                                    </a>
                                <?php elseif($swf && !empty($swf['document'])) : ?>
                                    <?php if( !empty($swf_link) ) : ?>
                                        <a href="<?php echo $swf_link; ?>" class="flash-thumb" style="background-image: url(<?php echo $main_img_url ? $main_img_url : get_placeholder_image_src('main-image'); ?>)"></a>
                                    <?php endif; ?>
                                    <a href="<?php echo $check_swf ? $check_swf : get_placeholder_image_src('full'); ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="swf" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                        <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
                                    </a>
                                <?php else : ?>
                                    <a href="<?php echo $check_main_img ? $main_img_url : get_placeholder_image_src('full'); ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="image" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                        <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
                                    </a>
                                <?php endif; ?>
                                <a href="<?php echo get_permalink($campaign_element->ID); ?>"><h3><?php echo apply_filters('the_title', $campaign_element->post_title); ?></h3></a>
                                <a href="<?php echo get_permalink($campaign_element->ID); ?>">
                                    <?php echo apply_filters('the_excerpt', $campaign_element->post_excerpt); ?>

                                      <div class="hidden modal-title">
                                        <?php if($is_london) : /* Add title only on London site */ ?>
                                            <?php echo apply_filters('the_title', $campaign_element->post_title); ?>
                                        <?php endif; ?>
                                    </div>

                                    <div class="hidden modal-description">
                                        <?php if($is_london) : /* Add description only on London site */ ?>
                                            <?php echo apply_filters('the_content', $campaign_element->post_content); ?>
                                        <?php endif; ?>
                                    </div>
                                </a>
                            </li>
                        <?php else : ?>
                            <li class="row-<?php echo ceil($key / 2); ?>">
                                <div class="holder">
                                    <figure>
                                        <?php if($video && isset($video['video_embedlink']) && !empty($video['video_embedlink'])) : ?>
                                            <a href="<?php echo $video['video_embedlink']; ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="iframe" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                                <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-small') : get_placeholder_image('campaign-element-small'); ?>
                                            </a>
                                        <?php elseif($swf && !empty($swf['document'])) : ?>
                                            <a href="<?php echo $check_swf ? $check_swf : get_placeholder_image_src('full'); ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="swf" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                                <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-small') : get_placeholder_image('campaign-element-small'); ?>
                                            </a>
                                        <?php else : ?>
                                            <a href="<?php echo $check_main_img ? $main_img_url : get_placeholder_image_src('full'); ?>" class="work-fancybox" data-fancybox-group="work-fancybox" data-fancybox-type="image" data-mobile-href="<?php echo get_permalink($campaign_element->ID); ?>">
                                                <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-small') : get_placeholder_image('campaign-element-small'); ?>
                                            </a>
                                        <?php endif; ?>
                                        <?php $post_category = get_post_category($campaign_element->ID); ?>
                                        <?php if($post_category) : ?>
                                            <strong class="title"><?php echo $post_category; ?></strong>
                                        <?php endif; ?>
                                    </figure>
                                    <header>
                                        <h3><?php echo apply_filters('the_title', $campaign_element->post_title); ?></h3>
                                    </header>
                                    <?php echo apply_filters('the_excerpt', $campaign_element->post_excerpt); ?>

                                    <div class="hidden modal-title">
                                        <?php if($is_london) : /* Add title only on London site */ ?>
                                            <?php echo apply_filters('the_title', $campaign_element->post_title); ?>
                                        <?php endif; ?>
                                    </div>

                                    <div class="hidden modal-description">
                                        <?php if($is_london) : /* Add description only on London site */ ?>
                                            <?php echo apply_filters('the_content', $campaign_element->post_content); ?>
                                        <?php endif; ?>
                                    </div>

                                    <a class="more" href="#"><?php _e('READ MORE', 'Single Campaign Page'); ?></a>
                                </div>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="article-info-container article-info-container-mob">
                <ul class="article-info">
                    <li>
                        <?php get_template_part('part', 'social_buttons'); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php get_template_part('part', 'related_items'); ?>
</div>

<?php get_footer(); ?>