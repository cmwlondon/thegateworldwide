<?php
/**
 * General page template - Used for default template pages such as Terms and Conditions, Accessibility and View Sitemap
 *
 * @package WordPress
 * @subpackage thegate_local
 */

the_post();
get_header(); ?>

</header>
<!-- main -->
<div id="main">
    <div class="main-holder">
        <div id="content">
            <h2><?php the_title(); ?></h2>
            <?php the_content(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
