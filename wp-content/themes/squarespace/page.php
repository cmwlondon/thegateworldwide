<?php
/**
 * General page template - Used for default template pages such as Terms and Conditions, Accessibility and View Sitemap
 *
 * @package WordPress
 * @subpackage thegate_local
 */

the_post();
?>
<html>
<head>
	<title>The Gate London</title>
	<meta charset="utf-8">
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />
</head>
<body>
	<div id="iframe-container" style="position: relative; width:100%; height:100%;">
		<iframe src="https://london.thegateworldwide.com" style="position: absolute; width: 100%; height: 100%" frameborder="0" ></iframe> 
	</div>
</body>
</html>