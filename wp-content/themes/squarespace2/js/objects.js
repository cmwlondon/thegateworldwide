(function () {
	"use strict";

	/* -----------------------------
	Sizer
	----------------------------- */

	/*
	position & scale images set within div.sizer/a.sizer containers
	*/

	function Sizer(parameters) {
		this.parameters = parameters;

		this.containers = parameters.containers;

		this.images = [];
		this.setup();
	}
	
	Sizer.prototype = {
		"constructor" : Sizer,
		"template" : function () { var that = this; },
		"setup" : function () {
			var that = this;

			this.indexAll();
			this.scaleAll();
		},
		"indexAll" : function () {
			var that = this,
			 	thisImage,
			 	thisContainer; 

			this.imageCount = this.containers.length;

			this.imageIndex = 0;
			do {
				thisContainer = this.containers.eq(this.imageIndex);
				thisImage = thisContainer.find('img');
			 	this.images.push({
			 		"container" : thisContainer,
			 		"image" : thisImage,
			 		"width" : thisImage.attr('data-width'),
			 		"height" : thisImage.attr('data-height')
			 	});
			 	this.imageIndex++;
			} while ( this.imageIndex < this.imageCount )
		},
		"scaleAll" : function () {
			var that = this;

			this.imageIndex = 0;
			do {
				this.scale();	
			 	this.imageIndex++;
			} while ( this.imageIndex < this.imageCount )
		},
		"scale" : function () {
			var that = this,
				thisImage = this.images[this.imageIndex],
				thisContainer = thisImage.container,
				thisContainerSize,
				thisImageSize,
				scale,
				nw,
				nh,
				offsetx,
				offsety;

			thisContainerSize = {
				"w" : thisContainer.width(),
				"h" : thisContainer.height()
			};
			thisContainerSize.aspect = thisContainerSize.w / thisContainerSize.h;

			thisImageSize = {
				"w" : thisImage.image.attr('data-width'),
				"h" : thisImage.image.attr('data-height')
			};
			thisImageSize.aspect = thisImageSize.w / thisImageSize.h;

			// determine how to scale/position the image based on aspect ratios of container and image
			if (thisImageSize.aspect > thisContainerSize.aspect ) {
				scale = thisContainerSize.h / thisImageSize.h;
				nw = thisImageSize.w * scale;
				nh = thisImageSize.h * scale;
			}  

			if (thisImageSize.aspect < thisContainerSize.aspect ) {
				scale = thisContainerSize.w / thisImageSize.w;
				nw = thisImageSize.w * scale;
				nh = thisImageSize.h * scale;
			}  

			offsetx = (thisContainerSize.w - nw) / 2;
			offsety = (thisContainerSize.h - nh) / 2;

			nh++; // increase height by 1 px to account for rounding errors
			
			thisImage.image.css({"width" : nw + "px", "height" : nh + "px", "margin-left" : offsetx + "px", "margin-top" : offsety + "px"});
			thisImage.image.removeClass('loading');

		},
		"resize" : function () {
			var that = this;

			this.scaleAll();
		}
	}

	window.Sizer = Sizer;

	/* -----------------------------
	MultiImage
	----------------------------- */

	/*
	full width multi image carousel with variable durations between slides, defined by data-daley attribute on div.column

	<div class="single multi" id="culture">
		<article class="column active" data-delay="2000"><div class="sizer"><img alt="" class="loading" data-width="1920" data-height="1080" src="<?php echo $theme_root; ?>/img/test/culture/pic1.jpg"></div></article>
		<article class="column active" data-delay="2000"><div class="sizer"><img alt="" class="loading" data-width="1920" data-height="1080" src="<?php echo $theme_root; ?>/img/test/culture/pic2.jpg"></div></article>
		<article class="column active" data-delay="2000"><div class="sizer"><img alt="" class="loading" data-width="1920" data-height="1080" src="<?php echo $theme_root; ?>/img/test/culture/pic3.jpg"></div></article>
	</div>
	*/

	function MultiImage(parameters) {
		this.parameters = parameters;

		this.container = parameters.container;

		this.images = [];
		this.imageCount = 0;
		this.imageIndex = 0;
		this.timer = null;

		this.setup();
	}
	
	MultiImage.prototype = {
		"constructor" : MultiImage,
		"template" : function () { var that = this; },
		"setup" : function () {
			var that = this,
				thisImage;

			this.containerID = this.container.attr('id'); 
			this.imageNodes = this.container.find('.column');
			this.imageCount = this.imageNodes.length;

			do {
				thisImage = this.imageNodes.eq(this.imageIndex);
				this.images.push({
					"node" : thisImage,
					"delay" : thisImage.attr('data-delay')
				});
				this.imageIndex++;
			} while (this.imageIndex < this.imageCount)

			this.imageIndex = 0;
			this.schedule();
		},
		"schedule" : function () {
			var that = this,
				thisDelay = this.images[this.imageIndex].delay;

			this.timer = window.setTimeout(function(){
				var oldIndex = that.imageIndex,
					newIndex = oldIndex + 1;

				if ( newIndex == that.imageCount ) {
					newIndex = 0;
				}

				that.images[oldIndex].node.removeClass('active');
				that.images[newIndex].node.addClass('active');
				that.imageIndex = newIndex;

				that.schedule();
			}, thisDelay);

		}
	}

	window.MultiImage = MultiImage;

	/* -----------------------------
	FlexBoxFix
	----------------------------- */

	var FlexBoxFix = function(parameters) {
		this.parameters = parameters;

		this.container = parameters.container;
		this.complete = parameters.complete;

		// minimum window width to apply flexbox, below this width the items are in a single column, don't need to normalize height of items
		this.switchoff = parameters.switchoff; 

		this.items;
		this.rows = [];
		this.rowCount = 0;
		this.maxHeight = 0;
		this.windowThrottle;

		this.init();
	};

	FlexBoxFix.prototype = {
		"constructor" : FlexBoxFix,
		"template" : function () {var that = this;},
		"init" : function () {
			var that = this;

			this.prepareItems();

			var pause = window.setTimeout(function(){
				that.rowSortItems();
				that.normalizeItems();

				jQuery(window).on('resize', function(){

					that.windowThrottle = window.setTimeout(function(){
						that.rowSortItems();
						that.normalizeItems();
						
					}, 100);
				});
			}, 30);

		},
		"prepareItems" : function () {
			var that = this,
				itemContent,
				newGetter,
				newSetter;

			this.items = this.container.find('.flexboxItem');
			this.items.each(function(i){
				itemContent = jQuery(this).contents().clone();
				jQuery(this).empty();
				newGetter = jQuery('<div></div>').addClass('getter');
				newSetter = jQuery('<div></div>').addClass('setter');
				newGetter.append(itemContent);
				newSetter.append(newGetter);
				jQuery(this).append(newSetter);
			});
			
		},
		"rowSortItems" : function () {
			var that = this,
				cw = this.container.width(),
				iw = this.items.eq(0).width(),
				itemIndex = 0,
				thisRow = [];

			this.columns = Math.floor(cw / iw);

			this.rows = [];

			if ( this.columns > 1 ) {
				do {

					if ( (itemIndex % this.columns == 0) && (itemIndex > 0) ) {
						this.rows.push({
							"max" : 0,
							"row" : thisRow
						});
						thisRow = [];
					}

					thisRow.push(itemIndex);

					itemIndex++;
				} while ( itemIndex < this.items.length )

				if ( thisRow.length > 0 ) {
					this.rows.push({
						"max" : 0,
						"row" : thisRow
					});
				}
			}
		},
		"normalizeItems" : function () {
			var that = this,
				itemHeight,
				maxHeight,
				set,
				rowsIndex = 0,
				itemIndex,
				thisRow,
				thisItem;

			/* single row
			that.maxHeight = 0;
			if ( windowWidth > this.switchoff) {

				this.items.each(function(i){
					itemHeight = parseInt(jQuery(this).find('.getter').height(), 10) + 32;
					if (that.maxHeight < itemHeight) {
						that.maxHeight = itemHeight;
					}
				});
				set = that.maxHeight + 'px';

			} else {

				set = 'auto';
			}

			this.items.find('.setter').css({"height" : set});
			*/

			if ( this.columns > 1 ) {
				do {
					thisRow = this.rows[rowsIndex].row;

					maxHeight = 0;
					itemIndex = 0;
					do {
						thisItem = thisRow[itemIndex];
						itemHeight = parseInt( this.items.eq(thisItem).find('.getter').height(), 10 );

						if ( maxHeight < itemHeight ) { maxHeight = itemHeight; } 

						itemIndex++;
					} while (itemIndex < thisRow.length)

					this.rows[rowsIndex].max = maxHeight;

					rowsIndex++;
				} while (rowsIndex < this.rows.length)

				rowsIndex = 0;
				do {
					thisRow = this.rows[rowsIndex].row;

					maxHeight = 0;
					itemIndex = 0;
					do {
						thisItem = thisRow[itemIndex];
						this.items.eq(thisItem).find('.setter').css({"height" : (this.rows[rowsIndex].max + 32) + "px"});

						if ( maxHeight < itemHeight ) { maxHeight = itemHeight; } 

						itemIndex++;
					} while (itemIndex < thisRow.length)

					this.rows[rowsIndex].max = maxHeight;

					rowsIndex++;
				} while (rowsIndex < this.rows.length)
			} else {
				this.items.find('.setter').css({"height" : set});
			}

			that.complete();
		}
	};

	window.FlexBoxFix = FlexBoxFix;

	/* -----------------------------
	PageEventHandler
	----------------------------- */

	function PageEventHandler(parameters) {
		this.parameters = parameters;
		this.events = parameters.events;
		this.complete = parameters.complete;

		this.setup();
	}
	
	PageEventHandler.prototype = {
		"constructor" : PageEventHandler,
		"template" : function () { var that = this; },
		"setup" : function () {
			var that = this,
				thisImage;

		},
		"signal" : function ( message ) {
			var that = this,
				requiredCount = this.events.length,
				requiredIndex = 0,
				thisEvent,
				index;

			do {
				thisEvent = this.events[requiredIndex];

				if ( thisEvent == message ) {
					this.complete[ this.events[requiredIndex] ]();
					this.removeEvent( requiredIndex );

					if (this.events.length == 0) {
						this.eventsComplete();
					}

					break;
				}

				requiredIndex++;				
			} while ( requiredIndex < requiredCount )
		},
		"removeEvent" : function ( index ) {
			var that = this;

			this.events.splice(index,1);
		},
		"eventsComplete" : function ( index ) {
			var that = this;

			this.complete[ 'all' ]();
		}
	}

	window.PageEventHandler = PageEventHandler;

	/* -----------------------------
	PageTransition
	----------------------------- */

	function PageTransition(parameters) {
		this.parameters = parameters;
		this.duration = parameters.duration;

		this.setup();
	}
	
	PageTransition.prototype = {
		"constructor" : PageTransition,
		"template" : function () { var that = this; },
		"setup" : function () {
			var that = this;

		},
		"open" : function () {
			var that = this;

			if ( supportsCSSTransitions ) {
				jQuery('#page').removeClass('loading');
			} else {
				jQuery('#page')
				.css({"opacity" : 0})
				.animate({"opacity" : 1}, this.duration, function(){});
			}
		},
		"close" : function ( parameters, endAction ) {
			var that = this;

			this.endAction = endAction; 

			if ( supportsCSSTransitions ) {
				jQuery('#page').addClass('loading');

				var dt = window.setTimeout(function(){
					endAction(parameters);
				}, this.duration);
			} else {
				jQuery('#page')
				.css({"opacity" : 1})
				.animate({"opacity" : 0}, this.duration, function(){
					endAction(parameters);
				});
			}
		}
	}

	window.PageTransition = PageTransition;

	/* -----------------------------
	imgPageLoader
	----------------------------- */

	function imgPageLoader(parameters) {
		this.parameters = parameters;

		this.singleImage = parameters.singleImage;
		this.allImages = parameters.allImages;

		this.images = [];
		this.imageCount = 0;
		this.required;

		this.setup();
	}
	
	imgPageLoader.prototype = {
		"constructor" : imgPageLoader,
		"template" : function () { var that = this; },
		"setup" : function () {
			var that = this;

			this.indexImages();
			this.queueImages();
		},			
		"indexImages" : function () {
			var that = this;

			jQuery('img').each(function(n){

				that.images.push({
					"index" : n,
					"src" : jQuery(this).attr('src')
				});

				jQuery(this)
				.attr({
					"data-index" : n,
					"src" : ""
				});
			});

			this.imageCount = this.images.length;
		},			
		"queueImages" : function () {
			var that = this,
				n = 0,
				thisImage;

			this.required = this.imageCount;

			do {
				thisImage = this.images[n];

				jQuery('img').eq(n)
				.load(function(e){
					that.required = that.required - 1;
					that.imageLoaded( jQuery(this) );
					if (that.required == 0) { that.allImagesLoaded(); }
				})
				.attr({"src" : thisImage.src });

				n++;
			} while (n < this.imageCount)
		},			
		"imageLoaded" : function ( image ) {
			var that = this;

			this.singleImage( image );
		},			
		"allImagesLoaded" : function () {
			var that = this;

			this.allImages();
		}
	}

	window.imgPageLoader = imgPageLoader;

	/**
	 * Copyright (c) 2007-2015 Ariel Flesler - aflesler ○ gmail • com | http://flesler.blogspot.com
	 * Licensed under MIT
	 * @author Ariel Flesler
	 * @version 2.1.3
	 */
	;(function(f){"use strict";"function"===typeof define&&define.amd?define(["jquery"],f):"undefined"!==typeof module&&module.exports?module.exports=f(require("jquery")):f(jQuery)})(function($){"use strict";function n(a){return!a.nodeName||-1!==$.inArray(a.nodeName.toLowerCase(),["iframe","#document","html","body"])}function h(a){return $.isFunction(a)||$.isPlainObject(a)?a:{top:a,left:a}}var p=$.scrollTo=function(a,d,b){return $(window).scrollTo(a,d,b)};p.defaults={axis:"xy",duration:0,limit:!0};$.fn.scrollTo=function(a,d,b){"object"=== typeof d&&(b=d,d=0);"function"===typeof b&&(b={onAfter:b});"max"===a&&(a=9E9);b=$.extend({},p.defaults,b);d=d||b.duration;var u=b.queue&&1<b.axis.length;u&&(d/=2);b.offset=h(b.offset);b.over=h(b.over);return this.each(function(){function k(a){var k=$.extend({},b,{queue:!0,duration:d,complete:a&&function(){a.call(q,e,b)}});r.animate(f,k)}if(null!==a){var l=n(this),q=l?this.contentWindow||window:this,r=$(q),e=a,f={},t;switch(typeof e){case "number":case "string":if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(e)){e= h(e);break}e=l?$(e):$(e,q);case "object":if(e.length===0)return;if(e.is||e.style)t=(e=$(e)).offset()}var v=$.isFunction(b.offset)&&b.offset(q,e)||b.offset;$.each(b.axis.split(""),function(a,c){var d="x"===c?"Left":"Top",m=d.toLowerCase(),g="scroll"+d,h=r[g](),n=p.max(q,c);t?(f[g]=t[m]+(l?0:h-r.offset()[m]),b.margin&&(f[g]-=parseInt(e.css("margin"+d),10)||0,f[g]-=parseInt(e.css("border"+d+"Width"),10)||0),f[g]+=v[m]||0,b.over[m]&&(f[g]+=e["x"===c?"width":"height"]()*b.over[m])):(d=e[m],f[g]=d.slice&& "%"===d.slice(-1)?parseFloat(d)/100*n:d);b.limit&&/^\d+$/.test(f[g])&&(f[g]=0>=f[g]?0:Math.min(f[g],n));!a&&1<b.axis.length&&(h===f[g]?f={}:u&&(k(b.onAfterFirst),f={}))});k(b.onAfter)}})};p.max=function(a,d){var b="x"===d?"Width":"Height",h="scroll"+b;if(!n(a))return a[h]-$(a)[b.toLowerCase()]();var b="client"+b,k=a.ownerDocument||a.document,l=k.documentElement,k=k.body;return Math.max(l[h],k[h])-Math.min(l[b],k[b])};$.Tween.propHooks.scrollLeft=$.Tween.propHooks.scrollTop={get:function(a){return $(a.elem)[a.prop]()}, set:function(a){var d=this.get(a);if(a.options.interrupt&&a._last&&a._last!==d)return $(a.elem).stop();var b=Math.round(a.now);d!==b&&($(a.elem)[a.prop](b),a._last=this.get(a))}};return p});

})();
