<?php global $post;

    $thumb = (has_post_thumbnail($post->ID)) ? wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full') : false;
    $thumb_url = ($thumb && is_array($thumb)) ? urlencode($thumb['0'] ): urlencode(get_bloginfo('template_url') . '/assets/images/logo-social.png');
    $title = urlencode($post->post_title);
    $description = urlencode($post->post_excerpt);
    $link = urlencode(trim(get_permalink($post->ID), '/'));
    $source = urlencode(get_bloginfo());
    $current_blog = get_current_blog_id();
?>

<ul class="social">
    <li><a href="http://pinterest.com/pin/create/button/?url=<?php echo $link; ?>&amp;media=<?php echo $thumb_url; ?>&amp;description=<?php echo $description; ?>" onclick="openSharePopup(this.href, 'pinterest', 500, 700);return false;" class="icon-pinterest" ><span><?php _e('Pinterest', 'Share Buttons'); ?></span></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $link; ?>&amp;title=<?php echo $title; ?>&amp;summary=<?php echo $description; ?>&amp;source=<? echo $source; ?>" class="icon-linkedin" onclick="openSharePopup(this.href, 'LinkedIn', 520, 570);return false;" ><span><?php _e('Linkedin', 'Share Buttons'); ?></span></a></li>
    <?php if($current_blog == 3) : ?>
        <li><a href="http://service.weibo.com/share/share.php?url=<?php echo $link; ?>&appkey=&title=<?php echo $title; ?>" class="icon-weibo" onclick="openSharePopup(this.href, 'Weibo', 500, 300);return false;" ><span><?php _e('Weibo', 'Share Buttons'); ?></span></a></li>
    <?php else : ?>
        <li><a href="http://twitter.com/share" class="icon-twitter" onclick="openSharePopup(this.href, 'Twitter', 500, 300);return false;" ><span><?php _e('Twitter', 'Share Buttons'); ?></span></a></li>
        <?php endif; ?>
        <li><a href="http://www.facebook.com/sharer.php?u=<?php echo $link; ?>" class="icon-facebook" onclick="openSharePopup(this.href, 'Facebook', 600, 400);return false;" ><span><?php _e('Facebook', 'Share Buttons'); ?></span></a></li>
</ul>