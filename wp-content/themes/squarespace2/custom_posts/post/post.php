<?php
add_action('init','bartlett_meta_init');

function bartlett_meta_init()
{
	// downloads / misc links section in 'product' admin page
	/*
	$product_admin = new WPAlchemy_MetaBox(array
	    (
	    'id'                => 'product_links',
	    'title'             => __( 'Product Downloads &amp; Misc links' ),
	    'types'             => array( 'product' ),
	    'context'           => 'normal',
	    'mode'              => WPALCHEMY_MODE_EXTRACT,
	    'priority'          => 'low',
	    'template'          => get_template_directory() . '/custom_posts/product/metaboxes/product_links_alchemy.php',
	    'autosave'          => true,
	    'prefix'            => 'product_links_',
	    // 'include_template'  => array( 'business-product2.php' ),
	    'hide_editor' => TRUE,
	    'save_filter'       => 'clear_duplicates'
	));
	*/

	// case study details section for 'case study' admin page
	$case_study_admin = new WPAlchemy_MetaBox(array
	    (
	    'id'                => 'case_study',
	    'title'             => __( 'Case Study Details' ),
	    'types'             => array( 'case_study' ),
	    'context'           => 'normal',
	    'mode'              => WPALCHEMY_MODE_EXTRACT,
	    'priority'          => 'low',
	    'template'          => get_template_directory() . '/custom_posts/case_study/metaboxes/case_study_alchemy.php',
	    'autosave'          => true,
	    'prefix'            => 'cs_',
	    // 'include_template'  => array( 'business-product2.php' ),
	    'hide_editor' => TRUE,
	    'save_filter'       => 'clear_duplicates'
	));
}
?>