<?php
    $metabox->the_field('scale');
    $cssScale = ($metabox->get_the_value() !== null) ? $metabox->get_the_value() : 1;
?>
<div class="hide-if-no-js">
    <div class="swf-details postbox">
        <div class="handlediv" title="Click to toggle"></div>
        <h3><?php _e('Attached File Details'); ?></h3>
        <div class="inside">
            <table>
                <tr>
                    <th><?php _e('File'); ?></th>
                    <td>
                        <div id="document-preview" class="document-preview-container">
                            <?php $metabox->the_field('document'); ?>
                            <?php $attachment_url = wp_get_attachment_url((int)$metabox->get_the_value()); ?>
                            <?php $attachment_name = basename(get_attached_file((int)$metabox->get_the_value())); ?>

                            <input type="hidden" class="document-id" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>"/>
                            <a class="document-preview" href="<?php echo $attachment_url; ?>" target="_blank"><?php echo $attachment_name; ?></a>
                            <a class="set-document <?php echo ($attachment_url) ? 'hidden' : ''; ?>" id="set-document" href="#" data-uploader-title="Set File" data-uploader-button-text="Select File" data-attachment-id="<?php echo $metabox->get_the_value(); ?>"><?php _e('Set File'); ?></a>
                            <a class="remove-document <?php echo (!$attachment_url) ? 'hidden' : ''; ?>" id="remove-document" href="#"><?php _e('Remove File'); ?></a>
                        </div>
                    </td>
                </tr>
                <!--
                <tr>
                    <th><?php _e('Link'); ?></th>
                    <td>
                        <div id="addit_url" >
                            <?php $metabox->the_field('addit_url'); ?>
                            <input type="text" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>"/>
                        </div>
                    </td>
                </tr>
                -->
                <tr class="vidcontrol" <?php if ($attachment_url == '') : ?>style="display:none;"<?php endif; ?>>
                    <th><?php _e('full size video preview'); ?></th>
                    <td>
                        <div class="vpWrapperFull">
                            <video preload onloadeddata="videoLoaded('full', this);">
                                <?php if ($attachment_url !== '') : ?><source src="<?php echo $attachment_url; ?>" type="video/mp4"></source><?php endif; ?>
                                
                            </video>
                            <div class="videoIcons"><div>play</div></div>
                            <div class="timeline">
                                <div class="outer">
                                    <div class="track"></div>
                                    <div class="inner"></div>
                                    <div class="marker"></div>
                                </div>
                                <p class="elapsed">00.00</p>
                                <p class="total">00.00</p>
                            </div>
                            <div class="controlLayer"></div>
                        </div>
                    </td>
                </tr>
                <tr class="vidcontrol" <?php if ($attachment_url == '') : ?>style="display:none;"<?php endif; ?>>
                    <th><?php _e('landing page video preview'); ?></th>
                    <td>
                        <div class="vpWrapper">
                            <video muted preload onloadeddata="videoLoaded('cell', this);" style="-webkit-transform:scale(<?php echo $cssScale; ?>);-moz-transform:(<?php echo $cssScale; ?>);-ms-transform:(<?php echo $cssScale; ?>);-o-transform:(<?php echo $cssScale; ?>);transform:(<?php echo $cssScale; ?>);">
                                <?php if ($attachment_url !== '') : ?><source src="<?php echo $attachment_url; ?>" type="video/mp4"></source><?php endif; ?>
                                
                            </video>
                            <div class="videoIcons"><div>play</div></div>
                            <div class="controlLayer"></div>
                        </div>
                    </td>
                </tr>
                <?php
                    $metabox->the_field('scale');
                ?>
                <tr class="vidcontrol" <?php if ($attachment_url == '') : ?>style="display:none;"<?php endif; ?>>
                    <th><?php _e('Video scale'); ?></th>
                    <td>
                       <div id="scale" >
                            <input type="text" name="<?php $metabox->the_name(); ?>" id="vidscale" value="<?php $metabox->the_value(); ?>"/>
                            <a href="#" class="scaleUpdate"><?php _e('Test this video scale setting'); ?></a>
                        </div>
                    </td>
                </tr>
                <!--
                <tr>
                    <th><?php _e('Video box size'); ?></th>
                    <td>
                            <?php $metabox->the_field('boxwidth'); ?>
                            <input type="radio" name="<?php $mb->the_name(); ?>" id="singleWidth" value="single"<?php echo $mb->is_value('single')?' checked="checked"':''; ?>/> <label for="singleWidth"><?php _e('Single width'); ?></label>
                            <input type="radio" name="<?php $mb->the_name(); ?>" id="doubleWidth" value="double"<?php echo $mb->is_value('double')?' checked="checked"':''; ?>/> <label for="doubleWidth"><?php _e('Double width'); ?></label>
                            <input type="radio" name="<?php $mb->the_name(); ?>" id="randomWidth" value="random"<?php echo $mb->is_value('random')?' checked="checked"':''; ?>/> <label for="randomWidth"><?php _e('Random width'); ?></label>
                    </td>
                </tr>
                -->
            </table>
        </div>
    </div>
    <script type="text/javascript">

    var fullPlayer,
        cellPlayer;

    (function () {
        "use strict";

        /* ------------------------------------------------ */
        // VideoPlayer
        /* ------------------------------------------------ */
        
        function VideoPlayer(parameters) {
            this.parameters = parameters;

            this.videoWrapper = parameters.videoWrapper;
            this.videoTag = parameters.videoTag;
            this.icon = parameters.icon;
            this.control = parameters.control;
            this.timeline = parameters.timeline;
            this.mode = parameters.mode;

            if (this.timeline) {
                this.elapsedTimeDisplay = this.timeline.find('.elapsed');
                this.timelineProgress = this.timeline.find('.inner');
                this.timelineMarker = this.timeline.find('.marker');
                this.videoDuration = this.timeline.find('.total');
            }

            this.init();
        }
        
        VideoPlayer.prototype = {
            "constructor" : VideoPlayer,
            "template" : function () {var that = this; },
            
            "init" : function () {
                var that = this;
        
                this.ready();
            },
            "init" : function () {
                var that = this;

                this.isFullMode = (this.mode === 'full');
                this.duration = 0;
                this.elapsed = 0;
                this.startTime = 0;
                this.offsetTime = 0;
                this.updater = null;
                this.isRunning = false;

                // bind events to control layer
                if (this.videoTag) {
                    this.videoTag
                    .bind('ended', function () {
                        that.ended();
                    });

                    if (this.isFullMode) {
                        this.videoTag
                        .bind('timeupdate', function(){
                            that.updateAction2(this.currentTime);
                        });
                    }

                    this.control
                    .bind('click', function(){
                        that.controlAction();
                    });
                }
            },
            "loaded" : function (videoDOMNode) {
                var that = this;

                this.duration = videoDOMNode.duration;
                if (this.isFullMode) {
                    this.videoDuration.text(this.formatTime(this.duration));
                }
            },
            "controlAction" : function () {
                var that = this;

                if (this.isRunning) {
                    this.pause();
                } else {
                    this.play();
                }
                this.isRunning = !this.isRunning;
                this.icon.toggleClass('pause');
            },
            "play" : function () {
                var that = this;
                /*
                if (this.isFullMode) {
                    this.startTime = new Date().getTime();
                    this.updater = window.setInterval(function(){
                        that.updateAction();
                    }, 250);

                }
                */
                this.videoTag.get(0).play();
            },
            "pause" : function () {
                var that = this;

                /*
                if (this.isFullMode) {
                    window.clearInterval(this.updater);
                    this.offsetTime = this.elapsed;
                }
                */
                this.videoTag.get(0).pause();
            },
            "ended" : function () {
                var that = this;

                if (this.isFullMode) {
                    window.clearInterval(this.updater);
                    this.offsetTime = 0;
                }
                that.isRunning = !that.isRunning;
            },
            "updateAction" : function () {
                var that = this,
                    percent;
                this.elapsed = (new Date().getTime() - this.startTime) + this.offsetTime;
                this.updateTimeline(this.elapsed / 1000);
            },
            "updateAction2" : function (elapsedTime) {
                var that = this;

                this.updateTimeline(elapsedTime);

            },
            "updateTimeline" : function (elapsedTime) {
                var that = this,
                    percent;

                percent = Math.floor((elapsedTime / this.duration) * 100);

                this.elapsedTimeDisplay.text(this.formatTime(elapsedTime));
                this.timelineProgress
                .css({
                    "width" : percent +"%"
                });
                this.timelineMarker
                .css({
                    "left" : percent +"%"
                });
            },
            "removeVideo" : function () {
                var that = this;

                this.videoTag.get(0).pause();
                this.videoTag.remove();

                if (this.isFullMode) {
                    this.updateTimeline(0);
                    this.videoDuration.text(this.formatTime(this.duration));
                }
            },
            "insertVideo" : function (videoURL) {
                var that = this;

                switch (this.mode) {
                    case "full" : {
                        var newVideoTag = jQuery('<video></video>')
                        .attr({
                            "preload" : true
                        });

                        var newVideoSource = jQuery("<source></source>")
                        .attr({
                            "type" : "video/mp4",
                            "src" : videoURL
                        })
                        newVideoTag.append(newVideoSource);
                        this.videoWrapper.append(newVideoTag);

                    } break;
                    case "cell" : {
                        var newScale = jQuery('#vidscale').val();
                        var scaleRegex = /^[0-9]+(?:\.[0-9]+)*$/;
                        newScale = (scaleRegex.test(newScale)) ? newScale : 1;

                        var newVideoTag = jQuery('<video></video>').
                        attr({
                            "muted" : true,
                            "preload" : true
                        })
                        .css({
                          '-webkit-transform' : 'scale(' + newScale + ')',
                          '-moz-transform'    : 'scale(' + newScale + ')',
                          '-ms-transform'     : 'scale(' + newScale + ')',
                          '-o-transform'      : 'scale(' + newScale + ')',
                          'transform'         : 'scale(' + newScale + ')'
                        });

                        var newVideoSource = jQuery("<source></source>")
                        .attr({
                            "type" : "video/mp4",
                            "src" : videoURL
                        })
                        newVideoTag.append(newVideoSource);
                        this.videoWrapper.append(newVideoTag);

                    } break;
                }

                window,setTimeout(function(){
                    that.videoTag = that.videoWrapper.find('video');
                    that.videoTag
                    .bind('loadeddata', function () {
                        that.loaded(this);
                    })
                    .bind('ended', function () {
                        that.ended();
                    });

                    if (that.isFullMode) {
                        that.videoTag
                        .bind('timeupdate', function(){
                            that.updateAction2(this.currentTime);
                        });
                    }

                    that.control
                    .unbind('click')
                    .bind('click', function(){
                        that.controlAction();
                    });

                }, 10);
            },
            "refreshVideo" : function (node) {
                var that = this;

                this.videoTag = node;
            },
            "formatTime" : function (time) {
                // format supplied time value into SS.HH
                // SS - seconds
                // HH hundredths of a second
                var seconds = Math.floor(time);
                var hundredths = Math.floor((time - seconds) * 100);
                hundredths = (hundredths < 10) ? "0" + hundredths : hundredths;
                return seconds + "." + hundredths;
            }
        };

        window.VideoPlayer = VideoPlayer;
    }) ();

        cellPlayer = new VideoPlayer({
            "videoWrapper" : jQuery('.vpWrapper'),
            "videoTag" : jQuery('.vpWrapper video'),
            "icon" : jQuery('.vpWrapper .videoIcons'),
            "control" : jQuery('.vpWrapper .controlLayer'),
            "timeline" : null,
            "mode" : "cell"
        });
        fullPlayer = new VideoPlayer({
            "videoWrapper" : jQuery('.vpWrapperFull'),
            "videoTag" : jQuery('.vpWrapperFull video'),
            "icon" : jQuery('.vpWrapperFull .videoIcons'),
            "control" : jQuery('.vpWrapperFull .controlLayer'),
            "timeline" : jQuery('.vpWrapperFull .timeline'),
            "mode" : "full"
        });

        var videoLoaded = function (mode, video) {
            switch (mode) {
                case "cell" : {
                    cellPlayer.loaded(video);
                } break;
                case "full" : {
                    fullPlayer.loaded(video);
                } break;
            }
        };

         jQuery(function() {

            jQuery('.scaleUpdate').on('click', function(e) {
                e.preventDefault();

                var newScale = jQuery('#vidscale').val();
                var scaleRegex = /^[0-9]+(?:\.[0-9]+)*$/;
                if (scaleRegex.test(newScale)) {

                    videoWrapper
                    .find('video')
                    .css({
                          '-webkit-transform' : 'scale(' + newScale + ')',
                          '-moz-transform'    : 'scale(' + newScale + ')',
                          '-ms-transform'     : 'scale(' + newScale + ')',
                          '-o-transform'      : 'scale(' + newScale + ')',
                          'transform'         : 'scale(' + newScale + ')'
                    });
                }
            });

            var documentPreviewContainer = jQuery('#document-preview');

            jQuery('.remove-document', documentPreviewContainer).on('click', function(e) {
                e.preventDefault();

                fullPlayer.removeVideo();
                cellPlayer.removeVideo();

                jQuery(this).siblings('input').val('');
                jQuery(this).siblings('.document-preview').attr('href', '#').data('thumbnail-id', '').addClass('hidden');
                jQuery(this).siblings('.set-document').removeClass('hidden');
                jQuery(this).addClass('hidden');
            })

            // set up behaviour fo follow when a video is added
            new AthMediaModal({
                calling_selector: jQuery('.set-document', documentPreviewContainer),
                cb: function(attachment) {
                    jQuery('.document-id', documentPreviewContainer).val(attachment.id);
                    this.calling_selector.data('attachment-id', attachment.id).addClass('hidden');
                    jQuery('.document-preview', documentPreviewContainer).attr('href', attachment.url).html(attachment.name).removeClass('hidden');
                    jQuery('.remove-document', documentPreviewContainer).removeClass('hidden');

                    cellPlayer = new VideoPlayer({
                        "videoWrapper" : jQuery('.vpWrapper'),
                        "videoTag" : null,
                        "icon" : jQuery('.vpWrapper .videoIcons'),
                        "control" : jQuery('.vpWrapper .controlLayer'),
                        "timeline" : null,
                        "mode" : "cell"
                    });
                    cellPlayer.insertVideo(attachment.url);

                    fullPlayer = new VideoPlayer({
                        "videoWrapper" : jQuery('.vpWrapperFull'),
                        "videoTag" : null,
                        "icon" : jQuery('.vpWrapperFull .videoIcons'),
                        "control" : jQuery('.vpWrapperFull .controlLayer'),
                        "timeline" : jQuery('.vpWrapperFull .timeline'),
                        "mode" : "full"
                    });
                    fullPlayer.insertVideo(attachment.url);
                },
                type: ''
            })
        })
    </script>
</div>