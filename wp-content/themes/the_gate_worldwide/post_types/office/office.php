<?php
/**
 * @package Wordpress Multisite
 * @subpackage The Gate Wordwide
 */

/**
 * Register post type  Offices
 *
 */

  function thegate_worldwide_office_init()
  {
    $labels = array(
        'name'                => _x( 'Offices', 'post type general name' ),
        'singular_name'       => _x( 'Office', 'post type singular name' ),
        'add_new'             => _x( 'Add New Office', 'office' ),
        'add_new_item'        => __( 'Add New Office'),
        'edit_item'           => __( 'Edit Office' ),
        'new_item'            => __( 'New Office' ),
        'all_items'           => __( 'All Offices' ),
        'view_item'           => __( 'View Office' ),
        'search_items'        => __( 'Search Office' ),
        'not_found'           => __( 'No office found' ),
        'not_found_in_trash'  => __( 'No office found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Offices'
    );
  $args = array(
        'labels'             => $labels,
        'public'             => false,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => 'offices',
        'hierarchical'       => true,
        'menu_position'      => null,
        'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/office/img/office.png',
        'supports'           => array( 'title' )
  );

  register_post_type( 'office', $args );
}
add_action( 'init', 'thegate_worldwide_office_init' );

/* Styling for the custom post type icon */
add_action( 'admin_head', 'add_office_large_icon' );

function add_office_large_icon()
{
?>
    <style type="text/css" media="screen">
        #icon-edit.icon32-posts-office {background: url(<?php bloginfo('template_url') ?>/post_types/office/img/office32x32.png) no-repeat;}
    </style>
<?php
}

/* Add filter to ensure the text office, or office, is displayed when user updates a office */
add_filter( 'post_updated_messages', 'codex_office_updated_messages' );
function codex_office_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['office'] = array(
    0 => '', /* Unused. Messages start at index 1.*/
    1 => sprintf( __( 'Office updated. <a href="%s">View office</a>' ), esc_url( get_permalink( $post_ID ) ) ),
    2 => __( 'Custom field updated.' ),
    3 => __( 'Custom field deleted.' ),
  );
  return $messages;
}

/* Custom Metaboxes */
$office_details = new WPAlchemy_MetaBox( array(
    'id'        => 'office_details',
    'title'     => 'Details',
    'types'     => array( 'office' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'prefix'    => 'office_',
    'template'  => get_template_directory() . '/post_types/office/metaboxes/details.php',
    'autosave'  => true,
    'save_filter' => 'gate_office_details_save_filter',
    'save_action'=> 'clear_offices_info_transient'
    ));

$contact_page_details = new WPAlchemy_MetaBox( array(
    'id'        => 'contact_page_details',
    'title'     => 'Contact Page Details',
    'types'     => array( 'office' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'prefix'    => 'contact_',
    'template'  => get_template_directory() . '/post_types/office/metaboxes/contact.php',
    'autosave'  => true,
    ));

$title_translations = new WPAlchemy_MetaBox( array(
    'id'        => 'title_translations',
    'title'     => 'Title Translations',
    'types'     => array( 'office' ),
    'context'   => 'normal',
    'priority'  => 'default',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'prefix'    => 'office_title_',
    'template'  => get_template_directory() . '/post_types/office/metaboxes/title_translations.php',
    'autosave'  => true,
    'lock'      => WPALCHEMY_LOCK_AFTER_POST_TITLE,
    ));

function gate_office_details_save_filter($meta, $post_id)
{
    $url_keys = array('contact_url', 'map_url', 'work_url');

    foreach ($url_keys as $url_key)
    {
        if(isset($meta[$url_key]))
        {
            $value = $meta[$url_key];
            $validation = preg_match( '/^(http|https):\/\/([a-z0-9-]\.)*/i', $value );

            if( !$validation && trim($value) !== '' )
            {
                $meta[$url_key] = 'http://' . $value;
            }
        }
    }

    return $meta;
}

add_filter( 'manage_edit-office_columns', 'add_new_office_columns' );
 function add_new_office_columns( $office_columns )
 {
    $new_columns['cb']              = '<input type="checkbox" />';
    $new_columns['title']           = _x( 'Office', 'column name' );
    $new_columns['phone_number']    = __( 'Phone number' );
    $new_columns['contact_url']     = __( 'Contact page url' );
    $new_columns['work_url']        = __( 'Work page url' );
    $new_columns['map_url']         = __( 'Map page url' );
    $new_columns['date']            = __( 'Date' );

    return $new_columns;
 }
add_action( 'manage_office_posts_custom_column', 'manage_office_columns', 10, 2 );

 function manage_office_columns( $column_name, $id )
 {
    global $office_details;
    $office_details->the_meta();

    switch( $column_name )
    {
        case 'phone_number':
            echo $office_details->the_value( 'office_phone' );
            break;

        case 'contact_url':
            $contact_url = $office_details->get_the_value( 'contact_url' );
            echo valid_url( $contact_url );
            break;

        case 'work_url':
            $work_url = $office_details->get_the_value( 'work_url' );
            echo valid_url( $work_url );
            break;

        case 'map_url':
            $map_url = $office_details->get_the_value( 'map_url' );
            echo valid_url( $map_url );
            break;
    }
}

function valid_url( $url )
{
    $validation = preg_match( '/^(http|https):\/\/([a-z0-9-]\.)*/i', $url );
    if( $validation )
    {
        return '<a href="' . $url . '">' .  $url . '</a>';
    }
    return '<a href="http://' . $url . '">' .  $url . '</a>';
}

function clear_offices_info_transient()
{
    for ($i=2; $i <=5; $i++) {
        switch_to_blog($i);
        delete_transient('offices_information');
        delete_transient('offices_information_zh-hant');
        delete_transient('offices_information_zh-hans');
        delete_transient('leaders_info_with_bio');
        delete_transient('everyone_info_with_bio');
    }

    switch_to_blog(1);
}

add_action('trash_office', 'clear_offices_info_transient');
add_action('publish_office', 'clear_offices_info_transient');
?>