<?php
/**
 * Template Name: Contact Local
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $local_page,
       $validation_mapping;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

if($local_page)
{
    wp_enqueue_script('contact_radio_button');


    $office_id = get_option('office');
    $office_info = !empty($office_id) ? get_office_contact_info_by_id($office_id) : '';
    $get_first_addit_office_id = get_option('first_office');
    $check_first_addit_office = !empty($get_first_addit_office_id) ? get_office_contact_info_by_id($get_first_addit_office_id) : '';
    $get_second_addit_office_id = get_option('second_office');
    $check_second_addit_office = !empty($get_second_addit_office_id) ? get_office_contact_info_by_id($get_second_addit_office_id) : '';
    $get_third_addit_office_id = get_option('third_office');
    $check_third_addit_office = !empty($get_third_addit_office_id) ? get_office_contact_info_by_id($get_third_addit_office_id) : '';
    $list_offices = array($check_first_addit_office, $check_second_addit_office, $check_third_addit_office);

    $office_name = get_current_blog_id() === 3 ? str_replace(' ', '<br/>', $office_info->post_title) : $office_info->post_title;

    $contact_fields = get_post_meta($local_page->ID, 'contact_fields_field', true);
    $contact_success_message = get_post_meta($local_page->ID, 'contact_messages_success', true);
    $contact_fail_message = get_post_meta($local_page->ID, 'contact_messages_fail', true);

    $validation_mapping = array();
    $fields = array();
    if(isset($contact_fields) && is_array($contact_fields))
    {
        foreach ($contact_fields as $number => $field)
        {
            foreach ($field as $key => $field_setting)
            {
                if(strpos($key, 'validate_against_') === 0 && $field_setting)
                {
                    $validation_mapping[$field['id']][] = substr($key, 17);
                }
            }

            if($field['id'] && trim($field['id']) !== '' && $field['title'] && trim($field['title']) !== '' )
            {
                $fields[$number]['id'] = $field['id'];
                $fields[$number]['title'] = $field['title'];
                $fields[$number]['type'] = (isset($field['type'])) ? $field['type'] : 'text_input';
                $fields[$number]['error_message'] = (isset($field['error_message'])) ? $field['error_message'] : '';
            }
        };
    }

    if(isset($_POST['send']) && $_POST['send'])
    {
        $validation = new ValidationHelper($_POST, $validation_mapping);
        $is_valid = $validation->validate();

        if($is_valid)
        {
            $is_mail_sent = send_email($local_page->ID, $_POST);
        }
    }
?>
    <li id="local" class="gray local">
        <div class="visual">
            <div class="visual-holder">
                <?php echo (has_post_thumbnail($local_page->ID)) ? get_the_post_thumbnail($local_page->ID, 'full') : get_placeholder_image('contact-local'); ?>
            </div>
        <!--
            <div class="local-office">
                <span class="local-office-inner">
                    <span class="centered">
                        <?php if(isset($office_info->post_title)) : ?>
                            <h4><?php echo $office_name; ?></h4>
                        <?php endif; ?>
                        <?php if(isset($office_info->street)) : ?>
                            <p><?php echo $office_info->street; ?></p>
                        <?php endif; ?>
                        <?php if(isset($office_info->office_floor)) : ?>
                            <p><?php echo $office_info->office_floor; ?></p>
                        <?php endif; ?>
                        <?php if(isset($office_info->post_code)) : ?>
                            <p><?php echo $office_info->post_code; ?></p>
                        <?php endif; ?>
                        <?php if(isset($office_info->phone)) : ?>
                            <em><?php echo $office_info->phone; ?></em>
                        <?php endif; ?>
                    </span>
                </span>
            </div>
        -->
        </div>
        <h2><?php echo apply_filters('the_title', $local_page->post_title); ?></h2>
        <span id="success" class="<?php if(!isset($is_mail_sent) || !$is_mail_sent) echo 'hidden'; ?>"><?php echo $contact_success_message; ?></span>
        <span id="fail" class="<?php if(!isset($is_mail_sent) || $is_mail_sent) echo 'hidden'; ?>"><?php echo $contact_fail_message; ?></span>
        <form class="contact" action="<?php echo get_permalink($local_page->post_parent); ?>" method="POST">
            <?php if($check_first_addit_office || $check_second_addit_office || $check_third_addit_office) : ?>
                <div class="select-office">
                    <h4><?php _e('Select an office'); ?></h4>
                    <fieldset>
                        <?php $count = 1; foreach( $list_offices as $key => $list_office) : ?>
                            <?php if(!empty($list_office)) : ?>
                                <div class="office<?php echo $count; ?>">
                                    <input type="radio" name="office" id="office<?php echo $count; ?>" value="<?php echo $list_office->ID; ?>" />
                                    <label class="office-label" for="office<?php echo $count; ?>"><?php echo $list_office->post_title; ?></label>
                                </div>
                            <?php endif; ?>
                        <?php $count++; endforeach; ?>
                    </fieldset>
                </div>
                <div class="form-right">
                    <?php $count = 1; foreach( $list_offices as $key => $list_office) : ?>
                        <?php if(!empty($list_office)) : ?>
                            <div class="office<?php echo $count; ?> offices">
                                <h3><?php _e('The Gate', 'Contact Page'); ?></h3>
                                <?php if($list_office->post_title) : ?>
                                    <h4><?php echo $list_office->post_title; ?></h4>
                                <?php endif; ?>
                                <?php if($list_office->street) : ?>
                                    <p><?php echo $list_office->street; ?></p>
                                <?php endif; ?>
                                <?php if($list_office->office_floor) : ?>
                                    <p><?php echo $list_office->office_floor; ?></p>
                                <?php endif; ?>
                                <?php if($list_office->post_code) : ?>
                                    <p><?php echo $list_office->post_code; ?></p>
                                <?php endif; ?>
                                <br />
                                <?php if($list_office->phone) : ?>
                                    <p>T&nbsp;&nbsp;&nbsp;<?php echo $list_office->phone; ?></p>
                                <?php endif; ?>
                                <?php if($list_office->email) : ?>
                                    <p>E&nbsp;&nbsp;&nbsp;<a href="mailto:<?php echo $list_office->email; ?>"><?php echo $list_office->email; ?></a></p>
                                <?php endif; ?>
                                <p></p>
                            </div>
                        <?php endif; ?>
                    <?php $count++; endforeach; ?>
                </div>
            <?php else : ?>
                <div class="form-right">
                    <div class="office1 offices">
                        <h3><?php _e('The Gate', 'Contact Page'); ?></h3>
                        <?php if($office_info->post_title) : ?>
                            <h4><?php echo $office_info->post_title; ?></h4>
                        <?php endif; ?>
                        <?php if($office_info->street) : ?>
                            <p><?php echo $office_info->street; ?></p>
                        <?php endif; ?>
                        <?php if($office_info->office_floor) : ?>
                            <p><?php echo $office_info->office_floor; ?></p>
                        <?php endif; ?>
                        <?php if($office_info->post_code) : ?>
                            <p><?php echo $office_info->post_code; ?></p>
                        <?php endif; ?>
                        <br />
                        <?php if($office_info->phone) : ?>
                            <p>T&nbsp;&nbsp;&nbsp;<?php echo $office_info->phone; ?></p>
                        <?php endif; ?>
                        <?php if($office_info->email) : ?>
                            <p>E&nbsp;&nbsp;&nbsp;<a href="mailto:<?php echo $office_info->email; ?>"><?php echo $office_info->email; ?></a></p>
                        <?php endif; ?>
                        <p></p>
                    </div>
                </div>
            <?php endif;?>
            <?php foreach ($fields as $field) : ?>
                <div class="contact-field <?php if(isset($validation->errors[$field['id']])) echo 'error'; ?>">
                    <?php switch ($field['type']) :
                        default:
                        case 'text_input': ?>
                            <input id="<?php echo $field['id']; ?>" data-defaultvalue="<?php echo $field['title']; ?>" type="text" value="<?php echo (isset($_POST[$field['id']])) ? $_POST[$field['id']] : $field['title']; ?>" name="<?php echo $field['id']; ?>">

                            <?php break;

                        case 'textarea': ?>
                            <textarea id="<?php echo $field['id']; ?>" data-defaultvalue="<?php echo $field['title']; ?>" cols="40" rows="8" name="<?php echo $field['id']; ?>"><?php echo (isset($_POST[$field['id']])) ? $_POST[$field['id']] : $field['title']; ?></textarea>

                            <?php break; ?>
                    <?php endswitch; ?>
                    <span class="error-msg"><span class="left-arrow"></span><span class="text"><?php echo $field['error_message']; ?></span></span>
                </div>
            <?php endforeach; ?>
            <div class="contact-success">
            	<p>Thank you.<br>Your message has been sent.</p>
            </div>
            <div class="form-right social-contact">
            <?php
				$lURL = get_post_meta($local_page->ID, 'contact_messages_cu_linkedin', true);
				$tURL = get_post_meta($local_page->ID, 'contact_messages_cu_twitter', true);
				$fURL = get_post_meta($local_page->ID, 'contact_messages_cu_facebook', true);
			?>
            	<?php if($lURL != ''): ?>
            	<a target="_blank" href="<?= $lURL ?>"><div class="social-but-l"></div></a>
               	<?php endif; ?>
               <?php if($tURL != ''): ?> 
               	<a target="_blank" href="<?= $tURL ?>"><div class="social-but-t"></div></a>
               	<?php endif; ?>
				<?php if($fURL != ''): ?>
               	<a target="_blank" href="<?= $fURL ?>"><div class="social-but-f"></div></a>
               	<?php endif; ?>
            </div>
            <input type="submit" name="send" value="SEND" />
        </form>
    </li>
<?php } ?>
