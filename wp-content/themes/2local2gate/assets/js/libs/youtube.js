function callPlayer(frame_id, func, args){
    if(!frame_id) return;
    if(frame_id.id) frame_id = frame_id.id;
    else if(typeof jQuery != "undefined" && frame_id instanceof jQuery && frame_id.length) frame_id = frame_id.get(0).id;
    if(!document.getElementById(frame_id)) return;
    args = args || [];

    /*Searches the document for the IFRAME with id=frame_id*/
    var all_iframes = document.getElementsByTagName("iframe");
    for(var i=0, len=all_iframes.length; i<len; i++){
        if(all_iframes[i].id == frame_id || all_iframes[i].parentNode.id == frame_id){
           /*The index of the IFRAME element equals the index of the iframe in
             the frames object (<frame> . */
           window.frames[i].postMessage(JSON.stringify({
                "event": "command",
                "func": func,
                "args": args,
                "id": frame_id
            }), "*");
        }
    }
}


function playYouTubeVideo() {
    if($('#player-youtube-1').length > 0) {
        $('.video-holder').removeClass('hidden');

        setTimeout(function() {
            callPlayer('player-youtube-1', 'playVideo');
        }, 500);
    }
}