<?php
/**
 * WP Theme Public settings
 *
 * @package WordPress
 * @subpackage thegate_worldwide
 */


/**
 * Register required assets
 */
function thegate_worldwide_load_assets()
{
	if( !is_admin() )
    {
        $template_url = get_stylesheet_directory_uri();

        /** Styles
		 * Example use: wp_register_style( $handle, $src, $deps, $ver, $media );
         */

		wp_register_style( 'checkbox', ASSETS_URL . '/css/libs/jquery.checkBox.css' );

        /** Scripts
		 * Example use: wp_register_script( $handle, $src, $deps, $ver, $in_footer );
         */

        wp_deregister_script( 'jquery' );
        wp_register_script( 'jquery', ASSETS_URL . '/js/libs/jquery-1.8.3.min.js', false, '1.8.3', ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'jquery_animate_colors', ASSETS_URL . '/js/libs/jquery.animate-colors-min.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'jquery_main', ASSETS_URL . '/js/libs/jquery.main.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'equal_heights', ASSETS_URL . '/js/libs/equal-heights.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );

        wp_register_script( 'init',  ASSETS_URL . '/js/init.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
	}
}
add_action( 'init', 'thegate_worldwide_load_assets' );

/**
 * Enqueue a IE-specific style sheet (for all browsers).
 *
 * @author Gary Jones
 * @link http://code.garyjones.co.uk/ie-conditional-style-sheets-wordpress/
 * @modified Kris Arsov
 */
function thegate_worldwide_add_ie_custom_styles() {
    // wp_enqueue_style( 'ie7', ASSETS_URL . '/css/ie7.css', false, '1.0' );
    // wp_enqueue_style( 'ie6', ASSETS_URL . '/css/ie6.css', false, '1.0' );
}
add_action( 'wp_print_styles', 'thegate_worldwide_add_ie_custom_styles', 200 );

/**
 * Add conditional comments around IE-specific style sheet link.
 *
 * @author Gary Jones & Michael Fields (@_mfields)
 * @link http://code.garyjones.co.uk/ie-conditional-style-sheets-wordpress/
 * @modified Kris Arsov
 *
 * @param string $tag Existing style sheet tag.
 * @param string $handle Name of the enqueued style sheet.
 *
 * @return string Amended markup
 */
function thegate_worldwide_make_ie_custom_styles_conditional( $tag, $handle ) {
    if( 'ie7' == $handle )
        $tag = '<!--[if IE 7]>' . "\n" . $tag . '<![endif]-->' . "\n";

    if( 'ie6' == $handle )
        $tag = '<!--[if lte IE 6]>' . "\n" . $tag . '<![endif]-->' . "\n";

    return $tag;
}
add_filter( 'style_loader_tag', 'thegate_worldwide_make_ie_custom_styles_conditional', 10, 2 );

/**
 * Enqueue a IE-specific script (for all browsers).
 *
 * @author Gary Jones
 * @modified Kris Arsov
 */
function thegate_worldwide_add_ie_custom_scripts() {
    // wp_enqueue_script( 'ie7', ASSETS_URL . '/js/ie7.js', false, '1.0' );
    // wp_enqueue_style( 'ie6', ASSETS_URL . '/js/ie6.js', false, '1.0' );
    wp_enqueue_style( 'ie', ASSETS_URL . '/js/ie.js', false, '1.0' );
}
add_action( 'wp_print_scripts', 'thegate_worldwide_add_ie_custom_scripts', 200 );

/**
 * Add conditional comments around IE-specific style sheet link.
 *
 * @author Gary Jones & Michael Fields (@_mfields)
 * @modified Kris Arsov
 *
 * @param string $tag Existing script tag.
 * @param string $handle Name of the enqueued script.
 *
 * @return string Amended markup
 */
function thegate_worldwide_make_ie_custom_scripts_conditional( $tag, $handle ) {
    if( 'ie7' == $handle )
        $tag = '<!--[if IE 7]>' . "\n" . $tag . '<![endif]-->' . "\n";

    if( 'ie6' == $handle )
        $tag = '<!--[if lte IE 6]>' . "\n" . $tag . '<![endif]-->' . "\n";

    if( 'ie' == $handle )
        $tag = '<!--[if IE]>' . "\n" . $tag . '<![endif]-->' . "\n";

    return $tag;
}
add_filter( 'script_loader_tag', 'thegate_worldwide_make_ie_custom_scripts_conditional', 10, 2 );

?>