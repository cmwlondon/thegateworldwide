<?php
$args = array(
    'numberposts'     => -1,
    'orderby'         => 'post_date',
    'post_type'       => 'office',
    'post_status'     => 'any',
    'suppress_filters' => true
);

$offices = get_posts($args);
?>
<div class="office box">
    <?php $mb->the_field('office'); ?>
    <label for="<?php $mb->the_name(); ?>"><?php _e("Select this person's working office: "); ?></label><br/>
    <select name="<?php $mb->the_name(); ?>" id="<?php $mb->the_name(); ?>" style="width: 100%">
        <?php foreach ($offices as $office) : ?>
            <option value="<?php echo $office->ID; ?>" <?php $mb->the_select_state($office->ID); ?>>
                <?php echo $office->post_title;?>
            </option>
        <?php endforeach; ?>
    </select>
</div>
