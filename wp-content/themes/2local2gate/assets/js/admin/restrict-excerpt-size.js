jQuery(function()
{
    maxLength = 160;

    var excerpt = jQuery('#excerpt');

    if(excerpt.length > 0)
    {
        var remainingLengthTextContainer = jQuery('<div/>').insertAfter(excerpt).text(' characters remaining.');
        var remainingLengthContainer = jQuery('<strong/>').prependTo(remainingLengthTextContainer).text(maxLength);

        checkMaxLength(excerpt, maxLength, remainingLengthTextContainer, remainingLengthContainer);

        jQuery(excerpt).bind('keyup change', function(){
            checkMaxLength(jQuery(this), maxLength, remainingLengthTextContainer, remainingLengthContainer);
        });
    }
});

function checkMaxLength(textarea, maxLength, textContainer, remainingLengthContainer) {
    var currentLength = jQuery(textarea).val().length;
    var remainingLength = Number(maxLength - currentLength);

    if(remainingLength > 0) {
        remainingLengthContainer.text(remainingLength).css('color', 'green');
    }
    else {
        textarea.val(textarea.val().slice(0, maxLength));
        remainingLengthContainer.text(0).css('color', 'red');
    }
}