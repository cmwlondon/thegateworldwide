<?php
$args = array(
    'numberposts'     => -1,
    'orderby'         => 'post_date',
    'post_type'       => 'client',
    'post_status'     => 'any',
    'suppress_filters' => true
);

$clients = get_posts($args);
?>
<div class="office box">
    <?php $mb->the_field('client'); ?>
    <label for="<?php $mb->the_name(); ?>"><?php _e("Select the client of this campaign: "); ?></label><br/>
    <select name="<?php $mb->the_name(); ?>" id="<?php $mb->the_name(); ?>" style="width: 100%">
        <?php foreach ($clients as $client) : ?>
            <option value="<?php echo $client->ID; ?>" <?php $mb->the_select_state($client->ID); ?>>
                <?php echo $client->post_title;?>
            </option>
        <?php endforeach; ?>
    </select>
</div>
