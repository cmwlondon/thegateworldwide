<?php switch_to_blog(1);

$args = array(
    'numberposts'     => -1,
    'offset'          => 0,
    'post_type'       => 'office',
    'post_status'     => 'publish',
    'suppress_filters' => true
);

$offices = get_posts($args); ?>

<p><?php _e('Choose and order the represented by leaders offices. (If non is chosen the default offices will be presented)'); ?></p>

<?php $office_number = 1; ?>
<?php while($mb->have_fields('offices', 6)): ?>
    <div class="misc-pub-section">
        <?php $mb->the_field('office'); ?>
        <?php if($office_number > 2 && $office_number < 6) : ?>
            <span><?php _e('Asia - '); ?></span>
            <?php $width = '92%'; ?>
        <?php else : ?>
            <?php $width = '95%'; ?>
        <?php endif; ?>
        <select name="<?php $mb->the_name(); ?>" style="width: <?php echo $width; ?>">
            <option value=""></option>
            <?php foreach ($offices as $office) : ?>
                <option value="<?php echo $office->ID; ?>"<?php $mb->the_select_state($office->ID); ?>><?php echo $office->post_title; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <?php $office_number++; ?>
<?php endwhile; ?>
<div class="clear"></div>

<?php restore_current_blog();