<?php
/**
 * Single Campaign Page (Individual Campaign Page)
 *
 * @package WordPress
 * @subpackage TheGate
 */
global $related_posts;

the_post();

$client = get_client();
$related_posts = get_related_posts(3, array('post', 'campaign', 'campaign_element'), true);
$video = get_post_meta($post->ID, 'campaign_element_video', true);
$swf = get_post_meta($post->ID, 'campaign_element_swf', true);
$swf_link = isset($swf['addit_url']) && !empty($swf['addit_url']) ? $swf['addit_url'] : '#';
if(is_array($swf) && isset($swf['document']) && !empty($swf['document']))
{
    $get_swf_src = wp_get_attachment_url((int)$swf['document']);
	$swfURL = get_full_path($get_swf_src);
    $swf_sizes = getimagesize($swfURL != "" ? $swfURL : $get_swf_src);
}
$galery_thumbs_ids = explode(',', get_post_meta($post->ID, 'gallery_thumbs', true));

$previous_post = get_previous_post();
$next_post = get_next_post();

wp_enqueue_script('share_popup');
wp_enqueue_script('fancybox_pack');
wp_enqueue_script('anything_slider');
wp_enqueue_script('youtube');
wp_enqueue_script('vimeo');

get_header();
?>
    <!-- visual -->
</header>
<div id="main">
    <div class="work-individual-media-wrap">
        <div class="work-individual-media">
            <ul class="featured-prev-next">
                <?php if (!empty( $next_post )): ?>
                    <li class="prev"><a href="<?php echo get_permalink( $next_post->ID ); ?>"></a></li>
                <?php endif; ?>
                <li class="mobile-carousel-msg"><span><?php _e('WORK', 'Single Campaign Element Page'); ?></span></li>
                <?php if (!empty( $previous_post )): ?>
                    <li class="next"><a href="<?php echo get_permalink( $previous_post->ID ); ?>"></a></li>
                <?php endif; ?>
            </ul>
            <div class="work-slider-wrapper<?php if($swf && isset($swf_sizes)){ ?> flash-override<?php } ?>" >
                <?php if($video && isset($video['video_embedcode']) && isset($video['video_share_site'])) : ?>
                    <?php if($video['video_share_site'] == 'vimeo') : ?>
                        <a href="#" onclick="$(this).hide();playVimeoVideo();" class="single-image" style="background-image: url(<?php echo (MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'main_image', $post->ID)) ? MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'main_image', $post->ID,  'main-image') : get_placeholder_image_src('main-image'); ?>)"></a>
                        <div class="video-holder hidden">
                            <?php echo $video['video_embedcode']; ?>
                        </div>
                    <?php elseif($video['video_share_site'] == 'youtube') : ?>
                        <a href="#" onclick="$(this).hide();playYouTubeVideo();" class="single-image" style="background-image: url(<?php echo (MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'main_image', $post->ID)) ? MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'main_image', $post->ID,  'main-image') : get_placeholder_image_src('main-image'); ?>)"></a>
                        <div class="video-holder hidden">
                            <?php echo $video['video_embedcode']; ?>
                        </div>
                    <?php else : ?>
                        <a class="single-image fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo (MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'main_image', $post->ID)) ? MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'main_image', $post->ID,  'full') : get_placeholder_image_src('main-image'); ?>" style="background-image: url(<?php echo (MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'main_image', $post->ID)) ? MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'main_image', $post->ID,  'main-image') : get_placeholder_image_src('main-image'); ?>)"></a>
                    <?php endif; ?>
                <?php elseif($swf && isset($swf_sizes)) : ?>
                    <a href="<?php echo $swf_link; ?>" class="flash-thumb" style="background-image: url(<?php echo (MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'main_image', $post->ID)) ? MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'main_image', $post->ID,  'main-image') : get_placeholder_image_src('main-image'); ?>)" <?php if(!empty($swf['addit_url'])) : ?>target="_blank"<?php endif;?>></a>
                    <div class="flash-holder">
                        <div class="centered">
                            <embed class="flash-content" <?php echo $swf_sizes[3]; ?> allowscriptaccess="always" allowfullscreen="true" wmode="transparent" type="application/x-shockwave-flash" src="<?php echo $get_swf_src; ?>">
                        </div>
                    </div>
                <?php else: ?>
                    <a class="single-image fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo (MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'main_image', $post->ID)) ? MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'main_image', $post->ID,  'full') : get_placeholder_image_src('main-image'); ?>" style="background-image: url(<?php echo (MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'main_image', $post->ID)) ? MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'main_image', $post->ID,  'main-image') : get_placeholder_image_src('main-image'); ?>)"></a>
                <?php endif; ?>
                <div class="visual">
                </div>
            </div>
        </div>
    </div>
    <div class="main-holder">
        <!-- content -->
        <div id="content" class="ind-port">
            <div class="indport-left">
                <?php echo (has_post_thumbnail($client->ID)) ? get_the_post_thumbnail($client->ID, 'client-logo', array('class' => 'do-not-open-in-modal')) : get_placeholder_image('client-logo', null, array('do-not-open-in-modal')); ?>
            </div>
            <div class="indport-right">
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
                <?php get_template_part('part', 'social_buttons'); ?>
            </div>
        </div>
    </div>
    <?php get_template_part('part', 'related_items'); ?>
</div>

<?php get_footer(); ?>