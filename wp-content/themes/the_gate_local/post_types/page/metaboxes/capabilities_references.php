<?php
    $references_categories = array(
        'first_category'    => 'First Category',
        'second_category'   => 'Second Category',
        'third_category'    => 'Third Category',
        'fourth_category'   => 'Fourth Category',
    )
?>

<?php foreach ($references_categories as $category_name => $category_title) : ?>
    <div class="references_categories postbox">
        <div class="handlediv" title="Click to toggle"></div>
        <h3><?php _e($category_title); ?></h3>
        <div class="inside" style="position: relative">

            <div class="misc-pub-section">
                <h4><?php _e('Title'); ?></h4>
                <?php $metabox->the_field($category_name . '_title'); ?>
                <input type="text" id="<?php echo $category_name; ?>-title" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>" style="width: 100%"/>
            </div>

            <?php while($metabox->have_fields_and_multi($category_name . '_references')): ?>
                <?php $mb->the_group_open(); ?>
                    <div class="misc-pub-section">
                            <table>
                                <tr>
                                    <?php $metabox->the_field('text'); ?>
                                    <textarea rows="5" cols="30" name="<?php $mb->the_name(); ?>"><?php $mb->the_value(); ?></textarea></td>
                                </tr>
                                <tr>
                                    <td><a href="#" class="dodelete button"><?php _e('Remove Reference'); ?></a></td>
                                </tr>
                            </table>
                    </div>
                <?php $mb->the_group_close(); ?>
            <?php endwhile; ?>
            <p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-<?php echo $category_name; ?>_references button"><?php _e('Add Reference'); ?></a></p>
        </div>
    </div>
<?php endforeach; ?>

<div class="clear"></div>
