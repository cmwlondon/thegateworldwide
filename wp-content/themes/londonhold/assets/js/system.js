	var supportsCSSTransitions,
	ishighdpi,
	dpiSelectItems,
	testBox,
	videoDOM,
	videoLoaded = false,
	isIOS;


(function () {
	"use strict";

	window.requestAnimFrame = (function(callback) {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
		};
	})();

	Modernizr.addTest('isios', function() {
	    return navigator.userAgent.match(/(iPad|iPhone|iPod)/g);
	});

	/* ---------------------------------------------------------------------------------- */
	// document ready START
	/* ---------------------------------------------------------------------------------- */
	
	$(document).ready(function () {

		// $('#test').html(navigator.userAgent);

		supportsCSSTransitions = $('html').hasClass('csstransitions');
		ishighdpi = ( window.devicePixelRatio > 1);

		isIOS = Modernizr.isios;
		if ( isIOS ) { jQuery('html').addClass('ios'); } else { jQuery('html').addClass('not-ios'); } 

		videoDOM = document.getElementById('vp');
		if (videoDOM != null) {

			videoDOM.addEventListener("playing", function() {
				if (!videoLoaded) {
					videoLoaded = true;
					// $('.page').css({"opacity" : 0}).animate({"opacity" : 1}, 1000, function(){});
				}
			}, true);

			jQuery('.page').on('click', function(e){
				videoDOM.play();
			});

		}

	});

	/* ---------------------------------------------------------------------------------- */
	// document ready END
	/* ---------------------------------------------------------------------------------- */
	
	/* ---------------------------------------------------------------------------------- */
	// window.load START
	/* ---------------------------------------------------------------------------------- */

	$(window).load(function(){

		/*
		windowSize = {
			"w" : $(window).width(), 
			"h" : $(window).width()
		};

		if (windowSize.w > 1200) {
			windowSize.w = 1200;
		}

		// define canvas and context for canvas#fullScreenCanvas
		thisStage = new CanvasStage({
			"canvasNode" : "threeStage",
			"width" : windowSize.w,
			"height" : windowSize.h
		})
		*/

	});

	/* ---------------------------------------------------------------------------------- */
	// window.load END
	/* ---------------------------------------------------------------------------------- */

})();