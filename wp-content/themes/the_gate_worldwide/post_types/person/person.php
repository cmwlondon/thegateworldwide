<?php
/**
 * Person
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Creation of the custom posttype */
add_action( 'init', 'create_person_post_type');
function create_person_post_type()
{
	$labels = array(
	    'name' => __('People'),
	    'singular_name' => __('Person'),
	    'add_new' => __('Add New Person'),
	    'add_new_item' => __('Add New Person'),
	    'edit_item' => __('Edit Person'),
	    'new_item' => __('New Person'),
	    'all_items' => __('All People'),
	    'view_item' => __('View Person'),
	    'search_items' => __('Search People'),
	    'not_found' =>  __('No people found'),
	    'not_found_in_trash' => __('No people found in Trash'),
	    'parent_item_colon' => __(''),
	    'menu_name' => __('People')
  	);

    register_post_type( 'person',
            array(
            'labels' => $labels,
            'public' => false,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'show_ui' => true,
            'query_var' => true,
            'show_in_nav_menus' => true,
            'has_archive' => false,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 25,
            'menu_icon' => get_bloginfo('template_directory') . '/post_types/person/img/person_small.png',
            'supports' => array('title', 'excerpt', 'editor', 'thumbnail'),
            'hierarchical' => true,
            'rewrite' => array('slug' => 'people'),
            'taxonomies' => array('post_tag'),
        )
    );
 }

/* Styling for the custom post type icon */
add_action( 'admin_head', 'add_person_large_icon' );

function add_person_large_icon()
{
?>
    <style type="text/css" media="screen">
        #icon-edit.icon32-posts-person {background: url(<?php bloginfo('template_url') ?>/post_types/person/img/person_large.png) no-repeat;}
    </style>
<?php
}

/* add filter to ensure the text thas Pizza is updated, is displayed when user updates a document */
add_filter( 'post_updated_messages', 'set_person_updated_messages' );

function set_person_updated_messages( $messages )
{
  global $post, $post_ID;

  $messages['person'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Person updated. <a href="%s">View Person</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Person updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Person restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Person published. <a href="%s">View Person</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Person saved.'),
    8 => sprintf( __('Person submitted. <a target="_blank" href="%s">Preview Person</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Person scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Person</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Person draft updated. <a target="_blank" href="%s">Preview Person</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}


/* Manage Post Columns */
add_filter('manage_edit-person_columns', 'add_new_person_columns');

function add_new_person_columns($columns)
{
    $new_columns['cb'] = $columns['cb'];
    $new_columns['title'] = __('Full Name');
    $new_columns['first_name'] = __('First Name');
    $new_columns['last_name'] = __('Last Name');
    $new_columns['position'] = __('Position');
    $new_columns['thumb'] = __('Image');
    $new_columns['office'] = __('Office');
    $new_columns['leader'] = __('Leader');
    $new_columns['date'] = $columns['date'];

    return $new_columns;
}

add_action('manage_person_posts_custom_column', 'manage_person_columns');
function manage_person_columns($column)
{
    global $post, $people_personal_information, $people_social_networks_accounts, $people_workplace;

    switch ($column)
    {
        case 'thumb':

            echo get_the_post_thumbnail($post->ID, array(80,60));
            break;
        case 'first_name':
        case 'last_name':
        case 'position':

            $meta = $people_personal_information->the_meta();
			echo (isset($meta[$column])) ? $meta[$column] : __('');
            break;

        case 'office':
            $meta = $people_workplace->the_meta() ;
            $office = isset($meta[$column]) ? get_post($meta[$column]) : false;

			echo ($office && $office->post_type == "office") ? '<a href="' . get_edit_post_link( $office->ID ) . '" target="_blank">' . $office->post_title . '</a>' : __('No office set');
            break;

        case 'leader':
            echo (in_array('leader', wp_get_post_tags($post->ID, array('fields' => 'names')))) ? __('Yes') : __('No');
            break;
    }
}

/* Add sortable columns */
add_filter('manage_edit-person_sortable_columns', 'add_sortable_columns_to_person');
function add_sortable_columns_to_person($columns)
{
	$columns['office'] = 'office';
	$columns['first_name'] = 'first_name';
	$columns['last_name'] = 'last_name';
	$columns['position'] = 'position';

	return $columns;
}

/* Set the needed query var if some of the custom sortable columns is sorted by */
add_filter( 'request', 'person_columns_orderby' );
function person_columns_orderby( $vars )
{
	$custom_meta_sortable_columns = array('office', 'first_name', 'last_name', 'position');

	if ( isset( $vars['orderby'] ) && in_array('person_' . $vars['orderby'], $custom_meta_sortable_columns))
	{
		$vars = array_merge( $vars, array(
			'meta_key' => 'person_' . $vars['orderby'],
			'orderby' => 'meta_value'
		));
	}

	return $vars;
}


/* Add contextual help for the people */
add_action( 'contextual_help', 'person_contextual_help', 10, 3 );
function person_contextual_help( $contextual_help, $screen_id, $screen )
{
    if ( 'edit-person' == $screen->id )
    {
         $contextual_help = '<h2>People</h2>
         <p>You can see the list of people on this page in reverse chronological order - the latest added appear first. Of course you are able to sort the list by any sortable column.</p>
         <p>You can filter the list by office, by leadership or by date.</p>
         <p>You can view/edit the details of every person by clicking on its name, or you can perform bulk actions using the dropdown menu and selecting multiple items.</p>';
    }
    elseif ( 'person' == $screen->id )
    {
         $contextual_help = '<h2>Editing Person</h2>
         <p>This page allows you to view/modify a person details. </p>';
    }
    return $contextual_help;
}

add_action( 'restrict_manage_posts', 'manage_person_list_filters' );
function manage_person_list_filters()
{
    global $typenow, $wp_query;

    if ($typenow == 'person') {

    	$selected = (isset($wp_query->query['tag']) && $wp_query->query['tag'] == 'leader') ? 'selected="selected"' : '';

        echo '<select name="tag" id="tag" class="postform">';
        echo '<option value="">Show All</option>';
        echo '<option value="leader" ' . $selected . '>Only Leaders</option>';
        echo '</select>';

        $offices = get_posts(array(
            'post_type' => 'office'
        ));


        echo '<select name="person_office" id="person_office" class="postform">';
        echo '<option value="">All Offices</option>';
        foreach ($offices as $office)
        {
        	$selected = (isset($wp_query->query['meta_key']) && $wp_query->query['meta_key'] == 'person_office' && $wp_query->query['meta_value'] == $office->ID) ? 'selected="selected"' : '';
            echo '<option value="' . $office->ID . '" ' . $selected . '>' . $office->post_title . '</option>';
        }
        echo '</select>';

    }
}

/* Make the functionalty to filter by office */
add_filter( 'request', 'filter_people_by_office' );
function filter_people_by_office( $vars )
{
    if ( isset( $vars['person_office'] ) && $vars['person_office'] ) {
        $vars = array_merge( $vars, array(
            'meta_key' => 'person_office',
            'meta_value' => $vars['person_office']
        ) );
    }

    return $vars;
}

add_filter( 'query_vars', 'add_office_to_person_query_vars' );
function add_office_to_person_query_vars($query_vars)
{
	$query_vars[] = 'person_office';

	return $query_vars;
}


/* Custom metaboxes */


/* Register People Metabox 'Personal Information' */
$people_personal_information = new WPAlchemy_MetaBox(array
(
    'id'        => 'personal_information',
    'title'     => __('Personal Information'),
    'types'     => array('person'),
    'context'   => 'normal',
    'mode' 		=> WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'high',
    'template'  => get_template_directory() . '/post_types/person/metaboxes/personal_information.php',
    'autosave'  => true,
    'prefix'    => 'person_',
    'save_action'=> 'clear_person_transients',
));

/* Register People Metabox 'Social Networks Accounts' */
$people_social_networks_accounts = new WPAlchemy_MetaBox(array
(
    'id'        => 'social_networks_accounts',
    'title'     => __('Social Networks Accounts'),
    'types'     => array('person'),
    'context'   => 'normal',
    'priority'  => 'low',
    'mode' 		=> WPALCHEMY_MODE_EXTRACT,
    'template'  => get_template_directory() . '/post_types/person/metaboxes/social_networks_accounts.php',
    'autosave'  => true,
    'prefix'    => 'person_'
));

/* Register People Metabox 'Social Networks Accounts' */
$people_workplace = new WPAlchemy_MetaBox(array
(
    'id'        => 'workplace',
    'title'     => __('Workplace'),
    'types'     => array('person'),
    'context'   => 'side',
    'priority'  => 'low',
    'mode' 		=> WPALCHEMY_MODE_EXTRACT,
    'template'  => get_template_directory() . '/post_types/person/metaboxes/workplace.php',
    'autosave'  => true,
    'prefix'    => 'person_'
));

function clear_person_transients()
{
    for ($i=2; $i <=5; $i++) {
        switch_to_blog($i);

        delete_transient('everyone_info_with_bio_en');
        delete_transient('everyone_info_with_bio_zh-hant');
        delete_transient('everyone_info_with_bio_zh-hans');
        delete_transient('everyone_info_en');
        delete_transient('everyone_info_zh-hant');
        delete_transient('everyone_info_zh-hans');
        delete_transient('leaders_info_with_bio_en');
        delete_transient('leaders_info_with_bio_zh-hant');
        delete_transient('leaders_info_with_bio_zh-hans');
        delete_transient('leaders_info_en');
        delete_transient('leaders_info_zh-hant');
        delete_transient('leaders_info_zh-hans');
        delete_transient('listview_en');
        delete_transient('listview_zh-hant');
        delete_transient('listview_zh-hans');
    }

    switch_to_blog(1);
}
add_action( 'wp_ajax_update-menu-order', 'clear_person_transients');
add_action('trash_person', 'clear_person_transients');
add_action('publish_person', 'clear_person_transients');