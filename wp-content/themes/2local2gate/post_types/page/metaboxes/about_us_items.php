<?php
$about_us_Items = get_about_us_items2(null,'list');
while($metabox->have_fields_and_multi('items')) :
	$mb->the_group_open();
	$metabox->the_field('markedaboutus');
?>
<div class="london_leadership">
    <select class="landing-page-entry-post" name="<?php $mb->the_name(); ?>" style="width: 250px;">
    	<option value=""><?php _e(''); ?></option>
        <?php foreach($about_us_Items as $item) : ?>
            <option value="<?php echo $item->ID; ?>" <?php $mb->the_select_state($item->ID); ?>><?php echo $item->post_title ?></option>
        <?php endforeach; ?>
    </select>
    <a href="#" class="dodelete delete-landing-page-entry button"><?php _e('Remove Entry'); ?></a>
</div>
<?php
	$mb->the_group_close();
endwhile;
?>
<div class="clear"></div>
<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-items button"><?php _e('Add Item'); ?></a></p>
