<?php
/**
 * People Utility class
 */

class PeopleHelper
{
    public $seed = null;

    public function __construct()
    {

    }

    public function get_list($list, $page_id, $seed = null, $bio = null)
    {
        $is_everyone = false;

        switch ($list) {
            case 'listview':

                $meta_key = 'filter_people_offices';
                $function_name = 'get_listview';
                break;

            case 'everyone':

                $meta_key = 'filter_people_offices';
                $is_everyone = true;
                $function_name = 'get_everyone_info';
                break;

            case 'leadership':

                $meta_key = 'leadership_allowed_offices_offices';
                $function_name = 'get_leaders_info';
                break;

            default:
                return false;
                break;
        }

        $current_language = (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE) ? ICL_LANGUAGE_CODE : 'en';

        $offices_meta = get_post_meta($page_id, $meta_key, true);


        if(is_array($offices_meta) && !empty($offices_meta))
        {
            $offices_to_be_presented = array_map(create_function('$office', 'return $office["office"];'), $offices_meta);
        }

        $offices_to_be_presented = (isset($offices_to_be_presented)) ? $offices_to_be_presented : null;
        $result = call_user_func_array(array($this, $function_name), array($offices_to_be_presented, $current_language, $bio));

        if($is_everyone && !empty($result))
        {
            $this->seed = ($seed) ? $seed : rand(1, sizeof($result));
            $result = reorder_array_by_seed($result, $this->seed);
        }
        return $result;
    }

    /* Function to get people list filtered by offices */
    private function get_everyone_info($offices, $current_language, $all = false)
    {
        $transient_title = $all ? 'everyone_info_with_bio_' . $current_language: 'everyone_info_' . $current_language;

        $transient = get_transient($transient_title);

        if( $transient )
        {
            return $transient;
        }
        else
        {

            $args = array(
                'numberposts'      => -1,
                'offset'           => 0,
                'post_type'        => 'person',
                'post_status'      => 'publish',
                'suppress_filters' => true,
                'tag'        => 'en'
            );

            if($current_language !== 'en')
            {
                $args = array_merge($args, array('tag' => 'cn'));
            }

            switch_to_blog(1);

            if( !empty($offices) )
            {
                $everyone_list = array();
                foreach($offices as $office)
                {
                    $args = array_merge($args, array(
                        'meta_key'   => 'person_office',
                        'meta_value' => $office,
                    ));

                    $everyone      = get_posts($args);
                    $everyone_list = array_merge($everyone_list, $this->get_people_by_office($everyone, $office, $all, 'person-medium'));
                }
            }
            else
            {
                $everyone      = get_posts($args);
                $everyone_list = $this->get_people_by_office($everyone, null, $all, 'person-medium');
            }

            $everyone_list_flatten = array();

            foreach($everyone_list as $office_group)
            {
                foreach($office_group as $person)
                {
                    $everyone_list_flatten[] = $person;
                }
            }

            restore_current_blog();
            set_transient($transient_title, $everyone_list_flatten);

            return $everyone_list_flatten;
        }
    }

    /* Function to get people leadership list by offices */
    private function get_leaders_info($offices, $current_language, $all = false)
    {
        $transient_title = $all ? 'leaders_info_with_bio_' . $current_language: 'leaders_info_' . $current_language;

        $transient = get_transient($transient_title);

        if($transient)
        {
            return $transient;
        }
        else
        {
            $args = array(
                'numberposts'     => -1,
                'offset'          => 0,
                'post_type'       => 'person',
                'orderby'         => 'menu_order',
                'order'           => 'ASC',
                'tag'             => 'en+leader',
                'post_status'     => 'publish',
                'suppress_filters' => true,
            );

            if($current_language !== 'en')
            {
                $args = array_merge($args, array('tag' => 'cn+leader'));
            }

            switch_to_blog(1);

            if(!empty($offices))
            {
                $leaders_list = array();

                foreach($offices as $office)
                {
                    $args = array_merge($args, array(
                        'meta_key'    => 'person_office',
                        'meta_value'  => $office,
                    ));

                    $leaders      = get_posts($args);
                    $leaders_list = array_merge($leaders_list, $this->get_people_by_office($leaders, $office, $all));
                }
            }
            else
            {
                $leaders      = get_posts($args);
                $leaders_list = $this->get_people_by_office($leaders, null, $all);
            }

            restore_current_blog();
            set_transient($transient_title, $leaders_list);

            return $leaders_list;
        }
    }

    private function get_people_by_office($people, $office = null, $all = false, $thumb_size = 'person-small')
    {
        $people_list = array();

        foreach ($people as $person)
        {
            $office_id          = get_post_meta($person->ID, 'person_office', true);
            $office             = ($office_id) ? get_post($office_id)->post_title : null;
            $first_name         = get_post_meta($person->ID, 'person_first_name', true);
            $last_name          = get_post_meta($person->ID, 'person_last_name', true);
            $position           = get_post_meta($person->ID, 'person_position', true);
            $small_thumbnail    = (has_post_thumbnail($person->ID)) ? get_the_post_thumbnail($person->ID, $thumb_size) : get_placeholder_image($thumb_size);
            $search_thumbnail   = (has_post_thumbnail($person->ID)) ? get_the_post_thumbnail($person->ID, 'search-thumb') : get_placeholder_image($thumb_size);

            if($all)
            {
                $office_street      = get_post_meta($office_id, 'contact_office_street', true);
                $office_floor       = get_post_meta($office_id, 'contact_office_floor', true);
                $office_postcode    = get_post_meta($office_id, 'contact_office_post_code', true);
                $large_thumbnail    = (has_post_thumbnail($person->ID)) ? get_the_post_thumbnail($person->ID, 'person-large') : get_placeholder_image('person-large');
                $phones             = get_post_meta($person->ID, 'person_phones', true);
                $phones             = (is_array($phones)) ? $phones : array();
                $faxes              = get_post_meta($person->ID, 'person_faxes', true);
                $faxes              = (is_array($faxes)) ? $faxes : array();
                $emails             = get_post_meta($person->ID, 'person_emails', true);
                $emails             = (is_array($emails)) ? $emails : array();
                $facebook_account   = get_post_meta($person->ID, 'person_facebook', true);
                $twitter_account    = get_post_meta($person->ID, 'person_twitter', true);
                $linked_in_account  = get_post_meta($person->ID, 'person_linked_in', true);
				 $featured_articles = get_post_meta($person->ID, 'person_featured_articles', true);
            }

            if($office)
            {
                if($all)
                {
                    $people_list[$office][] = array(
                        'id'                => $person->ID,
                        'title'             => $person->post_title,
                        'slug'              => $person->post_name,
                        'content'           => $person->post_content,
                        'first_name'        => $first_name,
                        'last_name'         => $last_name,
                        'position'          => $position,
                        'small_thumbnail'   => $small_thumbnail,
                        'search_thumbnail'  => $search_thumbnail,
                        'street'            => $office_street,
                        'floor'             => $office_floor,
                        'postcode'          => $office_postcode,
                        'phones'            => $phones,
                        'faxes'             => $faxes,
                        'emails'            => $emails,
                        'large_thumbnail'   => $large_thumbnail,
                        'search_thumbnail'  => $search_thumbnail,
                        'facebook_account'  => $facebook_account,
                        'twitter_account'   => $twitter_account,
                        'linked_in_account' => $linked_in_account,
						   'featured_articles' => $featured_articles,
                        'office'            => $office,
                        'office_id'         => $office_id,
                    );
                }
                else
                {
                    $people_list[$office][] = array(
                        'id'                => $person->ID,
                        'slug'        		=> $person->post_name,
                        'title'             => $person->post_title,
                        'first_name'        => $first_name,
                        'last_name'         => $last_name,
                        'position'          => $position,
                        'small_thumbnail'   => $small_thumbnail,
                        'search_thumbnail'  => $search_thumbnail,
                        'office'            => $office,
                    );
                }
            }
        }

        return $people_list;
    }

    /* Function to get listview list by offices */
    private function get_listview($offices, $current_language)
    {
        $transient = get_transient('listview_' . $current_language);

        if($transient)
        {
            return $transient;
        }
        else
        {
            $args = array(
                'numberposts'     => -1,
                'offset'          => 0,
                'post_type'       => 'person',
                'post_status'     => 'publish',
                'tag'             => 'en',
                'suppress_filters' => true
            );

            if($current_language !== 'en')
            {
                $args = array_merge($args, array('tag' => 'cn'));
            }

            switch_to_blog(1);

            if(!empty($offices))
            {
                $args = array_merge($args, array(
                    'meta_key'        => 'person_office',
                    'meta_value'      => $offices,
                ));
            }

            $people = get_posts($args);

            $listview_list = array();

            foreach ($people as $person) {
                $first_name = get_post_meta($person->ID, 'person_first_name', true);
                $last_name = get_post_meta($person->ID, 'person_last_name', true);
                $position = get_post_meta($person->ID, 'person_position', true);

                $listview_list[] = array(
                    'id'         => $person->ID,
                    'slug'       => $person->post_name,
                    'first_name' => $first_name,
                    'last_name'  => $last_name,
                    'position'   => $position,
                );
            }

            restore_current_blog();

            set_transient('listview', $listview_list);
        }

        return $listview_list;
    }
}