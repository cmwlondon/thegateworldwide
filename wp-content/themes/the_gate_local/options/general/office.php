<?php
/**
 * Contact Options in the Settings
 */


/*  Register and define the settings */
add_action('admin_init', 'office_admin_init');
function office_admin_init(){

    $offices = array('office' => array('id' => 'site-office', 'field_name' => 'Office', 'callback' => 'offices_selectbox'),
                    'first_office' => array('id' => 'first-site-office', 'field_name' => 'Additional office on the contact form', 'callback' => 'first_offices_selectbox'),
                    'second_office' => array('id' => 'second-site-office', 'field_name' => 'Additional office on the contact form', 'callback' => 'second_offices_selectbox'),
                    'third_office' => array('id' => 'third-site-office', 'field_name' => 'Additional office on the contact form', 'callback' => 'third_offices_selectbox')
                    );

    foreach($offices as $key => $office)
    {
        register_setting(
            'general',
            $key,
            'validate_office'
        );

        add_settings_field(
            $office['id'],
            $office['field_name'],
            $office['callback'],
            'general',
            'default'
        );

    }
}
/* Callbacks functions */

function offices_selectbox($args)
{
    $office_name = get_option( 'office' );
    $offices_info = get_offices_information();
    ?>
   <select name="office">
       <?php foreach( $offices_info as $office ) : $office_id = $office->ID;?>
            <?php echo '<option value="' . $office_id . '"' . selected($office_id, $office_name) . '>' . $office->post_title . '</option>'; ?>
       <?php endforeach; ?>
   </select>
<?php
}

function first_offices_selectbox($args)
{
    $office_name = get_option( 'first_office' );
    $offices_info = get_offices_information();
    ?>
   <select name="first_office">
       <option value=""><?php _e('Select a office..'); ?></option>
       <?php foreach( $offices_info as $office ) : $office_id = $office->ID;?>
            <?php echo '<option value="' . $office_id . '"' . selected($office_id, $office_name) . '>' . $office->post_title . '</option>'; ?>
       <?php endforeach; ?>
   </select>
<?php
}

function second_offices_selectbox($args)
{
    $office_name = get_option( 'second_office' );
    $offices_info = get_offices_information();
    ?>
   <select name="second_office">
       <option value=""><?php _e('Select a office..'); ?></option>
       <?php foreach( $offices_info as $office ) : $office_id = $office->ID;?>
            <?php echo '<option value="' . $office_id . '"' . selected($office_id, $office_name) . '>' . $office->post_title . '</option>'; ?>
       <?php endforeach; ?>
   </select>
<?php
}

function third_offices_selectbox($args)
{
    $office_name = get_option( 'third_office' );
    $offices_info = get_offices_information();
    ?>
   <select name="third_office">
       <option value=""><?php _e('Select a office..'); ?></option>
       <?php foreach( $offices_info as $office ) : $office_id = $office->ID;?>
            <?php echo '<option value="' . $office_id . '"' . selected($office_id, $office_name) . '>' . $office->post_title . '</option>'; ?>
       <?php endforeach; ?>
   </select>
<?php
}

function validate_office($input)
{
    return $input;
}
