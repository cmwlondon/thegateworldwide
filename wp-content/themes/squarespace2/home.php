<?php
/*
Template Name: home

@package WordPress
@subpackage stackbartlett
*/

$metadata = get_post_meta($post->ID);
$banner = [
	"title" => $metadata['hb_title'][0],
	"link" => $metadata['hb_link'][0],
	"vimeo" => $metadata['hb_vimeo'][0],
	"image" => $metadata['hb_image'][0],
	"width" => $metadata['hb_image_w'][0],
	"height" => $metadata['hb_image_h'][0],
	"image_id" => $metadata['hb_image_id'][0]
];
$culture = [
	"title" => $metadata['hc_title'][0],
	"link" => $metadata['hc_link'][0],
	"image" => $metadata['hc_image'][0],
	"width" => $metadata['hc_image_w'][0],
	"height" => $metadata['hc_image_h'][0],
	"image_id" => $metadata['hc_image_id'][0]
];
$people = [
	"title" => $metadata['hp_title'][0],
	"link" => $metadata['hp_link'][0],
	"image" => $metadata['hp_image'][0],
	"width" => $metadata['hp_image_w'][0],
	"height" => $metadata['hp_image_h'][0],
	"image_id" => $metadata['hp_image_id'][0]
];
$work = [
	"title" => $metadata['hw_title'][0],
	"link" => $metadata['hw_link'][0],
	"image" => $metadata['hw_image'][0],
	"width" => $metadata['hw_image_w'][0],
	"height" => $metadata['hw_image_h'][0],
	"image_id" => $metadata['hw_image_id'][0]
];

?>
<?php get_header();?>
	</div>

	<div id="content" class="site-content">

		<div class="single gatevideo">
			<article class="column">
				<div class="sizer">
					<img alt="" class="loading" data-width="<?php echo $banner['width']; ?>" data-height="<?php echo $banner['height']; ?>" src="<?php echo $banner['image']; ?>">
				</div>
				<div class="iframeSizer loading" data-url="<?php echo $banner['vimeo']; ?>" data-width="1920" data-height="1080">
					
				</div>
				<a href="<?php echo $banner['link']; ?>"></a>
			</article>
		</div>


		<div class="double">
			<article class="column left"><a href="<?php echo $culture['link']; ?>" class="sizer"><img alt="<?php echo $culture['title']; ?>" class="loading" data-width="<?php echo $culture['width']; ?>" data-height="<?php echo $culture['height']; ?>" src="<?php echo $culture['image']; ?>"></a></article>
			<article class="column right"><a href="<?php echo $people['link']; ?>" class="sizer"><img alt="<?php echo $people['title']; ?>" class="loading" data-width="<?php echo $people['width']; ?>" data-height="<?php echo $people['height']; ?>" src="<?php echo $people['image']; ?>"></a></article>
		</div>

		<div class="single">
			<article class="column"><a href="<?php echo $work['link']; ?>" class="sizer"><img alt="<?php echo $work['title']; ?>" class="loading" data-width="<?php echo $work['width']; ?>" data-height="<?php echo $work['height']; ?>" src="<?php echo $work['image']; ?>"></a></article>
		</div>

<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
