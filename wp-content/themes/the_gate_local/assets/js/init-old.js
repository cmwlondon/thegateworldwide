// JavaScript Document
document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/g, '') + 'js';
var peopleSliderTimer;
$.setEqualHeightByClassWithImages = function(listClasses){
    $.each(listClasses, function(index, elementClass) {
        $("img", $('.list li.' + elementClass)).one('load', function() {
            var maxRowItemHeight = $('.list li.' + elementClass).find('.holder').first().height();
            $('.list li.' + elementClass).each(function(index, element) {
                if ( $(element).find('.holder').height() > maxRowItemHeight ) {
                    maxRowItemHeight = $(element).find('.holder').height();
                }
            });
            $('.list li.' + elementClass).find('.holder').css("height", maxRowItemHeight + "px");
        }).each(function() {
            if(this.complete) $(this).load();
        });
    });
};

$(document).ready(function() {
    initCarousel();
    initOpenClose();
    initPopups();
    initAnchorNav();
    initAnchorMenu();

    // Loading Optimizations
	$('.work-desc-wrap, .about-us #main, #main.office-home').removeAttr('style');

    var correctDocHeight = 0;
    // tabs
    $(".tab-titles a").click(function (e) {
        var target = $(this).attr("class");
        $(this).parent().parent().children().removeClass("current");
        $(this).parent().addClass("current");
        $(this).parent().parent().next().children().hide();
        $(this).parent().parent().next().find("." + target).show();
        return false;
    });
    // contact placeholders
    if ($(".contact").length > 0) {
        var defaultValue = '';
        $('.contact input, .contact textarea').focus(function() {
            var newValue = $(this).val();
            var defValue = $(this).data("defaultvalue");
            if (newValue == defValue) {
                $(this).val("");
            }
        });
        $('.contact input, .contact textarea').blur(function() {
            if ($(this).val() === '') {
                var defValue = $(this).data("defaultvalue");
                $(this).val(defValue);
            }
        });
    }


    var setupSwipe = function(slider) {
        var time = 1000,
        // allow movement if < 1000 ms (1 sec)
        range = 50,
        // swipe movement of 50 pixels triggers the slider
        x = 0,
        t = 0,
        touch = "ontouchend" in document,
        st = (touch) ? 'touchstart' : 'mousedown',
        mv = (touch) ? 'touchmove' : 'mousemove',
        en = (touch) ? 'touchend' : 'mouseup';

        slider.$window
        .bind(st, function(e) {
            // prevent image drag (Firefox)
            // e.preventDefault();
            t = (new Date()).getTime();
            x = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
        })
        .bind(en, function(e) {
            t = 0;
            x = 0;
        })
        .bind(mv, function(e) {
            // e.preventDefault();
            var newx = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX,
            r = (x === 0) ? 0 : Math.abs(newx - x),
            // allow if movement < 1 sec
            ct = (new Date()).getTime();
            if (t !== 0 && ct - t < time && r > range) {
                if (newx < x) {
                    slider.goForward();
                }
                if (newx > x) {
                    slider.goBack();
                }
                t = 0;
                x = 0;
            }
        });
    };

    var descriptionAnimating = false;
    if ($(".featured-news-slider").length) {
        $(".featured-news-slider").anythingSlider({
            autoPlay: false,
            delay: 4000,
            buildStartStop: false,
            hashTags: false,
            expand: true,
            appendForwardTo: $(".featured-prev-next .next a"),
            appendBackTo: $(".featured-prev-next .prev a"),
            appendNavigationTo: $(".featured-controls"),
            clickControls: "click",
            onInitialized: function(e, slider) {
                setupSwipe(slider);
            },
            onSlideBegin : function(e, slider){
                if (!descriptionAnimating) {
                    descriptionAnimating = true;

                    if (slider.targetPage == slider.currentPage) {

                    } else {
                        $(".featured-news-side .featured-info-inner").fadeOut(200);
                        $(".featured-news-side .featured-info-inner[data-id="+(slider.targetPage)+"]").delay(200).fadeIn(400, function() {
                            descriptionAnimating = false;
                        });
                    }

                }

            },
            onSlideComplete : function(e, slider) {
                descriptionAnimating = false;
            }
        });
    }

    if ($(".work-category-slider").length) {
        $(".work-category-slider").anythingSlider({
            autoPlay: true,
            delay: 8000,
            expand: true,
            hashTags: false,
            buildStartStop: false,
            buildNavigation: true,
            pauseOnHover : false,
            appendNavigationTo: '.slidecontrols',
            appendForwardTo: $(".featured-prev-next .next a"),
            appendBackTo: $(".featured-prev-next .prev a"),
            onInitialized: function(slider){
                $('.featured-prev-next').children().css('display','');
            }
        });
    }

    // Spacings to compensate for aspect ratio changes, etc.
    setTopPaddingToContactPage();

    var worknyCarouselHeight = $(".workny-slider img").height();
    if (worknyCarouselHeight < $(".workny-slider-wrapper").height()) {
        $(".workny-slider-wrapper").css("height", worknyCarouselHeight);
    }


    $(window).resize(function() {
        // recalculate the spacing according to .visual height
        setTopPaddingToContactPage();

        var worknyCarouselHeight = $(".workny-slider img").height();
        if (worknyCarouselHeight < $(".workny-slider-wrapper").height()) {
            $(".workny-slider-wrapper").css("height", worknyCarouselHeight);
        }

        if($(window).width() > 767) {
            $('.simple-list>li').css('display', 'block');

        } else {
            if ($('.tab-titles').length > 0) {
                var target = $('.tab-titles li.current').find('a').attr('class');
                $('.simple-list>li').css('display', 'none');
                $('.simple-list>li.' + target).css('display', 'list-item');
            }
            if($('#the-gate-map').length > 0) {
                $('.simple-list li.worldwide').css('display', 'none');
            }
            if($('.people-ind').length > 0) {
                $('.simple-list>li#everyone, .simple-list>li#leadership').css('display', 'none');
            }
            if ($('.home').length > 0) {
                $(".slide-area").addClass('active');
                $(".slide-area .slide").removeClass("js-slide-hidden");
                $(".slide-area .slide").css("display", "block");
            }
        }
    });

    if ($("#myTable").length) {
        $("#myTable").tablesorter();
    }

    // dropdown menus
    $("#sort-client, #sort-media, #sort-leadership-people, #sort-everyone-people, #show-category1 .dropdown-title-wrap, #show-category2 .dropdown-title-wrap, #sort-news").click(function(e) {
        e.preventDefault();

        if ($(this).find(".dropdown-title").hasClass("shown")) {
            $(this).find(".dropdown-title").removeClass("shown");
            $(this).parent().find(".dropdown-submenu").slideToggle(200);
        }
        else {
            $(this).find(".dropdown-title").addClass('shown');
            $(this).parent().find(".dropdown-submenu").slideToggle(200);
        }
    });

    //Twitter auto scrolling
    if ($("#scroller").length) {
        $("#scroller").simplyScroll();
    }

    $('.people > a').click(function(event){

        var refIndex = lookupSlideIndex($(this));
	
        if(refIndex === -1)
            return;	

        $('.people-ind-slider').data('AnythingSlider').gotoPage(refIndex, false, {}, 0);
        $('.menu li.active').removeClass('active');
        
        var heightToRemember = 0;
        $('.people-ind-slider li:nth-child('+(refIndex+1)+')').find('.main, .additional').each(function(index){
            var childHeight = $(this).height();
            if(childHeight > heightToRemember)
                heightToRemember = childHeight; 
        });
        
        var referenceElement = $('li#individual .main-holder');
        heightToRemember += parseInt(referenceElement.css('padding-top')) + parseInt(referenceElement.css('padding-bottom'));
        
        $('li#individual').css({
            height: 0,
            position: '',
            left: ''
        }).delay(500).animate({height: heightToRemember}, {duration: 1000, complete: function(){
            $('li#individual').css('height',''); 
            
            // Calls to Fix the Height (If updated - reference ind-people-slider
            $('.wrapper-holder, .pt-page-current').height($('header').height() + $('#main').offset().top + $('.pt-page').children().height());
            ntop = ($(document).height() - $(window).height());
        }});
        
        $('.menu li:first-child').addClass('active').delay(500).slideDown(1000);
        $('html, body').animate({scrollTop: 0}, 500);
        

        disableLinks();
        event.preventDefault();
    });

    //People carousel
    if ($(".people-slider").length) {
        $(".people-slider").anythingSlider({
            autoPlay: false,
            delay: 8000,
            expand: true,
            infiniteSlides : false,
            buildStartStop: false,
            buildNavigation     : false,
            hashTags : false,
            appendForwardTo: $("#everyone .featured-prev-next .next a"),
            appendBackTo: $("#everyone .featured-prev-next .prev a")
        })
    }

    var ntop = 0;
    setTimeout(function () {
        ntop = ($(document).height() - $(window).height());
    }, 600);

    startAt = (typeof peopleSliderStartingPanel != 'undefined' && peopleSliderStartingPanel.hasOwnProperty('id')) ? Number(peopleSliderStartingPanel.id) : 1;
    // People individual carousel
    if ($(".people-ind-slider").length) {

        //Start People Carousel
        $(".people-ind-slider").anythingSlider({
            startPanel: startAt,
            autoPlay: false,
            expand: true,
            buildStartStop: false,
            buildNavigation: false,
            hashTags: false,
            appendForwardTo: $("#individual .featured-prev-next .next a"),
            appendBackTo: $("#individual .featured-prev-next .prev a"),
            onInitialized: function(e, slider) {
                $(".anythingSlider").first().css({
                    position: 'absolute',
                    left: -815*$('.people-ind-slider').length
                });
            },
            onSlideComplete: function(e, slider) {
            
                $('.people-ind-slider').data('AnythingSlider').options.enableArrows = true;
            
                var mainBoxHeight = $('ul.people-ind-slider li.activePage .main').height();
                var additionalBoxHeight = $('ul.people-ind-slider li.activePage .additional').height();
                var mobileInfoBoxHeight = additionalBoxHeight + mainBoxHeight + 60;
				var title = $('ul.people-ind-slider li.activePage .people-individual-page').data('page-title');
                document.title = title;

                if($(window).width() > 767) {
                    
                    $('.people-ind-slider-wrap').animate({
                        height: additionalBoxHeight > mainBoxHeight ? additionalBoxHeight : mainBoxHeight
                    }, 400, "swing", function(){
                        $('.wrapper-holder, .pt-page-current').height($('header').height() + $('#main').offset().top + $('.pt-page').children().height());
                        ntop = ($(document).height() - $(window).height());
                    });

                } else {
                    $('.people-ind-slider-wrap').css("height", mobileInfoBoxHeight);
                    $('.pt-page').height($('header').height() + $('.people-ind-slider-wrap').height());
                    $('.wrapper-holder').animate({height: $('header').height() + $('.people-ind-slider-wrap').height()});
                }
            }
        });

        //Initial height of People carousel
        //Update People Carousel Height on resize
        $(window).on('load resize', function() {
            setTimeout(function(){
                $('.people-ind-slider-wrap, ul.people-ind-slider li.activePage .main, ul.people-ind-slider li.activePage .additional').css('height', 'auto');
                var mainBoxHeight = $('ul.people-ind-slider li.activePage .main').height();
                var additionalBoxHeight = $('ul.people-ind-slider li.activePage .additional').height();
                var mobileInfoBoxHeight = additionalBoxHeight + mainBoxHeight + 60;
                var indSilent = $('li#individual').css('position') != 'absolute';

                if($(window).width() > 767) {
                    if (additionalBoxHeight > mainBoxHeight ) {
                        $('.people-ind-slider-wrap').css('height', additionalBoxHeight);
                    } else {
                        $('.people-ind-slider-wrap').css('height', mainBoxHeight);
                    }

                    if ($("#pt-main").length > 0) {

                        $('.pt-page').height($('.pt-page-current').children().height() + $('.header-area').height() + $('#footer').height());
                        $('.wrapper-holder').height($('header').height() + $('#main').offset().top + $('.pt-page-current').children().height());

                        $('#returnPage1, #returnPage2, #returnPage3').on('click', function(){
                            $('.pt-page').height($('.pt-page-1').children().height() + $('.header-area').height() + $('#footer').height());
                            $('.wrapper-holder').height($('header').height() + $('#main').offset().top + $('.pt-page-1').children().height());
                        });

                        $('#iterateEffects').on('click', function(){
                            $('.pt-page').height($('.pt-page-2').children().height() + $('.header-area').height() + $('#footer').height());
                            $('.wrapper-holder').height($('header').height() + $('#main').offset().top + $('.pt-page-2').children().height());
                        });
                    }

                    ntop = ($(document).height() - $(window).height());
					
                    if(indSilent){
                        $('.menu li').removeClass('active');
                        $('.menu li a#returnPage3').parent().addClass('active');
                    }
                } else {
                    $('.people-ind-slider-wrap').css('height', mobileInfoBoxHeight);
                    $('.pt-page').height($('header').height() + $('.people-ind-slider-wrap').height());
                    $('.wrapper-holder').height($('header').height() + $('.people-ind-slider-wrap').height());
                }
				
				$(".anythingSlider").first().css({ position: '', left: '' });
                $('.people-ind-slider-wrap > .people-individual-page').remove();
                $('.people-ind-slider').css({ 'visibility': '', 'overflow': ''});
                if(indSilent) disableLinks();

            }, 500);
            
            setTimeout(function(){$('.featured-prev-next').first().find('.next, .prev').fadeIn(1000)}, 500);
            
        });
    }

    // Add a slide to carousel
    $('.add-new-slide').click(function(){
        $('.people-ind-slider').append('<li>I am brand new slide</li>').anythingSlider();
    });

    //Height to people page
    $(window).on('load resize', function() {

        //Set height to list view and other view on people page
        if ( $(".people").length > 0) {
            if($(window).width() > 767) {
                $('.pt-page').height($('.pt-page-current').children().height() + $('.header-area').height() + $('#footer').height());
                $('.wrapper-holder').height($('header').height() + $('#main').offset().top + $('.pt-page-current').children().height());
               
                $('#returnPage1, #returnPage2, #returnPage3').on('click', function(){
                    $('.pt-page').height($('.pt-page-1').children().height() + $('.header-area').height() + $('#footer').height());
                    $('.wrapper-holder').height($('header').height() + $('#main').offset().top + $('.pt-page-1').children().height());
                });

                $('#iterateEffects').on('click', function(){
                    $('.pt-page').height($('.pt-page-2').children().height() + $('.header-area').height() + $('#footer').height());
                    $('.wrapper-holder').height($('header').height() + $('#main').offset().top + $('.pt-page-2').children().height());
                });
            }
            setTimeout(function(){
                $('.people .wrapper-holder').height($('header').height() + $('#main').offset().top + $('.pt-page').children().height());
                ntop = ($(document).height() - $(window).height());
            }, 400);

	    $('#footer').css('display','block');

        }
        /* Equal height for leaders in leadership box*/
        if ($(".by-office-place").length > 0) {
            $('.by-office-place').equalHeights();
        }
        //$('.by-office-place li').css('padding-bottom','20px');

        //Equal height for leadership box
        setTimeout(function(){
            if ($("#leadership").length > 0) {
                $('#leadership .main-holder').equalHeights();
            }
        }, 300);
        if ($(".people").length || $(".people-ind").length ) {
            var leadershipMinHeight = $('.leadership ul.by-office-place li').css('min-height');
            $('.leadership ul.by-office-place li').height(leadershipMinHeight);
        }
    });

    //Equal height of slides in carousel on ww page
    if ($(".home").length > 0) {
        $('.slideset .slide').equalHeights();
    }

    // Fancy Box
    var notWrappedImgTags = $(".news-article img, .indport-piece img").filter(function(){
        return ($(this).parents('a').length === 0  && !($(this).hasClass('do-not-open-in-modal')));
    });

    /* Wrap this tags in a with appropriate attributes to be opened in fancybox */
    notWrappedImgTags.wrap(function() {
        return '<a href="' + removeSizeFromImgSrc($(this).attr('src'), $(this).attr('width'), $(this).attr('height')) + '"class="fancybox-thumb" data-fancybox-group="work-fancybox" />';
    });

    function removeSizeFromImgSrc(imgUrl, width, height)
    {
        if(width && height) {
            regex = new RegExp('/\-' + Number(width) + 'x' + Number(height) + '\./')
            return imgUrl.replace(regex ,'.');
        }
        else {
            return imgUrl.replace(/\-\d+x\d+\./ ,'.');
        }
    }

    //Display appropriate image on rollover
    $('.home #nav ul li a').on('mouseover', function(){
        var navItemClass = $(this).attr('class');
        var activeSrc = $('.visual img.active').attr("src");

        if( $('.btn-search').parent().hasClass('search-open') ){
            $("#nav .direction").removeClass("direction-visible");
        }
        else {
            $(".visual-holder").css("background-image", "url(" + activeSrc + ")");

            $('.visual img').removeClass('active').css("opacity", "0");
            $('.visual img.' + navItemClass).addClass('active').animate({
                "opacity": 1
            }, 500);

            $("#nav .direction").removeClass("direction-visible");
            $(this).next().addClass("direction-visible");
        }
    });


    //Google maps
    if ($("#the-gate-map").length > 0) {
        if(typeof(googleMapsCoordinates) != 'undefined') {
            initializeMap(googleMapsCoordinates.markers, googleMapsCoordinates.center);
        } else {
            initializeMap();
        }
    }
    //Contacts fake tabs
    $('.fake-tab-titles li a.worldwide').on('click',function(){
        $("#footer .slide-area").addClass("active");
        $("#footer .slide").slideDown(400, function() {
            $.scrollTo('#footer');
            footerHidden = false;
            footerAnimating = false;
        });
        return false;
    });

    var footerHidden = true;
    var footerAnimating = false;
    ntop = ($(document).height() - $(window).height());

    if($(window).width() > 767) {
    } else {
        // show the footer on the mobile homepage
        if ($('.home').length > 0) {
            $("#footer .slide-area").addClass("active");
            $("#footer .slide").slideDown(400, function() {
                footerHidden = false;
                footerAnimating = false;
            });
        }
    }

    $("#footer .opener").click(function (e) {
        if (footerHidden) {
            ntop = ($(document).height() - $(window).height());
        } else {
            ntop = ($(document).height() - $(window).height() - $("#footer .slide").height() - 20);
        }

        if (!footerAnimating) {
            footerAnimating = true;
            if (footerHidden) {
                $(this).addClass("fixed");
                $("#footer .slide-area").addClass("active");
                $("html, body").animate({ scrollTop: $(document).height() }, 400);
                $("#footer .slide").slideDown(400, function() {
                    footerHidden = false;
                    footerAnimating = false;
                    $("#footer .opener").removeClass("fixed");
                });
            } else {
                $(this).addClass("fixed");
                $("html, body").animate({ scrollTop: ntop }, 400);
                $("#footer .slide").slideUp(400, function() {
                    footerHidden = true;
                    footerAnimating = false;
                    $("#footer .opener").removeClass("fixed");
                    $("#footer .slide-area").removeClass("active");
                    ntop = ($(document).height() - $(window).height());
                })
            }
            return false;
        } else {
            return false;
        }
    });

    $(".logo-area").hover(function() {
        $(".sub-menu").stop().fadeToggle(250);
    });

    // Dropdown menu content "saver/changer" of the filter
    $(".dropdown-submenu li").click(function(){
        var selectedOption = $(this).html();
        $(this).parent().prev().find('.dropdown-title').html(selectedOption).find("a").addClass("filtered");
    });

    // to be removed - fading scroll hint button
    setTimeout(function (){
        $(".scroll-hint img").fadeOut();
    }, 3000);
    setTimeout(function (){
        $(".scroll-hint").fadeOut();
    }, 6000);

    // scroll hint button click function
    $('div.scroll-hint').click(function(){
        var scr = $("#main").offset().top;
        $("body, html").animate({
            scrollTop: scr
        }, 600);
        return false;
    });

    //People sorting
    $('#sort-people li a').on('click', function(e) {
        e.preventDefault();

        var sortBy = $(this).data('sort-by');
        var initialPeopleList = $(this).parents('.dropdown-menu').first().siblings('.people-names-list');
        var peopleListItems = $('li', initialPeopleList);

        peopleListItems.sort(function(a, b){
             return $("[data-sort-handle='" + sortBy +"']", a).html().toLowerCase() > $("[data-sort-handle='" + sortBy +"']", b).html().toLowerCase() ? 1 : -1;
        });

        initialPeopleList.html(peopleListItems);
    });

    setTimeout(function (){
        ntop = ($(document).height() - $(window).height());
    }, 500);

    //Make z-index active for iframes
    $('iframe').each(function(){
        var url = $(this).attr("src");
        var char = "?";
        if(url.indexOf("?") != -1){
            var char = "&";
        }
        $(this).attr("src",url+char+"wmode=transparent");
    });

    //Modal for work campaign page
    if ( $(".individual-campaign").length > 0) {
        $(".work-fancybox").fancybox({
            'prevEffect'		: 'fade',
            'nextEffect'		: 'fade',
            'transitionIn'		: 'elastic',
            'transitionOut'		: 'elastic',
            'speedIn'			: 600,
            'speedOut'			: 200,
            'width'             : 693,
            'height'            : 470,
            'titlePosition'		: 'outside',
            'title'				: formatTitle,
            'padding'			: 0
        });
    }

    if ( $(".fancybox-thumb").length > 0) {
        $(".fancybox-thumb").fancybox({
            'prevEffect'		: 'fade',
            'nextEffect'		: 'fade',
            'transitionIn'		: 'elastic',
            'transitionOut'		: 'elastic',
            'speedIn'			: 600,
            'speedOut'			: 200,
            'width'             : 693,
            'height'            : 470,
            'padding'			: 0
        });
    }

    //Triger Modal on click on carousel slide
    $('.work-fancybox-fake').on('click', function(){
        if($(window).width() > 767) {
            $(".campaign-big .work-fancybox").trigger('click');
        }
    });

    $(".work-fancybox").on('click', function(){
        if($(window).width() <= 767) {
            if($(this).data('mobile-href')) {
                window.location = $(this).data('mobile-href');
            }
            return false;
        }
    });

    // Show whole dropdown when content is empty
    if ($('.work').length > 0 || $('.news-landing').length > 0) {
        //Desktop
        $('.dropdown-title-wrap').on('click', function(){
            var thisDropDownMenu = $(this).parent().parent('.dropdown-menu');

            if($(window).width() > 767) {
                if ( $(thisDropDownMenu).find('.dropdown-submenu').css('display') == 'none') {
                    if ($(thisDropDownMenu).find('ul.dropdown-submenu').height() > $(thisDropDownMenu).next('ul.list').height() ) {
                        $(thisDropDownMenu).next('ul.list').height($(thisDropDownMenu).find('ul.dropdown-submenu').outerHeight());
                    }

                    $('.dropdown-submenu').on('click', function(){
                        $(thisDropDownMenu).next('.list').height('auto');
                    });
                }
                else {
                    $(thisDropDownMenu).next('.list').height('auto');
                }
            }
        });

        //Mobile
        $('.dropdown-title').on('click', function(){
            var thisDropDownMenu = $(this).parent().parent().parent('.dropdown-menu');

            if($(window).width() <= 767) {
                if ( $(thisDropDownMenu).find('.dropdown-submenu').css('display') == 'none') {
                    if ($(thisDropDownMenu).find('ul.dropdown-submenu').height() > $(thisDropDownMenu).next('ul.list').height() ) {
                        $(thisDropDownMenu).next('ul.list').height($(thisDropDownMenu).find('ul.dropdown-submenu').outerHeight() );
                    }

                    $('.dropdown-submenu').on('click', function(){
                        $(thisDropDownMenu).next('.list').height('auto');
                    });
                }
                else {
                    $(thisDropDownMenu).next('.list').height('auto');
                }
            }
        });
    }
    //Modal window for about page
    $(window).on('load resize', function() {
        if ($('.about-us').length > 0) {
            if($(window).width() > 767) {

				$('.fancybox-overlay').css('display', 'none');
                $('.modal-window').css('display', 'none');
                $('.roll li a').addClass('fancybox');

                $(".fancybox").fancybox({
                    'prevEffect'		: 'fade',
                    'nextEffect'		: 'fade',
                    'transitionIn'		: 'elastic',
                    'transitionOut'		: 'elastic',
                    'speedIn'			: 600,
                    'speedOut'			: 200,
                    'fitToView'			: true
                });

            } else {

				$('.fancybox-overlay').css('display', 'none');
                $('.roll li a').removeClass('fancybox');

                $('.roll li a').on('click', function(){
                    if($(window).width() < 767) {
                        $('.modal-window').not($(this).next('.modal-window')).slideUp();
                        $(this).next('.modal-window').slideDown();

                        $('.roll li a').not(this).css('display', 'block');
                        $(this).css('display', 'none');
                    }
                });
                $('.modal-window h2, .top-close, .bottom-close').on('click', function(){
                    if($(window).width() < 767) {
                        $(this).parent('.modal-window').slideUp();
                        $(this).parent().parent().find('a').css('display', 'block');
                    }
                });
            }
        }
    });

    //Set height to portfolio images for mobile
    $(window).on('load resize', function() {
        if ($('.indport-piece').length > 0) {
            if($(window).width() < 767) {

                if ($('.indport-piece .work-slider-wrapper').find('a.single-image, a.flash-thumb').length) {
                    var bgImage = $('.indport-piece .work-slider-wrapper a');
                    var bgImageHeight = getBgHeight(bgImage);
					var bgImageWidth = getBgWidth(bgImage);
					
					if (bgImageWidth > bgImageHeight) {
						$('.indport-piece .work-slider-wrapper').css('height', $('body').width()*(bgImageHeight / bgImageWidth)); 	
					} else {
						if (bgImageHeight > 400) {
							$('.indport-piece .work-slider-wrapper').css('height', 400);	
						} else {
                   			$('.indport-piece .work-slider-wrapper').css('height', bgImageHeight);
						}
					}
                } else {
                    $('.indport-piece .work-slider-wrapper').css('height', 215);
                }
            } else {
                $('.indport-piece .work-slider-wrapper').css('height', 519);	
            }
        }
    });

    //Contact radio buttons
     if ($('.contact-wrap').length) {
        if(typeof(siteDetails) != 'undefined' && siteDetails.hasOwnProperty('url')) {
            var radio_img = 'url("' + siteDetails.url + '/assets/images/radio.png")';
        } else {
            var radio_img = 'url("images/radio.png")'
        }
        $('input:radio').screwDefaultButtons({
            image: radio_img,
            width: 21,
            height: 20
        });
        $('input#office1:radio').screwDefaultButtons('check');
    }
    //Show appropriate office (for Asia only) on click on radio button
    $('.styledRadio, .office-label').on('click', function(){
        var office = $(this).parent().attr('class');
        $('.form-right .offices').css('display', 'none');
        $('.form-right .' + office).css('display', 'block');
    });

    $('.video-thumb').on('click', function(){
        $('.video-holder').removeClass('hidden');	
    });
	
	// Do not play flash immidiately when it is not supported from device

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('.flash-holder').css('display', 'none');
		$('.flash-thumb').css('display', 'block');
    }

    //Hide video when it is stopped

    // Listen for the ready event for any vimeo video players on the page
    /*var vimeoPlayers = document.querySelectorAll('iframe'),
      player;

      for (var i = 0, length = vimeoPlayers.length; i < length; i++) {
      player = vimeoPlayers[i];
      $f(player).addEvent('ready', ready);
      }

      function addEvent(element, eventName, callback) {
      if (element.addEventListener) {
      element.addEventListener(eventName, callback, false);
      }
      else {
      element.attachEvent(eventName, callback, false);
      }
      }

      function ready(player_id) {
    // Keep a reference to Froogaloop for this player
    var froogaloop = $f(player_id);

    function onFinish() {
    froogaloop.addEvent('finish', function(data) {
    alert(1)
    });
    }

    onFinish();

    }*/
});

$(window).load(function(){
    //Set equal height for every row in archive/feature modules
    var listClasses = [];
    $('.list li').each(function(index, element) {
        var elementClass = $(element).attr('class');
        listClasses.push(elementClass);
    });
    listClasses = $.unique(listClasses);
    $.setEqualHeightByClassWithImages(listClasses);
});

//Get height of background image
function getBgHeight (a) {
    var height = 0;
    var path = $(a).css('background-image').replace('url', '').replace('(', '').replace(')', '').replace('"', '').replace('"', '');
    var tempImg = '<img id="tempImg" src="' + path + '"/>';
    $('body').append(tempImg); // add to DOM before </body>
    $('#tempImg').hide(); //hide image
    height = $('#tempImg').height(); //get height
    $('#tempImg').remove(); //remove from DOM
    return height;
};
function getBgWidth (a) {
    var width = 0;
    var path = $(a).css('background-image').replace('url', '').replace('(', '').replace(')', '').replace('"', '').replace('"', '');
    var tempImg = '<img id="tempImg" src="' + path + '"/>';
    $('body').append(tempImg); // add to DOM before </body>
    $('#tempImg').hide(); //hide image
    width = $('#tempImg').width(); //get width
    $('#tempImg').remove(); //remove from DOM
    return width;
};

function formatTitle() {
    var workTitle = $(this).parent().parent().find('.modal-title').html();
    var workDesc = $(this).parent().parent().find('.modal-description').html();

    var workTitleAndDesc = '';

    if(workTitle.trim() || workDesc.trim())
        {
            workTitleAndDesc =  '<div class=\"work-title\">' + workTitle + '</div><div class=\"work-desc\">' + workDesc + '</div>';
        }

        return workTitleAndDesc
}

function initializeMap(markers, center) {
    if(!markers || typeof(markers) == 'undefined') {
        var markers = [
            ['<div class="city">New York</div>','<div class="local-office"><span class="local-office-inner"><span class="centered"><h4>New York</h4><p>14th Floor</p><p>11 East 26th Street,</p><p>New York, NY 10010</p><em>212 508 3400</em></span></span></div>', 40.743213,-73.987386],
            ['<div class="city">London</div>','<div class="local-office"><span class="local-office-inner"><span class="centered"><h4>London</h4><p>Devon House</p><p>58 St Katharine’s Way</p><p>London E1W 1LB</p><em>+44(0)20 7423 4500</em></span></span></div>', 51.506212,-0.071125],
            ['<div class="city">Edinburgh</div>','<div class="local-office"><span class="local-office-inner"><span class="centered"><h4>Edinburgh</h4><p>100 Ocean Drive</p><p>Edinburgh EH6 6JJ</p><em>+44 131 555 0425</em></span></span></div>', 55.980059,-3.179668],
            ['<div class="city">Hong Kong</div>','<div class="local-office big"><span class="local-office-inner"><span class="centered"><h4>Hong Kong</h4><p>13th Floor<p>Chinachem Hollywood Cntr</p><p>1 Hollywood Road</p><p>Central, Hong Kong</p><em>+852 2827 2411</em></span></span></div>', 22.281996,114.154444],
            ['<div class="city">Isle of Man</div>','<div class="local-office"><span class="local-office-inner"><span class="centered"><h4>Isle of Man</h4><p>Devon House</p><p>58 St Katharine’s Way</p><p>London E1W 1LB</p><em>+44(0)20 7423 4500</em></span></span></div>', 54.1473,4.26888],
            ['<div class="city">Guernsey</div>','<div class="local-office big"><span class="local-office-inner"><span class="centered"><h4>Guernsey</h4><p>Betley Whitehorne</p><p>55 Le Bordage</p><p>St Peter Port</p><p>Guernsey GY1 1BP</p><p>Channel Islands</p><em>+44 (0)1481 725115</em></span></span></div>', 49.454168,-2.549707],
            ['<div class="city">Shanghai</div>','<div class="local-office big"><span class="local-office-inner"><span class="centered"><h4>Shanghai</h4><p>Suite 1307</p><p>BEA Finance Tower</p><p>66 Hua Yuan Shi Qiao Rd</p><p>Pu Dong District</p><p>Shanghai 200120 P.R.C.</p><em>+8621 3383 0138</em></span></span></div>', 31.234287,121.501714],
            ['<div class="city">Singapore</div>','<div class="local-office"><span class="local-office-inner"><span class="centered"><h4>Singapore</h4><p>10 Raeburn Park #01-10</p><p>Singapore 088702</p><em>+65 6513 0520</em></span></span></div>', 1.273548,103.833406]
        ];
    }

    if(!center || typeof(center) == 'undefined') {
        var center = [27.279969, 5.949872];
    }

    var mapOptions = {
        center: new google.maps.LatLng(center[0], center[1]),
        zoom: 2,
        scrollwheel: false,
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var boxOptions = {
        pixelOffset: new google.maps.Size(-106, 20)
    };

    var boxOptions2 = {
        pixelOffset: new google.maps.Size(-61, -80),
        boxStyle: {
            background: "url('images/local-pin-wide.png') no-repeat",
            width: "113px",
            height: "63px"
        }
    };

    if(typeof(siteDetails) != 'undefined' && siteDetails.hasOwnProperty('url'))
        {
            boxOptions2.boxStyle.background = "url('" + siteDetails.url + "/assets/images/local-pin-wide.png') no-repeat";
        }

        var styles = [
            { "featureType": "landscape", "elementType": "geometry", "stylers": [ { "visibility": "on" }, { "color": "#ffffff" } ] },
            { "featureType": "administrative", "elementType": "geometry", "stylers": [ { "visibility": "off" } ] },
            { "featureType": "administrative", "elementType": "labe,ls", "stylers": [ { "visibility": "off" } ] },
            { "featureType": "water", "elementType": "labels", "stylers": [ { "visibility": "off" } ] },
            { "featureType": "water", "elementType": "geometry", "stylers": [ { "visibility": "on" }, { "color": "#c2d4d8" } ] }
        ]

        var map = new google.maps.Map(document.getElementById("the-gate-map"), mapOptions);
        map.setOptions({styles: styles});

        var bigInfowindow = new InfoBox(boxOptions), smallInfowindow = new InfoBox(boxOptions2), marker, i;
        for (i = 0; i < markers.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(markers[i][2], markers[i][3]),
                map: map,
                icon: "images/pin.png"
            });

            if(typeof(siteDetails) != 'undefined' && siteDetails.hasOwnProperty('url'))
                {
                    marker.icon = siteDetails.url + "/assets/images/pin.png";
                }


                if (i == 0) {
                    smallInfowindow.setContent(markers[i][0]);
                    bigInfowindow.setContent(markers[i][1]);
                    smallInfowindow.open(map, marker);
                    bigInfowindow.open(map, marker);
                }

                google.maps.event.addListener(marker, 'mousedown', (function(marker, i) {
                    return function() {
                        smallInfowindow.setContent(markers[i][0]);
                        bigInfowindow.setContent(markers[i][1]);
                        smallInfowindow.open(map, marker);
                        bigInfowindow.open(map, marker);
                    }
                })(marker, i));

                google.maps.event.addListener(map, 'mousedown', (function(marker, i) {
                    return function() {
                        smallInfowindow.close(map, marker);
                        bigInfowindow.close(map, marker);
                    }
                })(marker, i));

        }
        google.maps.event.trigger(map, 'resize');
}
jQuery.fn.sort = function() {
    return this.pushStack( [].sort.apply( this, arguments ), []);
};


function setTopPaddingToContactPage (){
    $(".contact-wrap .visual img").one('load', function() {
        var contactVisualSpacing = $(".contact-wrap .visual").height();
        $(".simple-list .local").css("paddingTop", contactVisualSpacing);

    }).each(function() {
        if(this.complete) $(this).load();
    });
}

function disableLinks(){
    $('.people > a').unbind('click');
	$('.people > a').click(function(event){

		var clickedIndex = lookupSlideIndex($(this));		

		if(clickedIndex === -1)
			return;

		$('html, body').animate({ scrollTop: 0 });
      $('.people-ind-slider').data('AnythingSlider').gotoPage(clickedIndex, false, {}, 0);

		event.preventDefault();
	});
}

function lookupSlideIndex(reference)
{
	var retVal;

	if(reference.closest('.by-office-place').length != 0){
		var identifier = reference.attr('href');
 		var q = identifier.indexOf('?')+6;
		identifier = identifier.substr(q, identifier.indexOf('&') - q);

		retVal = $('.peoples a.photo[href*="'+identifier+'"]').closest('li');
	}
	else{
		retVal = reference.closest('li');
	}

	if(retVal.length == 0)
		return -1;

	return retVal.closest('.panel').index()*15 + retVal.index()+1;
}
