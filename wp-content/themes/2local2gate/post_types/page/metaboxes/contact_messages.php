<div class="my_meta_control">
    <div class="misc-pub-section">
        <label><?php _e('Success Message'); ?></label>
        <p>
            <?php $mb->the_field('success'); ?>
            <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
            <span><?php _e('Enter in a success message'); ?></span>
        </p>
    </div>

    <div class="misc-pub-section">
        <label><?php _e('Mail Sending Failure Message'); ?></label>

        <p>
            <?php $mb->the_field('fail'); ?>
            <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
            <span><?php _e('Enter in a message to be shown when sending the mail has failed'); ?></span>
        </p>
    </div>

</div>