<?php
/*
office map image select
*/
?>

<div class="customField longtext imageField">
<label for="personImageLarge" class="mediaTrigger_large"><?php echo _e( 'Main image', 'myplugin_textdomain' ); ?></label>
<input type="text" id="personImageLarge" name="personImageLarge" value="<?php echo esc_attr( $value ); ?>" />  
<img id="personImageLarge_image" alt="" src="<?php echo esc_attr( $value ); ?>">

<script type="text/javascript">
var thisMediaPanel_large;

jQuery(document).ready(function(){

	thisMediaPanel_large = new MediaPanel({
		"trigger" : jQuery('.mediaTrigger_large'),
		"target" : jQuery('#personImageLarge'),
		"preview" : jQuery('#personImageLarge_image')
	});
});
</script>


</div>
