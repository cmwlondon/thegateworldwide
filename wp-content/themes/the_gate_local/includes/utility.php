<?php
/**
 * Utility functions
 *
 * @package WordPress
 * @subpackage thegate_local
 */

/**
 * Truncates the given string to the given length
 * @param string $string  sdsfsdf
 * @param int $length
 * @param string $suffix
 *
 * @return string
 */
function util_truncate_string($string, $max = 20, $suffix = '')
{
    if(strlen($string) <= $max)
        return $string;

    return substr( $string, 0, strrpos( substr( $string, 0, $max), ' ' ) ) . ' ' . $suffix;
}

/**
 * Gets the terms associated to this post
 *
 * @param integer $_post_id
 *
 * @return array Array('my_taxonomy' => array([term Object1], [term Object2])); [term Object] holds the info for the term
 */
function get_post_taxonomy_terms($_post_id = false)
{
    if($_post_id != false)
    {
        $_post_taxonomy_terms = array();

        /* Determine the post type */
        $_post_type =  get_post_type( $_post_id ) ;

        /* Get the taxonomies registered for this post type */
        $_post_taxonomies = get_object_taxonomies( $_post_type );

        /* For each taxonomy collect the terms for this post */
        foreach($_post_taxonomies as $taxonomy)
        {
            $_temp_terms = array();
            $_temp_terms = get_the_terms( get_the_ID(), $taxonomy );

            if($_temp_terms && !is_wp_error($_temp_terms))
                $_post_taxonomy_terms[$taxonomy] = $_temp_terms;
        }

        return $_post_taxonomy_terms;
    }
    else return false;
}


function the_gate_format_date($date)
{
    return date(get_option('date_format'), strtotime($date));
}

/* Custom Menu Walker */
class The_Gate_Custom_Walker extends Walker_Nav_Menu{

   function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

        global $post;

        if($post && get_post_type($post->ID) == 'post' && url_to_postid($element->url) && get_post_meta(url_to_postid($element->url), '_wp_page_template', true) == 'full-news.php')
        {

            $element->classes[] = 'current-menu-item';
            $element->current_item_parent = true;
        }
        elseif($post && (get_post_type($post->ID) == 'campaign_element' || get_post_type($post->ID) == 'campaign') && url_to_postid($element->url) && get_post_meta(url_to_postid($element->url), '_wp_page_template', true) == 'full-work.php')
        {

            $element->classes[] = 'current-menu-item';
            $element->current_item_parent = true;
        }

        if(is_search())
        {
            $element->classes = array();
        }

        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

class The_Gate_Footer_Menu_Walker extends Walker_Nav_Menu {
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

        $element->classes[] = 'link';

        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

class The_Gate_Left_Footer_Menu_Mobile_Walker extends The_Gate_Footer_Menu_Walker {
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

        $element->classes[] = 'mobile-link';

        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

class The_Gate_Left_Footer_Menu_Desktop_Walker extends The_Gate_Footer_Menu_Walker {
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

        $element->classes[] = 'desktop-link';

        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

if(function_exists('lcfirst') === false) {
    function lcfirst($str) {
        $str[0] = strtolower($str[0]);
        return $str;
    }
}

function modify_tag_size($html, $width = 300, $height = 200)
{

    $html = preg_replace('/(width)="[0-9%]+"/', '$1="' . $width . '"', $html);
    $html = preg_replace('/(height)="[0-9%]+"/', '$1="' . $height . '"', $html);

    return $html;
}

function get_img_tag_src($img_tag)
{

    preg_match('/src="([^"]+)"/', $img_tag, $matches);

    return $matches[1];
}

function get_page_id_by_slug($page_slug)
{
    $page = get_page_by_path($page_slug);
    if( $page )
    {
        return $page->ID;
    }
    else
    {
        return null;
    }
}

function get_full_path($url)
{
    return realpath(str_replace(get_bloginfo('url'), '.', $url));
}
?>