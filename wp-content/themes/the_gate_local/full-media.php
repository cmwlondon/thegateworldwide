<?php
/**
 * Template Name: Media
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $media_capabilities_page,
       $media_experience_page,
       $media_philosophy_page,
       $media_values_page;

the_post();

/* Get first children that has template "Capabilities" */
$media_capabilities_page = get_first_children_page_by_template('part-media_capabilities.php');
/* Get first children that has template "Experience" */
$media_experience_page = get_first_children_page_by_template('part-media_experience.php');
/* Get first children that has template "Philosophy" */
$media_philosophy_page = get_first_children_page_by_template('part-media_philosophy.php');
/* Get first child that has template "Values" */
$media_values_page = get_first_children_page_by_template('part-media_values.php');

get_header();

?>
    <!-- visual -->
    <div id="company" class="visual">
        <div class="visual-holder">
            <?php echo (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'homepage-thumbnail') : get_placeholder_image('homepage-thumbnail'); ?>
        </div>
        <div class="text">
            <h2><?php echo get_post_meta($post->ID, 'about_subtitle_subtitle', true); ?></h2>
            <?php the_content(); ?>
        </div>
    </div>
    <!-- menu -->
    <ul class="menu">
        <li class="active"><a href="#company"><?php echo get_post_meta($post->ID, 'menu_label', true); ?></a></li>
        <?php if($media_philosophy_page) : ?>
            <li><a href="#philosophy"><?php echo get_post_meta($media_philosophy_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($media_values_page) : ?>
            <li><a href="#values"><?php echo get_post_meta($media_values_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($media_capabilities_page) : ?>
            <li><a href="#capabilities"><?php echo get_post_meta($media_capabilities_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($media_experience_page) : ?>
            <li><a href="#experience"><?php echo get_post_meta($media_experience_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
    </ul>
</header>
<!-- main -->
<div id="main">
    <div class="main-holder">
        <!-- content -->
        <div id="content">
            <!-- simple-list -->
            <ul class="simple-list">
                <li id="about">
                    <div id="about-mob">
                        <a href="#" class="opener"><?php echo get_post_meta($post->ID, 'menu_label', true); ?></a>
                        <div class="slide">
                            <div class="text">
                                <h3><?php echo get_post_meta($post->ID, 'about_subtitle_subtitle', true); ?></h3>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </li>
                <?php get_template_part('part', 'media_philosophy'); ?>
                <?php get_template_part('part', 'media_values'); ?>
                <?php get_template_part('part', 'media_capabilities'); ?>
                <?php get_template_part('part', 'media_experience'); ?>
            </ul>
        </div>
    </div>
</div>
<?php

get_footer();