$(function(){
    var category = $('span.sectional-excerpts').val();

    if(category === '') {
        $('.sectional-excerpts').addClass('hidden');
    } else {
        $('.sectional-excerpts').removeClass('hidden');
    }

    $('#filter-news li a').on('click', function(e) {
        e.preventDefault();

        var category = $(this).data('category');
        var categoryDesc = (Number(category)) ? $(this).data('category-description') : '';
        var categoryName = (Number(category)) ? ' / ' +  $(this).data('category-name') : '';

        if(categoryDesc === '') {
            $('.sectional-excerpts').addClass('hidden');
        } else {
            $('.sectional-excerpts').removeClass('hidden');
        }

        $('span.category-title').html(categoryName);
        $('.sectional-excerpts').html(categoryDesc);
        showFilteredNews({numberposts: 20, category: category});
    })
});

function showFilteredNews(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'filter_news_by_category'}),
        beforeSend  : function() {
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
                // clear ALL existing news items
                newsContainer.find('.dynamic').addClass('appended');

                var t3 = window.setTimeout(function (){
                    newsContainer.find('.dynamic').remove();
                    $.each(response.posts, function(key, post) {
                        var listElement = newsContainer.find('.itemTemplate').clone();

                        listElement
                        .removeClass('itemTemplate')
                        .addClass('appended')
                        .addClass('dynamic');

                        // post.date
                        // post.category
                        listElement.find ('a.overlayLink').attr({
                            "href" : post.url
                        });
                        listElement.find ('figure').append(post.thumbnail);
                        listElement.find ('header h3').html(post.title);
                        listElement.find ('header p').html(post.excerpt);

                        newsContainer.append(listElement);
                    });

                    if(!response.hide_show_more)
                    $('#show-more').removeClass('hidden');
                    newsContainer.masonry( 'reloadItems' );
                    newsContainer.masonry( 'layout' );

                }, itemFadeDuration);
            }
            else {
                $('#show-more').removeClass('hidden');
            }
        },
        error       : function(xhrObj, status, error) {
            $('#show-more').removeClass('hidden');
        }
    });
}
