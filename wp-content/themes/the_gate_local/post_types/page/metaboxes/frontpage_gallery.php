<?php

$metabox->the_field('gallery_thumbs');
$images = $metabox->get_the_value();
?>
<div class="misc-pub-section hide-if-no-js">
    <input class="the-gate-gallery-upload-button" data-uploader_title="Upload Image" data-uploader_button_text="Select" class="upload_button button" type="button" value="<?php echo __('Upload Image'); ?>" rel="" />
    <input class="the-gate-gallery-attachments-ids" type="hidden" value="<?php $metabox->the_value(); ?>" name="<?php $mb->the_name(); ?>" />
    <div class="the-gate-gallery-container">
        <ul class="the-gate-gallery-thumbs clearfix"><?php
            $images = explode(',', $images);
            if (is_array($images) && count($images) > 0) {
                foreach ($images as $id) {
                    if((int)$id)
                    {
                        echo admin_thumb((int)$id);
                    }
                }
            }
            ?>
        </ul>
    </div>
</div>
