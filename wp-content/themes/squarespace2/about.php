<?php
/*
Template Name: about

@package WordPress
@subpackage stackbartlett
*/

$metadata = get_post_meta($post->ID);
$banner = array(
	"title" => $metadata['ab_title'][0],
	"src" => $metadata['ab_banner'][0],
	"id" => $metadata['ab_banner_id'][0],
	"w" => $metadata['ab_banner_w'][0],
	"h" => $metadata['ab_banner_h'][0]
);

// add list of logo items with src/alt values for each
$clients = array(
    "header" => $metadata['al_header'][0],
    "items" => unserialize(base64_decode( $metadata['client_items'][0] ))
);

$copy1 = array(
	"header" => $metadata['ac1_header'][0],
	"text" => html_entity_decode($metadata['ac1_text'][0])
);

$pic1 = array(
	"alt" => $metadata['ap1_alt'][0],
	"src" => $metadata['ap1_image'][0],
	"id" => $metadata['ap1_image_id'][0],
	"w" => $metadata['ap1_image_w'][0],
	"h" => $metadata['ap1_image_h'][0]
);

$pic2 = array(
	"alt" => $metadata['ap2_alt'][0],
	"src" => $metadata['ap2_image'][0],
	"id" => $metadata['ap2_image_id'][0],
	"w" => $metadata['ap2_image_w'][0],
	"h" => $metadata['ap2_image_h'][0]
);

$nextPageID = $metadata['ab_nextpage'][0];
$nextPageURL = get_permalink($nextPageID);
$nextPage = get_page_by_postid($nextPageID)[0];

get_header();

?>
	</div>

	<div id="content" class="site-content aboutus">

		<div class="single carimage">
			<article class="column"><div class="car"><img alt="<?php echo $banner['title']; ?>" data-width="<?php echo $banner['w']; ?>" data-height="<?php echo $banner['h']; ?>" src="<?php echo $banner['src']; ?>"></div></article>
		</div>

		<div class="logogrid">
			<h2><?php echo $clients['header']; ?></h2>
			<ul class="clearfix">
				<?php foreach ( $clients['items'] AS $item ) :
                    $ip = wp_get_attachment_image_src($item['picid'],'full');
                ?>
				<li><img alt="<?php echo $item['alt']; ?>" title="<?php echo $item['alt']; ?>" src="<?php echo $ip[0]; ?>"></li>
				<?php endforeach; ?>
			</ul>
		</div>

		<div class="text">
			<h2><?php echo $copy1['header']; ?></h2>
			<p><?php echo $copy1['text']; ?></p>
		</div>

		<div class="single carimage">
			<article class="column"><div class="car"><img alt="<?php echo $pic1['alt']; ?>" data-width="<?php echo $pic1['w']; ?>" data-height="<?php echo $pic1['h']; ?>" src="<?php echo $pic1['src']; ?>"></div></article>
		</div>

		<div class="single carimage">
			<article class="column"><div class="car"><img alt="<?php echo $pic2['alt']; ?>" data-width="<?php echo $pic2['w']; ?>" data-height="<?php echo $pic2['h']; ?>" src="<?php echo $pic2['src']; ?>"></div></article>
		</div>


		<div class="text nextlink">
			<p>Next: <a href="<?php echo $nextPageURL; ?>"><?php echo strtoupper($nextPage->post_title); ?></a></p>
		</div>

<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
