$(function() {
    $('#show-more-landing-posts').on('click', function(e) {
        e.preventDefault();

        var offset = $('.list li').length,
            exclude_ids = $(this).data('exclude-ids').split(',');

        if(!$(this).hasClass('hide')) {
            showMore({offset: offset, numberposts: 10, exclude_ids: exclude_ids});
        };
    });
});

function showMore(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'get_landing_page_posts_ajax'}),
        beforeSend  : function() {
            /* Disable button */
            $('#show-more-landing-posts').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
                var listClasses = [],
                    show_show_more_button = false;

                /* Enable button if posts are as much as expected */
                if(response.posts.length === data.numberposts) {
                    response.posts.pop();
                    show_show_more_button = true;
                }

                /* Append new posts */
                $.each(response.posts, function(key, post) {
                    var listElement = $('.list li').first().clone();
                    var rowClass = 'row-' + post.row;
                    listClasses.push(rowClass);

                    /* Add post id to exvlude ids array */
                    data.exclude_ids.push(post.id);

                    $(listElement).attr('class', rowClass);

                    if(post.category) {
                        if($('figure .title', listElement).length > 0) {
                            $('figure .title', listElement).text(post.category);
                        }
                        else {
                            $('figure', listElement).append($('<strong/>').addClass('title').text(post.category));
                        }
                    }
                    else {
                        $('figure .title', listElement).remove();
                    }

                    post.thumbnail = post.thumbnail.replace('<img','<img onload="this.setAttribute(\'data-loaded\',\'yep\')" data-loaded="nope"');
                    if(post.post_type === 'campaign_element' && post.campaign) {
                        $('figure a', listElement).attr('href', post.campaign).html(post.thumbnail);
                    } else {
                        $('figure a', listElement).attr('href', post.url).html(post.thumbnail);
                    }

                    $('header h3', listElement).html(post.title);

                    if(post.client && post.post_type === 'campaign_element') {
                        $('header span', listElement).text(post.client);
                    }
                    else{
                        $('header span', listElement).text('');
                    }

                    if(post.post_type === 'post') {
                        $('header h4', listElement).html(post.date);
                    }

                    $('.holder', listElement).height('auto');

                    if($('p', listElement).length > 0) {
                        var excerpts = $('p', listElement);
                        excerpts.remove();
                    }
                    $('header', listElement).after($(post.excerpt));

                    $('a.more', listElement).attr('href', post.url);

                    listElement.appendTo($('.list'));
                });

                /* Set equal hight for list items in same row */
                setTimeout(function(){
                    if($('[data-loaded="nope"]').length != 0){
                        setTimeout(arguments.callee, 250);
                        return;
                    }
                    $.setEqualHeightByClassWithImages($.unique(listClasses));
                }, 100)

                /*Add new posts ids to the exclude list */
                $('#show-more-landing-posts').data('exclude-ids', data.exclude_ids.join(','));

                if(show_show_more_button) {
                    $('#show-more-landing-posts').removeClass('hidden');
                }
            }
            else {
                /* Enable button */
                $('#show-more-landing-posts').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            /* Enable button */
            $('#show-more-landing-posts').removeClass('hidden');
        }
    });

}
