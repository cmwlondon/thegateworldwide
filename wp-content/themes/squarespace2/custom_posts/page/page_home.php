<?php
/*
-----------------------------------------------
landing page
main banner video vimeo url
main banner fallback image
main banner link
main banenr title

culture
image
title
url

people
image
title
url

work
image
title
url

*/

// about banner START
function home_banner_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'home_banner_save_meta_box_data', 'home_banner_meta_box_nonce' );

    $values['hb_title'] = get_post_meta( $post->ID, 'hb_title', true );
    $values['hb_link'] = get_post_meta( $post->ID, 'hb_link', true );
    $values['hb_image'] = get_post_meta( $post->ID, 'hb_image', true );
    $values['hb_image_id'] = get_post_meta( $post->ID, 'hb_image_id', true );
    $values['hb_image_w'] = get_post_meta( $post->ID, 'hb_image_w', true );
    $values['hb_image_h'] = get_post_meta( $post->ID, 'hb_image_h', true );
    $values['hb_vimeo'] = get_post_meta( $post->ID, 'hb_vimeo', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/home_banner.php';
}

function home_banner_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['home_banner_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['home_banner_meta_box_nonce'], 'home_banner_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('hb_title', 'hb_title'),
        array('hb_link', 'hb_link'),
        array('hb_image', 'hb_image'),
        array('hb_image_id', 'hb_image_id'),
        array('hb_image_w', 'hb_image_w'),
        array('hb_image_h', 'hb_image_h'),
        array('hb_vimeo', 'hb_vimeo')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'home_banner_save_meta_box_data' );
// about banner END

//
function home_culture_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'home_culture_save_meta_box_data', 'home_culture_meta_box_nonce' );

    $values['hc_title'] = get_post_meta( $post->ID, 'hc_title', true );
    $values['hc_link'] = get_post_meta( $post->ID, 'hc_link', true );
    $values['hc_image'] = get_post_meta( $post->ID, 'hc_image', true );
    $values['hc_image_id'] = get_post_meta( $post->ID, 'hc_image_id', true );
    $values['hc_image_w'] = get_post_meta( $post->ID, 'hc_image_w', true );
    $values['hc_image_h'] = get_post_meta( $post->ID, 'hc_image_h', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/home_culture.php';
}

function home_culture_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['home_culture_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['home_culture_meta_box_nonce'], 'home_culture_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('hc_title', 'hc_title'),
        array('hc_link', 'hc_link'),
        array('hc_image', 'hc_image'),
        array('hc_image_id', 'hc_image_id'),
        array('hc_image_w', 'hc_image_w'),
        array('hc_image_h', 'hc_image_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'home_culture_save_meta_box_data' );
//

//
function home_people_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'home_people_save_meta_box_data', 'home_people_meta_box_nonce' );

    $values['hp_title'] = get_post_meta( $post->ID, 'hp_title', true );
    $values['hp_link'] = get_post_meta( $post->ID, 'hp_link', true );
    $values['hp_image'] = get_post_meta( $post->ID, 'hp_image', true );
    $values['hp_image_id'] = get_post_meta( $post->ID, 'hp_image_id', true );
    $values['hp_image_w'] = get_post_meta( $post->ID, 'hp_image_w', true );
    $values['hp_image_h'] = get_post_meta( $post->ID, 'hp_image_h', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/home_people.php';
}

function home_people_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['home_people_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['home_people_meta_box_nonce'], 'home_people_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('hp_title', 'hp_title'),
        array('hp_link', 'hp_link'),
        array('hp_image', 'hp_image'),
        array('hp_image_id', 'hp_image_id'),
        array('hp_image_w', 'hp_image_w'),
        array('hp_image_h', 'hp_image_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'home_people_save_meta_box_data' );
//

//
function home_work_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'home_work_save_meta_box_data', 'home_work_meta_box_nonce' );

    $values['hw_title'] = get_post_meta( $post->ID, 'hw_title', true );
    $values['hw_link'] = get_post_meta( $post->ID, 'hw_link', true );
    $values['hw_image'] = get_post_meta( $post->ID, 'hw_image', true );
    $values['hw_image_id'] = get_post_meta( $post->ID, 'hw_image_id', true );
    $values['hw_image_w'] = get_post_meta( $post->ID, 'hw_image_w', true );
    $values['hw_image_h'] = get_post_meta( $post->ID, 'hw_image_h', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/home_work.php';
}

function home_work_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['home_work_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['home_work_meta_box_nonce'], 'home_work_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('hw_title', 'hw_title'),
        array('hw_link', 'hw_link'),
        array('hw_image', 'hw_image'),
        array('hw_image_id', 'hw_image_id'),
        array('hw_image_w', 'hw_image_w'),
        array('hw_image_h', 'hw_image_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'home_work_save_meta_box_data' );
//
?>
