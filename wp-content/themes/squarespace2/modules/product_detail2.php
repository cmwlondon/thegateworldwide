		<article class="generalContent pair <?php echo $arrangement.' '.$background.' product'.$product->ID;?>" >
			<div class="doubleColumnWrapper">
				<div class="doubleColumnItem imageBox">
					<img alt="" src="<?php echo $metadata['pr_sidebar'][0]; ?>">

					<div class="desktopOnly">
						<!-- key contact START -->
						<?php
						if ($metadata['pr_keycontact'][0] != '' && $metadata['pr_keycontact'][0] != '-999') :
							$keyContactMeta = get_post_meta($metadata['pr_keycontact'][0]);
						?>
						<div class="keyContact">
							<h4>Key contact</h4>
							<p><?php echo $keyContactMeta['p_forename'][0].' '.$keyContactMeta['p_surname'][0]; ?></p>
							<p><?php echo $keyContactMeta['p_role'][0]; ?></p>
							<p class="mobile"><?php echo $keyContactMeta['p_mobile'][0]; ?></p>
							<p class="email"><a href="mailto:<?php echo $keyContactMeta['p_email'][0]; ?>"><?php echo $keyContactMeta['p_email'][0]; ?></a></p>
						</div>
						<?php endif; ?>
						<!-- key contact END -->
					</div>

				</div>
				<div class="doubleColumnItem textBox">
					<h3>HEADING</h3>
					<div><?php echo html_entity_decode($metadata['pr_main'][0]); ?></div>

					<div class="accordion">
						<a href="" class="opener">tell me more</a>
						<div class="window">
							<div class="info">
								EXTRA COPY

								<!-- download list START -->
								<?php
								if ($metadata['product_links_doc_links'][0] != '') :
									$document_links = unserialize($metadata['product_links_doc_links'][0]);
									foreach($document_links AS $document_link) :
										$data = json_decode($document_link['docdata']);
								?>
								<a href="<?php echo $document_link['link']; ?>" target="_blank" class="misc download"><?php echo $document_link['text']; ?> (<?php echo $data[1].' '.$data[4]; ?>)</a>
								<?php
									endforeach;
								?>
								<?php
								endif;
								?>
								<!-- download list END -->


								<!-- misc links list START -->
								<?php
								if ( $metadata['product_links_misc_links'][0] != '' ) :
									$misc_links = unserialize($metadata['product_links_misc_links'][0]);
									foreach( $misc_links AS $misc_link) :
								?>
								<a href="<?php echo $misc_link['link']; ?>" target="_blank" class="misc caret"><?php echo $misc_link['text']; ?></a>
								<?php
									endforeach;
								?>
								<?php
								endif;
								?>
								<!-- misc links list END -->

							</div>
						</div>
						<a href="" class="closer">close</a>
					</div>

					<div class="mobileOnly">
						<!-- key contact START -->
						<?php
						if ($metadata['pr_keycontact'][0] != '' && $metadata['pr_keycontact'][0] != '-999') :
							$keyContactMeta = get_post_meta($metadata['pr_keycontact'][0]);
						?>
						<div class="keyContact">
							<h4>Key contact</h4>
							<p><?php echo $keyContactMeta['p_forename'][0].' '.$keyContactMeta['p_surname'][0]; ?></p>
							<p><?php echo $keyContactMeta['p_role'][0]; ?></p>
							<p class="mobile"><?php echo $keyContactMeta['p_mobile'][0]; ?></p>
							<p class="email"><a href="mailto:<?php echo $keyContactMeta['p_email'][0]; ?>"><?php echo $keyContactMeta['p_email'][0]; ?></a></p>
						</div>
						<?php endif; ?>
						<!-- key contact END -->
					</div>

				</div>
				<br class="cb">
			</div>
		</article>
