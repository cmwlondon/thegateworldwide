;
(function () {
	//----------------------------------------------------------------------
	// STORIES
	//----------------------------------------------------------------------
	var $allAnchors = $('.stories__row').find('a'),
		$videoWrapper = $('#stories__modal'),
		$video = $videoWrapper.find('video'),
		$videoSrc = $video.find('source'),
		$close = $videoWrapper.find('.close-video');


	// handle dynamic video loading for each box
	$allAnchors.each(function () {
		var $this = $(this);
		$this.on('click', function () {
			// set the video source to the clicked elem data-video
			$video.attr({
				'preload': 'yes',
				'autoplay': 'yes',
				"controls": true
			});

			$videoSrc.attr('src', $this.data('video'));
			$video.load();
			$video.play();
		});
	});


	// handle video close
	$videoWrapper.on('hidden.bs.modal', function () {
		$video.get(0).pause();
		$videoSrc.attr('src', '');
	});


	//----------------------------------------------------------------------
	// HOME
	//----------------------------------------------------------------------
	// $(".cross").hide();
	// $(".menu").hide();


	/*
	$(".hamburger").click(function () {
		$(".menu").slideToggle("slow", function () {
			$(".hamburger").hide();
			$(".cross").show();
		});
	});
	$(".cross").click(function () {
		$(".menu").slideToggle("slow", function () {
			$(".cross").hide();
			$(".hamburger").show();
		});
	});
	*/

	$(".compass").click(function (e) {
		e.preventDefault();
		$(this).toggleClass('open');
		$(".menu").slideToggle("slow", function () {
			$(this).toggleClass('open');
		});
	});

	//----------------------------------------------------------------------
	// Contact us
	//----------------------------------------------------------------------
	var $contact_us = $('#contact-page');
	$contact_us.appendTo('.intro-body > .container');
})();

var myAjax = {"ajaxUrl":"/london\/wp-admin\/admin-ajax.php"};
var pageID = 30;
var validationMapping = new Array();

var properties = new Array();

properties.push('isRequired');
validationMapping.push({name: 'username', properties: properties});
var properties = new Array();

properties.push('isRequired');
properties.push('isEmail');
validationMapping.push({name: 'email', properties: properties});
var properties = new Array();

properties.push('isRequired');
validationMapping.push({name: 'message', properties: properties});

/* change logo size when user scrolls down from top of page
$(window).on("scroll", function() {
	var s = $(document).scrollTop();
	console.log(s);
	if (s > 57) {
		$('#logo').addClass('miniLogo');
	} else {
		$('#logo').removeClass('miniLogo');
	}
});
*/