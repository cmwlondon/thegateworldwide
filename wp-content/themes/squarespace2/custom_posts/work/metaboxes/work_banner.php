<?php 
$options = array(
	array(
		"value" => "video",
		"label" => "Video"
	),
	array(
		"value" => "image",
		"label" => "Image"
	)
);
?>

<div class="customField typechooser">
	<label for="banner_type"></label>
	<select id="banner_type" name="banner_type">
		<option value="null">Select banner type</option>
		<?php foreach ( $options AS $option) : ?>
		<option value="<?php echo $option['value']; ?>" <?php if ($option['value'] == $values['banner_type']) { echo 'selected'; } ?>><?php echo $option['label']; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<?php switch( $values['banner_type'] ) : ?>
<?php case "video" : ?>
<div class="customField mediumtext">
	<label for="<?php echo $settings['prefix']; ?>_vimeo">Vimeo Video</label>
	<input type="text" id="<?php echo $settings['prefix']; ?>_vimeo" name="<?php echo $settings['prefix']; ?>_vimeo" value="<?php echo $values[$settings['prefix'].'_vimeo']; ?>">
</div>
<?php break; ?>
<?php case "image" : ?>
<div class="customField typechooser">
<div class="customField mediumtext">
	<label for="<?php echo $settings['prefix']; ?>_alt">image alt</label>
	<input type="text" id="<?php echo $settings['prefix']; ?>_alt" name="<?php echo $settings['prefix']; ?>_alt" value="<?php echo $values[$settings['prefix'].'_alt']; ?>">
</div>
</div>
<?php break; ?>
<?php endswitch; ?>

 
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Banner image</label>
		<input type="hidden" id="<?php echo $settings['prefix']; ?>_image" name="<?php echo $settings['prefix']; ?>_image" value="<?php echo $values[$settings['prefix'].'_image']; ?>">
		<input type="hidden" id="<?php echo $settings['prefix']; ?>_id" name="<?php echo $settings['prefix']; ?>_image_id" value="<?php echo $values[$settings['prefix'].'_image_id']; ?>" class="ident">
		<input type="hidden" id="<?php echo $settings['prefix']; ?>_image_w" name="<?php echo $settings['prefix']; ?>_image_w" value="<?php echo $values[$settings['prefix'].'_image_w']; ?>" class="width">
		<input type="hidden" id="<?php echo $settings['prefix']; ?>_image_h" name="<?php echo $settings['prefix']; ?>_image_h" value="<?php echo $values[$settings['prefix'].'_image_h']; ?>" class="height">
		<a href="" class="<?php echo $settings['prefix']; ?>_image_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values[$settings['prefix'].'_image']; ?>" <?php if ($values[$settings['prefix'].'_image'] == '' || $values[$settings['prefix'].'_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values[$settings['prefix'].'_image'] == '' || $values[$settings['prefix'].'_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values[$settings['prefix'].'_image']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.<?php echo $settings['prefix']; ?>_image_Picker')
    }));
});
</script>
