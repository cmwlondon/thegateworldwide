<?php
/**
 * Single Campaign Page (Individual Campaign Page)
 *
 * @package WordPress
 * @subpackage TheGate
 */
global $related_posts;

the_post();

$client = get_client();
$related_posts = get_related_posts(3, array('post', 'campaign', 'campaign_element'), true);

$galery_thumbs_ids = explode(',', get_post_meta($post->ID, 'gallery_thumbs', true));

$showmp4 = false;
$mp4_id = 0;
$mp4File = '';
$mp4scale = 1.0;
$items = get_post_meta($post->ID, 'campaign_element_mp4', true);
if ($items) {
    if (array_key_exists('document', $items)) {
        $showmp4 = true;
        $mp4_id = $items['document']; // get post_id of attached video file
        $mp4post = get_post_meta($mp4_id, '_wp_attached_file', false); // get path to attached video file (within uploads directory)
        $mp4File = wp_upload_dir()['baseurl']."/".$mp4post[0]; // get full URL for video file
        $mp4scale = ( $items['scale'] != null ) ? $items['scale'] : 1.0;
        $mp4boxwidth = ( $items['boxwidth'] != null ) ? $items['boxwidth'] : 'random';
    }
}
$imageData = wp_get_attachment_image_src( get_post_thumbnail_id($campaign_element->ID), 'main_image');
$thumbnailURL   = $imageData[0];

if(class_exists('MultiPostThumbnails'))
{
    $check_main_img = MultiPostThumbnails::has_post_thumbnail($campaign_element->post_type, 'main_image', $campaign_element->ID, 'full');
    $main_img_url   = MultiPostThumbnails::get_post_thumbnail_url($campaign_element->post_type, 'main_image', $campaign_element->ID, 'full');
}


wp_enqueue_script('share_popup');
wp_enqueue_script('fancybox_pack');
wp_enqueue_script('anything_slider');
wp_enqueue_script('youtube');
wp_enqueue_script('vimeo');

get_header();
?>
    <!-- visual -->
</header>
<div id="main">
    <!--
    <div class="work-individual-media-wrap">
        <div class="work-individual-media">
            <?php if($showmp4) : ?>
                <video controls preload poster="<?php echo $thumbnailURL; ?>">
                    <source type="video/mp4" src="<?php echo $mp4File; ?>"> 
                </video>
            <?php else: ?>
                <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
            <?php endif; ?>

        </div>
    </div>
    -->
    <div class="main-holder">
        <!-- content -->
        <div id="content">

            <div id="values" class="outerBox">
                <div class="innerPadding">
                    <h1>Work</h1>
                    <div class="titleCol"><h3><?php echo get_post_meta($post->ID, 'campaign_subtitle', true); ?></h3></div>
                    <div class="campaignItems">
                        <?php if($showmp4) : ?>
                            <video autoplay preload poster="<?php echo $thumbnailURL; ?>">
                                <source type="video/mp4" src="<?php echo $mp4File; ?>"> 
                            </video>
                        <?php else: ?>
                            <?php echo (has_post_thumbnail($campaign_element->ID)) ? get_the_post_thumbnail($campaign_element->ID, 'campaign-element-large') : get_placeholder_image('campaign-element-large'); ?>
                        <?php endif; ?>
                        
                    </div>
                    <div class="textCol">
                        <h3><?php the_title(); ?></h3>
                        <?php /* echo (has_post_thumbnail($client->ID)) ? get_the_post_thumbnail($client->ID, 'client-logo', array('class' => 'do-not-open-in-modal')) : get_placeholder_image('client-logo', null, array('do-not-open-in-modal'));  */ ?>
                        <?php the_content(); ?>
                        <?php get_template_part('part', 'social_buttons'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('part', 'related_items'); ?>
</div>

<?php get_footer(); ?>