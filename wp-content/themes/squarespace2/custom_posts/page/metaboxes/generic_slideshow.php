<?php
$slideshowPrefix = $fieldSettings['prefix'];
$itemArray = explode(',', $values[$slideshowPrefix.'_order']);
$itemString = '';
foreach ( $itemArray AS $itemArrayItem ) {
	$itemString = ($itemString != '') ? $itemString.',' : $itemString;
	$itemString = $itemString . "'$itemArrayItem'";
}

/*
useless buggy php a single *ESCAPED* quote in a serialized string breaks unserialize()
https://davidwalsh.name/php-serialize-unserialize-issues
//to safely serialize
$safe_string_to_store = base64_encode(serialize($multidimensional_array));

//to unserialize...
$array_restored_from_db = unserialize(base64_decode($encoded_serialized_string));
*/
$csSlides = unserialize(base64_decode($values[$slideshowPrefix.'_items']));
?>
<div class="customField multiImageChooser" id="<?php echo $slideshowPrefix; ?>SlideItems">
	<p>Add a single slide for a static image, multiple slides for a slideshow. Use the 'delay' parameter to set how long each slide is displayed for. Drag and drop slides to set the order in which they are displayed.</p>
	<input type="hidden" id="<?php echo $slideshowPrefix; ?>_order" name="<?php echo $slideshowPrefix; ?>_order" value="<?php echo $values[$slideshowPrefix . '_order'] ?>">
	<input type="hidden" id="<?php echo $slideshowPrefix; ?>_nextid" name="<?php echo $slideshowPrefix; ?>_nextid" value="<?php if ($values[$slideshowPrefix . '_nextid'] == '' ) { echo '1'; } else { echo $values[$slideshowPrefix . '_nextid']; } ?>">

	<div class="item clearfix template" id="<?php echo $slideshowPrefix; ?>_NNXNN">
		<div class="sizer">
			<label><span>alt</span><input type="text" id="<?php echo $slideshowPrefix; ?>_NNXNN_alt" name="<?php echo $slideshowPrefix; ?>_NNXNN_alt" value=""></label>
			<label><span>delay (in milliseconds)</span><input type="text" id="<?php echo $slideshowPrefix; ?>_NNXNN_delay" name="<?php echo $slideshowPrefix; ?>_NNXNN_delay" value=""></label>
			<div class="customField imageSelector">
				<input type="hidden" id="<?php echo $slideshowPrefix; ?>_NNXNN_image" name="<?php echo $slideshowPrefix; ?>_NNXNN_image" value="">
				<input type="hidden" id="<?php echo $slideshowPrefix; ?>_NNXNN_image_id" name="<?php echo $slideshowPrefix; ?>_NNXNN_image_id" value="" class="ident">
				<input type="hidden" id="<?php echo $slideshowPrefix; ?>_NNXNN_image_w" name="<?php echo $slideshowPrefix; ?>_NNXNN_image_w" value="" class="width">
				<input type="hidden" id="<?php echo $slideshowPrefix; ?>_NNXNN_image_h" name="<?php echo $slideshowPrefix; ?>_NNXNN_image_h" value="" class="height">
				<a href="" class="<?php echo $slideshowPrefix; ?>_NNXNN_Picker picker action"><img class="imageThumbnail" alt="" src="" style="opacity:0;"></a>
			</div>
			<a href="#" class="removeItem">Remove item</a>
		</div>
	</div>

	<a href="#" class="addItem">Add new item</a>
	<div class="items" id="<?php echo $slideshowPrefix; ?>List">
	<?php
		foreach ( $csSlides AS $csSlide ) :
		$ip = wp_get_attachment_image_src($csSlide['picid'],'full');

	?>
		<div class="item clearfix" id="<?php echo $csSlide['id']; ?>">
			<div class="sizer">
				<label><span>alt</span><input type="text" id="<?php echo $csSlide['id']; ?>_alt" name="<?php echo $csSlide['id']; ?>_alt" value="<?php echo stripslashes($csSlide['alt']); ?>"></label>
				<label><span>delay (in milliseconds)</span><input type="text" id="<?php echo $csSlide['id']; ?>_delay" name="<?php echo $csSlide['id']; ?>_delay" value="<?php echo $csSlide['delay']; ?>"></label>
				<div class="customField imageSelector">
					<input type="hidden" id="<?php echo $csSlide['id']; ?>_image" name="<?php echo $csSlide['id']; ?>_image" value="<?php echo $ip[0]; ?>">
					<input type="hidden" id="<?php echo $csSlide['id']; ?>_image_id" name="<?php echo $csSlide['id']; ?>_image_id" value="<?php echo $csSlide['picid']; ?>" class="ident">
					<input type="hidden" id="<?php echo $csSlide['id']; ?>_image_w" name="<?php echo $csSlide['id']; ?>_image_w" value="<?php echo $csSlide['width']; ?>" class="width">
					<input type="hidden" id="<?php echo $csSlide['id']; ?>_image_h" name="<?php echo $csSlide['id']; ?>_image_h" value="<?php echo $csSlide['height']; ?>" class="height">
					<a href="" class="<?php echo $csSlide['id']; ?>_Picker picker action"><img class="imageThumbnail" alt="" src="<?php echo $ip[0]; ?>"></a>
				</div>
				<a href="#" class="removeItem">Remove item</a>
			</div>
		</div>
	<?php endforeach; ?>

	</div>
	<a href="#" class="addItem">Add new item</a>

</div>

<script type="text/javascript">

jQuery(document).ready(function(){
	multiImageChoosers.push(new MultiImageChooser({
		"module" : jQuery('#<?php echo $slideshowPrefix; ?>SlideItems'),
		"prefix" : "<?php echo $slideshowPrefix; ?>",
		"preexisting" : [<?php echo $itemString; ?>]
	}));
});
</script>
