<?php
/*
quotes carousel module
/custom_posts/quotes/
custom post type: 'quote'
functions.php -> synchronise_quotes(), get_quotes_in_order();
*/

/* query variables 
postid
quote_context -> 'home' or 'product' 
metakey -> metakey in which quote order id list is stored: home -> 'bgh_quotes' or single-product -> 'quotes_order'
*/

$metadata = get_post_meta($postid);
// are there any quotes assigned to this page?
if ($metadata[$metakey][0] != '') :
	// yes
	$isProductContext = ($quote_context == 'product');
	$interval = ($isProductContext) ? 16 : 8;
	$quotes_order = explode(',', $metadata[$metakey][0]);	
	$syncedQuotes = synchronise_quotes($quotes_order, $quote_context, $metakey, $postid);
	$quotes = (count($syncedQuotes['assigned']) > 0 ) ? get_quotes_in_order($syncedQuotes['assigned']) : [];
?>
		<article class="module">
			<section class="quotes carousel <?php if ($isProductContext) { echo 'productQuotes'; } ?>" data-interval="<?php echo $interval; ?>">

			<?php
			foreach($quotes AS $key => $quote) :
				$quote_id = $quote->ID;
				$metadata = $metadata = get_post_meta($quote_id);
				$quote_text = $metadata['q_quote_text'][0];
				$quote_source = $metadata['q_quote_source'][0];
			?>
				<article <?php if ($key == 0) { echo "class=\"active\""; } ?>>
					<div class="singleColumn">
						<blockquote><?php echo $quote_text; ?></blockquote>
						<p><?php echo $quote_source; ?></p>
					</div>
				</article>
			<?php
			endforeach;
			?>
				<div class="controls"></div>
				<br class="cb">
			</section>
		</article>
<?php endif; ?>