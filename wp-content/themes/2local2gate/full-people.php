<?php
/**
 * Template Name: People
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $bio_page,
       $bio,
       $bio2,
       $everyone_page,
       $everyone,
       $ll_page,
       $ll,
       $leadership_page,
       $leaders,
       $listview_page,
       $listview,
       $seed,
       $individual_page_permalink,
	   $viewEveryone,
       $lp_leaders,
       $lp_followers;

	   
/* If id is not set, then view everyone... */
$viewEveryone = true;
$everyone_with_bio = '1';
$post = get_post();
$individual_page_permalink = get_permalink(get_page_by_path('people/people-individual'));
$people = new PeopleHelper();

/*

*/

/* get the seed for the order of the people on the everyone carousel */
$seed = (isset($_GET['o']) && $_GET['o']) ? (int)$_GET['o'] : null;
/* Get first children that has template "People Everyone" */
$everyone_page = get_first_children_page_by_template('part-people_everyone.php');
/* Get everyone's list */
$everyone = $people->get_list('everyone', $everyone_page->ID, $seed, $everyone_with_bio);
$seed = $people->seed;

$ll_page = $everyone_page;

// get listed of nominated leaders
$leaders = get_post_meta( get_queried_object_id(), 'london_leadership_entries', false)[0];
$lx = array();
foreach ($leaders as $leader) {
    $lx[] = $leader['londonleaders'];
}

// build lists of leaders only and followers only to display in two sections
$people = new PeopleHelper();
$lp_leaders = $people->get_london_people('leaders', $lx);
$lp_followers =  $people->get_london_people('followers', $lx);

/* Get first children that has template "People Leadership" */
// $leadership_page = get_first_children_page_by_template('part-people_leadership.php');
/* Get leadership's list of offices */
// $leaders = $people->get_list('leadership', $leadership_page->ID, null, !$everyone_with_bio);
// $leaders_name_list = call_user_func_array('array_merge', $leaders);

/* Get first children that has template "People Listview" */
// $listview_page = get_first_children_page_by_template('part-people_listview.php');
// $listview = $people->get_list('listview', $listview_page->ID);

/* Get first children that has template "People Bio" */
$bio_page = get_first_children_page_by_template('part-people_bio.php');
/* in Bio carousel show everyone or leaders depending on the way the user came to this page */
$bio = ($everyone_with_bio) ? $everyone : call_user_func_array('array_merge', $leaders);

$bio2 = $people->get_list('leadership', $leadership_page->ID, null, 1);

get_header();

wp_enqueue_script('anything_slider');
wp_enqueue_script('tablesorter');
wp_enqueue_script('equal_heights');
wp_enqueue_script('modernizr');
wp_enqueue_script('dl_menu');
wp_enqueue_script('page_transitions');

?>
</header>
<!-- main -->
<div id="main">
    

    <div class="outerBox">
        <div class="innerPadding">

            <!-- content -->
            <a href="#" class="title"><?php the_title(); ?></a>
            
            <!-- mobile people list START -->
            <ul class="simple-list mobile-listview" style="display:none;">
                <li class="everyone">
                    <ul class="dropdown-menu">
                        <li id="sort-everyone-people" class="headlink">
                            <div class="dropdown-title-wrap">
                                <span class="dropdown-label"><span><?php _e('View by', 'People Page'); ?></span><?php _e('Sort by', 'People Page'); ?>:</span>
                                <div href="#" class="dropdown-title"><?php _e('Select', 'People Page'); ?></div>
                            </div>
                            <ul id="sort-people" class="dropdown-submenu">
                                <li><a href="#" class="first" data-sort-by="first-name"><?php _e('First name', 'People Page'); ?></a></li>
                                <li><a href="#" data-sort-by="last-name"><?php _e('Last name', 'People Page'); ?></a></li>
                                <li><a href="#" data-sort-by="title"><?php _e('Title', 'People Page'); ?></a></li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="people-names-list">
                        <?php foreach ($everyone as $person) : $bio_link_in_list_view = $individual_page_permalink . '?' . http_build_query(array('slug' => $person['slug'], 'e' => true, 'o' => $seed), '', '&amp;');?>
                            <li>
                                <a href="<?php echo $bio_link_in_list_view; ?>">
                                    <p>
                                        <strong data-sort-handle="first-name"><?php echo $person['first_name']; ?></strong>
                                        <span data-sort-handle="last-name"> <?php echo $person['last_name']; ?></span>
                                    </p>
                                    <em data-sort-handle="title"><?php echo $person['position']; ?></em>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
            <!-- mobile people list END -->
            <?php
                get_template_part('part', 'people_bio');
                get_template_part('part', 'people_ll'); // leaders
                get_template_part('part', 'people_everyone'); // followers
                // get_template_part('part', 'people_listview');
                ?>
        </div>
    </div>

    <div class="overlay off individual" id="biobox">
        <div class="relpos">
            <a class="closer" href="">X</a>
            <div class="bioContent people-individual-page group">
            </div>
        </div>
    </div>

    <div class="overlay off individual" id="contactbox">
        <div class="relpos">
            <a class="closer" href="">X</a>
            <div class="cContent people-individual-page group">
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
