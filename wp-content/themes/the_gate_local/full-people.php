<?php
/**
 * Template Name: People
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $bio_page,
       $bio,
       $everyone_page,
       $everyone,
       $leadership_page,
       $leaders,
       $listview_page,
       $listview,
       $seed,
       $individual_page_permalink,
	   $viewEveryone;
	   
/* If id is not set, then view everyone... */
$viewEveryone = true;
$everyone_with_bio = '1';
$post = get_post();
$individual_page_permalink = get_permalink(get_page_by_path('people/people-individual'));
$people = new PeopleHelper();

/* get the seed for the order of the people on the everyone carousel */
$seed = (isset($_GET['o']) && $_GET['o']) ? (int)$_GET['o'] : null;
/* Get first children that has template "People Everyone" */
$everyone_page = get_first_children_page_by_template('part-people_everyone.php');
/* Get everyone's list */
$everyone = $people->get_list('everyone', $everyone_page->ID, $seed, $everyone_with_bio);
$seed = $people->seed;

/* Get first children that has template "People Leadership" */
$leadership_page = get_first_children_page_by_template('part-people_leadership.php');
/* Get leadership's list of offices */
$leaders = $people->get_list('leadership', $leadership_page->ID, null, !$everyone_with_bio);
$leaders_name_list = call_user_func_array('array_merge', $leaders);

/* Get first children that has template "People Listview" */
$listview_page = get_first_children_page_by_template('part-people_listview.php');
$listview = $people->get_list('listview', $listview_page->ID);

/* Get first children that has template "People Bio" */
$bio_page = get_first_children_page_by_template('part-people_bio.php');
/* in Bio carousel show everyone or leaders depending on the way the user came to this page */
$bio = ($everyone_with_bio) ? $everyone : call_user_func_array('array_merge', $leaders);

get_header();

wp_enqueue_script('anything_slider');
wp_enqueue_script('tablesorter');
wp_enqueue_script('equal_heights');
wp_enqueue_script('modernizr');
wp_enqueue_script('dl_menu');
wp_enqueue_script('page_transitions');

?>
    <!-- menu -->
    <ul class="menu">
        <?php if($bio_page) : ?>
            <li style="display: none"><a id="returnPage3" href="#individual"><?php echo get_post_meta($bio_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($bio_page) : ?>
            <li class="active"><a id="returnPage1" href="#everyone"><?php echo get_post_meta($everyone_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($leadership_page) : ?>
            <li><a id="returnPage2" href="#leadership"><?php echo get_post_meta($leadership_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
        <?php if($listview_page) : ?>
            <li><a id="iterateEffects" href="#list-view"><?php echo get_post_meta($listview_page->ID, 'menu_label', true); ?></a></li>
        <?php endif; ?>
    </ul>
</header>
<!-- main -->
<div id="main">
    <!-- content -->
    <a href="#" class="title"><?php the_title(); ?></a>
    <ul class="tab-titles">
        <li class="current"><a href="#" class="everyone"><?php echo get_post_meta($everyone_page->ID, 'menu_label', true); ?></a></li>
        <li><a href="#" class="leadership"><?php echo get_post_meta($leadership_page->ID, 'menu_label', true); ?></a></li>
    </ul>
    <ul class="simple-list mobile-listview">
        <li class="everyone">
            <ul class="dropdown-menu">
                <li id="sort-everyone-people" class="headlink">
                    <div class="dropdown-title-wrap">
                        <span class="dropdown-label"><span><?php _e('View by', 'People Page'); ?></span><?php _e('Sort by', 'People Page'); ?>:</span>
                        <div href="#" class="dropdown-title"><?php _e('Select', 'People Page'); ?></div>
                    </div>
                    <ul id="sort-people" class="dropdown-submenu">
                        <li><a href="#" class="first" data-sort-by="first-name"><?php _e('First name', 'People Page'); ?></a></li>
                        <li><a href="#" data-sort-by="last-name"><?php _e('Last name', 'People Page'); ?></a></li>
                        <li><a href="#" data-sort-by="title"><?php _e('Title', 'People Page'); ?></a></li>
                    </ul>
                </li>
            </ul>
            <ul class="people-names-list">
                <?php foreach ($everyone as $person) : $bio_link_in_list_view = $individual_page_permalink . '?' . http_build_query(array('slug' => $person['slug'], 'e' => true, 'o' => $seed), '', '&amp;');?>
                    <li>
                        <a href="<?php echo $bio_link_in_list_view; ?>">
                            <p>
                                <strong data-sort-handle="first-name"><?php echo $person['first_name']; ?></strong>
                                <span data-sort-handle="last-name"> <?php echo $person['last_name']; ?></span>
                            </p>
                            <em data-sort-handle="title"><?php echo $person['position']; ?></em>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>
        <li class="leadership">
            <ul class="dropdown-menu">
                <li id="sort-leadership-people" class="headlink">
                    <div class="dropdown-title-wrap">
                        <span class="dropdown-label"><span><?php _e('View by', 'People Page'); ?></span><?php _e('Sort by', 'People Page'); ?>:</span>
                        <div href="#" class="dropdown-title"><?php _e('Select', 'People Page'); ?></div>
                    </div>
                    <ul id="sort-people" class="dropdown-submenu">
                        <li><a href="#" class="first" data-sort-by="first-name"><?php _e('First name', 'People Page'); ?></a></li>
                        <li><a href="#" data-sort-by="last-name"><?php _e('Last name', 'People Page'); ?></a></li>
                        <li><a href="#" data-sort-by="title"><?php _e('Title', 'People Page'); ?></a></li>
                    </ul>
                </li>
            </ul>
            <ul class="people-names-list">
                <?php foreach ($leaders_name_list as $leader) : $bio_link_in_list_view = $individual_page_permalink . '?' . http_build_query(array('slug' => $leader['slug'], 'e' => true, 'o' => $seed), '', '&amp;');?>
                    <li>
                        <a href="<?php echo $bio_link_in_list_view; ?>">
                            <p>
                                <strong data-sort-handle="first-name"><?php echo $leader['first_name']; ?></strong>
                                <span data-sort-handle="last-name"> <?php echo $leader['last_name']; ?></span>
                            </p>
                            <em data-sort-handle="title"><?php echo $leader['position']; ?></em>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>
    </ul>
    <div id="pt-main" class="pt-perspective">
        <div class="pt-page pt-page-1">
            <ul class="simple-list individual-mobile">
                <?php
                    get_template_part('part', 'people_bio');
                    get_template_part('part', 'people_everyone');
                    get_template_part('part', 'people_leadership');
                ?>
            </ul><!--.simple-list-->
        </div><!--.pt-page-1-->
        <?php get_template_part('part', 'people_listview'); ?>
    </div>
</div>
<?php get_footer(); ?>
