<?php


function ppl_items_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'ppl_items_save_meta_box_data', 'ppl_items_meta_box_nonce' );

    $values['ppl_order'] = get_post_meta( $post->ID, 'ppl_order', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/ppl_items.php';
}

function ppl_items_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['ppl_items_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['ppl_items_meta_box_nonce'], 'ppl_items_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ppl_order', 'ppl_order')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'ppl_items_save_meta_box_data' );

function ppl_nextpage_meta_box_callback( $post ) {
    wp_nonce_field( 'ppl_nextpage_save_meta_box_data', 'ppl_nextpage_meta_box_nonce' );

    $values = array(
        "ppl_nextpage" => get_post_meta( $post->ID, 'ppl_nextpage', true )
    );

    $fieldSettings = array(
        "nextpagefield" => "ppl_nextpage"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_nextpage.php';
}

function ppl_nextpage_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['ppl_nextpage_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['ppl_nextpage_meta_box_nonce'], 'ppl_nextpage_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ppl_nextpage', 'ppl_nextpage')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'ppl_nextpage_save_meta_box_data' );

?>