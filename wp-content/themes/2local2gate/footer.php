<?php

/* Do not show footer on authentication page */
if (!defined('WP_INSTALLING') || !WP_INSTALLING) :
	?>


	<!-- Footer -->
	<footer>
	        <div class="footer-above">
			<div class="container">
				<div class="row">
					<div class="footer-col col-md-4 threebox-pad">

						<p class="address-fontsize">90 Tottenham Court Road, London, W1T 4TJ<br><span class="color-chng">Part of MSQ Partners.</span></p>
					</div>

					<div class="footer-col col-md-4 text-center threebox-pad">
						<p class="call-fontsize"><span class="call-clr"><strong>CALL ON US</strong> </span> 0207 927 3555<br>
							<a href="mailto:underdog@thegateworldwide.com" class="footer-email">underdog@thegateworldwide.com</a></p>
					</div>
					<div class="footer-col col-md-4 text-right threebox-pad">
						<ul class="list-inline btn-twitter1">
							<li>
                                                            <a href="https://twitter.com/thegatelondon"  target="_blank" class="btn-social btn-outline"><img src="<?php bloginfo('template_directory') . _e('/assets/images/twitter_08.png') ?>" /></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
	        </div>

	</footer>
	</div>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>
