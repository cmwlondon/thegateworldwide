<?php
function pageInit(){
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    // $post_type = get_post($post_id)->post_type;
    $post_type = ( array_key_exists('post_type', $_GET) ) ? $_GET['post_type'] : get_post_type($_GET['post']);
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

    // assign wpalchemy metaboxes to page templates in admin 
    switch( $template_file ) {
        case "generic.php" : {
        } break;
        
        case "home.php" : {
            
        } break;

        case "about.php" : {

        } break;

        case "work.php" : {

        } break;

        case "culture.php" : {

        } break;

        case "culture.php" : {

        } break;

        case "news.php" : {

        } break;

        case "people.php" : {
        } break;

        case "contact.php" : {
        } break;
    }
}
// add_action( 'init', 'pageInit' );

// standard wp metaboxes to specific pages based on page template
function my_meta_init() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

    switch( $template_file ) {
        case "generic.php" : {
        } break;
        
        case "home.php" : {
            remove_post_type_support('page', 'editor');
            
            add_meta_box(
                'home_banner',
                __( 'Banner', 'myplugin_textdomain' ),
                'home_banner_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'home_culture',
                __( 'Culture', 'myplugin_textdomain' ),
                'home_culture_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'home_people',
                __( 'People', 'myplugin_textdomain' ),
                'home_people_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'home_work',
                __( 'Work', 'myplugin_textdomain' ),
                'home_work_meta_box_callback',
                'page',
                'normal'
            );
        } break;

        case "about.php" : {
            remove_post_type_support('page', 'editor');

            // banner
            add_meta_box(
                'about_banner',
                __( 'Page Banner Image', 'myplugin_textdomain' ),
                'about_banner_meta_box_callback',
                'page',
                'normal'
            );

            // logos
            // about_logos
            // about_logos.php
            add_meta_box(
                'about_logos',
                __( 'Clients', 'myplugin_textdomain' ),
                'about_logos_meta_box_callback',
                'page',
                'normal'
            );

            // copy 1
            add_meta_box(
                'about_copy1',
                __( 'Copy block 1', 'myplugin_textdomain' ),
                'about_copy1_meta_box_callback',
                'page',
                'normal'
            );

            // pic 1
            add_meta_box(
                'about_pic1',
                __( 'Picture 1', 'myplugin_textdomain' ),
                'about_pic1_meta_box_callback',
                'page',
                'normal'
            );

            // pic 2
            add_meta_box(
                'about_pic2',
                __( 'Picture 2', 'myplugin_textdomain' ),
                'about_pic2_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'about_nextpage',
                __( 'Next Page link', 'myplugin_textdomain' ),
                'about_nextpage_meta_box_callback',
                'page',
                'normal'
            );

        } break;

        case "ourwork.php" : {
            remove_post_type_support('page', 'editor');

            // work items
            add_meta_box(
                'work_items',
                __( 'Work Items', 'myplugin_textdomain' ),
                'work_items_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'work_nextpage',
                __( 'Next Page link', 'myplugin_textdomain' ),
                'work_nextpage_meta_box_callback',
                'page',
                'normal'
            );

        } break;

        case "culture.php" : {
            remove_post_type_support('page', 'editor');

            add_meta_box(
                'culture_banner',
                __( 'Page Banner Image', 'myplugin_textdomain' ),
                'culture_banner_meta_box_callback',
                'page',
                'normal'
            );

            // copy 1
            add_meta_box(
                'culture_copy1',
                __( 'Copy block 1', 'myplugin_textdomain' ),
                'culture_copy1_meta_box_callback',
                'page',
                'normal'
            );

            // pic 1 - multiimage slideshow - replace pic with about_logos stil itemChooser
            add_meta_box(
                'culture_slideshow1',
                __( 'Slideshow 1', 'myplugin_textdomain' ),
                'culture_slideshow1_meta_box_callback',
                'page',
                'normal'
            );

            // copy 1
            add_meta_box(
                'culture_copy2',
                __( 'Copy block 2', 'myplugin_textdomain' ),
                'culture_copy2_meta_box_callback',
                'page',
                'normal'
            );

            // pic 2 - multiimage slideshow - replace pic with about_logos stil itemChooser
            add_meta_box(
                'culture_slideshow2',
                __( 'Slideshow 2', 'myplugin_textdomain' ),
                'culture_slideshow2_meta_box_callback',
                'page',
                'normal'
            );

            // copy 3
            /*
            add_meta_box(
                'culture_copy3',
                __( 'Copy block 3', 'myplugin_textdomain' ),
                'culture_copy3_meta_box_callback',
                'page',
                'normal'
            );
            */

            add_meta_box(
                'culture_nextpage',
                __( 'Next Page link', 'myplugin_textdomain' ),
                'culture_nextpage_meta_box_callback',
                'page',
                'normal'
            );

        } break;

        case "contact.php" : {
            remove_post_type_support('page', 'editor');

            add_meta_box(
                'contact_people',
                __( 'Contact People', 'myplugin_textdomain' ),
                'contact_people_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'contact_map',
                __( 'Contact Map', 'myplugin_textdomain' ),
                'contact_map_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'contact_nextpage',
                __( 'Next Page link', 'myplugin_textdomain' ),
                'contact_nextpage_meta_box_callback',
                'page',
                'normal'
            );

        } break;

        case "people.php" : {
            remove_post_type_support('page', 'editor');

            add_meta_box(
                'ppl_items',
                __( 'People', 'myplugin_textdomain' ),
                'ppl_items_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'ppl_nextpage',
                __( 'Next Page link', 'myplugin_textdomain' ),
                'ppl_nextpage_meta_box_callback',
                'page',
                'normal'
            );

        } break;
    }    
}
add_action('admin_init','my_meta_init');

require get_template_directory() . '/custom_posts/page/page_home.php';
require get_template_directory() . '/custom_posts/page/page_about.php';
require get_template_directory() . '/custom_posts/page/page_work.php';
require get_template_directory() . '/custom_posts/page/page_culture.php';
require get_template_directory() . '/custom_posts/page/page_people.php';
require get_template_directory() . '/custom_posts/page/page_contact.php';

/*
-----------------------------------------------
*/

/*
-----------------------------------------------
*/

?>