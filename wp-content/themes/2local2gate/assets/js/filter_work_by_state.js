$(function(){
    
    $('.statesSelect li').on('click', function(e) {
        e.preventDefault();

        if (!$(this).hasClass('current')) {
            $('.statesSelect li.current').removeClass('current');
            $(this).addClass('current');
            var state = $(this).data('state'),
                listState = clientContainer.attr('data-state'),
                excludeList = clientContainer.attr('data-ids');

            showFilteredCampaignsByClient({
                "itemsperpage": 12,
                "exclude" : excludeList,
                "state": state
            });
        }
    })
});

function showFilteredCampaignsByClient(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'filter_work_by_state'}),
        beforeSend  : function() {
            /* Disable button */
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {

                clientContainer.attr({"data-ids" : response.campaign.ids.join(', ') });
                clientContainer.attr({"data-state" : response.campaign.state });

                clientContainer.find('.dynamic').addClass('appended');

                var t1 = window.setTimeout(function (){
                    clientContainer.find('.dynamic').remove();
                    $.each(response.campaign.posts, function(key, post) {
                        var listElement = clientContainer.find('.itemTemplate').clone();

                        listElement
                        .removeClass('itemTemplate')
                        .addClass('appended')
                        .addClass('dynamic')
                        .attr({"id" : "work" + post.id});

                        listElement.find ('a.overlayLink').attr({
                            "href" : post.url
                        });
                        listElement.find ('figure').append(post.thumbnail);
                        listElement.find ('header h3').html(post.title);
                        listElement.find ('header p').html(post.excerpt);

                        clientContainer.append(listElement);
                    });
                    
                    clientContainer.masonry( 'reloadItems' );
                    clientContainer.masonry( 'layout' );
                    $('#show-more').removeClass('hidden');
                }, itemFadeDuration);

            }
        },
        error       : function(xhrObj, status, error) {
        }
    });
};
