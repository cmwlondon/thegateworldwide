<?php
/**
 * Template Name: News All
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $all_news_page,
       $category;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

$recent_news = get_recent_news(7, 0, ($category) ? $category->term_id : 0);
$hide_show_more = count($recent_news) < 7;
if(count($recent_news) == 7) array_pop($recent_news);

?>

<?php if($all_news_page) : ?>
<li id="all-news" class="all-news">
    <div class="list-area">
        <h2><span class="full-title"><?php echo apply_filters('the_title', $all_news_page->post_title); ?><span class="category-title"><?php echo ($category) ? ' / ' . $category->name : '' ; ?></span></span></h2>
        <?php $news_categories = get_categories(array('parent' => 0)); ?>
        <ul class="dropdown-menu">
            <li id="sort-news" class="headlink">
                <div class="dropdown-title-wrap">
                    <span class="dropdown-label"><span><?php _e('Choose category', 'News Listing Page'); ?></span></span>
                    <span class="dropdown-title-label"><?php _e('Category:', 'News Listing Page'); ?></span>
                    <div class="dropdown-title">
                        <?php if($category) : ?>
                            <a class="filtered" data-category="<?php echo $category->term_id; ?>" data-category-name="<?php echo $category->name; ?>" data-category-description="<?php echo $category->description; ?>" href="#"><?php echo $category->name; ?></a>
                        <?php else : ?>
                            <a class="filtered" data-category="" href="#"><?php _e('All', 'News Listing Page'); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <ul id="filter-news" class="dropdown-submenu">
                    <li><a href="#" data-category="" class="first"><?php _e('All', 'News Listing Page'); ?></a></li>
                    <?php foreach ($news_categories as $news_category) : ?>
                        <li><a href="#" data-category="<?php echo $news_category->term_id; ?>" data-category-name="<?php echo $news_category->name; ?>" data-category-description="<?php echo $news_category->description; ?>" ><?php echo $news_category->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>
        <span class="sectional-excerpts"><?php echo ($category) ? $category->description : '' ; ?></span>
        <ul class="list">
            <?php foreach ($recent_news as $key => $single_news) : ?>
                <li class="row-<?php echo ceil(($key + 1) / 3); ?>">
                    <div class="holder">
                        <figure>
                            <a href="<?php echo get_permalink($single_news->ID); ?>">
                                <?php echo (has_post_thumbnail($single_news->ID)) ? get_the_post_thumbnail($single_news->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?>
                            </a>
                            <?php $post_category = get_post_category($single_news->ID); ?>
                            <?php if(!$category) : ?>
                                <strong class="title"><?php echo $post_category; ?></strong>
                            <?php endif; ?>
                        </figure>
                        <header>
                            <h3><a href="<?php echo get_permalink($single_news->ID); ?>"><?php echo apply_filters('the_title', $single_news->post_title); ?></a></h3>
                            <h4><?php echo the_gate_format_date($single_news->post_date); ?></h4>
                            <?php $isVisible = !$is_edinburgh && get_post_meta($single_news->ID, 'fake_views_check', true); ?>
                            <div class="views-counter" <?= !$isVisible ? "style='display:none'" : "" ?>>
                				<img width="20" class="do-not-open-in-modal" src="<?php bloginfo('template_directory'); ?>/assets/images/news-views.png">
                   				<span><?php echo do_shortcode('[post_view id='.$single_news->ID.']'); ?></span>
                			 </div>
                        </header>
                        <?php echo apply_filters('the_excerpt', $single_news->post_excerpt); ?>
                        <a href="<?php echo get_permalink($single_news->ID); ?>" class="more"><?php _e('READ MORE', 'News Listing Page'); ?></a>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="show-more-work">
            <a id="show-more" <?php echo ($hide_show_more) ? 'class="hidden"' : ''; ?> href="#"><p><?php _e('SHOW MORE NEWS', 'News Listing Page'); ?></p><span></span></a>
        </div>
        <?php if($category) : ?>
            <div class="visual">
            </div>
        <?php endif; ?>
    </div>
</li>
<?php endif; ?>