<?php
    global $post;
    $get_campaigns = get_posts( array('post_type' => 'campaign', 'numberposts' => -1, 'suppress_filters' => false));
    $mb->the_field('associate_campaign');
?>

<select name="<?php $mb->the_name(); ?>">
    <option value=""><?php _e('Select a campaign...'); ?></option>
    <?php foreach($get_campaigns as $key => $campaign) : ?>
        <option value="<?php echo $campaign->ID; ?>" <?php $mb->the_select_state($campaign->ID); ?>><?php echo $campaign->post_title; ?></option>
    <?php endforeach;?>
</select>
