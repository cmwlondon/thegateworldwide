<?php
/**
 * General page template - Used for default template pages such as Terms and Conditions, Accessibility and View Sitemap
 *
 * @package WordPress
 * @subpackage thegate_local
 */

the_post();
?>
<!DOCTYPE html>
<html lang="en-GB" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<link rel='stylesheet'  href='<?php echo get_template_directory_uri(); ?>/assets/css/normalize.css' type='text/css' media='all' />	
	<link rel='stylesheet'  href='<?php echo get_template_directory_uri(); ?>/style.css' type='text/css' media='all' />	
</head>
<body>
	<div class="page">
		<div class="video">
			<div class="clipper">
				<video id="vp" poster="<?php echo get_template_directory_uri(); ?>/assets/img/tunnel.jpg" preload="metadata" autoplay loop muted nocontrols playsinline>
					<source src="<?php echo get_template_directory_uri(); ?>/assets/video/header_v3.mp4" type="video/mp4">
				</video>
			</div>
		</div>
		<a class="msq" href="" target="_blank" title="msq"><img alt="msq" src="<?php echo get_template_directory_uri(); ?>/assets/img/msq.png"></a>
		<div class="sites"><a href="" class="ny">NEW YORK</a><a href="" class="hk">HONG KONG</a><a href="" class="singapore">SINGAPORE</a><a href="" class="shanghai">SHANGHAI</a></div>
		<div class="core">
			<img alt="THE GATE London" src="<?php echo get_template_directory_uri(); ?>/assets/img/gatelogo.png">
			<h1>NEW WEBSITE COMING SOON</h1>
			<H2><span class="l"></span>CONTACT<span class="r"></span></H2>
				<h3><a href="mailto:jamie.elliott@thegateworldwide.com">JAMIE.ELLIOTT@THEGATEWORLDWIDE.COM</a></h3>
				<p>+44 (0)7881 802594</p>
				<hr>
				<h3><a href="mailto:beri.cheetham@thegateworldwide.com">BERI.CHEETHAM@THEGATEWORLDWIDE.COM</a></h3>
				<p>+44 (0)7522 427280</p>
				<hr>
				<h3>90 TOTTENHAM COURT ROAD, LONDON W1T 4TJ</h3>
				<p>+44 (0)20 7927 3555</p>
				<hr>
		</div>

	</div>
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/modernizr-custom.js'></script>
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/jquery-2.2.3.min.js'></script>
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/system.js'></script>

	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-43354006-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview');

	</script>	
</body>
</html>
