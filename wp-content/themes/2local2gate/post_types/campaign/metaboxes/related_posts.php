<div class="my_meta_control">
    <label for="related_items_ids"><?php _e('Related Items IDs'); ?></label>

    <p>
        <?php $mb->the_field('ids'); ?>
        <input id="related_items_ids" type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
        <span><?php _e("Enter in the IDs of the related posts,  separating them with comma"); ?></span>
    </p>

</div>