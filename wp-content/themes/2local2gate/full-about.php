<?php
/**
 * Template Name: About
 *
 * @package WordPress
 * @subpackage TheGate
 */

/* Make all variables that are needed in parts global */
global $capabilities_page,
       $experience_page,
       $philosophy_page,
       $values_page;

the_post();

$items = get_post_meta($post->ID, 'about_us_mp4', true);
if ($items) {
    if (array_key_exists('document', $items)) {
        $mp4_id = $items['document']; // get post_id of attached video file
        $mp4post = get_post_meta($mp4_id, '_wp_attached_file', false); // get path to attached video file (within uploads directory)
        $uploadDir = wp_upload_dir();
        $mp4File = $uploadDir['baseurl']."/".$mp4post[0]; // get full URL for video file
    }
}


/*
$values = get_posts(array(
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'numberposts' => -1,
    'post_type' => array('value'),
    'suppress_filters' => 0, // If it is true filters added by WPML will not work and thus posts from all languages will be displayed
));
*/
wp_enqueue_script('isotope_grid');
wp_enqueue_script('masonry_grid');
wp_enqueue_script('fancybox_pack');
wp_enqueue_script('share_popup');

get_header();

// about us items as custom post of type 'about_us_item'
// function.php line 841
/* ----------------------------------------------------------------- */
// $about_items2 = get_about_us_items2($post->ID, 'grid');
/* ----------------------------------------------------------------- */

?>
</header>
<!-- main -->
<div id="main">


    <!-- simple-list -->
    <div class="outerBox">
        <div class="innerPadding">
            <a href="#" class="title"><?php _e('About us'); ?></a>
            <h1><?php _e('About us'); ?></h1>

            <?php
            $about_items2 = get_about_us_items2($post->ID, 'grid');
            ?>
            <div class="aboutVideo">
                <video autoplay controls>
                    <source src="<?php echo $mp4File; ?>" type="video/mp4"></source>
                </video>
            </div>
            <div class="list-area">
                <div class="gutter-sizer about-gutter-sizer"></div>
                <div class="grid-sizer about-grid-sizer"></div>
                <ul class="isotopeList aboutList">
                    <?php
                    foreach ($about_items2 as $single_item) :
                    $imageData = wp_get_attachment_image_src( get_post_thumbnail_id($single_item->ID), 'single-post-thumbnail');
                    $tile = $imageData[0];
                    ?>

                    <li class="appended dynamic" id="post<?php echo $single_item->ID; ?>">
                        <!--
                        <div class="holder">
                            <a class="overlayLink" href="<?php echo get_permalink($single_item->ID); ?>"></a>
                            <figure>
                                <div class="videoFrame videoNotReady"></div>
                                <?php echo (has_post_thumbnail($single_item->ID)) ? get_the_post_thumbnail($single_item->ID, 'about-value') : get_placeholder_image('about-value'); ?>
                            </figure>
                            <header>
                                <h3><?php echo apply_filters('the_title', $single_item->post_title); ?></h3>
                            </header>
                        </div>
                        -->
                        <div class="holder">
                            <a class="overlayLink" href="<?php echo get_permalink($single_item->ID); ?>"></a>
                            <figure class="tilebackground">
                                <img alt="" src="<?php echo $tile; ?>">
                            </figure>
                            <header>
                                <h3><?php echo apply_filters('the_title', $single_item->post_title); ?></h3>
                            </header>
                        </div>
                    </li>

                    <?php endforeach; ?>
                </ul>
            </div>

        </div>
    </div>
</div>
<?php

get_footer();