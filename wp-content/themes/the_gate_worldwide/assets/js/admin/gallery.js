jQuery(function() {
    jQuery('.the-gate-gallery-upload-button').each(function(key, element){
        new MediaModal({
            calling_selector: element,
            cb: function(attachment) {
                var addedAttachments = jQuery('.the-gate-gallery-attachments-ids', jQuery(element).parents('.hide-if-no-js').first()).val().split(",").map(function(value){
                    return Number(jQuery.trim(value));
                });

                if(jQuery.inArray(attachment.id, addedAttachments) == -1){
                    addedAttachments.push(attachment.id);
                    jQuery('.the-gate-gallery-thumbs', jQuery(element).parents('.hide-if-no-js').first()).append('<li><img src="' + attachment.url + '" width="100" height="75" /><a href="#" class="the-gate-gallery-remove" data-id="' + attachment.id + '">Remove</a></li>');
                }

                addedAttachments = jQuery.grep(addedAttachments, Number);

                jQuery('.the-gate-gallery-attachments-ids', jQuery(element).parents('.hide-if-no-js').first()).val(addedAttachments.join(','));

            }
        });
    })


    jQuery('.the-gate-gallery-thumbs').on('click', '.the-gate-gallery-remove', function() {
        if (confirm('Are you sure you want to delete this?')) {
            var element = this;
            var idsContainer = jQuery('.the-gate-gallery-attachments-ids', jQuery(this).parents('.hide-if-no-js').first());

            jQuery(this).parent().fadeOut(1000, function() {
                jQuery(this).remove();

                var addedAttachments = jQuery(idsContainer).val().split(",").map(function(value){
                    return jQuery.trim(value);
                });

                addedAttachments = jQuery.grep(addedAttachments, function(i, n) {
                    return i != jQuery(element).data('id');
                });

                jQuery(idsContainer).val(addedAttachments.join(','));

            });
        }
        return false;
    });

    function initializeSortable(){
        jQuery('.the-gate-gallery-thumbs').each(function(index, element){
            jQuery(element).sortable({
                stop: function( event, ui ) {
                    var ids = new Array();

                    jQuery('.the-gate-gallery-thumbs li a', jQuery(element).parents('.hide-if-no-js').first()).each(function(key, element){
                        ids.push(jQuery(element).data('id'));
                    });
                    jQuery('.the-gate-gallery-attachments-ids', jQuery(element).parents('.hide-if-no-js').first()).val(ids.join(','));
                }
            });
        });
    }


    initializeSortable();
})