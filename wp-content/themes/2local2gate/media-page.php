<?php
/**
 * Template name: media
 *
 * @package WordPress
 * @subpackage 2local2gate
 */
get_header();
?>

<div class="container"> 
	<div class="col-md-6 media-pd-btm"><img src="<?php bloginfo('template_directory') . _e('/assets/images/media_03.png') ?>" width="100%" />
	</div>
	<div class="col-md-6 media-pd-btm">
		<div class="media-pd-img-right"><img src="<?php bloginfo('template_directory') . _e('/assets/images/media_05.png') ?>" />
		</div>
		<div>
			<p class="p-media">Our modern full service model connects both content creation and channel delivery through a single communication idea. This means brands have the flexibility to deliver relevant messages, in different formats, to the right customers.</p>
			<p class="p-media">That is why we were the first agency to partner with Point Logic to help qualify the customer journey and channel influence across many sectors and customers, giving greater insight beyond those offered from traditional data sources.</p>
			<p class="p-media">We also embed the digital and measurement conversation into all that we do, which is why have our own, in house digital platform – ADGATE.com – offering ad-serving, programmatic trading and attribution modelling that dramatically improves performance on the path to any desired conversion point.</p>
			<p class="p-media">So, if you are a brand that wants to challenge the status quo with a partner that offers connected advice and bespoke solutions, we would love to hear from you.</p>
		</div>
	</div>
</div>



<?php
get_footer();
