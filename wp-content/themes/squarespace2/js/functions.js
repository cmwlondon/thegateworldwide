
var pageheader,
	mobileIsOpen = false,
	supportsCSSTransitions,
	supportsFlexbox,
	ishighdpi,
	dpi,
	windowSize,
	isIOS,
	sizersLoaded = false,
	sizers,
	images = [],
	imagesToLoad = 0,
	imagesLoaded = 0,
	sizerIndex = 0,
	sizerCount = 0,
	thisMultiImage = [],
	thisSizer = null,
	flexBoxFixers = [],
	nextlinkOffsetTop;

var iframeWrapperNode, iframeNode, url, ww, wh, wa, iw, ih, ia;

(function () {
	"use strict";
})();

function scaleVideo() {
}

Modernizr.addTest('isios', function() {
    return navigator.userAgent.match(/(iPad|iPhone|iPod)/g);
});

jQuery( document ).ready( function() {

	// creates dummy function for devices which don't support console.log natively
	if (!window.console) window.console = {};
	if (!window.console.log) window.console.log = function () { };

	supportsCSSTransitions = jQuery('html').hasClass('csstransitions');
	supportsFlexbox = jQuery('html').hasClass('flexbox');
	supportsTouch = jQuery('html').hasClass('touch');

	// supportsTouch = true;

	isIOS = Modernizr.isios;
	// id device supports devicePixelRatio check for retina display, otherwise assume standard res (IE 9/10) 
	if (typeof window.devicePixelRatio != 'undefined' ) {
		ishighdpi = ( window.devicePixelRatio > 1);	
		dpi = window.devicePixelRatio;
	} else {
		ishighdpi = false;
		dpi = 1;
	}

	windowSize = {
		"width" : jQuery(window).width(),
		"height" : jQuery(window).height()
	};
	aspectRatio = windowSize.width / windowSize.height,
	docSize = {
		"width" : jQuery('body').width(),
		"height" : jQuery('body').height()
	};

	// jQuery('.meettheteam .bg img').css({"height" : Math.floor(docSize.height / 2) + "px", "width" : "auto"})
	// ishighdpi = true;

	/*
	iPad 2: dpr:2 1024 x 768
	iPhone 6 plus: dpr:3 736 X 414
	*/

	// test for iOS devices - relevant to background image attachment/dedvice pixel ratio
	if ( isIOS ) { jQuery('html').addClass('ios'); } else { jQuery('html').addClass('not-ios'); } 

	// TEST mode - simulate iOS
	// jQuery('html').addClass('ios');

	jQuery('html').addClass('dpr' + dpi);

	pageheader = jQuery('.pageHeader');
	jQuery('#hamburgermenu').on ('click', function(e){
		e.preventDefault();
		if ( !mobileIsOpen ) {
			pageheader.addClass('mobileOpen');
			mobileIsOpen = true;
		}
	});
	jQuery('.closeMenu').on ('click', function(e){
		e.preventDefault();
		if ( mobileIsOpen ) {
			pageheader.removeClass('mobileOpen');
			mobileIsOpen = false;
		}
	});

	thisSizer = new Sizer({
		"containers" : jQuery('.sizer')
	});
	
	jQuery('.multi').each(function(i){
		thisMultiImage.push( new MultiImage({
			"container" : jQuery(this)
		}) );
	});

	// image placeholder with 'play' button which displays vimeo iframe
	var a = jQuery('.videobutton'),
		b1 = a.find('.trigger'),
		b2 = a.find('.image'),
		b3 = a.find('.video'),
		c = b3.attr('data-video'),
		d = jQuery('<iframe></iframe>').attr({"frameborder" : "0", "src" : c}),
		e = jQuery('<div></div>');

	b1.on('click',function(evnt){
		evnt.preventDefault();

		e.append(d);
		b3.append(e);

		b1.css({"display" : "none"});
		b2.css({"opacity" : "0"});
		var hideImage = window.setTimeout(function(){
			b2.css({"display" : "none"});
		},1000);
		// set display none on image layer after 1s
	});

	// home page autoplay video, 100% width, 70% viewport height
	// scale and position iframe so horizontal axis of video is vertically centered in container
	jQuery('.iframeSizer').each(function(i){
		var scale, nw, nh, ox, oy, newiframe;

		iframeWrapperNode = jQuery(this);
		url = iframeWrapperNode.attr('data-url');

		// iframe dimensions 1920 x 1080 1.777777
		iw = iframeWrapperNode.attr('data-width');
		ih = iframeWrapperNode.attr('data-height');
		ia = iw / ih;

		// container dimensions
		ww = iframeWrapperNode.width();
		wh = iframeWrapperNode.height();
		wa = ww / wh;

		if (ia > wa ) {
			scale = wh / ih;
			nw = iw * scale;
			nh = ih * scale;
		}  

		if (ia < wa ) {
			scale = ww / iw;
			nw = iw * scale;
			nh = ih * scale;
		}  

		ox = (ww - nw) / 2;
		oy = (wh - nh) / 2;

		nh++; // increase height by 1 px to account for rounding errors

		newiframe = jQuery('<iframe></iframe>')
		.attr({"frameborder" : "0", "src" : url, "id" : "vimeovideo001"})
		.css({"width" : nw + "px", "height" : nh + "px", "margin-left" : ox + "px", "margin-top" : oy + "px"})
		.on('load',function(e){
			// console.log("iframe loaded: %s", url);
			// Vimeo object exists here

			var iframe = document.querySelector('#vimeovideo001');
			var player = new Vimeo.Player(iframe);

		    player.on('play', function() {
		        // console.log('played the video!');
				thisPageEventHandler.signal("video");
				iframeWrapperNode.removeClass('loading');	
		    });

		});

		iframeWrapperNode.append(newiframe);
	});

	jQuery(window).on('resize', function(e){
		thisSizer.resize();

		jQuery('.iframeSizer').each(function(i){
			var scale, nw, nh, ox, oy, newiframe;

			iframeWrapperNode = jQuery(this);
			newiframe = iframeWrapperNode.find('iframe');

			// container dimensions
			ww = iframeWrapperNode.width();
			wh = iframeWrapperNode.height();
			wa = ww / wh;

			if (ia > wa ) {
				scale = wh / ih;
				nw = iw * scale;
				nh = ih * scale;
			}  

			if (ia < wa ) {
				scale = ww / iw;
				nw = iw * scale;
				nh = ih * scale;
			}  

			ox = (ww - nw) / 2;
			oy = (wh - nh) / 2;

			nh++; // increase height by 1 px to account for rounding errors

			newiframe
			.css({"width" : nw + "px", "height" : nh + "px", "margin-left" : ox + "px", "margin-top" : oy + "px"});
		});

		/*
		var rw1Throttle = window.setTimeout(function(){

			// switch between mobile and desktop nav
			var windowWidth = jQuery(window).width();
			if ( mobileIsOpen && windowWidth > 880 ) {
				pageheader.removeClass('mobileOpen');
				mobileIsOpen = false;
			}
		},10);
		*/
	});

	thisPageTransition = new PageTransition({
		"content" : jQuery('.content'),
		"duration" : 500
	});

	var thisPageImageLoader = new imgPageLoader({
		"singleImage" : function( image ) {  },
		"allImages" : function() { thisPageEventHandler.signal("image"); }
	});

	if ( jQuery('.flexboxContainer').length > 0 ) {
		// supportsFlexbox = false;
		// set page load events based on whether the client supports flexbox
		// if so, trigger on images loading and font load
		// else trigger on flexbox javascript fix completion and font load
		squarespace2_events.events = (supportsFlexbox) ? ['image','font'] : ['flexbox','font'] ;
	}

	thisPageEventHandler = new PageEventHandler({
		// "events" : ["font", "image", "video", "map"],
		"events" : squarespace2_events.events,
		"complete" : {
			"all" : function(){
				thisPageTransition.open();

				if ( jQuery('.nextlink').length > 0 ) {
					nextlinkOffsetTop = jQuery('.nextlink').offset().top;
				} else {
					nextlinkOffsetTop = 9999;
				}

				// jQuery('html, body').animate({ "scrollTop" : nextlinkOffsetTop }, 1000);
			},
			"font" : function(){},
			"image" : function(){},
			"map" : function(){},
			"video" : function(){},
			"flexbox" : function(){}
		}
	})

	// thisPageEventHandler.signal("font");
	// thisPageEventHandler.signal("image");
	// thisPageEventHandler.signal("map");
	// thisPageEventHandler.signal("video");

	// google webfont loader
	WebFont.load({
		"google" : { "families" : ['Abel:400,700,300,300i,600,700i,800', 'Open+Sans:400,700,300,300i,600,700i,800'] },
		"active" : function() {
			thisPageEventHandler.signal("font"); // fonts loaded, page can now be displayed

			// flexbox items for IE */
			if ( !supportsFlexbox && jQuery('.flexboxContainer').length > 0 ) {
				flexBoxFixers.push( new FlexBoxFix({
					"container" : jQuery('.flexboxContainer'),
					"switchoff" : 480,  // below 480px wide people display in single column, no need to get/set height of boxes
					"complete" : function(){ thisPageEventHandler.signal("flexbox"); }
				}));
			}
		}
	});

	if ( jQuery('.nextlink').length > 0 ) {
		jQuery(window).on('scroll', function(e){
			var wh = jQuery(window).height(),
				nx = nextlinkOffsetTop - wh,
				ws = jQuery(window).scrollTop();

			if (ws > nx) {
				// next link in view, change style
				// jQuery('body').css({"background-color" : "#f00" });
			} else {
				// next link out of view
				// jQuery('body').css({"background-color" : "#fff" });
			}
		});
	}

	// add page fade out when moving between pages within site
	// omit fade on mobile menu open/close and video 'play' button in work page
	jQuery('a').not('a.button').on('click', function (e) {
		e.preventDefault();
		var thisLink = jQuery(this),
			thisHref = thisLink.attr('href');

		if ( thisHref.indexOf('mailto') == -1 ) {
			/*
			jQuery('#page').animate({"opacity" : 0}, 1000, function(){
				location.assign( thisHref );
			});
			*/
			thisPageTransition.close( { "href" : thisHref }, function(parameters) { location.assign( parameters.href ); });
		}
	});
});
