<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <!--- Specific function for i8 browser and under --->
	<script type="text/javascript">
          function showHideIE8(elementid){
            if (document.getElementById(elementid).style.display == ''){
                document.getElementById(elementid).style.display = 'none';
            } else {
                document.getElementById(elementid).style.display = '';
            }
        }		
	</script>
	<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/ie8andunder.css" media="screen" />
	<![endif]-->
	<!--- ////////// --->
    <script type="text/javascript" src="//use.typekit.net/tpt2fek.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <?php
        /* Scripts */
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'jquery_animate_colors' );
        wp_enqueue_script( 'equal_heights' );
        wp_enqueue_script( 'jquery_main' );
        wp_enqueue_script( 'init' );
        wp_head();
    ?>
</head>
<!--- Browser update box  --->
<!--[if lt IE 9]>
    <div id="div_1035677" >
        <div class="ie8message" align="center">
        <div id="ie8mscopy">
        <div id="ie8closebutton"><a href="#" onclick="showHideIE8('div_1035677');"><img src="<?php bloginfo('template_directory'); ?>/assets/images/ie8closebutton.jpg"></a></div>
        <div id="ie8mscopybox">
        <p>This site will work for 95.6% of the world's web browsers.<br>
         If it doesn't work for you, it might be time to update your technology.<br>
         <a href="http://browsehappy.com/" class="ie8updatedlink" target="_blank">Click here to get started.</a></p>
        </div>
        </div>
       </div>
       </div>
 <![endif]-->
<!--- ////////// ---> 
<a href="#" class="backToTop"></a>