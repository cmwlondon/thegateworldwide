<?php
/**
 * 404 Page
 *
 * @package WordPress
 * @subpackage TheGate local
 */
get_header();
?>
    <!-- visual -->
    <div class="visual">
    </div>
</header>
    <div class="main-holder" style="padding-top: 100px;">
       <p><?php _e( 'Woops! The page you are looking for doesn&#39;t currently exist.' ); ?></p>
       <p><?php _e( '很抱歉！您访问的网页不存在。' ); ?></p>
    </div>
     <!--.main-holder end -->
<?php get_footer();?>