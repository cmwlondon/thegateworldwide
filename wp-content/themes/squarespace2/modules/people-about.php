<?php
/*
people:about list module
/custom_posts/people/
custom post type: 'person'
functions.php -> get_people(), get_people_in_order();
*/

$theme_root = get_template_directory_uri();
$people_about = get_people_in_order($team);
$standardSpectRatio = 234 / 265; // 265px wide by 234px tall
?>
			<div class="people clearfix">
				<?php
				foreach($people_about AS $key => $person) :
					$person_id = $person->ID;
					$metadata = $metadata = get_post_meta($person_id);

					$forename = $metadata['p_forename'][0];
					$surname = $metadata['p_surname'][0];
					$role = $metadata['p_role'][0];
					$section = $metadata['p_section'][0];
					$email = $metadata['p_email'][0];
					$linkedin = $metadata['p_linkedin'][0];
					$phone = $metadata['p_phone'][0];
					$mobile = $metadata['p_mobile'][0];
					$normal = $metadata['person_normal'][0];
					$retina = $metadata['person_retina'][0];
					$bio = ($metadata['p_bio'][0] != '') ? html_entity_decode($metadata['p_bio'][0]) : 'bio pending';
					// $bio = ($metadata['p_bio'][0] != '') ? $metadata['p_bio'][0] : 'bio pending';
					
					$fullName = $forename . ' ' . $surname;

					$picClass = '';
					$picStyle = '';

				?>

				<div class="person">
					<div class="interior">
						<div class="clipper<?php echo $picClass; ?>">
							<div class="inlinedpiSelect" data-normal="<?php echo $normal; ?>" data-retina="<?php echo $retina; ?>" data-width="<?php echo $portrait_w; ?>" data-height="<?php echo $portrait_h; ?>"><img alt="<?php echo $fullName; ?>" <?php echo $picStyle; ?>></div>
						</div>
						<div class="slider">
							<div class="text">
								<div class="marker"></div>
								<div class="trigger">
									<h3><?php echo $fullName; ?></h3>
									<h4><?php echo $role; ?></h4>
									<h5><?php echo $section; ?></h5>
								</div>
								<div class="actions"><?php if ($email != '-') : ?><a href="mailto:<?php echo $email; ?>" class="email svgdpiSelect"><svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 52 52" class="email"><defs><style></style></defs><path class="cls-1" d="M8,15H44V38H8V15Zm2,2H42V36H10V17Z"/><path class="cls-2" d="M26,32L9,18"/><path class="cls-2" d="M9,35L19,25"/><path id="Shape_2_copy" data-name="Shape 2 copy" class="cls-2" d="M33,25L43,35"/><path id="Shape_1_copy" data-name="Shape 1 copy" class="cls-2" d="M43,18L27,32"/></svg></a><?php endif; if ($linkedin != '-') : ?><a href="<?php echo $linkedin; ?>" target="_blank" class="linkedin svgdpiSelect"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="52" height="52" viewBox="0 0 52 52" class="linkedinblue"><filter id="filter" x="9" y="9" width="34" height="34" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#fff"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="_" data-name="" class="li1" d="M14.252,22.1h5.11V37.457h-5.11V22.1ZM18.9,19.261a2.911,2.911,0,0,1-2.1.763H16.774a2.812,2.812,0,0,1-2.046-.763,2.531,2.531,0,0,1-.785-1.892,2.488,2.488,0,0,1,.807-1.9,2.945,2.945,0,0,1,2.091-.752,2.828,2.828,0,0,1,2.057.752,2.615,2.615,0,0,1,.8,1.9A2.514,2.514,0,0,1,18.9,19.261Zm13.738,9.988q0-3.473-2.566-3.473a2.5,2.5,0,0,0-1.637.542,3.417,3.417,0,0,0-1,1.316,3.655,3.655,0,0,0-.155,1.239v8.584h-5.11q0.066-13.893,0-15.353h5.11v2.234H27.238a5.143,5.143,0,0,1,4.624-2.588,5.538,5.538,0,0,1,4.27,1.748,7.349,7.349,0,0,1,1.615,5.155v8.8h-5.11V29.249ZM41.12,10.876a6.138,6.138,0,0,0-4.5-1.869H15.381a6.382,6.382,0,0,0-6.371,6.371V36.616a6.382,6.382,0,0,0,6.371,6.371H36.618a6.383,6.383,0,0,0,6.371-6.371V15.378A6.138,6.138,0,0,0,41.12,10.876Z"/></svg></a><?php endif; ?></div>
							</div>
							<div class="bio"><p><?php echo $bio; ?></p></div>
						</div>
					</div>
				</div>

				<?php
					endforeach;
				?>
				<br class="cb">
			</div>
