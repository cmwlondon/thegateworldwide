<?php
/**
 * Template Name: News All
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $all_news_page,
       $category;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

$recent_news = get_recent_news(21, 0, ($category) ? $category->term_id : 0);
$hide_show_more = count($recent_news) < 21;
if(count($recent_news) == 21) array_pop($recent_news);
$news_categories = get_categories(array(
    'parent' => 0,
    'exclude' => '24' // exclude 'About us' category from news item filter
));
?>

<?php if($all_news_page) : ?>
    <div class="list-area">
        <h2><span class="full-title"><?php echo apply_filters('the_title', $all_news_page->post_title); ?><span id="newsFilterTitle" class="category-title"><?php echo ($category) ? ' / ' . $category->name : '' ; ?></span></span></h2>
        
        <span class="sectional-excerpts"><?php echo ($category) ? $category->description : '' ; ?></span>
        <!--
        <div class="filterBox" id="newsFilter">
            <div class="labelBox"><span class="label"><?php _e('View by', 'News Listing Page'); ?> <?php _e(' Category', 'News Listing Page'); ?>: </span><span class="state2"><?php _e('All', 'News Listing Page'); ?></span></div>
            <div class="listWindowOuter">
                <div class="listWindowInner"><ul><li class="current" data-category=""><?php _e('All', 'News Listing Page'); ?></li><?php foreach ($news_categories as $news_category) : ?><li data-category="<?php echo $news_category->term_id; ?>" data-category-name="<?php echo $news_category->name; ?>" data-category-description="<?php echo $news_category->description; ?>"><?php echo apply_filters('the_category', $news_category->name); ?></li><?php endforeach; ?></ul></div>
            </div>
        </div>
        -->
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
        <ul class="isotopeList newsList">
            <li class="itemTemplate">
                <div class="holder">
                    <a class="overlayLink"></a>
                    <figure>
                        <div class="videoFrame videoNotReady"></div>
                    </figure>
                    <header>
                        <h3></h3>
                        <p></p>
                    </header>
                </div>
            </li>
        <!-- <ul class="list"> -->
            <?php foreach ($recent_news as $key => $single_news) : ?>

            <li class="appended dynamic">
                <div class="holder">
                    <a class="overlayLink" href="<?php echo get_permalink($single_news->ID); ?>"></a>
                    <figure>
                        <div class="videoFrame videoNotReady"></div>
                        <?php echo (has_post_thumbnail($single_news->ID)) ? get_the_post_thumbnail($single_news->ID, 'medium-thumbnail') : get_placeholder_image('medium-thumbnail'); ?>
                        <img class="former" src="<?php bloginfo('template_directory'); ?>/assets/images/former_408_229.gif" alt="" >
                    </figure>
                    <header>
                        <h3><?php echo apply_filters('the_title', $single_news->post_title); ?></h3>
                        <h4><?php echo the_gate_format_date($single_news->post_date); ?></h4>
                        <p><?php echo apply_filters('the_excerpt', $single_news->post_excerpt); ?></p>
                    </header>
                </div>
            </li>

            <?php endforeach; ?>
        </ul>
        <div class="show-more-work">
            <a id="show-more" <?php echo ($hide_show_more) ? 'class="hidden"' : ''; ?> href="#"><p><?php _e('SHOW MORE NEWS', 'News Listing Page'); ?></p><span></span></a>
        </div>
        <?php if($category) : ?>
            <div class="visual">
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>