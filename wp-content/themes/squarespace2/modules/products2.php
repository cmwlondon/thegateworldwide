		<article class="module homeProductList2 greybg">
			<div class="flexboxContainer clearfix ">
<?php
$productFilter = array('business');
$business_products = synchronise_products( explode(',', $business), $productFilter, $postid, 'bgh_business_order' );
$highlights = (count($business_products['assigned']) > 0) ? get_products_by_order($business_products['assigned']) : []; // ;
?>
		<article class="flexboxItem businessHighlights whitebg" data-style="display: flex;">
			<header>
				<p>I'M LOOKING FOR:</p>
				<h2><?php echo $business_title; ?></h2>
			</header>
			
			<ul>
			<?php 
			foreach ($highlights AS $highlight) :
				$highlightMeta = get_post_meta($highlight->ID);

				if ($highlightMeta['product_openlink'][0] == 'openlink') : 
			?>
				<li><a href="<?php echo $highlightMeta['product_link'][0]; ?>" target="_blank"><?php echo $highlight->post_title; ?></a> </li>
			<?php 
			else :
			?>
				<li><a href="<?php echo get_permalink($highlight->ID); ?>"><?php echo $highlight->post_title; ?></a> </li>
			<?php 
			endif;
			?>
			<?php 
			endforeach;
			?>
			</ul>
		</article>
<?php
$productFilter = array('individual');
$individual_products = synchronise_products( explode(',', $individual), $productFilter, $postid, 'bgh_individual_order' );
$highlights = (count($individual_products['assigned']) > 0) ? get_products_by_order($individual_products['assigned']) : []; // ;
?>
		<article class="flexboxItem individualHighlights whitebg" data-style="display: flex;">
			<header>
				<p>I'M LOOKING FOR:</p>
				<h2><?php echo $individual_title; ?></h2>
				
				
			</header>
			<ul>
			<?php 
			foreach ($highlights AS $highlight) :
				$highlightMeta = get_post_meta($highlight->ID);

				if ($highlightMeta['product_openlink'][0] == 'openlink') : 
			?>
				<li><a href="<?php echo $highlightMeta['product_link'][0]; ?>" target="_blank"><?php echo $highlight->post_title; ?></a> </li>
			<?php 
			else :
			?>
				<li><a href="<?php echo get_permalink($highlight->ID); ?>"><?php echo $highlight->post_title; ?></a> </li>
			<?php 
			endif;
			?>
			<?php 
			endforeach;
			?>
			</ul>
		</article>
			</div>
		</article>
