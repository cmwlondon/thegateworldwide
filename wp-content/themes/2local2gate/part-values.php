<?php
/**
 * Template Name: About Values
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $values_page;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

/* Get Values */
$values = get_posts(array(
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'numberposts' => -1,
    'post_type' => array('value'),
    'suppress_filters' => 0, /* If it is true filters added by WPML will not work and thus posts from all languages will be displayed */
));

if($values_page) : ?>
<li id="values">
    <h2><?php echo apply_filters('the_title', $values_page->post_title); ?></h2>
    <a href="#" class="opener"><?php echo apply_filters('the_title', get_post_meta($values_page->ID, 'menu_label', true)); ?></a>

    <?php $count = 0; ?>
    <?php foreach ($values as $value) : ?>
        <div>
            <figure class="alignright border">XXXX<?php echo (has_post_thumbnail($value->ID)) ? get_the_post_thumbnail($value->ID, 'about-value') : get_placeholder_image('about-value'); ?></figure>
            <div class="text">
                <div class="count-area">
                    <span><?php echo ++$count; ?></span>
                </div>
                <h3><?php echo apply_filters('the_title', $value->post_title); ?></h3>
                <?php echo apply_filters('the_content', $value->post_content); ?>
            </div>
        </div>
    <?php endforeach; ?>


</li>
<?php endif;