$(function(){
    var category = $('span.sectional-excerpts').val();

    if(category === '') {
        $('.sectional-excerpts').addClass('hidden');
    } else {
        $('.sectional-excerpts').removeClass('hidden');
    }

    $('#filter-news li a').on('click', function(e) {
        e.preventDefault();

        var category = $(this).data('category');
        var categoryDesc = (Number(category)) ? $(this).data('category-description') : '';
        var categoryName = (Number(category)) ? ' / ' +  $(this).data('category-name') : '';

        if(categoryDesc === '') {
            $('.sectional-excerpts').addClass('hidden');
        } else {
            $('.sectional-excerpts').removeClass('hidden');
        }

        $('span.category-title').html(categoryName);
        $('.sectional-excerpts').html(categoryDesc);
        showFilteredNews({numberposts: 6, category: category});
    })
});

function showFilteredNews(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'filter_news_by_category'}),
        beforeSend  : function() {
            /* Disable dropdown */

            /* Disable show more button */
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            if(response.status == 'success') {
                var listClasses = [];

                /* Replace posts */
                var patternElement = $('#all-news .list li').first();
                var list = $('#all-news .list').clone();

                list.html('');

                $.each(response.posts, function(key, post) {
                    var listElement = patternElement.clone();
                    var rowClass = 'row-' + post.row;

                    listClasses.push(rowClass);

                    $(listElement).attr('class', rowClass);

                    if(post.category && !Number(data.category)) {
                        if($('.title', listElement).length > 0) {
                            $('.title', listElement).text(post.category);
                        }
                        else {
                            $('figure', listElement).append($('<strong/>').addClass('title').text(post.category));
                        }
                    }
                    else {
                        $('.title', listElement).remove();
                    }

                    $('.title', listElement).text(post.category);
                    
                    post.thumbnail = post.thumbnail.replace('<img','<img onload="this.setAttribute(\'data-loaded\',\'yep\')" data-loaded="nope"');
                    $('figure a', listElement).attr('href', post.url).html(post.thumbnail);
                    $('header h3 a', listElement).html(post.title).attr('href', post.url);
                    $('header h4', listElement).html(post.date);
                    $('.holder', listElement).height('auto');
					if(post.postCount != -1){
					 	$('.views-counter', listElement).css('display','');
					 	$('.views-counter span', listElement).html(post.postCount);
					 }else
						$('.views-counter', listElement).css('display', 'none');

                    if($('p', listElement).length > 0) {
                        var excerpts = $('p', listElement);
                        excerpts.remove();
                    }
                    $('header', listElement).after($(post.excerpt));

                    $('a.more', listElement).attr('href', post.url);

                    list.append(listElement);
                });

                $('#all-news .list').html(list.html());

                /* Set equal hight for list items in same row */
                setTimeout(function(){
                    if($('[data-loaded="nope"]').length != 0){
                        setTimeout(arguments.callee, 250);
                        return;
                    }
                    $.setEqualHeightByClassWithImages($.unique(listClasses));
                }, 500)

                /* Enable button if posts are as much as expected */
                if(!response.hide_show_more)
                $('#show-more').removeClass('hidden');

                /* Enable Dropdown */
            }
            else {
                /* Enable button */
                $('#show-more').removeClass('hidden');

                /* Enable Dropdown */
            }

        },
        error       : function(xhrObj, status, error) {
            /* Enable button */
            $('#show-more').removeClass('hidden');

            /* Enable Dropdown */
        }
    });

}
