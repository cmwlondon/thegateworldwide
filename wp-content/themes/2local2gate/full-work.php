    <?php
/**
 * Template Name: Work
 *
 * @package WordPress
 * @subpackage TheGate
 */

// get all published campaigns
$all_campaigns = filter_work_by_state('', 12); // no parameter = fetch all published campaigns

wp_localize_script( 'filter_work_by_state', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
wp_enqueue_script('filter_work_by_state');

wp_localize_script( 'show_more_pieces', 'myAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
wp_enqueue_script('show_more_pieces');

wp_enqueue_script('isotope_grid');
wp_enqueue_script('masonry_grid');

the_post();
get_header();
?>

</header>
<div id="main">

    <div class="about outerBox">
        <div class="innerPadding">

            <a href="#" class="title"><?php _e('Work', 'Work Listing Page'); ?></a>
            <div class="workPanel">
                <h2>Work</h2>
                <div class="tabContentContainer">
                    <div class="tabContent active" id="clientList">
                        <div class="statesSelect">
                            <h3>View:</h3>
                            <ul>
                                <li class="current" data-state=""><?php _e('All', 'Work Listing Page'); ?></li>
                                <Li data-state="evo">Evolutionary</li>
                                <Li data-state="revo">Revolutionary</li>
                            </ul>
                        </div>

                        <div class="gutter-sizer"></div>
                        <div class="grid-sizer"></div>

                        <ul class="isotopeList clientList" data-state="" data-ids="<?php echo implode(',', $all_campaigns['ids']); ?>">
                            <li class="itemTemplate">
                                <div class="holder">
                                    <a class="overlayLink"></a>
                                    <figure></figure>
                                    <header>
                                        <h3></h3>
                                        <h4></h4>
                                        <p></p>
                                    </header>
                                </div>
                            </li>
                            <?php $count = 1; ?>
                            <?php
                            // foreach ($clients_latest_campaigns as $client => $campaign) :
                            foreach ($all_campaigns['posts'] as $campaign) :
                            ?>

                                <li class="appended dynamic <?php echo $campaign['state']; ?>" id="work<?php echo $campaign['ids']; ?>">
                                    <div class="holder">
                                        <a class="overlayLink" href="<?php echo $campaign['url']; ?>"></a>
                                        <figure>
                                            <?php echo $campaign['thumbnail']; ?>
                                        </figure>
                                        <header>
                                            <h3><?php echo $campaign['title']; ?></h3>
                                            <h4><?php echo $campaign['client']; ?></h4>
                                            <p><?php echo $campaign['excerpt']; ?></p>
                                            <p><?php echo $campaign['media']; ?></p>
                                        </header>
                                    </div>
                                </li>

                                <?php $count++; ?>
                            <?php endforeach; ?>
                        </ul>
                        <div class="show-more-work">
                            <a id="show-more" href="#"><p><?php _e('SHOW MORE', 'Work Listing Page'); ?></p><span></span></a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<?php get_footer();
