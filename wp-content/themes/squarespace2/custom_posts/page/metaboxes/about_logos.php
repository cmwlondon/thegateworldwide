<?php
$itemArray = explode(',', $values['client_order']);
$itemString = '';
foreach ( $itemArray AS $itemArrayItem ) {
	$itemString = ($itemString != '') ? $itemString.',' : $itemString;
	$itemString = $itemString . "'$itemArrayItem'";
}

/*
useless buggy php a single *ESCAPED* quote in a serialized string breaks unserialize()
https://davidwalsh.name/php-serialize-unserialize-issues
//to safely serialize
$safe_string_to_store = base64_encode(serialize($multidimensional_array));

//to unserialize...
$array_restored_from_db = unserialize(base64_decode($encoded_serialized_string));
*/
$clientLogos = unserialize(base64_decode($values['client_items']));


?>

<div class="customField mediumtext">
	<label for="al_header">Client list header</label>
	<input type="text" id="al_header" name="al_header" value="<?php echo $values['al_header']; ?>">
</div>

<div class="customField itemChooser clientLogoItems">
	<input type="hidden" id="client_order" name="client_order" value="<?php echo $values['client_order'] ?>">
	<input type="hidden" id="client_nextid" name="client_nextid" value="<?php echo $values['client_nextid'] ?>">

	<a href="#" class="addItem">Add new item</a>
	<div class="items" id="clientList">
		<div class="item clearfix template" id="cl_NNXNN">
			<div class="sizer">
				<label><span>alt</span><input type="text" id="cl_NNXNN_alt" name="cl_NNXNN_alt" value=""></label>
				<div class="customField imageSelector">
					<input type="hidden" id="cl_NNXNN_image" name="cl_NNXNN_image" value="">
					<input type="hidden" id="cl_NNXNN_image_id" name="cl_NNXNN_image_id" value="" class="ident">
					<input type="hidden" id="cl_NNXNN_image_w" name="cl_NNXNN_image_w" value="" class="width">
					<input type="hidden" id="cl_NNXNN_image_h" name="cl_NNXNN_image_h" value="" class="height">
					<a href="" class="cl_NNXNN_Picker picker action"><img class="imageThumbnail" alt="" src="" style="opacity:0;"></a>
				</div>
				<a href="#" class="removeItem">Remove item</a>
			</div>
		</div>

	<?php
		foreach ( $clientLogos AS $clientLogo ) :
		$ip = wp_get_attachment_image_src($clientLogo['picid'],'full');

	?>
		<div class="item clearfix" id="<?php echo stripslashes($clientLogo['id']); ?>">
			<div class="sizer">
				<label><span>alt</span><input type="text" id="<?php echo $clientLogo['id']; ?>_alt" name="<?php echo $clientLogo['id']; ?>_alt" value="<?php echo stripslashes($clientLogo['alt']); ?>"></label>
				<div class="customField imageSelector">
					<input type="hidden" id="<?php echo $clientLogo['id']; ?>_image" name="<?php echo $clientLogo['id']; ?>_image" value="<?php echo $ip[0]; ?>">
					<input type="hidden" id="<?php echo $clientLogo['id']; ?>_image_id" name="<?php echo $clientLogo['id']; ?>_image_id" value="<?php echo $clientLogo['picid']; ?>" class="ident">
					<input type="hidden" id="<?php echo $clientLogo['id']; ?>_image_w" name="<?php echo $clientLogo['id']; ?>_image_w" value="<?php echo $clientLogo['width']; ?>" class="width">
					<input type="hidden" id="<?php echo $clientLogo['id']; ?>_image_h" name="<?php echo $clientLogo['id']; ?>_image_h" value="<?php echo $clientLogo['height']; ?>" class="height">
					<a href="" class="<?php echo $clientLogo['id']; ?>_Picker picker action"><img class="imageThumbnail" alt="" src="<?php echo $ip[0]; ?>"></a>
				</div>
				<a href="#" class="removeItem">Remove item</a>
			</div>
		</div>
	<?php endforeach; ?>

	</div>
	<a href="#" class="addItem">Add new item</a>

</div>

<script type="text/javascript">
var imagePickers = [],
	clientLogoItemTemplate,
	clientLogoItemsBox,
	orderfield,
	newIDfield,
	countfield,
	items,
	preexisting = [<?php echo $itemString; ?>];

jQuery(document).ready(function(){
	clientLogoItemTemplate = jQuery('.clientLogoItems .template');
	clientLogoItemsBox = jQuery('.clientLogoItems .items');
	orderfield = jQuery('#client_order');
	newIDfield = jQuery('#client_nextid');
	countfield = jQuery('#client_count');

	// bind image picker to pre-existing items
	var clIndex = 0;
	do {
		imagePickers.push(new MediaPanel({
	        "trigger" : jQuery('.' + preexisting[clIndex] + '_Picker')
	    }));

		clIndex++;
	} while (clIndex < preexisting.length );

	// bind 'remove item' action to pre-existing items
 	jQuery('.items .removeItem').on('click',function(e){
 		e.preventDefault();
 		var itemToDelete = jQuery(this).parents('.item'),
 			itemToDeleteID = itemToDelete.attr('id'),
 			currentCount = countfield.val();

 		console.log("remove logo: %s (%s)", itemToDeleteID, currentCount);

 		itemToDelete.remove();

		countfield.val( currentCount - 1 ) ;
		items = [];
		jQuery('.item').not('.template').each(function(i){
			items.push(jQuery(this).attr('id'));
		});
		orderfield.val(items.join(','));

 	});

 	// bind and new item action
 	jQuery('.clientLogoItems .addItem').on('click',function(e){
 		e.preventDefault();

 		newIDfield.css({"border" : "1px solid red"});

 		var newID = newIDfield.val();
 		console.log("add new logo: %s", newID);

 		var newItem = clientLogoItemTemplate.clone();

 		var v = newItem.html(),
 			r = new RegExp(/NNXNN/, 'g');
 		v = v.replace(r, newID);

 		newItem.html(v);

 		newItem.attr({"id" : "cl_" + newID});
 		newItem.removeClass('template');

 		// bind remove item action to new items
	 	newItem.find('.removeItem').on('click',function(e){
	 		e.preventDefault();
	 		var itemToDelete = jQuery(this).parents('.item'),
	 			itemToDeleteID = itemToDelete.attr('id'),
	 			currentCount = countfield.val();

	 		console.log("remove logo: %s (%s)", itemToDeleteID, currentCount);

	 		itemToDelete.remove();

			countfield.val( currentCount - 1 ) ;
			items = [];
			jQuery('.item').not('.template').each(function(i){
				items.push(jQuery(this).attr('id'));
			});
			orderfield.val(items.join(','));

	 	});

	 	// bind image picker to new items
		imagePickers.push(new MediaPanel({
	        "trigger" : jQuery('.cl_' + newID + '_Picker')
	    }));

		clientLogoItemsBox.append(newItem);
		newIDfield.val( (newID * 1) + 1 );
		countfield.val( (countfield.val() * 1) + 1 ) ;

		items = [];
		jQuery('.item').not('.template').each(function(i){
			items.push(jQuery(this).attr('id'));
		});
		orderfield.val(items.join(','));
 	});

	// set up item order sorter
    jQuery('#clientList').sortable({
        "receive" : function ( event, ui ) {
	    	console.log('receive');
        },
        "start" : function( event, ui ) {
        	console.log('start');
        },
        "stop" : function( event, ui ) {
        	console.log('stop');
        }
    }).disableSelection();

    jQuery('#clientList').on('sortstop',function(event, ui) {
		items = [];
		jQuery('.item').not('.template').each(function(i){
			items.push(jQuery(this).attr('id'));
		});
		orderfield.val(items.join(','));
    });
});
</script>
