<?php
/**
 * Template Name: Contact Worldwide
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $worldwide_page;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

$office_id = get_option('office');
$offices_info = get_offices_information();

$markers = array();
$center_coordinates = array();

$first_marker = array();
foreach($offices_info as $id => $office)
{
    if($office->latitude && trim($office->latitude) !== '' && $office->longitude && trim($office->longitude) !== '')
    {
        $office_name = get_current_blog_id() === 3 ? str_replace(' ', '<br/>', $office->post_title) : $office->post_title;
        $city_title = '<div class="city">' . $office->post_title . '</div>';
        $city_desc = '<div class="local-office"><span class="local-office-inner"><span class="centered"><h4>' . $office_name . '</h4><p>' . $office->street . '</p><p>' . $office->office_floor . '</p><p>' . $office->post_code . '</p><em>' . $office->phone . '</em></span></span></div>';

        if($id == $office_id)
        {
            $center_coordinates = array($office->latitude, $office->longitude);
            $first_marker = array($city_title, $city_desc, $office->latitude, $office->longitude);
        }
        else
        {
            $markers[] = array($city_title, $city_desc, $office->latitude, $office->longitude);
        }

    }
}

if(!empty($first_marker)) array_unshift($markers, $first_marker);

if(empty($center_coordinates) && isset($markers[0])) $center_coordinates = array($markers[0][2], $markers[0][3]);

wp_localize_script('init', 'googleMapsCoordinates', array('markers' => $markers, 'center' => $center_coordinates));
?>

<?php if($worldwide_page) : $offices_info = get_offices_information(); ?>

    <li id="worldwide" class="blue worldwide">
        <h2><?php apply_filters('the_title', get_the_title($worldwide_page->ID)); ?></h2>
        <div id="the-gate-map" class="gmap"></div>
    </li>
<?php endif; ?>

