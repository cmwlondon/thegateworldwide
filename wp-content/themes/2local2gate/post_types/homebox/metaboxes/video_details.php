<div class="video-embedcode">
    <div class="sites-share">
        <?php $mb->the_field( 'video_share_site' ); ?>
        <span><?php _e( 'Select video sharing web site' ); ?></span><br />
        <select name="<?php $mb->the_name(); ?>" id="video_site">
            <option value="none"<?php $mb->the_select_state( 'none' ); ?>>None</option>
            <option value="youtube"<?php $mb->the_select_state( 'youtube' ); ?>>YouTube</option>
            <option value="vimeo"<?php $mb->the_select_state( 'vimeo' ); ?>>Vimeo</option>
        </select>
    </div>
    <div class="video-id">
        <?php $mb->the_field( 'video_id' ); ?>
        <span><?php _e( 'Enter video id' ); ?></span>
        <input id="video_id" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" />
        <?php $mb->the_field( 'video_embedlink' ); ?>
        <input id="video_embedlink" type="hidden" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" />
        <?php $mb->the_field( 'video_embedcode' ); ?>
        <textarea name="<?php $mb->the_name(); ?>" rows="1" cols="30" readonly="readonly" id="embedcode"><?php $mb->the_value(); ?></textarea>
    </div>
    <div class="postbox"">
        <div class="handlediv" title="Click to toggle"></div>
        <h3><?php _e( 'Video Preview' ); ?></h3>
        <div class="inside" id="video_preview" style="text-align: center"></div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function() {
        generate_iframe();
        jQuery('#video_site').on('change', generate_iframe);
        jQuery('#video_id').on('change', generate_iframe);
    })

 var generate_iframe = function () {
    var video_id = jQuery('#video_id').val();
    var video_site = jQuery('#video_site').val();
    switch(video_site) {
        case "none":
            var embedcode = '';
            jQuery('#embedcode').val(embedcode);
            jQuery('#video_preview').html('');
            jQuery('#video_embedlink').val('');
        break
        case "youtube":
            var embedcode = '<iframe id="player-youtube-1" width="854" height="519" src="https://www.youtube.com/embed/' + encodeURIComponent(video_id) +'?enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
            var embedlink = 'http://www.youtube.com/embed/' + encodeURIComponent(video_id);
            var previewcode = '<iframe width="420" height="315" src="https://www.youtube.com/embed/' + encodeURIComponent(video_id) +'" frameborder="0" allowfullscreen></iframe>';
            jQuery('#embedcode').val(embedcode);
            jQuery('#video_preview').html(previewcode);
            jQuery('#video_embedlink').val(embedlink);
            break;
        case "vimeo":
            var embedcode = '<iframe id="player-vimeo-1" src="http://player.vimeo.com/video/' + encodeURIComponent(video_id) + '?badge=0&amp;color=ff0179&api=1" width="854" height="519" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            var embedlink = 'http://player.vimeo.com/video/' + encodeURIComponent(video_id);
            var previewcode = '<iframe src="http://player.vimeo.com/video/' + encodeURIComponent(video_id) + '?badge=0&amp;color=ff0179" width="420" height="315" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            jQuery('#embedcode').val(embedcode);
            jQuery('#video_preview').html(previewcode);
            jQuery('#video_embedlink').val(embedlink);
            break;
    }
 }
</script>
