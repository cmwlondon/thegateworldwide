<?php
?>
<div class="customField mediumtext">
	<label for="hc_title">Title</label>
	<input type="text" id="hc_title" name="hc_title" value="<?php echo $values['hc_title']; ?>">
</div>
 
<div class="customField mediumtext">
	<label for="hc_link">Link</label>
	<input type="text" id="hc_link" name="hc_link" value="<?php echo $values['hc_link']; ?>">
</div>
 
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Image</label>
		<input type="hidden" id="hc_image" name="hc_image" value="<?php echo $values['hc_image']; ?>">
		<input type="hidden" id="hc_image_id" name="hc_image_id" value="<?php echo $values['hc_image_id']; ?>" class="ident">
		<input type="hidden" id="hc_image_w" name="hc_image_w" value="<?php echo $values['hc_image_w']; ?>" class="width">
		<input type="hidden" id="hc_image_h" name="hc_image_h" value="<?php echo $values['hc_image_h']; ?>" class="height">
		<a href="" class="hc_image_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['hc_image']; ?>" <?php if ($values['hc_image'] == '' || $values['hc_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['hc_image'] == '' || $values['hc_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['hc_image']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.hc_image_Picker')
    }));
});
</script>


