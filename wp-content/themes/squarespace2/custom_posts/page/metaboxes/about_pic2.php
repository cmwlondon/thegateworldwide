<?php
?>

<div class="customField mediumtext">
	<label for="ap2_alt">image alt</label>
	<input type="text" id="ap2_alt" name="ap2_alt" value="<?php echo $values['ap2_alt']; ?>">
</div>
 
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label>Banner image</label>
		<input type="hidden" id="ap2_image" name="ap2_image" value="<?php echo $values['ap2_image']; ?>">
		<input type="hidden" id="ap2_image_id" name="ap2_image_id" value="<?php echo $values['ap2_image_id']; ?>" class="ident">
		<input type="hidden" id="ap2_image_w" name="ap2_image_w" value="<?php echo $values['ap2_image_w']; ?>" class="width">
		<input type="hidden" id="ap2_image_h" name="ap2_image_h" value="<?php echo $values['ap2_image_h']; ?>" class="height">
		<a href="" class="ap2_image_Picker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['ap2_image']; ?>" <?php if ($values['ap2_image'] == '' || $values['ap2_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['ap2_image'] == '' || $values['ap2_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ap2_image']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
 
	imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.ap2_image_Picker')
    }));
});
</script>
