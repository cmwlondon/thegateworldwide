<?php
/**
 * Single Post Page (Individual News Page)
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $related_posts;

the_post();

$related_posts = get_related_posts(3, array('post', 'campaign', 'campaign_element'), true);

$news_listing_page = get_first_page_by_template('full-news.php');
$news_listing_page_permalink = ($news_listing_page) ? get_permalink($news_listing_page->ID) : false;

$about_listing_page = get_first_page_by_template('full-about.php');
$about_listing_page_permalink = ($about_listing_page) ? get_permalink($about_listing_page->ID) : false;

$featured_news_page = get_first_children_page_by_template('part-news_featured.php', $news_listing_page);

$post_category = get_post_category($post->ID);

$previous_post = get_previous_post(true);
$next_post = get_next_post(true);

$is_london = get_current_blog_id() === 4 ? true : false;
$has_gallery = (
                    MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'second-image', $post->ID) ||
                    MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'third-image', $post->ID) ||
                    MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fourth-image', $post->ID) ||
                    MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fifth-image', $post->ID)
                );

wp_enqueue_script('fancybox_pack');
wp_enqueue_script('share_popup');
get_header();

$news_categories = get_categories(array(
    'parent' => 0,
    'exclude' => '24' // exclude 'About us' category from news item filter
));

?>
</header>

<!-- main -->
<div id="main">
            <?php if ( $post_category != 'About us') : ?>
            <div class="newsPost outerBox">
                <div class="innerPadding">
                    <h1><?php _e('News'); ?></h1>
                        <div class="filterBox" id="newsFilter">
                            <div class="labelBox"><span class="label"><?php _e('View by', 'Single News Page'); ?> <?php _e(' Category', 'Single News Page'); ?>: </span><span class="state2"><?php echo $post_category; ?></span></div>
                            <div class="listWindowOuter">
                                <div class="listWindowInner">
                                    <ul><li data-category="<?php echo $news_listing_page_permalink ?>"><?php _e('All', 'Work Listing Page'); ?></li><?php foreach ($news_categories as $category) : ?><li <?php if ($post_category == $category->name) : ?>class="current"<?php endif; ?> data-category="<?php if($news_listing_page_permalink) echo $news_listing_page_permalink . '?' . http_build_query(array('category' => $category->term_id), '', '&amp;'); ?>"><?php echo $category->name; ?></li><?php endforeach; ?></ul>
                                </div>
                            </div>
                        </div>

                    <div class="campaignItems">
                        <?php if($has_gallery) : ?>
                            <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo (has_post_thumbnail($post->ID)) ? get_thumbnail_src($post->ID, 'news-large') : get_placeholder_image_src('news-large'); ?>">
                                <?php echo (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'news-large') : get_placeholder_image('news-large'); ?>
                            </a>
                            <ul>
                                <?php if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'second-image', $post->ID)) : ?>
                                    <li>
                                        <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'second-image', $post->ID,  'news-large'); ?>">
                                            <?php echo MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'second-image', $post->ID,  'news-small'); ?>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        <?php else : ?>
                            <a class="fancybox-thumb" data-fancybox-group="work-fancybox" href="<?php echo (has_post_thumbnail($post->ID)) ? get_thumbnail_src($post->ID, 'news-full-width') : get_placeholder_image_src('news-full-width'); ?>">
                                <?php echo (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'news-full-width', array('class' => 'full-width')) : get_placeholder_image('news-full-width', null, array('full-width')); ?>
                            </a>
                        <?php endif; ?>
                        <p><?php _e('POSTED', 'Single News Page'); ?> <?php echo the_gate_format_date($post->post_date); ?></p>
                    </div>
                    <div class="textCol">
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <?php print_r($post_category); ?>
                    </div>
                    <aside class="misc">
                        <ul class="article-info">
                            <?php $get_cat_name = get_the_category(); $cat_name = $get_cat_name[0]->slug; ?>
                            <?php if( $cat_name === 'davetrottblog' ) : ?>
                                <li class="subscribe-button-wrap">
                                    <div class="createsend-button" style="height:27px;display:inline-block;" data-listid="r/F5/475/7EA/79D38A7BF5F3C84B"></div>
                                    <script type="text/javascript">(function () { var e = document.createElement('script'); e.type = 'text/javascript'; e.async = true; e.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://btn.createsend1.com/js/sb.min.js?v=2'; e.className = 'createsend-script'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(e, s); })();</script>
                                </li>
                            <?php else : ?>
                                <li class="subscribe-button-wrap">
                                    <div class="createsend-button" style="height:27px;display:inline-block;" data-listid="r/DD/937/411/AD9065175910703C"></div>
                                    <script type="text/javascript">(function () { var e = document.createElement('script'); e.type = 'text/javascript'; e.async = true; e.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://btn.createsend1.com/js/sb.min.js?v=2'; e.className = 'createsend-script'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(e, s); })();</script>
                                </li>
                            <?php endif; ?>
                            <li>
                                <?php get_template_part('part', 'social_buttons'); ?>
                            </li>
                        </ul>
                    </aside>
                </div>
            </div>
            <?php get_template_part('part', 'related_items'); ?>
            <?php else : ?>
            <div class="outerBox">
                <div class="innerPadding">
                    <h1><?php the_title(); ?></h1>
                    <div style="clear:both;">
                        <div class="titleCol" style="display:none;"><h3></h3></div>
                        <figure class="imageCol"><?php echo (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'news-large') : get_placeholder_image('news-large'); ?></figure>
                        <div class="textCol">

                            <?php the_content(); ?>
                            <a href="<?php echo $about_listing_page_permalink; ?>">Back</a>
                        </div>

                        <figure class="alignRight"></figure>
                        <br style="clear:both;">
                    </div>
                </div>
            </div>
            <?php endif; ?>
    
    <!-- .connected-items end -->
</div>
<!--.main-holder end -->
<!-- connected-items -->
<?php get_footer(); ?>