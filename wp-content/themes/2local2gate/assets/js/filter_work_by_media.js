$(function(){
    var currentMediaFilter = 0;
        mediaFilterItems = $('#mediaFilter li'),
        mediaFilterLinks = $('#mediaFilter li a');
    
    mediaFilterItems.on('click', 'a', function(e) {
        e.preventDefault();

        var selectedFilter = mediaFilterLinks.index($(this));

        if (currentMediaFilter != selectedFilter) {
            mediaFilterItems.eq(currentMediaFilter).removeClass('current');
            currentMediaFilter = selectedFilter;
            mediaFilterItems.eq(currentMediaFilter).addClass('current');
            var media = $(this).data('media');

            showFilteredWorkByMedia({numberposts: 6, media: media});
        }
    })
});

function showFilteredWorkByMedia(data) {
    var request = $.ajax({
        url         : myAjax.ajaxUrl,
        type        : 'POST',
        data        : $.extend(data, {action: 'filter_work_by_media'}),
        beforeSend  : function() {
            /* Disable show more button */
            $('#show-more').addClass('hidden');
        },
        isLocal     : true,
        dataType    : 'json',
        async       : true,
        success     : function(response) {
            var listClasses = [];

            if(response.status == 'success') {
                mediaContainer.find('.dynamic').addClass('appended');

                var t2 = window.setTimeout(function (){
                    mediaContainer.find('.dynamic').remove();
                    $.each(response.posts, function(key, post) {
                        var listElement = mediaContainer.find('.itemTemplate').clone();

                        listElement
                        .removeClass('itemTemplate')
                        .addClass('appended')
                        .addClass('dynamic');

                        // post.media
                        // post.client
                        listElement.find ('a.overlayLink').attr({
                            "href" : post.url
                        });
                        listElement.find ('figure').append(post.thumbnail);
                        listElement.find ('header h3').html(post.title);
                        listElement.find ('header h4').html(post.client);
                        listElement.find ('header p.excerpt').html(post.excerpt);
                        listElement.find ('header p.category').html(post.category);

                        mediaContainer.append(listElement);
                    });
                    
                    if(!response.hide_show_more)
                    $('#show-more').removeClass('hidden');
                    mediaContainer.masonry( 'reloadItems' );
                    mediaContainer.masonry( 'layout' );

                }, itemFadeDuration);

            }
            else {
                /* Enable button */
                $('#show-more').removeClass('hidden');
            }

        },
        error       : function(xhrObj, status, error) {
            /* Enable button */
            $('#show-more').removeClass('hidden');
        }
    });

}
