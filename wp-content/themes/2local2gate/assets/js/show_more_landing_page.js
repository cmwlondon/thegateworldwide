var videoList = [],
    imagesloaded = 0,
    imagesRequired = 0,
    videosloaded = 0,
    videosRequired = 0,
    allImages = false,
    allVideos = false,
    totalVideoCount = 1,
    firstPageLoad = false;


$(document).ready(function(){
});

function LandingPageItemLoader( parameters ){
    this.parameters = parameters;

    this.offset = parameters.offset;
    this.postsPerLoad = parameters.postsPerLoad;
    this.excludeList = parameters.excludeList;
    this.container = parameters.container;
    
    this.init();
}

LandingPageItemLoader.prototype = {
    "constructor" : LandingPageItemLoader,

    "init" : function(){
        var _self = this;

        this.postCache = null;
        this.enableShowMoreButton = true;

        $('#show-more-landing-posts').on('click', function(e) {
            e.preventDefault();

            _self.renderPosts();
            _self.callback = function(){};
            _self.getPosts();
        });

        // set getPost to render first group of items as soon as the AJAX call completes
        this.callback = function(){
            _self.callback = function () {};
        };
        this.getPosts();
        
    },
    "getPosts" : function(){
        var _self = this;

        var request = $.ajax({
            url         : myAjax.ajaxUrl,
            type        : 'POST',
            data        : {
                "offset" : this.offset,
                "numberposts" : this.postsPerLoad,
                "exclude_ids" : this.excludeList,
                "action" : "get_landing_page_posts_ajax"
            }, // functions.php line 459
            beforeSend  : function() {
                /* Disable button */
                // $('#show-more-landing-posts').addClass('hidden');
            },
            isLocal     : true,
            dataType    : 'json',
            async       : true,
            complete : function(){
            },
            success     : function(response) {
                if(response.status == 'success') {
                    if (response.posts !== null) {
                        _self.template_path = response.template_path;
                        _self.postCache = response.posts;
                        _self.offset = _self.postCache.length + _self.offset;
                        _self.buildeExcludeList();

                        _self.callback();
                    } else {
                        _self.toggleShowMoreButton(false);
                    }
                }
            }
        });
    },
    "renderPosts" : function(){
        var _self = this,
            postIndex = 0,
            postCount = this.postCache.length,
            thisPost,
            thisPostID;

        videoList = [],
        imagesloaded = 0,
        imagesRequired = 0,
        videosloaded = 0,
        videosRequired = 0

        while (postIndex < postCount) {
            thisPost = this.postCache[postIndex];
            this.renderPost(thisPost);
            postIndex++;
        }

        _self.toggleShowMoreButton(postCount === _self.postsPerLoad);

    },
    "renderPost" : function(post){
        var _self = this,
            thisPostID = post.id,
            videoFlag = (post.mp4 !== 0),
            postVideoType,
            mediaType,
            feature_title = (post.feature_title !== '') ? post.feature_title : post.title,
            feature_subtitle = (post.feature_subtitle !== '') ? post.feature_subtitle : post.excerpt,
            listElement = $('.homeList li.itemTemplate').clone(),
            boxClass;

        if (videoFlag) {
            postVideoType = "mp4"
            mediaType = "video";
        } else {
            mediaType = "img";
        }

        listElement.removeClass()
        .addClass('appended')
        .addClass('type_' + post.category)
        .addClass('media_' + mediaType);
        if (post.mp4) {
            listElement
            .addClass('videoPost');    

            //
            switch (post.mp4boxwidth ) {
                case "single" : {
                    boxClass = "videoPostSingle";
                } break;
                case "double" : {
                    boxClass = "videoPostDouble";
                } break;
                default : {

                };
            }            
            listElement
            .addClass(boxClass);    
        }

        var overlayLink = listElement.find('a.overlayLink');
        if (post.campaign) {
            overlayLink.attr({"href" : post.campaign});
        } else {
            overlayLink.remove();
        }

        var videoFrameNode = listElement.find('figure div.videoFrame');
        if (videoFlag) {
            // build video element
            var videoID = postVideoType + "_" + post.mp4,
                videoWrapperID = videoID + "_wrapper",
                videoScale = post.mp4scale;

            var newHTML5Video = $("<video></video>").attr({
                    // "autoplay" : "true",
                    "loop" : "true",
                    "muted" : "true",
                    "preload" : "auto",
                    "id" : videoID
                }),
                newMP4Source = $("<source></source>").attr({
                    "src" : post.mp4file,
                    "type" : "video/mp4"
                });

                if (videoScale != 1 ) {
                    newHTML5Video
                    .css({
                      '-webkit-transform' : 'scale(' + videoScale + ')',
                      '-moz-transform'    : 'scale(' + videoScale + ')',
                      '-ms-transform'     : 'scale(' + videoScale + ')',
                      '-o-transform'      : 'scale(' + videoScale + ')',
                      'transform'         : 'scale(' + videoScale + ')'
                    });
                }
                newHTML5Video
                .bind("loadedmetadata", function () {
                    videoLoaded(this);
                });
                newHTML5Video.append(newMP4Source);

            videoFrameNode
            .addClass('vp_' + postVideoType)
            .attr({"id" : videoWrapperID})
            .empty()
            .append(newHTML5Video);

            videoList.push({
                "id" : videoID,
                "wrapper" : videoWrapperID 
            });
            videosRequired++;
            totalVideoCount++;
        } else {
            videoFrameNode.remove();
        }

        var landscapeFormer = $("<img></img>")
        .addClass('former')
        .attr({
            "src" : _self.template_path + "/assets/images/former_408_229.gif",
            "alt" : ""
        });

        listElement.find('figure')
        .append($(post.thumbnail).on('load', function(){
            imageLoaded();
        }))
        .append(landscapeFormer);
        imagesRequired++;

        var header = listElement.find('header');
        header.find('h3').html(feature_title);
        header.find('p').html(feature_subtitle);

        listElement.appendTo(this.container);

    },
    "toggleShowMoreButton" : function(flag){
        var _self = this;
        if (!flag) {
            $('#show-more-landing-posts').hide();    
        } else {
            $('#show-more-landing-posts').show();
        }
        
    },
    "buildeExcludeList" : function(flag){
        var _self = this,
            postIndex = 0,
            thisPost,
            thisPostID;

        while ( postIndex < this.postCache.length) {
            thisPost = this.postCache[postIndex];
            thisPostID = parseInt(thisPost.id, 10);
            this.excludeList.push(thisPostID);

            postIndex++;                
        }
    }
}


function imageLoaded () {
    imagesloaded++
    if (imagesloaded === imagesRequired) {
        allImagesLoaded();
    }
}

function allImagesLoaded () {
    allImages = true;

    if (videosRequired == 0) {
        allVideos = true;
    }

    layoutPosts();
}

function videoLoaded (videoItem) {
    videoItem.play();
    videosloaded++
    if (videosloaded === videosRequired) {
        allVideosLoaded();
    }
}

function allVideosLoaded () {
    var videoIndex = 0,
        thisVideo;

    allVideos = true;

    layoutPosts();

    while (videoIndex < videosRequired) {
        $("#" + videoList[videoIndex].wrapper).removeClass('videoNotReady');
        videoIndex++;
    }
}

function layoutPosts () {
    var container = $('.homeList')
    if (allVideos && allImages) {
        if (firstPageLoad) {
            // initialise masonry on page load
            firstPageLoad = false;

            container.masonry({
              columnWidth : $('.grid-sizer')[0],
              itemSelector: 'li',
              isInitLayout: false,
              transitionDuration: 0
              //isFitWidth: true
            });

            msnry = container.data('masonry');
            msnry.on( 'layoutComplete', function( laidOutItems ) {
                $('.isotopeList .appended').removeClass('appended');
            });
            msnry.layout();
        } else {
            // runs after 'show more' items have been added to page
            // trigger masonry layout update for newly appended items
            container.masonry( 'reloadItems' );
            container.masonry( 'layout' );
        }
        $('.isotopeList .appended').removeClass('appended');
    }
}
