<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php if(is_page_template('full-people_individual.php')) : ?>
    <title><?php echo ucwords(str_replace('-', ' ', esc_attr($_GET['slug']))) . ' - ' ; ?><?php bloginfo( 'name' ); ?></title>
<?php else : ?>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
<?php endif; ?>
<link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css"> 

<!--- Specific function for i8 browser and under --->
<script type="text/javascript">
          function showHideIE8(elementid){
            if (document.getElementById(elementid).style.display == ''){
                document.getElementById(elementid).style.display = 'none';
            } else {
                document.getElementById(elementid).style.display = '';
            }
        }		
</script>
<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/ie8andunder.css" media="screen" />
<![endif]-->
<!--- ////////// --->

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/ie8andunder.css" media="screen" />

<script type="text/javascript" src="//use.typekit.net/tpt2fek.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<?php
    /* Scripts */
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'jquery_main' );
    wp_enqueue_script( 'init' );
    wp_head();

    /* Create a var in js that will contain site theme url in order to be able to load images through js */
    wp_localize_script('init', 'siteDetails', array('url' => get_bloginfo('template_url')));
?>
</head>
<body <?php if(is_page_template('full-about.php')) :?>class="modal"<?php endif;?>>

<!--- Browser update box  --->
<!--[if lt IE 9]>
    <div id="div_1035677" >
        <div class="ie8message" align="center">
        <div id="ie8mscopy">
        <div id="ie8closebutton"><a href="#" onclick="showHideIE8('div_1035677');"><img src="<?php bloginfo('template_directory'); ?>/assets/images/ie8closebutton.jpg"></a></div>
        <div id="ie8mscopybox">
        <p>This site will work for 95.6% of the world's web browsers.<br>
         If it doesn't work for you, it might be time to update your technology.<br>
         <a href="http://browsehappy.com/" class="ie8updatedlink">Click here to get started.</a></p>
        </div>
        </div>
       </div>
       </div>
 <![endif]-->
<!--- ////////// ---> 
<a href="#" class="backToTop"></a>
<!-- wrapper -->
<div id="wrapper" class="<?php echo get_wrapper_classes($post); ?>">
    <div class="wrapper-holder">
        <!-- header -->
        <header id="header">
            <div class="header-area">
                <div class="header-holder">
                    <!-- logo-area -->
                    <div class="logo-area">
                        <h1 class="logo"><a href="<?php bloginfo('url'); ?>"><?php _e('The Gate'); ?></a></h1>
                        <!-- sub-menu -->
                        <ul class="sub-menu">
                            <li><a href="<?php echo network_site_url(); ?>"><?php _e('WORLDWIDE'); ?></a></li>
                            <li><a href="<?php echo network_site_url(); ?>asia"><?php _e('ASIA'); ?></a></li>
                            <li><a href="<?php echo network_site_url(); ?>edinburgh"><?php _e('EDINBURGH'); ?></a></li>
                            <li><a href="<?php echo network_site_url(); ?>london"><?php _e('LONDON'); ?></a></li>
                            <li><a href="<?php echo network_site_url(); ?>ny"><?php _e('NEW YORK'); ?></a></li>
                        </ul>
                    </div>
                    <!-- button-area -->
                    <div class="button-area">
                        <a href="#" class="open"><?php _e('menu'); ?></a>
                        <a href="javascript:javascript:history.go(-1)" class="back"><?php _e('back'); ?></a>
                    </div>
                    <div class="container nav-popup">
                        <!-- search-area -->
                        <div class="search-area">
                            <a href="#" class="btn-search"><?php _e('search'); ?></a>
                            <div class="popup-holder">
                                <?php get_search_form(true); ?>
                            </div>
                        </div>
                        <!-- nav -->
                        <nav id="nav">
                            <ul>
                                <?php
                                    /* Use custom walker to Add current class for single pages */
                                    wp_nav_menu(array('theme_location' => 'primary', 'container' => '', 'items_wrap' => '%3$s', 'depth' => 1, 'walker' => new The_Gate_Custom_Walker()));
                                ?>
                                <?php if(function_exists('icl_get_languages') && is_array(icl_get_languages('skip_missing=1'))) : /* Add language switcher if WPML Plugin is activated */ ?>
                                    <?php
                                        /* Do not show languages that the current post has no translation in */
                                        $languages_info = icl_get_languages('skip_missing=1');

                                        /* Do not show active language */
                                        $languages_info = array_filter($languages_info, create_function('$language', 'return !$language[\'active\'];'));
                                        $languages_count = count($languages_info);
                                        if($languages_count) :
                                    ?>
                                    <li>
                                        <ul class="language-list">
                                            <?php $count = 1; ?>
                                            <?php foreach($languages_info as $key => $language_info) : ?>
                                                <li><a href="<?php echo $language_info['url']; ?>" class="language-btn"><?php echo $language_info['native_name']; ?></a></li>
                                                <?php if($count !== $languages_count) : ?>
                                                    <li class="border-holder"><span class="border-line"></span></li>
                                                <?php endif; ?>
                                                <?php $count++; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                        <div class="language-list-mobile">
                                            <?php foreach($languages_info as $key => $language_info) : ?>
                                                <a <?php if($languages_count === 1) echo 'class="full-width"'; ?> href="<?php echo $language_info['url']; ?>"><?php echo $language_info['native_name']; ?></a>
                                            <?php endforeach; ?>
                                        </div>
                                    </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
