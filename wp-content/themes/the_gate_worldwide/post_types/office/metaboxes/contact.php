<div class="postbox">
    <h3><?php _e('English'); ?></h3>

    <div class="office-box">
        <span><?php _e( 'Enter office street name.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_street' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>

    <div class="office-box">
        <span><?php _e( 'Enter office floor number.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_floor' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>

    <div class="office-box">
        <span><?php _e( 'Enter office post code.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_post_code' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>
</div>

<div class="postbox">
    <h3><?php _e('Chinese Simplified'); ?></h3>

    <div class="office-box">
        <span><?php _e( 'Enter office street name.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_street_zh-hans' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>

    <div class="office-box">
        <span><?php _e( 'Enter office floor number.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_floor_zh-hans' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>

    <div class="office-box">
        <span><?php _e( 'Enter office post code.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_post_code_zh-hans' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>
</div>

<div class="postbox">
    <h3><?php _e('Chinese Traditional'); ?></h3>

    <div class="office-box">
        <span><?php _e( 'Enter office street name.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_street_zh-hant' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>

    <div class="office-box">
        <span><?php _e( 'Enter office floor number.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_floor_zh-hant' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>

    <div class="office-box">
        <span><?php _e( 'Enter office post code.' ); ?></span><br/ >
        <?php $mb->the_field( 'office_post_code_zh-hant' ); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
    </div>
</div>

<div class="office-box">
    <span><?php _e( 'Enter office e-mail.' ); ?></span><br/ >
    <?php $mb->the_field( 'office_email' ); ?>
    <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
</div>

<div class="office-box">
    <span><?php _e( 'Enter Latitude.' ); ?></span><br/ >
    <?php $mb->the_field( 'latitude' ); ?>
    <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
</div>

<div class="office-box">
    <span><?php _e( 'Enter Longitude.' ); ?></span><br/ >
    <?php $mb->the_field( 'longitude' ); ?>
    <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
</div>