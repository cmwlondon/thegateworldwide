<?php

/* Manage Post Columns */
add_filter('manage_edit-post_columns', 'add_new_post_columns');

function add_new_post_columns($columns)
{
    $new_columns['cb'] = $columns['cb'];
    $new_columns['id'] = __('ID');
    $new_columns['title'] = $columns['title'];
    $new_columns['author'] = $columns['author'];
    $new_columns['categories'] = $columns['categories'];
    $new_columns['tags'] = $columns['tags'];
    $new_columns['comments'] = $columns['comments'];
    $new_columns['date'] = $columns['date'];

    return $new_columns;
}

add_action('manage_post_posts_custom_column', 'manage_post_columns');
function manage_post_columns($column)
{
    global $post;

    switch ($column)
    {
        case 'id':

            echo $post->ID;
            break;
    }
}

/* Add sortable columns */
add_filter('manage_edit-post_sortable_columns', 'add_sortable_columns_to_post');
function add_sortable_columns_to_post($columns)
{
    $columns['id'] = 'id';

    return $columns;
}

/* Add metabox for secondary images */
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Second Image',
            'id' => 'second-image',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Third Image',
            'id' => 'third-image',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Fourth Image',
            'id' => 'fourth-image',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Fifth Image',
            'id' => 'fifth-image',
            'post_type' => 'post'
        )
    );
    new MultiPostThumbnails(
        array(
            'label' => 'Background Image',
            'id' => 'background-image',
            'post_type' => 'post'
        )
    );
}

/* Remove author support for post */
add_action( 'init', 'remove_author_support' );

function remove_author_support()
{
    remove_post_type_support('post', 'author');
}

/* Add new metabox for author */


/* Register Page Metabox 'Menu Label' */
$post_author = new WPAlchemy_MetaBox(array
(
    'id'        => 'post_fake_author',
    'title'     => __('Author'),
    'types'     => array('post'),
    'context'   => 'normal',
    'mode'      => WPALCHEMY_MODE_EXTRACT,
    'priority'  => 'low',
    'template'  => get_template_directory() . '/post_types/post/metaboxes/post_author.php',
    'autosave'  => true,
    'prefix'    => 'fake_',
));

