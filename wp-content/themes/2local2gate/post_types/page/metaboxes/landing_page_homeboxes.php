<?php while($metabox->have_fields_and_multi('entries')): ?>
    <?php $mb->the_group_open(); ?>
    <div class="misc-pub-section">
        <?php
            $metabox->the_field('category');
            $selected_category = $metabox->get_the_value('category');
            $selected_post_id = $metabox->get_the_value('post_id');

            $marked_items = new WP_Query(array(
                'post_type' => array('home_box'),
                // 'post__in'  => array(2496),
                // 'orderby'   => 'post__in',
                'post_status'     => 'publish',
                'posts_per_page' => -1
            ));
            $marked_items->get_posts();
            $home_boxes =  $marked_items->posts;
        ?>

        <?php $metabox->the_field('post_id'); ?>

        <select class="landing-page-entry-post" name="<?php $mb->the_name(); ?>" style="width: 250px;">
            <option value="">Select Item</option>
            <?php foreach($home_boxes as $home_box) : ?>
                <option value="<?php echo $home_box->ID; ?>" <?php $mb->the_select_state($home_box->ID); ?>><?php echo $home_box->post_title; ?></option>
            <?php endforeach; ?>
        </select>

        <span class="loading-placeholder">
            <img class="hidden" src="<?php echo ASSETS_URL; ?>/images/admin/loading.gif">
        </span>

        <a href="#" class="dodelete delete-landing-page-entry button"><?php _e('Remove Entry'); ?></a>
    </div>
    <?php $mb->the_group_close(); ?>
<?php endwhile; ?>
<div class="clear"></div>
<p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-entries button"><?php _e('Add Entry'); ?></a></p>