<?php
/*
-----------------------------------------------
*/

// about banner START
function about_banner_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_banner_save_meta_box_data', 'about_banner_meta_box_nonce' );

    $values['ab_title'] = get_post_meta( $post->ID, 'ab_title', true );
    $values['ab_banner'] = get_post_meta( $post->ID, 'ab_banner', true );
    $values['ab_banner_id'] = get_post_meta( $post->ID, 'ab_banner_id', true );
    $values['ab_banner_w'] = get_post_meta( $post->ID, 'ab_banner_w', true );
    $values['ab_banner_h'] = get_post_meta( $post->ID, 'ab_banner_h', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/about_banner.php';
}

function about_banner_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_banner_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_banner_meta_box_nonce'], 'about_banner_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ab_title', 'ab_title'),
        array('ab_banner', 'ab_banner'),
        array('ab_banner_id', 'ab_banner_id'),
        array('ab_banner_w', 'ab_banner_w'),
        array('ab_banner_h', 'ab_banner_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'about_banner_save_meta_box_data' );
// about banner END

function about_logos_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_logos_save_meta_box_data', 'about_logos_meta_box_nonce' );

    $values['al_header'] = get_post_meta( $post->ID, 'al_header', true );
    $values['client_order'] = get_post_meta( $post->ID, 'client_order', true );
    $values['client_nextid'] = get_post_meta( $post->ID, 'client_nextid', true );
    $values['client_items'] = get_post_meta( $post->ID, 'client_items', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/about_logos.php';
}

function about_logos_save_meta_box_data( $post_id ) {
    // echo "<pre>".print_r($_POST,true)."</pre>\n";

    if ( ! isset( $_POST['about_logos_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_logos_meta_box_nonce'], 'about_logos_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('al_header', 'al_header') // form element name/id, metakey name
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

    // handle client logos
    $clientOrder = $_POST['client_order'];
    $clientItems = array();

    if ($clientOrder != '') {
        $clients = explode(',', $clientOrder);

/*
useless buggy php a single *ESCAPED* quote in a serialized string breaks unserialize()
https://davidwalsh.name/php-serialize-unserialize-issues
//to safely serialize
$safe_string_to_store = base64_encode(serialize($multidimensional_array));

//to unserialize...
$array_restored_from_db = unserialize(base64_decode($encoded_serialized_string));
*/

        foreach ( $clients AS $key => $clientID ) {
            $clientItems[] = array(
                "id" => $clientID,
                "picid" => $_POST[$clientID . '_image_id'],
                // "image" => $_POST[$clientID . '_image'],
                "width" => $_POST[$clientID . '_image_w'],
                "height" => $_POST[$clientID . '_image_h'],
                "alt" => $_POST[$clientID . '_alt']
            );        
        }
    }

    update_post_meta( $post_id, 'client_order', $clientOrder );
    update_post_meta( $post_id, 'client_nextid', $_POST['client_nextid'] );
    update_post_meta( $post_id, 'client_items', base64_encode(serialize($clientItems)) );
}
add_action( 'save_post', 'about_logos_save_meta_box_data' );

function about_copy1_meta_box_callback( $post ) {
    wp_nonce_field( 'about_copy1_save_meta_box_data', 'about_copy1_meta_box_nonce' );

    $values['ac1_header'] = get_post_meta( $post->ID, 'ac1_header', true );
    $values['ac1_text'] = get_post_meta( $post->ID, 'ac1_text', true );

    $values = array(
        "ac1_header" => get_post_meta( $post->ID, 'ac1_header', true ),
        "ac1_text" => get_post_meta( $post->ID, 'ac1_text', true )
    );
    
    $fieldSettings = array(
        "headerfield" => "ac1_header",
        "headerlabel" => "header",
        "textfield" => "ac1_text",
        "textwpeditclass" => "ac1_wpedit",
        "textwpeditid" => "ac1wpe"
    );

    require get_template_directory() . '/custom_posts/page/metaboxes/generic_copy.php';
}

function about_copy1_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_copy1_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_copy1_meta_box_nonce'], 'about_copy1_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ac1_header', 'ac1_header'), // form element name/id, metakey name
        array('ac1_text', 'ac1_text')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'about_copy1_save_meta_box_data' );

function about_nextpage_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_nextpage_save_meta_box_data', 'about_nextpage_meta_box_nonce' );

    $values['ab_nextpage'] = get_post_meta( $post->ID, 'ab_nextpage', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/about_nextpage.php';
}

function about_nextpage_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_nextpage_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_nextpage_meta_box_nonce'], 'about_nextpage_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ab_nextpage', 'ab_nextpage')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'about_nextpage_save_meta_box_data' );

function about_pic1_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_pic1_save_meta_box_data', 'about_pic1_meta_box_nonce' );

    $values['ap1_alt'] = get_post_meta( $post->ID, 'ap1_alt', true );
    $values['ap1_image'] = get_post_meta( $post->ID, 'ap1_image', true );
    $values['ap1_image_id'] = get_post_meta( $post->ID, 'ap1_image_id', true );
    $values['ap1_image_w'] = get_post_meta( $post->ID, 'ap1_image_w', true );
    $values['ap1_image_h'] = get_post_meta( $post->ID, 'ap1_image_h', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/about_pic1.php';
}

function about_pic1_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_pic1_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_pic1_meta_box_nonce'], 'about_pic1_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ap1_alt', 'ap1_alt'),
        array('ap1_image', 'ap1_image'),
        array('ap1_image_id', 'ap1_image_id'),
        array('ap1_image_w', 'ap1_image_w'),
        array('ap1_image_h', 'ap1_image_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'about_pic1_save_meta_box_data' );

function about_pic2_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_pic2_save_meta_box_data', 'about_pic2_meta_box_nonce' );

    $values['ap2_alt'] = get_post_meta( $post->ID, 'ap2_alt', true );
    $values['ap2_image'] = get_post_meta( $post->ID, 'ap2_image', true );
    $values['ap2_image_id'] = get_post_meta( $post->ID, 'ap2_image_id', true );
    $values['ap2_image_w'] = get_post_meta( $post->ID, 'ap2_image_w', true );
    $values['ap2_image_h'] = get_post_meta( $post->ID, 'ap2_image_h', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/about_pic2.php';
}

function about_pic2_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_pic2_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_pic2_meta_box_nonce'], 'about_pic2_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ap2_alt', 'ap2_alt'),
        array('ap2_image', 'ap2_image'),
        array('ap2_image_id', 'ap2_image_id'),
        array('ap2_image_w', 'ap2_image_w'),
        array('ap2_image_h', 'ap2_image_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'about_pic2_save_meta_box_data' );

?>
