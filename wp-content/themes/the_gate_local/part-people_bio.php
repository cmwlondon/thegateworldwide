    <?php
    /**
     * Template Name: People Bio
     *
     * @package WordPress
     * @subpackage TheGate
     */

    global $bio_page, $bio, $viewEveryone;

    /* Redirect to 404 if opened directly #3342 */
    if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
    {
        if($post->post_parent)
        {
            wp_redirect(get_permalink($post->post_parent), 303);
        }
        else
        {
            wp_redirect(home_url('404'), 303);
        }
    }

    if($bio_page) : ?>
        <li id="individual" class="individual visual" <?php if($viewEveryone){ ?> style="position: absolute; left: -100000px" <?php } ?>>
            <div class="main-holder">
            	<?php
					$peopleThemeDir = get_template_directory_uri()."/assets/images/";
				?>
                <ul class="featured-prev-next">
                    <?php if(count($bio) > 1) : ?>
                        <li class="prev"><a href="#"></a></li>
                    <?php endif; ?>
                    <li class="mobile-carousel-msg"><span><?php the_title(); ?></span></li>
                    <?php if(count($bio) > 1) : ?>
                        <li class="next"><a href="#"></a></li>
                    <?php endif; ?>
                </ul>
                <div class="people-ind-slider-wrap">
                	<?php
						$quickDisplay = "";
						foreach($bio as $key => $person){
							if($person['slug'] == $_GET['slug']){
								$quickDisplay = $key;
								break;	
							}
						}
					
						if($quickDisplay !== ""){
							printPerson($bio[$quickDisplay], true);
						}
                	?>
                    <ul class="people-ind-slider" style="position: absolute; left: -100000px">
                        <?php foreach ($bio as $key => $person) : ?>
                        	<li><?php printPerson($person); ?></li>
                            
							<?php
                            /* Retrieve on which panel should the slider start */
                            if($_GET['slug'] == $person['slug'])
                            {
                                wp_localize_script('init', 'peopleSliderStartingPanel', array('id' => $key + 1));
                            }
                            ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </li>
    <?php endif;

	function printPerson($person, $sep = false){
	?>
	<div class="people-individual-page" data-page-title="<?php echo $person['first_name']; ?> <?php echo $person['last_name']; ?> - <?php bloginfo( 'name' ); ?>" <?php if($sep){ ?> style="display: table"<?php } ?>>
        <div class="main">
            <div class="photo">
                <div class="border"><?php echo $person['large_thumbnail']; ?></div>
            </div>
            <h4><?php echo $person['position']; ?></h4>
            <h3><?php echo $person['first_name']; ?> <?php echo $person['last_name']; ?></h3>
            <h5><?php _e('THE GATE', 'People Bio Page'); ?><span><?php echo $person['office']; ?></span></h5>
            <ul class="details">
                <li class="phone-numbers">
                    <?php if(isset($person['emails']) && is_array($person['emails'])) : ?>
                        <?php foreach ($person['emails'] as $email) : ?>
                            <span><?php echo __('E   ') . $email; ?></span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if(isset($person['phones']) && is_array($person['phones'])) : ?>
                        <?php foreach ($person['phones'] as $phone) : ?>
                            <span><?php echo __('T   ') . $phone; ?></span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if(isset($person['faxes']) && is_array($person['faxes'])) : ?>
                        <?php foreach ($person['faxes'] as $fax) : ?>
                            <span><?php echo __('F   ') . $fax; ?></span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </li>
                <li class="address-holder">
                    <?php if(isset($person['street']) && trim($person['street']) !== '') : ?>
                        <?php echo $person['street']; ?><br />
                    <?php endif; ?>
                    <?php if(isset($person['floor']) && trim($person['floor']) !== '') : ?>
                        <?php echo $person['floor']; ?><br />
                    <?php endif; ?>
                    <?php if(isset($person['postcode']) && trim($person['postcode']) !== '') : ?>
                        <?php echo $person['postcode']; ?>
                    <?php endif; ?>
                </li>
            </ul>
            <?php if(isset($person['featured_articles']) && trim($person['featured_articles']) !== ''): ?>
            	<?php
					// Get Blog ID for person
					switch_to_blog(1);
					$office_site = get_post_meta(intval($person['office_id']), 'office_site_association', true);
					restore_current_blog();
					
					// Switch to Blog before Query
					$switch = switch_to_blog($office_site);
            		$featured_articles = explode(',', str_replace(' ', '', $person['featured_articles']));
					$filtered_ids = array_filter($featured_articles, create_function('$post_id', 'return get_post_type($post_id) === "post";'));
					$featured_articles = is_array($filtered_ids) ? array_map('get_post', $filtered_ids) : false;
			 	?>	
           		<?php if($featured_articles): ?>
            	<ul class="author-featured-articles">
            		<?php foreach($featured_articles as $article): ?>
            		<li>
                		<a href="<?= get_permalink($article->ID); ?>" target="_blank">
                        	<?php
                            	if(($article_thumb = get_post_thumbnail_id($article->ID)) !== ""){
									$thumb_src = wp_get_attachment_image_src($article_thumb);
									$thumb_src = $thumb_src[0];
								}
								else{
									$thumb_src = get_placeholder_image_src(array(70,70));
								}
							?>
                         	<img style="width: 70px !important;" width="70" src="<?= $thumb_src ?>">
                     		<header><?= $article->post_title ?></header>
                      		<div class="author-featured-info">
                      			<time><?php echo the_gate_format_date($article->post_date); ?></time>
                            	<?php if(shortcode_exists('post_view') && get_post_meta($article->ID, 'fake_views_check', true)): ?>
                        		<div class="views-counter">
                					<img style="width: 20px !important;" width="20" src="<?php bloginfo('template_directory'); ?>/assets/images/news-views.png">
                   					<span><?php echo do_shortcode("[post_view id={$article->ID}]"); ?></span>
                			 	</div>
                             	<?php endif; ?>
                      		</div>
                  		</a>
              		</li>
               		<?php endforeach; ?>
              		<li class="more-by-me">
                    	<a href="<?= get_site_url()."?s=Articles+and+Blogs+by+{$person['first_name']}+{$person['last_name']}" ?>" target="_blank">
                        	<p>VIEW MORE ARTICLES</p><span></span>
                       	</a>
                   	</li>
            	</ul>
            	<?php endif; ?>
           		<?php if($switch) restore_current_blog(); ?>
            <?php endif; ?>
        </div>
        <div class="additional">
            <h4><?php echo $person['position']; ?></h4>
            <h3><?php echo $person['first_name']; ?> <?php echo $person['last_name']; ?></h3>
            <h5><?php _e('THE GATE', 'People Bio Page'); ?><span><?php echo $person['office']; ?></span></h5>

            <?php echo apply_filters('the_content', $person['content']); ?>
            <ul class="social">
                <?php if(isset($person['pinterest_account']) && $person['pinterest_account']) : ?>
                    <li><a href="http://pinterest.com/<?php echo $person['pinterest_account']; ?>" class="icon-pinterest"><span><?php _e('Pinterestv', 'People Bio Page'); ?></span></a></li>
                <?php endif; ?>
                <?php if(isset($person['linked_in_account']) && $person['linked_in_account']) : ?>
                    <li><a href="<?php echo $person['linked_in_account']; ?>" target="_blank" class="icon-linkedin"><span><?php _e('Linkedin', 'People Bio Page'); ?></span></a></li>
                <?php endif; ?>
                <?php if(isset($person['twitter_account']) && $person['twitter_account']) : ?>
                    <li><a href="https://twitter.com/<?php echo $person['twitter_account']; ?>" target="_blank" class="icon-twitter"><span><?php _e('Twitter', 'People Bio Page'); ?></span></a></li>
                <?php endif; ?>
                <?php if(isset($person['facebook_account']) && $person['facebook_account']) : ?>
                    <li><a href="https://www.facebook.com/<?php echo $person['facebook_account']; ?>" target="_blank" class="icon-facebook"><span><?php _e('Facebook', 'People Bio Page'); ?></span></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
	<?php
	}