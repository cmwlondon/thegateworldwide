<?php $mb->the_field( 'zh-hans' ); ?>
<label for="title_zh-hans"><?php _e('Chinese Simplified'); ?></label>
<div>
    <input id="title_zh-hans" class="title" type="text" autocomplete="off" value="<?php $mb->the_value(); ?>" size="30" name="<?php $mb->the_name(); ?>">
</div>
<?php $mb->the_field( 'zh-hant' ); ?>
<label for="title_zh-hant"><?php _e('Chinese Traditional'); ?></label>
<div>
    <input id="title_zh-hant" class="title" type="text" autocomplete="off" value="<?php $mb->the_value(); ?>" size="30" name="<?php $mb->the_name(); ?>">
</div>