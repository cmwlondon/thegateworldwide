<?php
/**
 * Template Name: People ListView
 *
 * @package WordPress
 * @subpackage TheGate
 */

global $listview_page, $listview, $individual_page_permalink, $seed;

/* Redirect to 404 if opened directly #3342 */
if(basename(get_page_template()) == pathinfo(__FILE__, PATHINFO_BASENAME))
{
    if($post->post_parent)
    {
        wp_redirect(get_permalink($post->post_parent), 303);
    }
    else
    {
        wp_redirect(home_url('404'), 303);
    }
}

if($listview_page) : ?>
    <div class="pt-page pt-page-2">
         <div class="people-listview visual" id="list-view" >
             <div class="main-holder">
                <table id="myTable" class="tablesorter">
                    <thead>
                        <tr>
                            <th><div><?php _e('First Name', 'People Listview Page'); ?></div></th>
                            <th><div><?php _e('Last Name', 'People Listview Page'); ?></div></th>
                            <th><div><?php _e('Title', 'People Listview Page'); ?></div></th>
                        </tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listview as $person) : $bio_link_in_list_view = $individual_page_permalink . '?' . http_build_query(array('slug' => $person['slug'], 'e' => true, 'o' => $seed), '', '&amp;'); ?>
                            <tr>
                                <td><a href="<?php echo $bio_link_in_list_view; ?>"><?php echo $person['first_name']; ?></a></td>
                                <td><a href="<?php echo $bio_link_in_list_view; ?>"><?php echo $person['last_name']; ?></a></td>
                                <td><a href="<?php echo $bio_link_in_list_view; ?>"><?php echo $person['position']; ?></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!--.pt-page-2-->
<?php endif;
