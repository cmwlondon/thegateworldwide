<?php
/**
 * Template name: About-2016
 *
 * @package WordPress
 * @subpackage 2local2gate
 */

get_header();
$uploadDir = wp_upload_dir();
$mp4File = $uploadDir['baseurl']."/2016/03/predatory_thinking_1200bps.mp4";
// about_poster.jpg
?>


<section>
	<div class="container"> <video autoplay controls style="width:100%;" poster="<?php bloginfo('template_directory') . _e('/assets/images/about_poster.jpg') ?>">
			<source src="<?php echo $mp4File; ?>" type="video/mp4"></source>
                </video>
	</div>
	<div class="container about-pad">
		<div style="text-align:center"><img src="<?php bloginfo('template_directory') . _e('/assets/images/Agency-Website---master.png') ?>" />

		</div>
		<div class="about-text">What gets us out of bed in the morning is a desire to help people level the playing field in life.<br>

			In Britain we love an underdog, and nowhere is that more true than at The Gate. You may<br>

                        not get to choose where you start in life, but with a little <a class="number-link" href="<?php echo site_url() ?>/predatory-thinking"><strong>Predatory Thinking</strong></a> you can sure have a<br> 

                        say where you finish. See how this attitude has transformed brands in our <a class="number-link" href="<?php echo site_url() ?>/stories"><strong>Stories</strong></a> page.
		</div>
</section>


<?php
get_footer();