<?php
/**
 * General listing page
 * 
 * @package WordPress
 * @subpackage thegate_worldwide_shared
 */
?>
<?php get_header();?>
<div id="main">
    <ul id="catalogue">
    <?php if (have_posts()) : while (have_posts()) : the_post(); $post_counter++; ?>
    <li<?php echo $post_counter % 2 ? ' class="margin"' : ''; ?>>
        <div class="article">
            <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="left"><?php the_title(); ?></a>
        </div>
    </li>
    <?php endwhile; else: ?>
        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>
    </ul>
</div>
<?php get_footer(); ?>