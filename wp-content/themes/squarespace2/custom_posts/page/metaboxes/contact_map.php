<?php
$settings = array(
	array(
		"field" => "contact_gate_header",
		"label" => "Header"
	),
	array(
		"field" => "contact_gate_phone",
		"label" => "Phone"
	),
	array(
		"field" => "contact_gate_address",
		"label" => "Address"
	),
	array(
		"field" => "contact_gate_latitude",
		"label" => "Latitude"
	),
	array(
		"field" => "contact_gate_longitude",
		"label" => "Longitude"
	)
);

foreach ($settings AS $index => $item) :
?>
<div class="customField mediumtext">
	<label for="<?php echo $item['field']; ?>"><?php echo $item['label']; ?></label>
	<input type="text" id="<?php echo $item['field']; ?>" name="<?php echo $item['field']; ?>" value="<?php echo $values[$item['field']]; ?>">
</div>
<?php
endforeach;
?>
