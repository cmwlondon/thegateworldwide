<?php $mb->the_field( 'subtitle' ); ?>
<label for="<?php echo $mb->id . '_' . $mb->name; ?>"><?php _e('Enter subtitle text:'); ?></label><br />
<textarea id="<?php echo $mb->id . '_' . $mb->name; ?>" name="<?php $mb->the_name(); ?>" rows="1" cols="40"><?php $mb->the_value(); ?></textarea>
<p>
    <?php _e("The subtitle is the text that appears right below client's logo in the campaign individual page."); ?>
</p>