<?php
/*
Template Name: culture

@package WordPress
@subpackage squarespace2
*/

$metadata = get_post_meta($post->ID);
$banner = array(
	"title" => $metadata['culture_title'][0],
	"src" => $metadata['culture_banner'][0],
	"id" => $metadata['culture_banner_id'][0],
	"w" => $metadata['culture_banner_w'][0],
	"h" => $metadata['culture_banner_h'][0]
);

$copy1 = array(
	"header" => $metadata['cc1_header'][0],
	"text" => html_entity_decode($metadata['cc1_text'][0])
);

$slideshow1 = array(
    "items" => unserialize(base64_decode( $metadata['cs1_items'][0] ))
);

$copy2 = array(
	"header" => $metadata['cc2_header'][0],
	"text" => html_entity_decode($metadata['cc2_text'][0])
);

$slideshow2 = array(
	"items" => unserialize(base64_decode( $metadata['cs2_items'][0] ))
);

$nextPageID = $metadata['culture_nextpage'][0];
$nextPageURL = get_permalink($nextPageID);
$nextPage = get_page_by_postid($nextPageID)[0];

?>
<?php get_header();?>
	</div>

	<div id="content" class="site-content culture">
		<div class="single carimage">
			<article class="column"><div class="car"><img alt="<?php echo $banner['title']; ?>" data-width="<?php echo $banner['w']; ?>" data-height="<?php echo $banner['h']; ?>" src="<?php echo $banner['src']; ?>"></div></article>
		</div>

		<div class="text">
			<h2><?php echo $copy1['header']; ?></h2>
			<p><?php echo $copy1['text']; ?></p>
		</div>

		<!-- slideshow 1 -->
		<?php if (count($slideshow1['items']) > 0 ) :
		// for a single image, omit class 'multi' used to trigger the slideshow 
		$moduleState = (count($slideshow1['items']) > 1 ) ? 'multi' : 'carimage';
		?>
		<div class="single <?php echo $moduleState; ?>" id="culture">
			<?php 
			foreach ( $slideshow1['items'] AS $key => $item) :
				$active = ($key == 0) ? ' active' : ''; // display first item
				$src = wp_get_attachment_image_src($item['picid'],'full');
			?>
			<article class="column<?php echo $active; ?>" data-delay="<?php echo $item['delay']; ?>"><div class="car"><img alt="<?php echo stripslashes($item['alt']); ?>"  data-width="<?php echo $item['width']; ?>" data-height="<?php echo $item['height']; ?>" src="<?php echo $src[0]; ?>"></div></article>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>

		<div class="text">
			<h2><?php echo $copy2['header']; ?></h2>
			<p><?php echo $copy2['text']; ?></p>
		</div>

		<!-- slideshow 2 -->
		<?php if (count($slideshow2['items']) > 0 ) :
		// for a single image, omit class 'multi' used to trigger the slideshow 
		$moduleState = (count($slideshow2['items']) > 1 ) ? 'multi' : 'carimage';
		?>
		<div class="single <?php echo $moduleState; ?>" id="culture">
			<?php 
			foreach ( $slideshow2['items'] AS $key => $item) :
				$active = ($key == 0) ? ' active' : ''; // display first item
				$src = wp_get_attachment_image_src($item['picid'],'full');
			?>
			<article class="column<?php echo $active; ?>" data-delay="<?php echo $item['delay']; ?>"><div class="car"><img alt="<?php echo stripslashes($item['alt']); ?>"  data-width="<?php echo $item['width']; ?>" data-height="<?php echo $item['height']; ?>" src="<?php echo $src[0]; ?>"></div></article>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>

		<div class="text nextlink">
			<p>Next: <a href="<?php echo $nextPageURL; ?>"><?php echo strtoupper($nextPage->post_title); ?></a></p>
		</div>
<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
