<?php
$sites = array(
    2 => 'new_york',
    3 => 'asia',
    4 => 'london',
    5 => 'edinburgh'
); ?>

<?php foreach ($sites as $site_id => $site_name) : ?>
    <div class="postbox">


    <h3><?php echo ucwords(str_replace('_', ' ', $site_name)); ?></h3>
    <?php while($metabox->have_fields_and_multi("{$site_name}-entries", array('limit' => 10))): ?>
        <?php $mb->the_group_open(); ?>
        <div class="misc-pub-section">
            <?php
                $metabox->the_field('category');
                $selected_category = $metabox->get_the_value('category');
                $selected_post_id = $metabox->get_the_value('post_id');
                switch_to_blog($site_id);
                if(!$selected_category)
                {
                    $posts = array();
                    /* $img_src = 'http://placehold.it/250&text=No entry'; */
                }
                elseif($selected_post_id === 'random' && $selected_category === 'random')
                {
                    $posts = array();
                    /* $img_src = 'http://placehold.it/250&text=Random Post'; */
                }
                elseif($selected_post_id === 'random')
                {
                    /* $selected_category_obj = get_post_type_object($selected_category); */
                    $posts = get_posts("post_type={$selected_category}&numberposts=-1");
                    /* $img_src = "http://placehold.it/250&text=Random {$selected_category_obj->labels->singular_name} Post"; */
                }
                else
                {
                    $posts = get_posts("post_type={$selected_category}&numberposts=-1");
                    /* $img_src = get_thumbnail_src($selected_post_id, array(250, 250));
                    $selected_post_title = get_the_title($selected_post_id);
                    $img_src = (!$img_src || trim($img_src) === '') ? "http://placehold.it/250&text={$selected_post_title}" : $img_src; */
                }
                switch_to_blog(1);
            ?>

            <!-- <div class="landing-post-img">
                <img src="<?php echo $img_src; ?>" />
            </div> -->
            <select class="landing-page-entry-category" onchange="fillPosts(this, <?php echo $site_id; ?>);" name="<?php $mb->the_name(); ?>">
                <option value="" ><?php _e(''); ?></option>
                <option value="random" <?php $mb->the_select_state('random'); ?>><?php _e('Random Category'); ?></option>
                <option value="post" <?php $mb->the_select_state('post'); ?>><?php _e('News'); ?></option>
                <option value="campaign" <?php $mb->the_select_state('campaign'); ?>><?php _e('Campaign'); ?></option>
                <option value="campaign_element" <?php $mb->the_select_state('campaign_element'); ?>><?php _e('Campaign Piece'); ?></option>
            </select>
            <?php $metabox->the_field('post_id'); ?>
            <select class="landing-page-entry-post" name="<?php $mb->the_name(); ?>" style="width: 250px;">
                <?php if($selected_category) : ?>
                    <option value="random" <?php $mb->the_select_state('random'); ?>><?php _e('Random Post'); ?></option>
                <?php else : ?>
                    <option value=""><?php _e(''); ?></option>
                <?php endif; ?>
                <?php foreach($posts as $post) : ?>
                    <option value="<?php echo $post->ID; ?>" <?php $mb->the_select_state($post->ID); ?>><?php echo $post->post_title . " ({$post->post_name})"; ?></option>
                <?php endforeach; ?>
            </select>
            <span class="loading-placeholder">
                <img class="hidden" src="<?php echo ASSETS_URL; ?>/images/admin/loading.gif">
            </span>
            <a href="#" class="dodelete delete-landing-page-entry button"><?php _e('Remove Entry'); ?></a>
        </div>
        <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>
    </div>
    <div class="clear"></div>
    <p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-<?php echo $site_name; ?>-entries button"><?php _e('Add Entry'); ?></a></p>
<?php endforeach; ?>