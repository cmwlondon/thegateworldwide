<?php
/**
 * WP Theme Public settings
 *
 * @package WordPress
 * @subpackage thegate_local
 */


/**
 * Register required assets
 */
function thegate_local_load_assets()
{
	if( !is_admin() )
    {
        $template_url = get_stylesheet_directory_uri();

        /** Styles
		 * Example use: wp_register_style( $handle, $src, $deps, $ver, $media );
         */

        /** Scripts
		 * Example use: wp_register_script( $handle, $src, $deps, $ver, $in_footer );
         */

        wp_deregister_script( 'jquery' );

        wp_register_script( 'jquery', ASSETS_URL . '/js/libs/jquery-2.0.1.min.js', false, '2.0.1', ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'jquery_animate_colors', ASSETS_URL . '/js/libs/jquery.animate-colors-min.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'jquery_main', ASSETS_URL . '/js/libs/jquery.main.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'equal_heights', ASSETS_URL . '/js/libs/equal-heights.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'google_maps_infobox', ASSETS_URL . '/js/libs/google.maps.infobox.js', false, false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'anything_slider', ASSETS_URL . '/js/libs/jquery.anythingslider.min.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'dl_menu', ASSETS_URL . '/js/libs/jquery.dlmenu.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'fancybox_pack', ASSETS_URL . '/js/libs/jquery.fancybox.pack.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'fancybox', ASSETS_URL . '/js/libs/jquery.fancybox.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'scroll_to', ASSETS_URL . '/js/libs/jquery.scrollTo-1.4.3.1-min.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'simply_scroll', ASSETS_URL . '/js/libs/jquery.simplyscroll.min.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'tablesorter', ASSETS_URL . '/js/libs/jquery.tablesorter.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'modernizr', ASSETS_URL . '/js/libs/modernizr.custom.js', false, false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'page_transitions', ASSETS_URL . '/js/libs/pagetransitions.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'vimeo', ASSETS_URL . '/js/libs/vimeo.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'youtube', ASSETS_URL . '/js/libs/youtube.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'validation_helper',  ASSETS_URL . '/js/libs/validation_helper.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'contact',  ASSETS_URL . '/js/contact.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'show_more',  ASSETS_URL . '/js/show_more.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'show_more_news',  ASSETS_URL . '/js/show_more_news.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'show_more_landing_page',  ASSETS_URL . '/js/show_more_landing_page.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'show_more_pieces',  ASSETS_URL . '/js/show_more_pieces.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'filter_news_by_category',  ASSETS_URL . '/js/filter_news_by_category.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'filter_campaigns_by_client',  ASSETS_URL . '/js/filter_campaigns_by_client.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'filter_work_by_media',  ASSETS_URL . '/js/filter_work_by_media.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'filter_work_by_state',  ASSETS_URL . '/js/filter_work_by_state.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'share_popup',  ASSETS_URL . '/js/share_popup.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'contact_radio_button',  ASSETS_URL . '/js/jquery.screwdefaultbuttonsV2.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'isotope_grid',  ASSETS_URL . '/js/libs/isotope.pkgd.min.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'masonry_grid',  ASSETS_URL . '/js/libs/masonry.pkgd.min.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
        wp_register_script( 'froogaloop_min',  ASSETS_URL . '/js/libs/froogaloop.min.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );

	}
        wp_register_script( 'init',  ASSETS_URL . '/js/init.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER );
}
add_action( 'init', 'thegate_local_load_assets' );

/**
 * Enqueue a IE-specific style sheet (for all browsers).
 *
 * @author Gary Jones
 * @link http://code.garyjones.co.uk/ie-conditional-style-sheets-wordpress/
 * @modified Kris Arsov
 */
function thegate_local_add_ie_custom_styles() {
    // wp_enqueue_style( 'ie7', ASSETS_URL . '/css/ie7.css', false, '1.0' );
    // wp_enqueue_style( 'ie6', ASSETS_URL . '/css/ie6.css', false, '1.0' );
}
add_action( 'wp_print_styles', 'thegate_local_add_ie_custom_styles', 200 );

/**
 * Add conditional comments around IE-specific style sheet link.
 *
 * @author Gary Jones & Michael Fields (@_mfields)
 * @link http://code.garyjones.co.uk/ie-conditional-style-sheets-wordpress/
 * @modified Kris Arsov
 *
 * @param string $tag Existing style sheet tag.
 * @param string $handle Name of the enqueued style sheet.
 *
 * @return string Amended markup
 */
function thegate_local_make_ie_custom_styles_conditional( $tag, $handle ) {
    if( 'ie7' == $handle )
        $tag = '<!--[if IE 7]>' . "\n" . $tag . '<![endif]-->' . "\n";

    if( 'ie6' == $handle )
        $tag = '<!--[if lte IE 6]>' . "\n" . $tag . '<![endif]-->' . "\n";

    return $tag;
}
add_filter( 'style_loader_tag', 'thegate_local_make_ie_custom_styles_conditional', 10, 2 );

/**
 * Enqueue a IE-specific script (for all browsers).
 *
 * @author Gary Jones
 * @modified Kris Arsov
 */
function thegate_local_add_ie_custom_scripts() {
    // wp_enqueue_script( 'ie7', ASSETS_URL . '/js/ie7.js', false, '1.0' );
    // wp_enqueue_style( 'ie6', ASSETS_URL . '/js/ie6.js', false, '1.0' );
    wp_enqueue_style( 'ie', ASSETS_URL . '/js/ie.js', false, '1.0' );
}
add_action( 'wp_print_scripts', 'thegate_local_add_ie_custom_scripts', 200 );

/**
 * Add conditional comments around IE-specific style sheet link.
 *
 * @author Gary Jones & Michael Fields (@_mfields)
 * @modified Kris Arsov
 *
 * @param string $tag Existing script tag.
 * @param string $handle Name of the enqueued script.
 *
 * @return string Amended markup
 */
function thegate_local_make_ie_custom_scripts_conditional( $tag, $handle ) {
    if( 'ie7' == $handle )
        $tag = '<!--[if IE 7]>' . "\n" . $tag . '<![endif]-->' . "\n";

    if( 'ie6' == $handle )
        $tag = '<!--[if lte IE 6]>' . "\n" . $tag . '<![endif]-->' . "\n";

    if( 'ie' == $handle )
        $tag = '<!--[if IE]>' . "\n" . $tag . '<![endif]-->' . "\n";

    return $tag;
}
add_filter( 'script_loader_tag', 'thegate_local_make_ie_custom_scripts_conditional', 10, 2 );