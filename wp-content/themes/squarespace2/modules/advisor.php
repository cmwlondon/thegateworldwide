<?php
/*
@package WordPress
@subpackage stackbartlett
*/
?>

		<article class="mailform">
			<div>
				<header>
					<h2>send us an email</h2>
				</header>
				<form name="mailform" id="mailform" action="" method="post">
					<div class="field split mandatory">
						<label for="forename">forename*</label>
						<input type="text" name="forename" id="forename" value="" placeholder="enter your forename" />
						<p class="error">Please supply your forename</p>
					</div><div class="field split mandatory">
						<label for="surname">surname*</label>
						<input type="text" name="surname" id="surname" value="" placeholder="enter your surname" />
						<p class="error">Please supply your surname</p>
					</div><div class="field split">
						<label for="role">Role</label>
						<input type="text" name="role" id="role" value="" placeholder="enter your role" />
					</div><div class="field split">
						<label for="company">Company</label>
						<input type="text" name="company" id="company" value="" placeholder="enter your company" />
					</div>
					<div class="field split ">
						<label for="phone">phone</label>
						<input type="text" name="phone" id="phone" value="" placeholder="enter your phone number" />
					</div><div class="field split ">
						<label for="mobile">mobile</label>
						<input type="text" name="mobile" id="mobile" value="" placeholder="enter your mobile number" />
					</div>
					<div class="field split mandatory">
						<label for="email1">Email*</label>
						<input type="text" name="email1" id="email1" value="" placeholder="enter your email" />
						<p class="error">Please supply your email</p>
					</div><div class="field split mandatory">
						<label for="email2">Confirm email*</label>
						<input type="text" name="email2" id="email2" value="" placeholder="confirm your email" />
						<p class="error">Please confirm your email</p>
					</div>
					<div class="field">
						<a href="" class="lozengeButton caret ajaxEmailSubmit">SEND</a>
					</div>
				</form>
			</div>
		</article>

		<article class="generalContent speakAdvisor">
			<div class="singleColumn p1">
				<h2>Speak to an advisor</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>

			<div class="doubleColumnWrapper p2">
				<div class="doubleColumnItem leftColumn individualBox">
					<div class="interior">
						<h3>I'm an individual</h3>
						<hr class="bgindividual02">
						<p>Call an advisor on 0800 000 0000</p>
						<a href="" class="lozengeButton caret ajaxEmail" data-context="individual" data-key="<?php echo wp_create_nonce( 'email_individual_'.$post->ID ); ?>" data-post="<?php echo $post->ID; ?>">SEND AN EMAIL</a>
					</div>
				</div>
				<div class="doubleColumnItem rightColumn businessBox">
					<div class="interior">
						<h3>I'm a business</h3>
						<hr class="bgindividual02">
						<p>Call an advisor on 0800 000 0000</p>
						<a href="" class="lozengeButton caret ajaxEmail" data-context="business" data-key="<?php echo wp_create_nonce( 'email_business_'.$post->ID ); ?>" data-post="<?php echo $post->ID; ?>">SEND AN EMAIL</a>
					</div>
				</div>
				<br class="cb">
			</div>
		</article>
